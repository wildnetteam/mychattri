//
//  AppDelegate.swift
//  MyChhatri
//
//  Created by Anshul on 03/07/18.
//  Copyright © 2018 Anshul. All rights reserved.
//

import UIKit
import Fabric
import Crashlytics
import IQKeyboardManagerSwift
import UserNotifications

@UIApplicationMain

class AppDelegate: UIResponder, UIApplicationDelegate, UITabBarControllerDelegate {

    var window: UIWindow?
    var menuNavController:UINavigationController?
    var tabBarController : UITabBarController!
    var lastSelectedIndex : Int?
    var controllers : NSArray?
    
    
    //WARNING:- rememeber to change this for changing base url
    var kBOOL_LIVE  =  true // To distingush live and staging URL
    
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        // add gesture on window to dismiss keyboard on tap of view...
//        let tapGesture = UITapGestureRecognizer(target: self, action: nil)
//        tapGesture.delegate = self
//        window?.addGestureRecognizer(tapGesture)
        
        // Enable IQKeyboardManager...
        IQKeyboardManager.shared.enable = true
        
        // crash analytics setup
        Fabric.with([Crashlytics.self])
       
      //  registerForPushNotifications()

        
        let language = Bundle.main.preferredLocalizations.first! as NSString
        
        //For the first time Language will be Eng
        
        if LocalDB.shared.currentLanguage == nil {
            
            if ((language as String) == " en"){
                LocalDB.shared.currentLanguage = Language.english
            }else if (language == "hi"){
                LocalDB.shared.currentLanguage = Language.hindi
            }else{
                LocalDB.shared.currentLanguage = Language.english
            }
        }
        
        setNavigationAndStatusAppearance()
        
        if let isAlreadyLoggedIn =  UserModel.getAutoLogin(){

            if isAlreadyLoggedIn == true{

                self.loadStudentHomeViewController()

            }else{
                self.navigateToLoginViewController()
            }
        }else{

        // navigate to login view controller...
          self.navigateToLoginViewController()

        }
        return true
    }

    func navigateToLoginViewController() {
        
        let loginViewController = UIStoryboard.instantiateLoginViewController()
        menuNavController = UINavigationController.init(rootViewController: loginViewController)
        menuNavController?.isNavigationBarHidden = true
        appDelegate.window?.rootViewController = menuNavController
        self.window?.makeKeyAndVisible()
        
    }
    
    func setNavigationAndStatusAppearance()  {
        
        UINavigationBar.appearance().setBackgroundImage(UIImage(), for: .default)
        // Sets shadow (line below the bar) to a blank image
        UINavigationBar.appearance().shadowImage = UIImage()
        // Sets the translucent background color
        UINavigationBar.appearance().backgroundColor = ColorConstants.appBackGroundCOLOR()
        // Set translucent. (Default value is already true, so this can be removed if desired.)
        UINavigationBar.appearance().isTranslucent = true
        // change navigation item title color and font
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedStringKey(rawValue: NSAttributedStringKey.foregroundColor.rawValue): UIColor.white , NSAttributedStringKey(rawValue: NSAttributedStringKey.font.rawValue): UIFont(name: "Amble-Bold", size: isIphone5 ? 13.0 : 18.0)!]
        
    }
    
    func loadStudentHomeViewController() {
        
        tabBarController = UITabBarController()
        let tabViewController1 = UIStoryboard.instantiateMyPagesBaseViewController()
        let tabViewController2 = UIStoryboard.instantiateSearchViewController()
        let tabViewController3 = UIStoryboard.instantiateNewsViewController()
        let tabViewController4 = UIStoryboard.instantiateContestViewController()
        let tabViewController5 = UIStoryboard.instantiateSettingViewController()
        
        var currentNavigationController1 = UINavigationController()
        var currentNavigationController2 = UINavigationController()
        var currentNavigationController3 = UINavigationController()
        var currentNavigationController4 = UINavigationController()
        var currentNavigationController5 = UINavigationController()
        
        currentNavigationController1 = UINavigationController.init(rootViewController: tabViewController1)
        currentNavigationController2 = UINavigationController.init(rootViewController: tabViewController2)
        currentNavigationController3 = UINavigationController.init(rootViewController: tabViewController3)
        currentNavigationController4 = UINavigationController.init(rootViewController: tabViewController4)
        currentNavigationController5 = UINavigationController.init(rootViewController: tabViewController5)
        
        controllers = [currentNavigationController1,currentNavigationController2,currentNavigationController3,currentNavigationController4, currentNavigationController5] as [Any] as NSArray
        tabBarController.viewControllers = controllers as? [UIViewController]
        
        tabViewController1.tabBarItem = UITabBarItem(
            title: "Dashboard".localized(lang: getLanguageKey(lang:  LocalDB.shared.currentLanguage!)),
            image: #imageLiteral(resourceName: "MyPages"),
            tag: 1)
        
        tabViewController2.tabBarItem = UITabBarItem(
            title: "Search".localized(lang: getLanguageKey(lang:  LocalDB.shared.currentLanguage!)),
            image: #imageLiteral(resourceName: "Contest"),
            tag: 2)
        
        tabViewController3.tabBarItem = UITabBarItem(
            title: "News".localized(lang: getLanguageKey(lang:  LocalDB.shared.currentLanguage!)),
            image: #imageLiteral(resourceName: "News"),
            tag:3)
        
        tabViewController4.tabBarItem = UITabBarItem(
            title: "Contest".localized(lang: getLanguageKey(lang:  LocalDB.shared.currentLanguage!)),
            image: #imageLiteral(resourceName: "Contest"),
            tag:4)
        
        tabViewController5.tabBarItem = UITabBarItem(
            title: "Profiles".localized(lang: getLanguageKey(lang:  LocalDB.shared.currentLanguage!)),
            image: #imageLiteral(resourceName: "settings"),
            tag:5)
        
        UITabBar.appearance().tintColor = UIColor(red: 54.0/255.0, green: 121.0/255.0, blue: 178.0/255.0, alpha: 1.0)
        let numberOfItems = CGFloat(tabBarController.tabBar.items!.count)
        let tabBarItemSize = CGSize(width: tabBarController.tabBar.frame.width / numberOfItems, height: tabBarController.tabBar.frame.height)
        tabBarController.tabBar.selectionIndicatorImage = UIImage.imageWithColor(color: UIColor(red: 54.0/255.0, green: 121.0/255.0, blue: 178.0/255.0, alpha: 1.0), size: tabBarItemSize).resizableImage(withCapInsets: UIEdgeInsets(top: 1, left: 0, bottom: 0, right: 0))
        tabBarController.selectedIndex = 0
        tabBarController.delegate = self
        
        // remove default border
        tabBarController.tabBar.frame.size.width = self.tabBarController.view.frame.width + 4
        tabBarController.tabBar.frame.origin.x = -2
        
        window?.rootViewController = tabBarController
        self.window?.makeKeyAndVisible()
    }
    
    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
        
        
       if let currentNavigation =  viewController as? UINavigationController {
            menuNavController = currentNavigation
        }
    }
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        // User tapped on screen, do whatever you want to do here.
        
        if let tempView = touch.view {
            if (tempView is UITextField) || (tempView is UITextView) || (tempView is UITableView) || (tempView is UICollectionView) {
                // do not dismiss keyboard
            } else {
                //self.window?.endEditing(true)
            }
        }
        
        return false
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
        print("applicationWillEnterForeground")

        if let isAlreadyLoggedIn =  UserModel.getAutoLogin(){
            
            if isAlreadyLoggedIn == true{
                checkIfSessionExpired()
            }
        }
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        print("applicationDidBecomeActive")
        if let isAlreadyLoggedIn =  UserModel.getAutoLogin(){
            
            if isAlreadyLoggedIn == true{
                checkIfSessionExpired()
            }
        }
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


    func checkIfSessionExpired()  {
        
        
        APICall.callSessionExpiredAPI( [:] , header: [ : ]) { (sucess, result, error) in
            
            weak var weakSelf = self
            
            
            DispatchQueue.main.async {
                
                
                if (error != nil) {
                    
                //EXPIRED
                    
                    return
                }
                if sucess {
                    
                    do {
                        let jsonData = try JSONSerialization.jsonObject(with: result!, options: .mutableContainers)
                        
                        if (jsonData is NSDictionary){
                            
                            if let statusCode = (jsonData as! NSDictionary).object(forKey: "status"), (statusCode is Int) {
                                
                                if (statusCode as! Int) == 401 {
                                    
                                    //session expired
//                                    weakSelf?.window?.showAlert(title: APP_NAME, message:"Session expired, Please login again.", withTag: 500, button: ["login"], isActionRequired: true)
//                                    return
                                    let alert = UIAlertController(title: APP_NAME, message: "Session expired, Please login again.", preferredStyle: UIAlertControllerStyle.alert)
                                    alert.addAction(UIAlertAction(title: "Okay",
                                                                  style: UIAlertActionStyle.default,
                                                                  handler: self.someHandler))
                                    weakSelf?.window?.rootViewController?.present(alert, animated: true, completion: nil)
                                    
                                    
                                }
                            }
                        }
                    }
                    catch  {
                        
                      
                        return
                    }
                }
                else{
                    //Alert not sucessfull
                  
                }
                
               
            }
        }
        
    }
    func someHandler(alert: UIAlertAction!) {

        
                UserModel.setLoginInfo(userData: nil)
                UserModel.setAutoLogin(isSucessfull:false)
                //LocalDB.shared.currentLanguage = nil
                appDelegate.navigateToLoginViewController()
                
        
        
    }
    func application(_ application: UIApplication,
                     didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        let tokenParts = deviceToken.map { data -> String in
            return String(format: "%02.2hhx", data)
        }
        
        let token = tokenParts.joined()
        print("Device Token: \(token)")
    }
    
    func application(_ application: UIApplication,
                     didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("Failed to register: \(error)")
    }
    
    func registerForPushNotifications() {
        UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .sound, .badge]) {
            (granted, error) in
            print("Permission granted: \(granted)")
            
            guard granted else { return }
            self.getNotificationSettings()
        }
    }
    
    func getNotificationSettings() {
        UNUserNotificationCenter.current().getNotificationSettings { (settings) in
            print("Notification settings: \(settings)")
            guard settings.authorizationStatus == .authorized else { return }
            UIApplication.shared.registerForRemoteNotifications()
        }
    }
}



