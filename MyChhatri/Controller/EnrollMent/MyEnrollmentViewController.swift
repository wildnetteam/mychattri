//
//  MyEnrollmentViewController.swift
//  MyChhatri
//
//  Created by Arpana on 12/09/18.
//  Copyright © 2018 Arpana. All rights reserved.
//

import UIKit

class MyEnrollmentViewController: UIViewController {
    
    var paymentStatus : String?
    
    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBOutlet weak var lblEmailId: UILabel!
    
    @IBOutlet weak var lblMobileNo: UILabel!
    @IBOutlet weak var lblDOB: UILabel!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblenrollmentNo: UILabel!
    
    @IBOutlet weak var lblRegNo: UILabel!
    @IBOutlet weak var lblRegDate: UILabel!
    @IBOutlet weak var lblStatus: UILabel!
    @IBOutlet weak var studentDetailView: UIView!
    @IBOutlet weak var lblBookingDate: UILabel!
    @IBOutlet weak var lblPaymentDate: UILabel!
    @IBOutlet weak var lblCancelledDate: UILabel!
    @IBOutlet weak var lblRegFee: UILabel!
    @IBOutlet weak var lblcourseFee: UILabel!
    @IBOutlet weak var lblAmountReceived: UILabel!
    @IBOutlet weak var cancelButton : UIButton!
    
    @IBOutlet weak var lblDays: UILabel!
    @IBOutlet weak var lblBatchNumber: UILabel!
    @IBOutlet weak var lblBatchTiming: UILabel!
    @IBOutlet weak var lblEndDate: UILabel!
    @IBOutlet weak var lblStartDate: UILabel!
    @IBOutlet weak var lblDuration: UILabel!
    @IBOutlet weak var lblTrainer: UILabel!
    @IBOutlet weak var lblOrganisation: UILabel!
    var dataDictionary : NSDictionary?
    var enrollmentId: String?
    
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var lblCourse: UILabel!
    
    @IBOutlet weak var registerView: UIView!
    
    @IBOutlet weak var btnTermCheck : UIButton!
    @IBOutlet weak var btnTermLink : UIButton!
    var acceptTerms:String = "0"

    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action:
            #selector(MyEnrollmentViewController.getprogramDetail),
                                 for: UIControlEvents.valueChanged)
        refreshControl.tintColor = .black
        
        return refreshControl
    }()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        scrollView.addSubview(refreshControl)
        setUI()
        getprogramDetail()
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        self.navigationController?.navigationBar.isHidden = false
        kAppDelegate.menuNavController = self.navigationController
        
        self.navigationController?.navigationBar.topItem?.title = "My Registration Details"
        
        kAppDelegate.setNavigationAndStatusAppearance()
        let statusBar: UIView = UIApplication.shared.value(forKey: "statusBar") as! UIView
        statusBar.backgroundColor = ColorConstants.appBackGroundCOLOR()
        UIApplication.shared.statusBarStyle = .lightContent
        setNeedsStatusBarAppearanceUpdate()
        
        
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    func setUI() {
        
        studentDetailView.layer.shadowOffset = CGSize(width: 0, height: 8)
        studentDetailView.layer.shadowColor = UIColor(red:0.8, green:0.8, blue:0.8, alpha:0.5).cgColor
        studentDetailView.layer.shadowOpacity = 1
        studentDetailView.layer.shadowRadius = 20
        
        registerView.layer.shadowOffset = CGSize(width: 0, height: 8)
        registerView.layer.shadowColor = UIColor(red:0.8, green:0.8, blue:0.8, alpha:0.5).cgColor
        registerView.layer.shadowOpacity = 1
        registerView.layer.shadowRadius = 20
    }
    
    func setData() {
        
        if paymentStatus != nil {
            
            if paymentStatus == ""{
                
                //could not retrieve correct status
            }else{
                
                if paymentStatus == "1"{
                    
                    //hide pay button
                }
                if paymentStatus == "2"{
                    
                    cancelButton.isHidden = true
                    btnTermLink.isHidden = true
                    btnTermCheck.isHidden = true
                }
            }
        }
        guard let dictionary = dataDictionary else{
            
            self.showAlert(title: "", message: "No data found", withTag: 101, button: ["OK"], isActionRequired: false)
            return
        }
        
         if let status = dictionary.value(forKey: "status_text") as? String {
            
            lblStatus.text = status
        }
        
        if let userDetailDict = dictionary.value(forKey: "user") as? NSDictionary {
            
            if let name = userDetailDict.value(forKey: "name") as? String {
                
                lblName.text = name
                
                if let email = userDetailDict.value(forKey: "email") as? String {
                    
                    lblEmailId.text = email
                }
                if let mobile = userDetailDict.value(forKey: "mobile") as? String {
                    
                    lblMobileNo.text = mobile
                    
                }
                if let dob = userDetailDict.value(forKey: "birthday") as? String {
                    
                    lblDOB.text = dob
                    
                }
            }
        }
        if let regNo = dictionary.value(forKey: "order_no") as? String {
            
            lblRegNo.text = regNo
            lblenrollmentNo.text = regNo
            
            
        }
        
        var regisFee = ""
        if let registrationFees = dictionary.value(forKey: "registration_price") as? String {
            regisFee = registrationFees
        }else  if let registrationFees = dictionary.value(forKey: "registration_price") as? Int {
            regisFee = String(registrationFees)
        }
        
        if  regisFee != nil {
            
            lblRegFee.text = regisFee
            
        }
        var courseFees = ""
        if let courseFee = dictionary.value(forKey: "course_fees") as? String {
            courseFees = courseFee
        }else  if let courseFee = dictionary.value(forKey: "course_fees")  as? Int {
            courseFees = String(courseFee)
        }
        
        if  courseFees != nil {
            
            lblcourseFee.text = courseFees
        }
        
        if let endDate = dictionary.value(forKey: "end_date") as? String {
            lblEndDate.text = endDate
        }
        
        if let startDate = dictionary.value(forKey: "start_date") as? String {
            lblStartDate.text = startDate
        }
        
        if let duration = dictionary.value(forKey: "duration") as? String {
            lblDuration.text = duration
        }
        
        if let vendor = dictionary.value(forKey: "vendor") as? NSDictionary {
            if let userDetails   = vendor.value(forKey: "userdetails") as? NSDictionary {
                if let organisationName = userDetails.value(forKey: "organization_name") as? String {
                    lblOrganisation.text = organisationName
                }
            }
        }
        
        if let batch = dictionary.value(forKey: "batch") as? NSDictionary {
            if let trainer   = batch.value(forKey: "trainer") as? NSDictionary {
                if let trainerName = trainer.value(forKey: "name") as? String {
                    lblTrainer.text = trainerName
                }
            }
        }
        
        if let vendor = dictionary.value(forKey: "course") as? NSDictionary {
            if let title   = vendor.value(forKey: "title") as? String {
                    lblCourse.text = title
                }
        }
        
        if let batch = dictionary.value(forKey: "batch") as? NSDictionary {
            if let duration = batch.value(forKey: "duration") as? String {
                lblDuration.text = duration
            }
        }
        
        if let batch = dictionary.value(forKey: "batch") as? NSDictionary {
            if let batchNumber = batch.value(forKey: "batch_no") as? String {
                lblBatchNumber.text = batchNumber
            }
        }
        
        var totalFees : Float = 0.0
        //calculate total price
        if let registrationFees = regisFee as? String {
            
            totalFees = Float(registrationFees)!
        }
        
        if let courseFees = courseFees as? String {
            totalFees = totalFees + Float(courseFees)!
            
        }
        if let totalFeesCalculated = totalFees as? Float{
            
            lblAmountReceived.text = String(totalFeesCalculated)
        }
        if let startDate = dictionary.value(forKey: "created_at") as? String {
            
            lblBookingDate.text = CalenderStruct.getFormatedDateStringWithouOrdinal(fromDateUTC:  CalenderStruct.getDateFromDateString(forUTC: startDate, byFormat: CalenderStruct.DATE_TIME_ZONE_FORMAT_LOCAL), inFormat: "dd MMM yyyy")
            
            lblRegDate.text = CalenderStruct.getFormatedDateStringWithouOrdinal(fromDateUTC:  CalenderStruct.getDateFromDateString(forUTC: startDate, byFormat: CalenderStruct.DATE_TIME_ZONE_FORMAT_LOCAL), inFormat: "dd MMM yyyy")
        }
        
        if let cancelledDate = dictionary.value(forKey: "cancelled_date") as? String {
            
            if let serverdat = CalenderStruct.getDateFromDateString(forUTC: cancelledDate, byFormat: CalenderStruct.DATE_TIME_ZONE_FORMAT_LOCAL) {
                
                lblCancelledDate.text = CalenderStruct.getFormatedDateStringWithouOrdinal(fromDateUTC: serverdat , inFormat: "dd MMM yyyy")
            }
            
        }
        
        if let paymentdDate = dictionary.value(forKey: "start_date") as? String {
            
            if let serverdat = CalenderStruct.getDateFromDateString(forUTC: paymentdDate, byFormat: "yyyy-MM-dd") {
                
                lblPaymentDate.text = CalenderStruct.getFormatedDateStringWithouOrdinal(fromDateUTC: serverdat , inFormat: "dd MMM yyyy")
            }
            
        }
        
    }
    
    @IBAction func btnCheckAcceptTerms(sender:UIButton) {
        
        //sender.isSelected = !sender.isSelected
        
        if sender.isSelected {
            acceptTerms = "0"
        } else {
            acceptTerms = "1"
        }
        sender.isSelected = !sender.isSelected
    }
    
    @IBAction func btnAcceptTerms(sender:UIButton) {
        let viewController = UIStoryboard.instantiateAcceptTermsViewController()
        appDelegate.menuNavController?.pushViewController(viewController, animated: true)
    }
    
    @IBAction func cancelButtonClicked(_ sender: Any){
    
        if acceptTerms == "0" {
            self.showAlert(title:APP_NAME, message:"Please accept Terms and Condition", withTag:101, button:["Ok"], isActionRequired:false)
            return
        }
        
            //reove api will be called
            self.showAlert(title: APP_NAME, message: "Are you sure, you want to cancel Enrollment?", withTag: 111, button: ["Cancel" , "Ok"], isActionRequired: true)
            
        
        
    }
    
    override func alertButtonAction(alertTag: Int, btnTitle: String) {
        
        if alertTag == 111  {
            if btnTitle == "Ok" {
                
                delete()
                
            }
        }
    }
    // MARK: - REMOVE API FOR SAVED STREAM
    func delete(){
        
        let params : [String:Any] = [:]
        
        guard let idTodelete = enrollmentId else{
            
            self.showAlert(title: "Error", message: "Id to delete is not found. Try again", withTag: 101, button: ["Ok"], isActionRequired: false)
            return
        }
        APICall.callCancelEnrolledBatchAPI(params, header: [ : ], enrolledID: idTodelete) { (sucess, result, error) in
            
            DispatchQueue.main.async {
                
                weak var weakSelf = self
                
                ProgressHUD.hideProgressHUD()
                
                if (error != nil) {
                    weakSelf?.showAlert(title: "Error", message: (error!.localizedDescription), withTag: 101, button: ["Ok"], isActionRequired: false)
                    return
                }
                
                if sucess {
                    
                    do {
                        let jsonData = try JSONSerialization.jsonObject(with: result!, options: .mutableContainers)
                        
                        if let responseDict = (jsonData as? NSDictionary){
                            
                            if let statusCode = responseDict.object(forKey: "status"), (statusCode is Int) {
                                
                                if (statusCode as! Int) == 200 {
                                    
                                    weakSelf?.showAlert(title: APP_NAME, message:"Cancelled sucessfully", withTag: 101, button: ["Ok"], isActionRequired: false)
                                    
                                    weakSelf?.cancelButton.setTitle("Cancelled", for: .normal)
                                    weakSelf?.cancelButton.isEnabled = false
                                    weakSelf?.lblStatus.text = "Cancelled"

                                    weakSelf?.navigationController?.popViewController(animated: true)
                                    
                                } else {
                                    var errStr = ""
                                    if let errormessage = (jsonData as? NSDictionary)?.object(forKey:"error") {
                                        if (errormessage is NSDictionary){
                                            if let errorDescription = (errormessage as? NSDictionary)?.object(forKey: "error_description"),!(errorDescription is NSNull){
                                                if (errorDescription is String){
                                                    errStr = errorDescription as! String
                                                    
                                                }
                                            }else {
                                                if let errorMessage = (errormessage as? NSDictionary)?.object(forKey: "message"),!(errorMessage is NSNull){
                                                    if (errorMessage is String){
                                                        errStr = errorMessage as! String
                                                        
                                                    }
                                                }else{
                                                    errStr = AlertMessages.UnidetifiedErrorMessage
                                                }
                                                
                                            }
                                            weakSelf?.showAlert(title: APP_NAME, message:errStr, withTag: 101, button: ["Ok"], isActionRequired: false)
                                        }
                                        
                                    }else{
                                        weakSelf?.showAlert(title: APP_NAME, message:  AlertMessages.UnidetifiedErrorMessage, withTag: 101, button: ["Ok"], isActionRequired: false)
                                    }
                                    
                                }
                            }
                        }else{
                            
                            weakSelf?.showAlert(title: "sorry", message: AlertMessages.NOTADICTIONARYMESSAGE , withTag: 101, button:  ["Ok"], isActionRequired: false)
                            return
                        }
                        
                    }catch{
                        
                        weakSelf?.showAlert(title: "sorry", message: AlertMessages.MODELERRORMESSAGE , withTag: 101, button:  ["Ok"], isActionRequired: false)
                        return
                    }
                    
                } else{
                    
                    //Success false
                    weakSelf?.showAlert(title: APP_NAME, message: AlertMessages.UnidetifiedErrorMessage, withTag: 101, button: ["Ok"], isActionRequired: false)
                    
                }
                
                
            }
            
        }
    }
    @IBAction func backButtonClicked(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @objc func getprogramDetail()  {
        
        ProgressHUD.showProgressHUDWithLabel(label: "Loading...")
        
        guard (enrollmentId) != nil else{
            
            self.showAlert(title: "Sorry", message: "Enrollment ID is not available please try again", withTag: 101, button: ["Ok"], isActionRequired: false)
            return
        }
        APICall.callEnrolledBatchesDetailsAPI([:], header: [ : ], enID: enrollmentId!) { (sucess, result, error) in
            
            DispatchQueue.main.async {
                
                weak var weakSelf = self
                
                ProgressHUD.hideProgressHUD()
                
                if (error != nil) {
                    weakSelf?.showAlert(title: "Error", message: (error!.localizedDescription), withTag: 101, button: ["Ok"], isActionRequired: false)
                    return
                }
                
                if sucess {
                    
                    do {
                        let jsonData = try JSONSerialization.jsonObject(with: result!, options: .mutableContainers)
                        
                        if let responseDict = (jsonData as? NSDictionary){
                            
                            if let statusCode = responseDict.object(forKey: "status"), (statusCode is Int) {
                                
                                if (statusCode as! Int) == 200 {
                                    
                                    //sucessfully retrieved data
                                    if let dataDict = responseDict.object(forKey: "data") as? NSDictionary{
                                        weakSelf?.dataDictionary = dataDict
                                        
                                        print(dataDict)
                                        weakSelf?.setData()
                                        
                                    }else{
                                        //No data found error
                                        weakSelf?.showAlert(title: APP_NAME, message:  AlertMessages.NoDataKeyFoundInResponse, withTag: 101, button: ["Ok"], isActionRequired: false)
                                    }
                                    
                                } else {
                                    var errStr = ""
                                    if let errormessage = (jsonData as? NSDictionary)?.object(forKey:"error") {
                                        if (errormessage is NSDictionary){
                                            if let errorDescription = (errormessage as? NSDictionary)?.object(forKey: "error_description"),!(errorDescription is NSNull){
                                                if (errorDescription is String){
                                                    errStr = errorDescription as! String
                                                    
                                                }
                                            }else {
                                                if let errorMessage = (errormessage as? NSDictionary)?.object(forKey: "message"),!(errorMessage is NSNull){
                                                    if (errorMessage is String){
                                                        errStr = errorMessage as! String
                                                        
                                                    }
                                                }else{
                                                    errStr = AlertMessages.UnidetifiedErrorMessage
                                                }
                                                
                                            }
                                            weakSelf?.showAlert(title: APP_NAME, message:errStr, withTag: 101, button: ["Ok"], isActionRequired: false)
                                        }
                                        
                                    }else{
                                        weakSelf?.showAlert(title: APP_NAME, message:  AlertMessages.UnidetifiedErrorMessage, withTag: 101, button: ["Ok"], isActionRequired: false)
                                    }
                                    
                                }
                            }
                        }else{
                            
                            weakSelf?.showAlert(title: "sorry", message: AlertMessages.NOTADICTIONARYMESSAGE , withTag: 101, button:  ["Ok"], isActionRequired: false)
                            return
                        }
                        
                    }catch{
                        
                        weakSelf?.showAlert(title: "sorry", message: AlertMessages.MODELERRORMESSAGE , withTag: 101, button:  ["Ok"], isActionRequired: false)
                        return
                    }
                    
                } else{
                    
                    //Success false
                    weakSelf?.showAlert(title: APP_NAME, message: AlertMessages.UnidetifiedErrorMessage, withTag: 101, button: ["Ok"], isActionRequired: false)
                    
                }
                
                if self.refreshControl.isRefreshing{
                    self.refreshControl.endRefreshing()
                }
                
                //                if weakSelf?.tableView != nil {
                //                    weakSelf?.tableView.reloadData()
                //                }
            }
            
        }
    }
    
}

/*{
 "admin_commission" = 0;
 attendance = 1;
 batch =     {
 "batch_no" = abc;
 duration = "1 month";
 id = 36;
 trainer =         {
 id = 33;
 name = Test;
 };
 "trainer_id" = 33;
 };
 "batch_id" = 36;
 "booking_batch_mode" = 2;
 "cancel_deduction" = 0;
 "canceled_by" = "<null>";
 "cancelled_by" = 0;
 "cancelled_date" = "0000-00-00 00:00:00";
 course =     {
 "address_line_1" = meerut;
 "address_line_2" = mawana;
 "caption_text" = "<null>";
 city =         {
 id = 5022;
 name = Noida;
 };
 "city_id" = 5022;
 country =         {
 id = 101;
 name = India;
 };
 "country_id" = 101;
 description = "<p>Stipend Internship</p>";
 id = 10;
 "internship_type" = 2;
 prerequisite = "Stipend Internship";
 slug = "<null>";
 state =         {
 id = 38;
 name = "Uttar Pradesh";
 };
 "state_id" = 38;
 "term_and_condition" = "Stipend Internship";
 title = "Stipend Internship";
 type = 2;
 user =         {
 id = 31;
 name = "Wildnet Technologies";
 };
 "user_id" = 31;
 };
 "course_fees" = 0;
 "course_id" = 10;
 "created_at" = "2018-10-10 09:05:59";
 duration = "";
 "end_date" = "2018-10-26";
 "end_time" = "13:00:00";
 gst = 0;
 id = 30;
 "is_rated" = 1;
 "order_no" = "WT/SI/B36/00030";
 "payment_datetime" = "0000-00-00 00:00:00";
 "receivable_amount" = 0;
 "refund_amount" = 0;
 "registration_price" = 0;
 "repeat_days" = "0,2";
 "start_date" = "2018-10-23";
 "start_time" = "13:00:00";
 status = 1;
 "status_text" = Enrolled;
 "total_price" = 0;
 "total_with_tax" = 0;
 user =     {
 birthday = "<null>";
 email = "mehtapriyankp@gmail.com";
 
 id = 6;
 mobile = 9811270480;
 name = "Priyank Mehta";
 };
 "user_id" = 6;
 vendor =     {
 id = 31;
 name = "Wildnet Technologies";
 userdetails =         {
 "organization_name" = "Wildnet Technologies";
 "user_id" = 31;
 };
 };
 "vendor_earning" = 0;
 "vendor_id" = 31;
 }
 (lldb) ven*/
