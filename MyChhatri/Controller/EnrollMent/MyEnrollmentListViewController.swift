//
//  MyEnrollmentListViewController.swift
//  MyChhatri
//
//  Created by Arpana on 13/09/18.
//  Copyright © 2018 Arpana. All rights reserved.
//

import UIKit

class MyEnrollmentListViewController: UIViewController {

    @IBOutlet weak var doneButton: UIButton!
    
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var pickerToolBar: UIView!
    @IBOutlet weak var pickerView: UIPickerView!
    var selectedLanguage :String?
    var appliedFilterparams : NSDictionary?
    fileprivate lazy var orderByParam = ["Start Date", "End Date"]

    
    @IBOutlet weak var tableView: UITableView!
    
    // MARK: - MAINTAIN PAGINATION AND WEBINAR LIST
    var pageNumber = 0
    var listlimit = 10 // Define to get maximum data one at a time
    var totalCount : Int?
    
    var enrollmentArray : Array<NSDictionary>?
    var offset : Int?
    
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action:
            #selector(MyEnrollmentListViewController.geEnrollmentList(filterElements:)),
                                 for: UIControlEvents.valueChanged)
        refreshControl.tintColor = .black
        
        return refreshControl
    }()
  
    override func viewDidLoad() {
        super.viewDidLoad()
       // tableView.addSubview(refreshControl)
        offset = 0
    }

    override func viewWillAppear(_ animated: Bool) {
        
            super.viewWillAppear(true)
            self.navigationController?.navigationBar.isHidden = false
            kAppDelegate.menuNavController = self.navigationController
            
            self.navigationController?.navigationBar.topItem?.title = "Registered Courses"
            kAppDelegate.setNavigationAndStatusAppearance()
            let statusBar: UIView = UIApplication.shared.value(forKey: "statusBar") as! UIView
            statusBar.backgroundColor = ColorConstants.appBackGroundCOLOR()
            UIApplication.shared.statusBarStyle = .lightContent
            setNeedsStatusBarAppearanceUpdate()
            

        if offset == 0 {
            geEnrollmentList(filterElements: appliedFilterparams)
            
        }
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    @IBAction func backButtonClicked(_ sender: Any) {
        
            self.navigationController?.popViewController(animated: true)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    // MARK:- GET WEB DATA FROM API
    
    
    @objc func geEnrollmentList(filterElements: NSDictionary?)  {
      
        if self.refreshControl.isRefreshing{
            offset = 0
        }
        
        var  infoDict : NSMutableDictionary?
        
        if  filterElements != nil {
            
            infoDict =  NSMutableDictionary.init(dictionary: filterElements!)
            infoDict?.setValue(offset ?? 0, forKey: "offset")
            infoDict?.setValue(listlimit , forKey: "limit")
        }else{
        
  
            let Dict = NSDictionary(objects: [ 10 ,offset ?? 0], forKeys: ["limit" as NSCopying ,"offset" as NSCopying])
            infoDict = NSMutableDictionary.init(dictionary: Dict)
        }
        if let userInfoDict = UserModel.getLoginInfo() {
            
            guard ((userInfoDict.object(forKey: "ID")) != nil) else{
                
                self.showAlert(title: "Sorry", message: "User details could not be found , Please logout and login again", withTag: 101, button: ["Ok"], isActionRequired: false)
                return
            }
            
            guard (userInfoDict.object(forKey: "token") as? String) != nil else{
                
                self.showAlert(title: "Sorry", message: "User accessToken could not be found , Please logout and login again", withTag: 101, button: ["Ok"], isActionRequired: false)
                return
            }
        }
        if tableView != nil {
            
            ProgressHUD.showProgressHUDWithLabel(label: "Loading...")
        }
        
        APICall.callUserEnrolledBatchesAPI([:], header: [ : ], information:infoDict! ) { (sucess, result, error) in
            
            DispatchQueue.main.async {
                
                weak var weakSelf = self
                
                ProgressHUD.hideProgressHUD()
                
                if (error != nil) {
                    weakSelf?.showAlert(title: "Error", message: (error!.localizedDescription), withTag: 101, button: ["Ok"], isActionRequired: false)
                    return
                }
                
                if sucess {
                    
                    do {
                        let jsonData = try JSONSerialization.jsonObject(with: result!, options: .mutableContainers)
                        
                        if let responseDict = (jsonData as? NSDictionary){
                            
                            if let statusCode = responseDict.object(forKey: "status"), (statusCode is Int) {
                                
                                if (statusCode as! Int) == 200 {
                                    
                                    //sucessfully retrieved data
                                    if let dataDict = responseDict.object(forKey: "data") as? NSDictionary{
                                        
                                        if let toptalR = dataDict.value(forKey: "totalCount") as? Int{
                                            weakSelf?.totalCount = toptalR
                                        }else if let toptalR = dataDict.value(forKey: "totalCount") as? String{
                                            weakSelf?.totalCount = Int(toptalR)
                                        }
                                        if let tempArray = dataDict.object(forKey: "bookings") as? Array<NSDictionary> {
                                            
                                            if tempArray.count > 0 {
                                                //store data
                                                if  weakSelf?.offset == 0 {
                                                    weakSelf?.enrollmentArray =  Array()
                                                    
                                                    weakSelf?.enrollmentArray = tempArray
                                                }else{
                                                    //Just add to previous array
                                                    weakSelf?.enrollmentArray?.append(contentsOf: tempArray)
                                                }
                                            }else{
                                                
                                              if  weakSelf?.offset == 0 {
                                                var message = "You do not have any Enrollment yet."
                                                 if  filterElements != nil {
                                                    message = "You do not have Enrollment for applied filter."
                                                }
                                                weakSelf?.enrollmentArray = nil
                                                    //No saved stream found message
                                                    weakSelf?.showAlert(title: APP_NAME, message: message , withTag: 101, button: ["Ok"], isActionRequired: false)
                                                }
                                            }
                                        }else {
                                            //
                                            weakSelf?.showAlert(title: APP_NAME, message: "bookings key not found", withTag: 101, button: ["Ok"], isActionRequired: false)
                                        }
                                    }else{
                                        //No data found error
                                        weakSelf?.showAlert(title: APP_NAME, message:  AlertMessages.NoDataKeyFoundInResponse, withTag: 101, button: ["Ok"], isActionRequired: false)
                                    }
                                    
                                } else {
                                    var errStr = ""
                                    if let errormessage = (jsonData as? NSDictionary)?.object(forKey:"error") {
                                        if (errormessage is NSDictionary) {
                                            if let errorDescription = (errormessage as? NSDictionary)?.object(forKey: "error_description"),!(errorDescription is NSNull){
                                                if (errorDescription is String){
                                                    errStr = errorDescription as! String
                                                    
                                                }
                                            }else {
                                                if let errorMessage = (errormessage as? NSDictionary)?.object(forKey: "message"),!(errorMessage is NSNull){
                                                    if (errorMessage is String){
                                                        errStr = errorMessage as! String
                                                        
                                                    }
                                                }else{
                                                    errStr = AlertMessages.UnidetifiedErrorMessage
                                                }
                                                
                                            }
                                            weakSelf?.showAlert(title: APP_NAME, message:errStr, withTag: 101, button: ["Ok"], isActionRequired: false)
                                        }
                                        
                                    }else{
                                        weakSelf?.showAlert(title: APP_NAME, message:  AlertMessages.UnidetifiedErrorMessage, withTag: 101, button: ["Ok"], isActionRequired: false)
                                    }
                                    
                                }
                            }
                        }else{
                            
                            weakSelf?.showAlert(title: "sorry", message: AlertMessages.NOTADICTIONARYMESSAGE , withTag: 101, button:  ["Ok"], isActionRequired: false)
                            return
                        }
                        
                    }catch{
                        
                        weakSelf?.showAlert(title: "sorry", message: AlertMessages.MODELERRORMESSAGE , withTag: 101, button:  ["Ok"], isActionRequired: false)
                        return
                    }
                    
                } else{
                    
                    //Success false
                    weakSelf?.showAlert(title: APP_NAME, message: AlertMessages.UnidetifiedErrorMessage, withTag: 101, button: ["Ok"], isActionRequired: false)
                    
                }
                
                if self.refreshControl.isRefreshing{
                    self.refreshControl.endRefreshing()
                }
                
                if weakSelf?.tableView != nil {
                    weakSelf?.tableView.reloadData()
                }
            }
            
        }
    }

    
    @IBAction func filterButtonClicked(_ sender: Any) {
        NavigationManager.moveToFilterCompanyProfileViewControllerIfNotExists(kAppDelegate.menuNavController! , fileterDel : self)
    }
    
    @IBAction func orderByClicked(_ sender: Any) {
        pickerView.isHidden = false
        pickerToolBar.isHidden = false
        doneButton.isHidden = false
        cancelButton.isHidden = false

    }
    
    //This is for the keyboard to GO AWAYY !! when user clicks anywhere on the view
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        //hide picker view with two language options to select any one, and restart app from  begining
        selectedLanguage = ""
        pickerView.isHidden = true
        pickerToolBar.isHidden = true
        doneButton.isHidden = true
        cancelButton.isHidden = true

        self.view.endEditing(true)
    }
    
    @IBAction func pickerDoneBtnClicked(_ sender: Any) {
        
        if selectedLanguage == nil {
            
            self.showAlert(title: "", message: "You have not selected any lanuage", withTag: 101, button: ["Ok"], isActionRequired: false)
            return
        }
        if selectedLanguage == "" {
            
            self.showAlert(title: "", message: "Please select any of the lanuage", withTag: 101, button: ["Ok"], isActionRequired: false)
            return
        }
        
        if let currentSelectedLanguage = selectedLanguage {
            
            if currentSelectedLanguage == "English"{
                
                if  LocalDB.shared.currentLanguage ==  Language.english
                {
                    //the same language is active now
                }else{
                    
                    LocalDB.shared.currentLanguage =  Language.english
                    //restart the app
                    kAppDelegate.loadStudentHomeViewController()
                    
                }
            }else if  currentSelectedLanguage == "Hindi"{
                
                if  LocalDB.shared.currentLanguage ==  Language.hindi
                {
                    //the same language is active now
                }else{
                    
                    LocalDB.shared.currentLanguage =  Language.hindi
                    //restart the app
                    kAppDelegate.loadStudentHomeViewController()
                    
                }
            }
        }
        //        selectedLanguage = ""
        pickerView.isHidden = true
        pickerToolBar.isHidden = true
        doneButton.isHidden = true
        cancelButton.isHidden = true

    }
}

extension MyEnrollmentListViewController : FilterParamDelegate {
    
    func passfilterparams(data: [String: Any]) {

        appliedFilterparams = nil
        appliedFilterparams = NSDictionary.init(dictionary: (data as? NSDictionary)!)
        geEnrollmentList(filterElements: data as? NSDictionary)
    }
}

extension MyEnrollmentListViewController : UITableViewDelegate, UITableViewDataSource , UIViewControllerTransitioningDelegate{
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section:
        Int) -> Int
    {
        if let mArray = enrollmentArray {
            return mArray.count
        }else {
            return 0
        }
        
    }
    
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        // Allocates a Table View Cell
        let cell =
            tableView.dequeueReusableCell(withIdentifier: "EnrollmentListTableViewCellID",
                                          for: indexPath) as! EnrollmentListTableViewCell
        
         cell.DetailView.layer.cornerRadius = 4
         cell.DetailView.layer.shadowOffset = CGSize(width: 0, height: 8)
         cell.DetailView.layer.shadowColor = UIColor(red:0.8, green:0.8, blue:0.8, alpha:0.5).cgColor
         cell.DetailView.layer.shadowOpacity = 1
         cell.DetailView.layer.shadowRadius = 20
        
        if enrollmentArray != nil {
            
            if let tempTrendsArray = enrollmentArray {
                
                if let dict = tempTrendsArray[indexPath.row] as?  NSDictionary {
                    
                    cell.setCellData(dict: dict)

                }
                //Reload the next amount of data
                if(indexPath.row ==  (enrollmentArray?.count)! - 1 && totalCount! > (enrollmentArray?.count)! ){
                    //we are at last index of currently displayed cell
                    offset = (enrollmentArray?.count)!
                    //reload more data
                    geEnrollmentList(filterElements: appliedFilterparams)
                }
            }
        }
        cell.selectionStyle = .none
        return cell
    }
    
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return  290
    }
    
}

extension MyEnrollmentListViewController : UIPickerViewDelegate, UIPickerViewDataSource {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return orderByParam.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return orderByParam[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        selectedLanguage = orderByParam[row]
    }
    
}
