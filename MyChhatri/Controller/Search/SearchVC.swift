//
//  ContestVC.swift
//  MyChhatri
//
//  Created by Anshul on 18/07/18.
//  Copyright © 2018 Anshul. All rights reserved.
//

import UIKit

class SearchVC: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var textFieldCompany: UITextField!
    @IBOutlet weak var textFieldCourse: UITextField!
    @IBOutlet weak var textFieldCity : UITextField!
    @IBOutlet weak var textFieldOfflineOnline : UITextField!
    @IBOutlet weak var textFieldInternTraining : UITextField!
    
    var selectedMode : Int = 0
    var selectedType : Int = 0
    //*********Picker Setup*********//
    private var picker = UIPickerView()
    private var txtFieldPicker = UITextField()
    private var arr_Picker = [""]
    
    // MARK: - MAINTAIN PAGINATION
    var pageNumber = 0
    var listlimit = 10 // Define to get maximum data one at a time
    var totalCount : Int?
    
    var searchArray : Array<NSDictionary>?
    
    var offset : Int?
    
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action:
            #selector(SearchVC.getSearchList),
                                 for: UIControlEvents.valueChanged)
        refreshControl.tintColor = .black
        
        return refreshControl
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        offset = 0
        tableView.addSubview(refreshControl)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.navigationItem.leftBarButtonItem = nil
        self.navigationController?.navigationItem.hidesBackButton = true
        let statusBar: UIView = UIApplication.shared.value(forKey: "statusBar") as! UIView
        statusBar.backgroundColor = ColorConstants.appBackGroundCOLOR()
        
        if offset == 0 {
            getSearchList()

        }
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
  
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func getSearchList()  {
        
        if self.refreshControl.isRefreshing{
            offset = 0
        }
        
        
        let infoDict = NSDictionary(objects: [listlimit , offset ?? 0 , textFieldCompany.text ?? "", textFieldCourse.text ?? "" ,textFieldCity.text ?? "" , selectedType , selectedMode ], forKeys: ["limit" as NSCopying ,"offset" as NSCopying, "companyname" as NSCopying, "coursename" as NSCopying , "keyword" as NSCopying , "course_type" as NSCopying , "mode" as NSCopying ])
        
        if tableView != nil {
            
            ProgressHUD.showProgressHUDWithLabel(label: "Loading...")
        }
        
        APICall.callSearchAPI([:], header: [ : ], information:infoDict ) { (sucess, result, error) in
            
            DispatchQueue.main.async {
                
                weak var weakSelf = self
                
                ProgressHUD.hideProgressHUD()
                
                if (error != nil) {
                    weakSelf?.showAlert(title: "Error", message: (error!.localizedDescription), withTag: 101, button: ["Ok"], isActionRequired: false)
                    return
                }
                
                if sucess {
                    
                    do {
                        let jsonData = try JSONSerialization.jsonObject(with: result!, options: .mutableContainers)
                        
                        if let responseDict = (jsonData as? NSDictionary){
                            
                            if let statusCode = responseDict.object(forKey: "status"), (statusCode is Int) {
                                
                                if (statusCode as! Int) == 200 {
                                    
                                    //sucessfully retrieved data
                                    if let dataDict = responseDict.object(forKey: "data") as? NSDictionary{
                                        
                                        if let toptalR = dataDict.value(forKey: "totalCount") as? Int{
                                            weakSelf?.totalCount = toptalR
                                        }else if let toptalR = dataDict.value(forKey: "totalCount") as? String{
                                            weakSelf?.totalCount = Int(toptalR)
                                        }
                                        
                                        if let tempArray = dataDict.object(forKey: "vendors") as? Array<NSDictionary> {
                                            
                                            if tempArray.count > 0 {
                                                //store data
                                                if  weakSelf?.pageNumber == 0 {
                                                    self.searchArray =  Array()
                                                    
                                                    weakSelf?.searchArray = tempArray
                                                }else{
                                                    //Just add to previous array
                                                    weakSelf?.searchArray?.append(contentsOf: tempArray)
                                                }
                                                
                                            }else{
                                                //No saved stream found message
                                                weakSelf?.showAlert(title: APP_NAME, message: "No result found.", withTag: 101, button: ["Ok"], isActionRequired: false)
                                            }
                                        }else {
                                            //
                                            weakSelf?.showAlert(title: APP_NAME, message: "vendors key not found", withTag: 101, button: ["Ok"], isActionRequired: false)
                                        }
                                    }else{
                                        //No data found error
                                        weakSelf?.showAlert(title: APP_NAME, message:  AlertMessages.NoDataKeyFoundInResponse, withTag: 101, button: ["Ok"], isActionRequired: false)
                                    }
                                    
                                } else {
                                    var errStr = ""
                                    if let errormessage = (jsonData as? NSDictionary)?.object(forKey:"error") {
                                        if (errormessage is NSDictionary){
                                            if let errorDescription = (errormessage as? NSDictionary)?.object(forKey: "error_description"),!(errorDescription is NSNull){
                                                if (errorDescription is String){
                                                    errStr = errorDescription as! String
                                                    
                                                }
                                            }else {
                                                if let errorMessage = (errormessage as? NSDictionary)?.object(forKey: "message"),!(errorMessage is NSNull){
                                                    if (errorMessage is String){
                                                        errStr = errorMessage as! String
                                                        
                                                    }
                                                }else{
                                                    errStr = AlertMessages.UnidetifiedErrorMessage
                                                }
                                                
                                            }
                                            weakSelf?.showAlert(title: APP_NAME, message:errStr, withTag: 101, button: ["Ok"], isActionRequired: false)
                                        }
                                        
                                    }else{
                                        weakSelf?.showAlert(title: APP_NAME, message:  AlertMessages.UnidetifiedErrorMessage, withTag: 101, button: ["Ok"], isActionRequired: false)
                                    }
                                    
                                }
                            }
                        }else{
                            
                            weakSelf?.showAlert(title: "sorry", message: AlertMessages.NOTADICTIONARYMESSAGE , withTag: 101, button:  ["Ok"], isActionRequired: false)
                            return
                        }
                        
                    }catch{
                        
                        weakSelf?.showAlert(title: "sorry", message: AlertMessages.MODELERRORMESSAGE , withTag: 101, button:  ["Ok"], isActionRequired: false)
                        return
                    }
                    
                } else{
                    
                    //Success false
                    weakSelf?.showAlert(title: APP_NAME, message: AlertMessages.UnidetifiedErrorMessage, withTag: 101, button: ["Ok"], isActionRequired: false)
                    
                }
                
                if self.refreshControl.isRefreshing{
                    self.refreshControl.endRefreshing()
                }
                if weakSelf?.totalCount == 0{
                   weakSelf?.searchArray = nil
                }
                if weakSelf?.tableView != nil {
                    weakSelf?.tableView.reloadData()
                }
            }
            
        }
    }
    
    @IBAction func searchBtnClicked(){
        
        offset = 0
        self.view.endEditing(true)
        getSearchList()
    }
    @IBAction func resetBtnClicked(){
        
        textFieldCompany.text = ""
        textFieldCourse.text = ""
        textFieldCity.text = ""
        textFieldOfflineOnline.text = ""
        textFieldInternTraining.text = ""
        offset = 0
        getSearchList()
    }
}
    extension SearchVC : UITableViewDelegate, UITableViewDataSource , UIViewControllerTransitioningDelegate , ViewCoursesDelegate{
        
        func viewCourseList(_ i: Int) {
            
            if searchArray != nil {
                
                if let tempcompanyArray = searchArray {
                    
                    if let dict = tempcompanyArray[i]  as? NSDictionary{
                        NavigationManager.moveToAllCoursesListingViewControllerIfNotExists(kAppDelegate.menuNavController!, courseDictDetail: dict)
                    }
                }
            }
        }
        
        
        func tableView(_ tableView: UITableView, numberOfRowsInSection section:
            Int) -> Int
        {
            if let array = searchArray {
                
                return array.count
                
            }
            return 0
        }
        
        
        
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
        {
            // Allocates a Table View Cell
            let cell =
                tableView.dequeueReusableCell(withIdentifier: "MyPageRegisterTableViewCellID",
                                              for: indexPath) as! MyPageRegisterTableViewCell
            
            cell.selectionStyle = UITableViewCellSelectionStyle.none
            cell.backGroundWhiteView.layer.cornerRadius =  (cell.backGroundWhiteView.frame.size.width * 0.01506)
            cell.backGroundWhiteView.layer.masksToBounds = true
            cell.buttonViewAll.tag = indexPath.row
            cell.viewCoursesDelegate = self
            // cell.shadowView.backgroundColor =  UIColor.white
            cell.shadowView.layer.shadowColor = UIColor(red:0.8, green:0.8, blue:0.8, alpha:0.5).cgColor
            cell.shadowView.layer.shadowOpacity = 1 ;
            cell.shadowView.layer.shadowRadius = 20
            cell.shadowView.layer.shadowOffset = CGSize(width: 0.0, height: cell.contentView.frame.height * 0.04)
            cell.shadowView.layer.masksToBounds = false
            cell.imgView.layer.cornerRadius =  (cell.imgView.frame.size.width * 0.18)
            cell.imgView.layer.masksToBounds = true
            if searchArray != nil {
                
                if let tempcompanyArray = searchArray {
                    
                    if let dict = tempcompanyArray[indexPath.row] as?  NSDictionary {
                        
                        if let userDetailDict = dict.value(forKey: "userdetails") as? NSDictionary{
                            
                            if let wTitle = userDetailDict.value(forKey: "organization_name") as? String {
                                
                                
                                cell.lblHeading.text = wTitle
                            }
                            if let wDes = userDetailDict.value(forKey: "description") as? String {
                                cell.lblDescription.text = wDes == "" ?"No descsription available":wDes.withoutHtml
                            }else{
                                cell.lblDescription.text = "No descsription available"
                            }
                        }
                        
                        if let courseArray = dict.value(forKey: "course") as? NSArray {
                            cell.lblCourse.text = ""
                            if courseArray.count > 0 {
                                
                                for i in 0..<courseArray.count {
                                    //Display only 2 course here, if more click on view All
                                    if i <= 1 {
                                        if let courseDetailDict = courseArray.object(at: i) as? NSDictionary {
                                            
                                            if let  title = courseDetailDict.value(forKey: "title")  as? String{
                                                
                                                if i == 1{
                                                    
                                                    cell.lblCourse.text?.append("    |    ")
                                                }
                                                cell.lblCourse.text?.append(title)
                                            }
                                        }
                                        
                                    }
                                }
                            }
                        }
                        var imageFinalUrl =  ""
                        
                        if let imageURL = dict.value(forKey: "profile_image") {
                            
                            imageFinalUrl = "\(URLS.IMAGE_BASE_URL)" + "\(imageURL)"
                            
                            cell.imgView.sd_setIndicatorStyle(.gray)
                            cell.imgView.sd_setShowActivityIndicatorView(true)
                            
                            cell.imgView.sd_setImage(with: URL(string: imageFinalUrl), completed: { (image, error, type, url) in
                                DispatchQueue.main.async {
                                    if error == nil{
                                        cell.imgView.image = image
                                    }else{
                                        cell.imgView.image = #imageLiteral(resourceName: "Placeholder")
                                    }
                                    cell.imgView.sd_setShowActivityIndicatorView(false)
                                }
                            })
                        }
                        
                    }
                    //Reload the next amount of data
                    if(indexPath.row ==  (searchArray?.count)! - 1 && totalCount! > (searchArray?.count)! ){
                        //we are at last index of currently displayed cell
                        //reload more data
                        offset = (searchArray?.count)!
                        getSearchList()
                    }
                }
            }
            cell.selectionStyle = .none
            return cell
        }
        
        
        func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
        {
            
        }
        
    }


extension SearchVC : UITextFieldDelegate, UIPickerViewDelegate, UIPickerViewDataSource {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
    
        
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        txtFieldPicker = textField
        
        if textField == textFieldInternTraining {
            self.arr_Picker = ["Training", "Internship",]
            self.showPickerView()
            
        } else if textField == textFieldOfflineOnline {
            self.arr_Picker = ["Online" , "Offline"]
            self.showPickerView()
            
        }
    }
    
 
    
    // UIPickerView
    private func showPickerView() {
        
        picker = UIPickerView(frame: CGRect(x: 0, y: 0, width: screenWidth, height: 216))
        picker.backgroundColor = .white
        picker.showsSelectionIndicator = true
        picker.delegate = self
        picker.dataSource = self
        txtFieldPicker.inputView = picker
        
        let toolBar = UIToolbar()
        toolBar.barStyle = UIBarStyle.default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor.black
        toolBar.sizeToFit()
        
        let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItemStyle.plain, target: self, action: #selector(cancelActionPicker))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.plain, target: self, action: #selector(doneActionPicker))
        
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        txtFieldPicker.inputAccessoryView = toolBar
    }
    
    @objc func doneActionPicker (sender:UIBarButtonItem)
    {
        let tempRow = picker.selectedRow(inComponent: 0)
        print("\(tempRow)")
        
        self.txtFieldPicker.text = arr_Picker[tempRow]
        
        
        if self.txtFieldPicker == self.textFieldInternTraining {
            selectedType = tempRow + 1
        }else  if self.txtFieldPicker == self.textFieldOfflineOnline {
            selectedMode = tempRow + 1
        }
        
        txtFieldPicker.resignFirstResponder()
        
    }
    
    @objc func cancelActionPicker (sender:UIBarButtonItem)
    {
        if txtFieldPicker != textFieldOfflineOnline {
            txtFieldPicker.text = ""
        }
        if txtFieldPicker != textFieldInternTraining {
            txtFieldPicker.text = ""
        }
        txtFieldPicker.resignFirstResponder()
        
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return arr_Picker.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return arr_Picker[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        self.txtFieldPicker.text = arr_Picker[row]
    }
    
}


    

    





