//
//  VideoListingViewController.swift
//  MyChhatri
//
//  Created by Arpana on 01/08/18.
//  Copyright © 2018 Anshul. All rights reserved.
//

import UIKit

class VideoListingViewController: UIViewController {
   
    @IBOutlet weak var shadowView: UIView!
    @IBOutlet weak var tableView:UITableView!
    var InterviewVideoModelObject :InterviewVideoModel?

    var rootCatID : String?
    
    // MARK: - MAINTAIN PAGINATION AND WEBINAR LIST
    var pageNumber = 0
    var listlimit = 5 // Define to get maximum data one at a time
    var totalCount : Int?
    var offset : Int?
    
    var videoArray : Array<NSDictionary>?
    
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action:
            #selector(VideoListingViewController.getVideosByCategory),
                                 for: UIControlEvents.valueChanged)
        refreshControl.tintColor = .black
        
        return refreshControl
    }()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Add shdow Below page menu
        shadowView.layer.masksToBounds = false
        shadowView.layer.shadowOpacity = 0.4 ;
        shadowView.layer.shadowColor = UIColor.black.cgColor
        shadowView.layer.shadowOffset = CGSize(width: 0.0, height: 4.0)
        tableView.addSubview(refreshControl)
        offset = 0
        getVideosByCategory()
        tableView.estimatedRowHeight = 60
        tableView.rowHeight = UITableViewAutomaticDimension

    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if offset == 0 {
            getVideosByCategory()
            
        }
    }
    
    @objc func getVideosByCategory()  {
        
        let params : [String:Any] = [:]
        
        if tableView != nil {
            
            ProgressHUD.showProgressHUDWithLabel(label: "Loading...")
        }
        
        APICall.callVideosByCategoryAPI(params, header: [ : ], categoryID: rootCatID!) { (sucess, result, error) in
            
            DispatchQueue.main.async {
                
                weak var weakSelf = self
                
                self.refreshControl.endRefreshing()
                
                ProgressHUD.hideProgressHUD()
                
                if (error != nil) {
                    
                    weakSelf?.showAlert(title: "Error", message: (error!.localizedDescription), withTag: 101, button: ["Ok"], isActionRequired: false)
                    return
                }
                if sucess {
                    
                    do {
                        let jsonData = try JSONSerialization.jsonObject(with: result!, options: .mutableContainers)
                        
                        if let responseDict = (jsonData as? NSDictionary){
                            
                            if let statusCode = responseDict.object(forKey: "status"), (statusCode is Int) {
                                
                                if (statusCode as! Int) == 200 {
                                    
                                    //sucessfully retrieved data
                                    if let dataDict = responseDict.object(forKey: "data") as? NSDictionary{
                                        
                                        if let toptalR = dataDict.value(forKey: "totalCount") as? Int{
                                            weakSelf?.totalCount = toptalR
                                        }else if let toptalR = dataDict.value(forKey: "totalCount") as? String{
                                            weakSelf?.totalCount = Int(toptalR)
                                        }
                                        if let tempArray = dataDict.object(forKey: "interviews") as? Array<NSDictionary> {
                                            
                                            if tempArray.count > 0 {
                                                //store data
                                                if  weakSelf?.offset == 0 {
                                                    self.videoArray =  Array()
                                                    
                                                    weakSelf?.videoArray = tempArray
                                                }else{
                                                    //Just add to previous array
                                                    weakSelf?.videoArray?.append(contentsOf: tempArray)
                                                }
                                            }else{
                                                //No saved stream found message
                                                weakSelf?.showAlert(title: APP_NAME, message: "You do not have any article yet.", withTag: 101, button: ["Ok"], isActionRequired: false)
                                            }
                                        }else {
                                            //
                                            weakSelf?.showAlert(title: APP_NAME, message: "trends key not found", withTag: 101, button: ["Ok"], isActionRequired: false)
                                        }
                                    }else{
                                        //No data found error
                                        weakSelf?.showAlert(title: APP_NAME, message:  AlertMessages.NoDataKeyFoundInResponse, withTag: 101, button: ["Ok"], isActionRequired: false)
                                    }
                                    
                                } else {
                                    var errStr = ""
                                    if let errormessage = (jsonData as? NSDictionary)?.object(forKey:"error") {
                                        if (errormessage is NSDictionary) {
                                            if let errorDescription = (errormessage as? NSDictionary)?.object(forKey: "error_description"),!(errorDescription is NSNull){
                                                if (errorDescription is String){
                                                    errStr = errorDescription as! String
                                                    
                                                }
                                            }else {
                                                if let errorMessage = (errormessage as? NSDictionary)?.object(forKey: "message"),!(errorMessage is NSNull){
                                                    if (errorMessage is String){
                                                        errStr = errorMessage as! String
                                                        
                                                    }
                                                }else{
                                                    errStr = AlertMessages.UnidetifiedErrorMessage
                                                }
                                                
                                            }
                                            weakSelf?.showAlert(title: APP_NAME, message:errStr, withTag: 101, button: ["Ok"], isActionRequired: false)
                                        }
                                        
                                    }else{
                                        weakSelf?.showAlert(title: APP_NAME, message:  AlertMessages.UnidetifiedErrorMessage, withTag: 101, button: ["Ok"], isActionRequired: false)
                                    }
                                    
                                }
                            }
                        }else{
                            
                            weakSelf?.showAlert(title: "sorry", message: AlertMessages.NOTADICTIONARYMESSAGE , withTag: 101, button:  ["Ok"], isActionRequired: false)
                            return
                        }
                        
                        
                        
                    } catch  {
                        
                        weakSelf?.showAlert(title: "sorry", message: AlertMessages.MODELERRORMESSAGE , withTag: 101, button:  ["Ok"], isActionRequired: false)
                        return
                    }
                } else{
                    //Alert not sucessfull
                    weakSelf?.showAlert(title: APP_NAME, message: "could not get response from server", withTag: 101, button: ["Ok"], isActionRequired: false)
                }
                
                if self.refreshControl.isRefreshing{
                    self.refreshControl.endRefreshing()
                }
                
                if weakSelf?.tableView != nil {
                    weakSelf?.tableView.reloadData()
                }
            }
            
        }
    }
        
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
    extension VideoListingViewController : UITableViewDelegate, UITableViewDataSource , UIViewControllerTransitioningDelegate{
        
        
        func tableView(_ tableView: UITableView, numberOfRowsInSection section:
            Int) -> Int
        {
            if let  array = videoArray {
                return array.count
                
            }
            return 0
        
        }
        
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
        {
            // Allocates a Table View Cell
            let cell =
                tableView.dequeueReusableCell(withIdentifier: "VideoTableViewCellID",
                                              for: indexPath) as! VideoTableViewCell
            
            if (videoArray) != nil {

                if let inteviewDetails = videoArray?[indexPath.row] as? NSDictionary {

                    if let intitle = inteviewDetails.object(forKey: "company") as? String {

                        cell.lblCompany.text = intitle

                    }

                    if let intitle = inteviewDetails.object(forKey: "interviewer") as? String {

                        cell.lblTitle.text = intitle

                    }

                    if let indescription = inteviewDetails.object(forKey: "title") as? String {

                        cell.lblSubTitle.text =  indescription

                    }
                    if let indescription = inteviewDetails.object(forKey: "designation") as? String {
                        
                        cell.lblDesignation.text =  indescription
                        
                    }
                    var imageFinalUrl =  ""

                    if let profileData =   inteviewDetails.object(forKey: "profile") as? NSDictionary {
                        if let imageURL = profileData.object(forKey: "thumbnail") {

                            imageFinalUrl = "\(URLS.IMAGE_BASE_URL)" + "\(imageURL)"

                            cell.profileImage.sd_setImage(with: URL(string: imageFinalUrl), completed: { (image, error, type, url) in
                                DispatchQueue.main.async {
                                    if error == nil{
                                        cell.profileImage.image = image
                                    }else{
                                        cell.profileImage.image = #imageLiteral(resourceName: "Placeholder")
                                    }
                                    cell.profileImage.sd_setShowActivityIndicatorView(false)
                                }
                            })
                        }else{
                            cell.profileImage.image = #imageLiteral(resourceName: "Placeholder")

                        }
                    }
                    cell.profileImage.layer.masksToBounds = true
                    cell.profileImage.layer.cornerRadius = cell.profileImage.frame.height * 0.063

                    cell.vedios =  inteviewDetails.object(forKey: "interviewvideos") as?  [NSDictionary]
                    cell.profileDict = videoArray?[indexPath.row]

                }
                cell.contentView.layer.shadowOpacity = 1;
                cell.contentView.layer.shadowRadius = 20
                // cell.shodowView.layer.shadowColor = UIColor(red:0, green:0, blue:0, alpha:0.1).cgColor
                cell.contentView.layer.shadowColor =  UIColor(red:0, green:0, blue:0, alpha:0.1).cgColor
                cell.contentView.layer.shadowOffset = CGSize(width: 0.0, height: 8)
                cell.contentView.layer.masksToBounds = false
                
                cell.reload()

            }
           
             return cell
        }
        
        
        
        func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
        {
            
            
        }
        func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
            return 100
        }
//        func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//
//             return  (tableView.bounds.height ) * 0.40
//          //  return  (tableView.bounds.height ) * 0.6
//
//
//        }

}
