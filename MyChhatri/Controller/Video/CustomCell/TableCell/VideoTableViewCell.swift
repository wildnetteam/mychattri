//
//  VideoTableViewCell.swift
//  MyChhatri
//
//  Created by Arpana on 01/08/18.
//  Copyright © 2018 Arpana. All rights reserved.
//

import UIKit

class VideoTableViewCell: UITableViewCell, UICollectionViewDelegate, UICollectionViewDataSource , UICollectionViewDelegateFlowLayout {
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblSubTitle: UILabel!
    @IBOutlet weak var profileImage : UIImageView!
     @IBOutlet weak var lblCompany: UILabel!
    @IBOutlet weak var lblDesignation: UILabel!
    
    @IBOutlet weak var categoryCollectionView: UICollectionView!
    var vedios : [NSDictionary]?
    var profileDict : NSDictionary?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        categoryCollectionView.dataSource = self as UICollectionViewDataSource
        categoryCollectionView.delegate = self as UICollectionViewDelegate
        
    }

    func reload(){
        categoryCollectionView.reloadData()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    // MARK:- Collection Delegates
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return (vedios?.count)!
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        
        return UIEdgeInsetsMake(0, 0, 0, 0)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat{
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat{
        return 0
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
      //  let size    =  CGSize(width:(screenWidth * 0.5 ), height: (screenWidth * 0.6))
        
        let size    =  CGSize(width:(screenWidth * 0.30), height: (screenWidth * 0.40 ))
        return size
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "VideoCollectionViewCellID", for: indexPath) as! VideoCollectionViewCell
 /*
        if  profileDict != nil {
            
            if let intitle = profileDict?.object(forKey: "company") as? String {
                
                cell.lblCompany.text = intitle
                
            }
            
            if let intitle = profileDict?.object(forKey: "interviewer") as? String {
                
                cell.lblTitle.text = intitle
                
            }
            
            if let indescription = profileDict?.object(forKey: "designation") as? String {
                
                cell.lblDesignation.text =  indescription
                
            }
            if let indescription = profileDict?.object(forKey: "title") as? String {
                
                cell.lblSubTitle.text =  indescription
                
            }
            var imageFinalUrl =  ""
            
            if let profileData =  profileDict?.object(forKey: "profile") as? NSDictionary {
                if let imageURL = profileData.object(forKey: "thumbnail") {
                    
                    imageFinalUrl = "\(URLS.IMAGE_BASE_URL)" + "\(imageURL)"
                    
                    cell.profileImage.sd_setImage(with: URL(string: imageFinalUrl), completed: { (image, error, type, url) in
                        DispatchQueue.main.async {
                            if error == nil{
                                cell.profileImage.image = image
                            }else{
                                cell.profileImage.image = #imageLiteral(resourceName: "Placeholder")
                            }
                            cell.profileImage.sd_setShowActivityIndicatorView(false)
                        }
                    })
                }else{
                    cell.profileImage.image = #imageLiteral(resourceName: "Placeholder")
                    
                }
            }
            cell.profileImage.layer.masksToBounds = true
            cell.profileImage.layer.cornerRadius = cell.profileImage.frame.height * 0.063
        }*/
        if (vedios) != nil {
        
            if let inteviewDetails = vedios?[indexPath.row] {
                
                
                cell.imgView.sd_setIndicatorStyle(.gray)
                cell.imgView.sd_setShowActivityIndicatorView(true)
                
                var imageFinalUrl =  ""
                
                if let imageURL = inteviewDetails.object(forKey: "thumbnail") {
                    
                    imageFinalUrl = "\(URLS.IMAGE_BASE_URL)" + "\(imageURL)"
                    
                    cell.imgView.sd_setImage(with: URL(string: imageFinalUrl), completed: { (image, error, type, url) in
                        DispatchQueue.main.async {
                            if error == nil{
                                cell.imgView.image = image
                            }else{
                                cell.imgView.image = #imageLiteral(resourceName: "top-bg")
                            }
                            cell.imgView.sd_setShowActivityIndicatorView(false)
                        }
                    })
                }else{
                    cell.imgView.image = #imageLiteral(resourceName: "top-bg")

                }
            }
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if (vedios) != nil {
            
            if let inteviewDetails = vedios?[indexPath.row] as? NSDictionary {
                
                if let isOfflineAvailable = inteviewDetails.object(forKey:"is_offline") as? String{
                    if isOfflineAvailable == "1" {
                        NavigationManager.moveToVideoPlayerViewControllerIfNotExists(kAppDelegate.menuNavController!, details: inteviewDetails)
                    }
                    else {
                        //OnlineUrl
                        if let url = inteviewDetails.object(forKey:"online_url") as? String{
                            
                            print("No offline video")
                            
                            //Its a video URL of you tube, Go to you tube app
                            goToYouTube(videoLink: url)
                            
                        }
                    }
                    
                } else if let isOfflineAvailable = inteviewDetails.object(forKey:"is_offline") as? Int{
                    if isOfflineAvailable == 1 {
                        NavigationManager.moveToVideoPlayerViewControllerIfNotExists(kAppDelegate.menuNavController!, details: inteviewDetails)
                    }
                    else {
                        //OnlineUrl
                        if let url = inteviewDetails.object(forKey:"online_url") as? String{
                            
                            print("No offline video")
                            
                            //Its a video URL of you tube, Go to you tube app
                            goToYouTube(videoLink: url)
                            
                        }
                    }
                }
            }
        }
    }
    
    func goToYouTube(videoLink: String?){
        
        let appName = "youtube"
        let appScheme = "\(appName)://"
        let url = URL(string: appScheme)
        let videourl = URL(string:videoLink ?? "https://www.youtube.com/") //"https://www.youtube.com/watch?v=N-GNV7jhKV4")
        
        if UIApplication.shared.canOpenURL(url! ){
            UIApplication.shared.open(videourl!, options: [:], completionHandler: nil)
        } else{
            let  youtubeUrl = NSURL(string:"https://itunes.apple.com/us/app/youtube-watch-listen-stream/id544007664?mt=8")!
            UIApplication.shared.open((youtubeUrl as URL), options: [:], completionHandler: nil)
            print("App not installed")
        }
        
    }
}
