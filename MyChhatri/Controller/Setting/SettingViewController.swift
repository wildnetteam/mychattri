//
//  SettingViewController.swift
//  MyChhatri
//
//  Created by Arpana on 21/08/18.
//  Copyright © 2018 Arpana. All rights reserved.
//

import UIKit

class SettingViewController: UIViewController {

   @IBOutlet  var tableView: UITableView!
   @IBOutlet  var tableViewHeader: UIView!
   @IBOutlet  var tableViewFooter: UIView!
   @IBOutlet  var profilerImageView :UIImageView!
   @IBOutlet weak var pickerView: UIPickerView!
   var selectedLanguage :String?
   var userDetailDictionary: NSDictionary?

    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var doneButton: UIButton!
    
    @IBOutlet weak var pickerToolBar: UIView!
    private let heightForHeader  :CGFloat  = 100
   private let heightForFooter  :CGFloat  = 60
    var tableTimer : Timer!
    var inc : Int!
    var ArrMutable : NSMutableArray?
    
   fileprivate lazy var settingMenuItems = ["Language Settings".localized(lang: getLanguageKey(lang:  LocalDB.shared.currentLanguage!)), "My Registered Courses".localized(lang: getLanguageKey(lang:  LocalDB.shared.currentLanguage!)) , "My Wishlist".localized(lang: getLanguageKey(lang:  LocalDB.shared.currentLanguage!)) ,"My Subscription".localized(lang: getLanguageKey(lang:  LocalDB.shared.currentLanguage!)), "My Rewards".localized(lang: getLanguageKey(lang:  LocalDB.shared.currentLanguage!)) , "About App".localized(lang: getLanguageKey(lang:  LocalDB.shared.currentLanguage!)) , "Contact Us".localized(lang: getLanguageKey(lang:  LocalDB.shared.currentLanguage!)) , "Change Password".localized(lang: getLanguageKey(lang:  LocalDB.shared.currentLanguage!))]
    
    fileprivate lazy var settingMenuIMAGEItems = [#imageLiteral(resourceName: "language") ,#imageLiteral(resourceName: "courses"), #imageLiteral(resourceName: "courses") , #imageLiteral(resourceName: "subscription"), #imageLiteral(resourceName: "rewards"),  #imageLiteral(resourceName: "about") , #imageLiteral(resourceName: "contact") , #imageLiteral(resourceName: "Password")]
    
    fileprivate lazy var languages = ["English", "Hindi"]
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        profilerImageView.layer.masksToBounds = true
        profilerImageView.layer.cornerRadius = profilerImageView.frame.width / 2
        profilerImageView.updateConstraintsIfNeeded()
        self.tableView.tableHeaderView = tableViewHeader
        self.tableView.tableFooterView = tableViewFooter
        inc = 0
        ArrMutable = NSMutableArray()
        ArrMutable?.removeAllObjects()
        
        if tableTimer != nil {
            tableTimer.invalidate()
        }
       //  tableTimer = Timer.scheduledTimer(timeInterval: 0.2, target: self, selector: #selector(SettingViewController.performTableUpdates(_:)), userInfo:nil, repeats: true)
        setNeedsStatusBarAppearanceUpdate()


    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = true
        kAppDelegate.menuNavController = self.navigationController
        let statusBar: UIView = UIApplication.shared.value(forKey: "statusBar") as! UIView
        statusBar.backgroundColor = .clear
        getUserDetails()

//        if LocalDB.shared.currentLanguage == .english{
//            selectedLanguage = "English"
//        }else{
//            selectedLanguage = "Hindi"
//
//        }
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
        super.viewWillDisappear(true)
        
        let statusBar: UIView = UIApplication.shared.value(forKey: "statusBar") as! UIView
        statusBar.backgroundColor = ColorConstants.appBackGroundCOLOR()
        
    }

    
    func getUserDetails()  {
        
        let params : [String:Any] = [:]
        
        ProgressHUD.showProgressHUDWithLabel(label: "Loading...")
        
        
        APICall.callGetUserDetailsAPI(params, header: [ : ]) { (sucess, result, error) in
            
            DispatchQueue.main.async {
                
                weak var weakSelf = self
                
                ProgressHUD.hideProgressHUD()
                
                if (error != nil) {
                    
                    weakSelf?.showAlert(title: "Error", message: (error!.localizedDescription), withTag: 101, button: ["Ok"], isActionRequired: false)
                    return
                }
                if sucess {
                    
                    do {
                        
                        let jsonData = try JSONSerialization.jsonObject(with: result!, options: .mutableContainers)
                        
                        if (jsonData is NSDictionary){
                            
                            if let statusCode = (jsonData as! NSDictionary).object(forKey: "status"), (statusCode is Int) {
                                
                                 if (statusCode as! Int) == 401 {
                                    
                                    //Authorization is expired
                                    //session expired
                                    weakSelf?.showAlert(title: APP_NAME, message:"Session expired, Please login again.", withTag: 500, button: ["login"], isActionRequired: true)
                                    return
                                }
                                
                                if (statusCode as! Int) == 200 {
                                    
                                    //sucessfully retrieved data
                                    if let dataDict = (jsonData as! NSDictionary).object(forKey: "data") as? NSDictionary{
                                        
                                        weakSelf?.userDetailDictionary = dataDict
                                        weakSelf?.lblName.text = dataDict.value(forKey: "name") as? String
                                        var imageFinalUrl =  ""
                                        
                                        if let imageURL = dataDict.value(forKey: "profile_image") {
                                            
                                            weak var weakSelf = self
                                            
                                            imageFinalUrl = "\(URLS.IMAGE_BASE_URL)" + "\(imageURL)"
                                            
                                            weakSelf?.profilerImageView.sd_setImage(with: URL(string: imageFinalUrl), completed: { (image, error, type, url) in
                                                DispatchQueue.main.async {
                                                    if error == nil{
                                                        weakSelf?.profilerImageView.image = image
                                                        
                                                    }else{
                                                        weakSelf?.profilerImageView.image = #imageLiteral(resourceName: "Placeholder")
                                                    }
                                                    weakSelf?.profilerImageView.sd_setShowActivityIndicatorView(false)
                                                }
                                            })
                                        }
                                        
                                    }else{
                                        weakSelf?.showAlert(title: APP_NAME, message: AlertMessages.NoDataKeyFoundInResponse, withTag: 101, button: ["Ok"], isActionRequired: false)
                                    }
                                    
                                }else{
                                    
                                    var errStr = ""
                                    if let errormessage = (jsonData as? NSDictionary)?.object(forKey:"error") {
                                        if (errormessage is NSDictionary){
                                            if let errorDescription = (errormessage as? NSDictionary)?.object(forKey: "error_description"),!(errorDescription is NSNull){
                                                if (errorDescription is String){
                                                    errStr = errorDescription as! String
                                                    
                                                }
                                            }else {
                                                if let errorMessage = (errormessage as? NSDictionary)?.object(forKey: "message"),!(errorMessage is NSNull){
                                                    if (errorMessage is String){
                                                        errStr = errorMessage as! String
                                                        
                                                    }
                                                }else{
                                                    errStr = "Something went wrong"
                                                }
                                                
                                            }
                                            weakSelf?.showAlert(title: APP_NAME, message:errStr, withTag: 101, button: ["Ok"], isActionRequired: false)
                                        }
                                        
                                    }else{
                                        weakSelf?.showAlert(title: APP_NAME, message: AlertMessages.UnidetifiedErrorMessage, withTag: 101, button: ["Ok"], isActionRequired: false)
                                    }
                                }
                            }
                            
                        }else{
                            
                            //mot a dict
                            weakSelf?.showAlert(title: "sorry", message: AlertMessages.NOTADICTIONARYMESSAGE , withTag: 101, button:  ["Ok"], isActionRequired: false)
                            return
                        }
                    }catch  let error as NSError {
                        print(error.localizedDescription)
                        
                        weakSelf?.showAlert(title: "sorry", message: AlertMessages.MODELERRORMESSAGE , withTag: 101, button:  ["Ok"], isActionRequired: false)
                        return
                    }
                }
                else{
                    //Alert not sucessfull
                    weakSelf?.showAlert(title: APP_NAME, message: "could not get response from server", withTag: 101, button: ["Ok"], isActionRequired: false)
                }
                
                
            }
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //This is for the keyboard to GO AWAYY !! when user clicks anywhere on the view
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        //hide picker view with two language options to select any one, and restart app from  begining
        selectedLanguage = ""
        pickerView.isHidden = true
        pickerToolBar.isHidden = true
        doneButton.isHidden = true
        self.view.endEditing(true)
    }
    
    @IBAction func logout(sender:UIButton) {
        
        self.showAlert(title: APP_NAME, message: "Are you sure,you want to log out?", withTag: 111, button: ["Cancel" , "Ok"], isActionRequired: true)
    }
    
    @IBAction func editProfile(sender:UIButton) {
        
        NavigationManager.moveToEditProfileViewControllerIfNotExists(kAppDelegate.menuNavController! , userDetail: userDetailDictionary)
    }
    
    @IBAction func pickerDoneBtnClicked(_ sender: Any) {
       
        if selectedLanguage == nil {
            
            self.showAlert(title: "", message: "You have not selected any lanuage", withTag: 101, button: ["Ok"], isActionRequired: false)
            return
        }
        if selectedLanguage == "" {
            
            self.showAlert(title: "", message: "Please select any of the lanuage", withTag: 101, button: ["Ok"], isActionRequired: false)
            return
        }
        
        if let currentSelectedLanguage = selectedLanguage {
            
            if currentSelectedLanguage == "English"{
                
                if  LocalDB.shared.currentLanguage ==  Language.english
                {
                    //the same language is active now
                }else{
                    
                    LocalDB.shared.currentLanguage =  Language.english
                    //restart the app
                    kAppDelegate.loadStudentHomeViewController()

                }
            }else if  currentSelectedLanguage == "Hindi"{
                
                if  LocalDB.shared.currentLanguage ==  Language.hindi
                {
                    //the same language is active now
                }else{
                    
                    LocalDB.shared.currentLanguage =  Language.hindi
                    //restart the app
                    kAppDelegate.loadStudentHomeViewController()

                }
            }
        }
//        selectedLanguage = ""
        pickerView.isHidden = true
        pickerToolBar.isHidden = true
        doneButton.isHidden = true
    }
    
    
    override func alertButtonAction(alertTag: Int, btnTitle: String) {
        
        if alertTag == 111  {
            if btnTitle == "Ok" {
                UserModel.setLoginInfo(userData: nil)
                UserModel.setAutoLogin(isSucessfull:false)
                LocalDB.shared.currentLanguage = nil
                appDelegate.navigateToLoginViewController()
                
            }else{
                
            }
        }
    }
    
    @objc func performTableUpdates(_ timer : Timer){
        
        let ip : IndexPath = IndexPath(row: inc, section: 0)
        
        guard let tempNewsArray = settingMenuItems as? Array<String> else {
            
            return
        }
        if(tempNewsArray.count == 0) {
            
            return
        }
        ArrMutable?.add(tempNewsArray[inc])
        tableView.beginUpdates()
        
        tableView.insertRows(at: [ip], with: UITableViewRowAnimation.fade)
        
        tableView.endUpdates()
        
        if(inc == ((tempNewsArray.count) - 1)){
            tableTimer.invalidate()
        }
        else{
            inc = inc + 1
        }
    }
}
extension SettingViewController : UIPickerViewDelegate, UIPickerViewDataSource {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return languages.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return languages[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
      selectedLanguage = languages[row]
    }
    
}
extension SettingViewController : UITableViewDelegate, UITableViewDataSource , UIViewControllerTransitioningDelegate{
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section:
        Int) -> Int
    {
        if let array = settingMenuItems  as? [String]{
            return  array.count
        }
        return 0
    }
    
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        // Allocates a Table View Cell
        
        let cell =
            tableView.dequeueReusableCell(withIdentifier: "SettingTableViewCellID",
                                          for: indexPath) as! SettingTableViewCell
        if let menu = settingMenuItems[indexPath.row] as? String{
            cell.lblHeading?.text = menu
            cell.imgView?.image = settingMenuIMAGEItems[indexPath.row]
        }
        cell.accessoryType = .disclosureIndicator
        cell.selectionStyle = .none
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        
        let controller : SettingMenu = SettingMenu(rawValue:indexPath.row)!
        
        switch controller {
            
        case  .Language:
            
             //if user does not select any lang, default is first one
            /* if LocalDB.shared.currentLanguage == .english{
                        selectedLanguage = "English"
                    }else{
                        selectedLanguage = "Hindi"
            
              }*/
             selectedLanguage = languages[0]
            //show picker view with two language options to select any one, and restart app from  begining
            pickerView.isHidden = false
            pickerToolBar.isHidden = false
            doneButton.isHidden = false

            break
        case .Courses:
            NavigationManager.moveToMyEnrollmentListViewControllerIfNotExists(kAppDelegate.menuNavController!)
            break
            
        case .MyWishList:
            NavigationManager.moveToWishListViewControllerIfNotExists(kAppDelegate.menuNavController!)
            break
            
        case .MySubscription:
            break
            
        case .AboutApp:  NavigationManager.moveToAboutUsViewControllerIfNotExists(kAppDelegate.menuNavController!)
            break
            
        case .ContactUs:
            NavigationManager.moveToContactUsViewControllerIfNotExists(kAppDelegate.menuNavController!)

            break
            
        case  .ChangePassword:
            
            if let userInfoDict = UserModel.getLoginInfo() {
                
                if let  userId = (userInfoDict.object(forKey: "ID") as? String){
                    NavigationManager.moveToResetPasswordViewControllerViewControllerIfNotExists(kAppDelegate.menuNavController!, userID: userId)
                    
                    
                }else if let userIded = (userInfoDict.object(forKey: "ID") as? Int){
                    NavigationManager.moveToResetPasswordViewControllerViewControllerIfNotExists(kAppDelegate.menuNavController!, userID: String(userIded))
                }else{
                    
                    self.showAlert(title: "Sorry", message: "User details could not be found , Please logout and login again", withTag: 101, button: ["Ok"], isActionRequired: false)
                    return
                }
                
                
            }
            
            break
        default: print("Need to implement")
            
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return  60
    }

//    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
//
//        return 60
//    }
    
    
}



