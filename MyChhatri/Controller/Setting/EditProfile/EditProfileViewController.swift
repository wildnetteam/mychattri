//
//  EditProfileViewController.swift
//  MyChhatri
//
//  Created by Arpana on 22/08/18.
//  Copyright © 2018 Arpana. All rights reserved.
//

import UIKit

class EditProfileViewController: UIViewController {
    
    @IBOutlet weak var txtMobile: UITextField!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtTitle: UITextField!
    @IBOutlet weak var txtFullName: UITextField!
    @IBOutlet weak var txtStudentName: UITextField!
    @IBOutlet weak var txtStudentMobileNumber: UITextField!
    @IBOutlet weak var txtStudentEmailId: UITextField!
    @IBOutlet weak var txtCountry: UITextField!
    @IBOutlet weak var txtState: UITextField!
    @IBOutlet weak var txtCity: UITextField!
    @IBOutlet weak var txtCollegeName: UITextField!
    @IBOutlet weak var txtManualCollegeName:UITextField!
    @IBOutlet weak var btnserchCollegeList:UIButton!

    @IBOutlet weak var btnProfile: UIButton!
    @IBOutlet weak var txtStudentTitle: UITextField!
    
    @IBOutlet weak var mobileSeparatorLine      : UIView!
    @IBOutlet weak var emailSeparatorLine       : UIView!
    @IBOutlet weak var titleSeparatorLine       :   UIView!
    @IBOutlet weak var fullNameSeparatorLine    : UIView!
    @IBOutlet weak var studentNameSeparatorLine : UIView!
    @IBOutlet weak var studentMobileSeparatorLine : UIView!
    @IBOutlet weak var studentEmailSeparatorLine  : UIView!
    @IBOutlet weak var txtStudentTitleSeparatorLine : UIView!
    @IBOutlet weak var countryrSeparatorLine : UIView!
    @IBOutlet weak var stateSeparatorLine : UIView!
    @IBOutlet weak var citySeparatorLine : UIView!
    @IBOutlet weak var collegeSeparatorLine : UIView!
    
    @IBOutlet weak var AddressView: UIView!
    @IBOutlet weak var guardianView: UIView!
    @IBOutlet weak var emailPhoneView: UIView!
    @IBOutlet weak var studentView: UIView!
    @IBOutlet weak var studentViewHeight: NSLayoutConstraint!
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var imgProfileLarge: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var imgAge:UIImageView!
    @IBOutlet weak var btnSave:UIButton!
    var isprofileImageChanged = false
    
    
    var storedMail: String?
     var storedPhone: String?
    //*********Picker Setup*********//
    private var picker = UIPickerView()
    private var txtFieldPicker = UITextField()
    private var arr_Picker = ["Mr.","Mrs.","Ms.","Miss."]
    //*********Picker Setup*********//
    
    var countryView:CountryView?
    
    //MARK: Internal Properties
    var imagePickedBlock: ((UIImage) -> Void)?
    
    
    //    @IBOutlet weak var txtEmailToclassConstraint: NSLayoutConstraint!
    //
    //    @IBOutlet weak var txtEmailToStudentNameConstant: NSLayoutConstraint!
    
    
    var userDetailDictionary: NSDictionary?
    
    var studentDetailDictionary: NSDictionary?
    
    var countryID: String?
    var cityID : String?
    var stateID :String?
    var collegeID : String?
    var userType : String?
    @IBOutlet weak var imgIconLeading: NSLayoutConstraint!
    
    
    @IBOutlet weak var updatebtnleadingConstant: NSLayoutConstraint!
    override func viewDidLoad() {
        super.viewDidLoad()
        setUPUI()
        setData()
        resetUIForViewMode()
    }
    
    func setUPUI() {
        
        let viewLeftAge = UIView(frame: CGRect(x: 0, y: 0, width: 8, height: 40))
        txtCountry.leftView = viewLeftAge
        txtCountry.leftViewMode = .always
        let viewLeftPassword = UIView(frame: CGRect(x: 0, y: 0, width: 8, height: 40))
        txtMobile.leftView = viewLeftPassword
        txtMobile.leftViewMode = .always
        let viewLeftEmail = UIView(frame: CGRect(x: 0, y: 0, width: 8, height: 40))
        txtEmail.leftView = viewLeftEmail
        txtEmail.leftViewMode = .always
        
        let viewTitle = UIView(frame: CGRect(x: 0, y: 0, width: 8, height: 40))
        self.txtTitle.leftView = viewTitle
        txtTitle.leftViewMode = .always
        
        
        let viewFullName = UIView(frame: CGRect(x: 0, y: 0, width: 8, height: 40))
        self.txtFullName.leftView = viewFullName
        txtFullName.leftViewMode = .always
        
        let viewStudentName = UIView(frame: CGRect(x: 0, y: 0, width: 8, height: 40))
        self.txtStudentName.leftView = viewStudentName
        txtStudentName.leftViewMode = .always
        
        
        let viewStudentEmailID = UIView(frame: CGRect(x: 0, y: 0, width: 8, height: 40))
        self.txtStudentEmailId.leftView = viewStudentEmailID
        txtStudentEmailId.leftViewMode = .always
        
        let viewtxtStudentMobileNumber = UIView(frame: CGRect(x: 0, y: 0, width: 8, height: 40))
        self.txtStudentMobileNumber.leftView = viewtxtStudentMobileNumber
        txtStudentMobileNumber.leftViewMode = .always
        
        let viewtxtStudentTitle = UIView(frame: CGRect(x: 0, y: 0, width: 8, height: 40))
        self.txtStudentTitle.leftView = viewtxtStudentTitle
        txtStudentTitle.leftViewMode = .always
        
        
        let viewtxtState = UIView(frame: CGRect(x: 0, y: 0, width: 8, height: 40))
        self.txtState.leftView = viewtxtState
        txtState.leftViewMode = .always
        
        let viewtxtCity = UIView(frame: CGRect(x: 0, y: 0, width: 8, height: 40))
        self.txtCity.leftView = viewtxtCity
        txtCity.leftViewMode = .always
        
        let viewtxtCollegeName = UIView(frame: CGRect(x: 0, y: 0, width: 8, height: 40))
        self.txtCollegeName.leftView = viewtxtCollegeName
        txtCollegeName.leftViewMode = .always
        
        let view10 = UIView(frame: CGRect(x: 0, y: 0, width: 8, height: 40))
        txtManualCollegeName.leftView = view10
        txtManualCollegeName.leftViewMode = .always
        
        
        imgProfile.layer.masksToBounds = true
        imgProfile.layer.cornerRadius =  imgProfile.frame.width / 2
        
        emailPhoneView.layer.shadowOffset = CGSize(width: 0, height:  emailPhoneView.frame.size.height + 30)
        emailPhoneView.layer.shadowColor = UIColor(red:0.8, green:0.8, blue:0.8, alpha:0.5).cgColor
        emailPhoneView.layer.shadowOpacity = 1
        emailPhoneView.layer.shadowRadius = 20
        emailPhoneView.layer.masksToBounds = false

    }
    
    //MARK: IBAction
    @IBAction func searchCollegeList(_ sender : Any) {
        
        self.txtManualCollegeName.isHidden = true
        self.txtCollegeName.isHidden = false
        self.txtManualCollegeName.resignFirstResponder()
        self.txtCollegeName.becomeFirstResponder()
    }
    
    func setData()  {
        if userDetailDictionary != nil{
            
            if let title = userDetailDictionary?.value(forKey: "title") as? String{
                txtTitle.text = title
            }
            
            if let name = userDetailDictionary?.value(forKey: "name") as? String{
                lblName.text = name
                txtFullName.text = name
            }
            if let email = userDetailDictionary?.value(forKey: "email") as? String{
                txtEmail.text = email
                storedMail = email
            }
            if let mobile = userDetailDictionary?.value(forKey: "mobile") as? String{
                txtMobile.text = mobile
                storedPhone =  mobile
            }else  if let mobile = userDetailDictionary?.value(forKey: "mobile") as? Int{
                txtMobile.text = String(mobile)
                 storedPhone =  String(mobile)
            }
            
           
            //            if let title = userDetailDictionary?.value(forKey: "title") as? String{
            //                txtStudentAcademicStatus.text = title
            //            }
            if let countryDict = userDetailDictionary?.value(forKey: "country") as? NSDictionary {
                txtCountry.text = countryDict.value(forKey: "name") as? String
                if let countryIDen =  countryDict.value(forKey: "id") as? String {
                    
                    countryID = countryIDen
                }else if let countryIDen =  countryDict.value(forKey: "id") as? Int {
                    countryID = String(countryIDen)
                    
                }
            }
            
            if let cityDict = userDetailDictionary?.value(forKey: "city") as? NSDictionary {
                txtCity.text = cityDict.value(forKey: "name") as? String
                if let countryIDen =  cityDict.value(forKey: "id") as? String {
                    
                    cityID = countryIDen
                }else if let countryIDen =  cityDict.value(forKey: "id") as? Int {
                    cityID = String(countryIDen)
                    
                }
            }
            
            if let stateDict = userDetailDictionary?.value(forKey: "state") as? NSDictionary {
                txtState.text = stateDict.value(forKey: "name") as? String
                if let countryIDen =  stateDict.value(forKey: "id") as? String {
                    
                    stateID = countryIDen
                }else if let countryIDen =  stateDict.value(forKey: "id") as? Int {
                    stateID = String(countryIDen)
                    
                }
            }
            if let collegeDict = userDetailDictionary?.value(forKey: "college") as? NSDictionary {
                txtCollegeName.text = collegeDict.value(forKey: "college_name") as? String
                if let countryIDen =  collegeDict.value(forKey: "id") as? String {
                    
                    collegeID = countryIDen
                }else if let countryIDen =  collegeDict.value(forKey: "id") as? Int {
                    collegeID = String(countryIDen)
                    
                }
            }

            // MARKS :- Student Detail
            
            if let studentRelatedDetails =  userDetailDictionary?.value(forKey: "userdetails") as? NSDictionary {
                
                studentDetailDictionary =  NSDictionary.init(dictionary: studentRelatedDetails)
                
                if let title = studentRelatedDetails.value(forKey: "student_title") as? String{
                    txtStudentTitle.text = title
                }
                if let name = studentRelatedDetails.value(forKey: "student_name") as? String{
                    txtStudentName.text = name
                }
                if let email = studentRelatedDetails.value(forKey: "other_email") as? String{
                    txtStudentEmailId.text = email
                }
                if let mobile = studentRelatedDetails.value(forKey: "other_mobile") as? String{
                    txtStudentMobileNumber.text = mobile
                }
                
            }
            
            
            var imageFinalUrl =  ""
            
            if let imageURL = userDetailDictionary?.value(forKey: "profile_image") {
                
                weak var weakSelf = self
                
                imageFinalUrl = "\(URLS.IMAGE_BASE_URL)" + "\(imageURL)"
                
                weakSelf?.imgProfile.sd_setImage(with: URL(string: imageFinalUrl), completed: { (image, error, type, url) in
                    DispatchQueue.main.async {
                        if error == nil{
                            weakSelf?.imgProfile.image = image
                            
                        }else{
                            weakSelf?.imgProfile.image = #imageLiteral(resourceName: "ProfileTab")
                        }
                        weakSelf?.imgProfile.sd_setShowActivityIndicatorView(false)
                    }
                })
            }
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(true)
        let statusBar: UIView = UIApplication.shared.value(forKey: "statusBar") as! UIView
        statusBar.backgroundColor = .clear
        self.navigationController?.navigationBar.isHidden = true
        //  getUserDetails()
        
        if let userTypeVal =  userDetailDictionary?.value(forKey: "role") as? String {
            
            userType = userTypeVal
        }else if let userTypeVal =  userDetailDictionary?.value(forKey: "role") as? Int {
            userType = String(userTypeVal)
            
        }
        
        
        if userType == "3" {
          //  student
            studentViewHeight.constant = 0
            studentView.isHidden = true
            self.view.layoutIfNeeded()
        }else{
            //guardian
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
        super.viewWillDisappear(true)
        
        let statusBar: UIView = UIApplication.shared.value(forKey: "statusBar") as! UIView
        statusBar.backgroundColor = ColorConstants.appBackGroundCOLOR()
        
    }
    
    
    override func alertButtonAction(alertTag: Int, btnTitle: String) {
        
        if alertTag == 111  {
            if btnTitle == "Ok" {
                
                updateDetails()
                
            }
        }
    }
    
    func updateUIForEditingPurpose()  {
        
        if btnSave.title(for: .normal) == "Save" {
            
              self.showAlert(title: APP_NAME, message: "Are you sure you want to save the changes?", withTag: 111, button: ["Cancel" , "Ok"], isActionRequired: true)
            return
        }
        else{
            UIView.animate(withDuration: 0.5, animations: {
                self.updatebtnleadingConstant.constant = 70
                
                self.btnProfile.isEnabled = true
                self.txtCountry.isEnabled = true
                self.txtMobile.isEnabled = true
                self.txtEmail.isEnabled = true
                self.txtEmail.becomeFirstResponder()

                self.txtTitle.isEnabled = true
                self.txtFullName.isEnabled = true
                
                self.txtStudentName.isEnabled = true
                self.txtStudentEmailId.isEnabled = true
                self.txtStudentMobileNumber.isEnabled = true
                self.txtStudentTitle.isEnabled = true
                
                self.txtCountry.isEnabled = true
                self.txtState.isEnabled = true
                self.txtCity.isEnabled = true
                self.txtCollegeName.isEnabled = true
                
                self.txtCountry.transform = CGAffineTransform.identity
                
                self.txtCountry.backgroundColor = UIColor(red: 0.921, green: 0.921, blue: 0.921, alpha: 1)
                self.txtMobile.backgroundColor = UIColor(red: 0.921, green: 0.921, blue: 0.921, alpha: 1)
                self.txtEmail.backgroundColor = UIColor(red: 0.921, green: 0.921, blue: 0.921, alpha: 1)
                
                self.txtTitle.backgroundColor = UIColor(red: 0.921, green: 0.921, blue: 0.921, alpha: 1)
                self.txtFullName.backgroundColor = UIColor(red: 0.921, green: 0.921, blue: 0.921, alpha: 1)
                self.txtStudentName.backgroundColor = UIColor(red: 0.921, green: 0.921, blue: 0.921, alpha: 1)
                
                self.txtStudentEmailId.backgroundColor = UIColor(red: 0.921, green: 0.921, blue: 0.921, alpha: 1)
                self.txtStudentMobileNumber.backgroundColor = UIColor(red: 0.921, green: 0.921, blue: 0.921, alpha: 1)
                
                self.txtStudentTitle.backgroundColor = UIColor(red: 0.921, green: 0.921, blue: 0.921, alpha: 1)
                self.txtCountry.backgroundColor = UIColor(red: 0.921, green: 0.921, blue: 0.921, alpha: 1)
                self.txtState.backgroundColor = UIColor(red: 0.921, green: 0.921, blue: 0.921, alpha: 1)
                self.txtCity.backgroundColor = UIColor(red: 0.921, green: 0.921, blue: 0.921, alpha: 1)
                self.txtCollegeName.backgroundColor = UIColor(red: 0.921, green: 0.921, blue: 0.921, alpha: 1)
                
                self.mobileSeparatorLine.isHidden = true
                self.emailSeparatorLine.isHidden = true
                self.titleSeparatorLine.isHidden = true
                self.fullNameSeparatorLine.isHidden = true
                self.studentNameSeparatorLine.isHidden = true
                self.txtStudentTitleSeparatorLine.isHidden = true
                
                self.studentMobileSeparatorLine.isHidden = true
                self.studentEmailSeparatorLine.isHidden = true
                self.countryrSeparatorLine.isHidden = true
                self.stateSeparatorLine.isHidden = true
                self.citySeparatorLine.isHidden = true
                self.collegeSeparatorLine.isHidden = true
                
                self.imgIconLeading.constant = -40
                self.btnSave.setTitle("Save", for: .normal)
                self.view.updateConstraints()
            }, completion: { (_) in
                self.view.updateConstraints()
                
            })
        }
    }
    
    func  resetUIForViewMode()  {
        
        UIView.animate(withDuration: 0.5, animations: {
            self.txtCountry.transform = CGAffineTransform.identity
            
            self.btnSave.setTitle("Edit", for: .normal)
            self.btnProfile.isEnabled = false

            self.txtCountry.isEnabled = false
            self.txtMobile.isEnabled = false
            self.txtEmail.isEnabled = false
            
            self.txtTitle.isEnabled = false
            self.txtFullName.isEnabled = false
            self.txtStudentName.isEnabled = false
            
            self.txtStudentEmailId.isEnabled = false
            self.txtStudentMobileNumber.isEnabled = false
          
            self.txtStudentTitle.isEnabled = false
            self.txtCountry.isEnabled = false
            self.txtState.isEnabled = false
            self.txtCity.isEnabled = false
            self.txtCollegeName.isEnabled = false

            
            
            self.txtCountry.backgroundColor = .clear
            self.txtMobile.backgroundColor = .clear
            self.txtEmail.backgroundColor = .clear
            
            self.txtTitle.backgroundColor = .clear
            self.txtFullName.backgroundColor = .clear
            self.txtStudentName.backgroundColor = .clear
            
            self.txtStudentEmailId.backgroundColor = .clear
            self.txtStudentMobileNumber.backgroundColor = .clear
            self.txtStudentTitle.backgroundColor = .clear
            self.txtCountry.backgroundColor = .clear
            self.txtState.backgroundColor = .clear
            self.txtCity.backgroundColor = .clear
            self.txtCollegeName.backgroundColor = .clear
            
            self.mobileSeparatorLine.isHidden = false
            self.emailSeparatorLine.isHidden = false
            self.titleSeparatorLine.isHidden = false
            self.fullNameSeparatorLine.isHidden = false
            self.studentNameSeparatorLine.isHidden = false
            
            self.txtStudentTitleSeparatorLine.isHidden = false
            self.studentMobileSeparatorLine.isHidden = false
            self.studentEmailSeparatorLine.isHidden = false
            self.countryrSeparatorLine.isHidden = false
            self.stateSeparatorLine.isHidden = false
            self.citySeparatorLine.isHidden = false
            self.collegeSeparatorLine.isHidden = false
            
            self.btnSave.setTitle("Edit", for: .normal)
            self.imgIconLeading.constant = 10
        }, completion: { (_) in
            self.view.updateConstraints()
            
        })
    }
    
    @IBAction func backButtonPressed(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    @IBAction func btnActionSave(sender:UIButton) {
        
        self.view.endEditing(true)
        updateUIForEditingPurpose()
        
    }
    
    @IBAction func updateEmail(_ sender: Any) {
        
        if let previousMail =  storedMail {
            
            if previousMail == txtEmail.text {
                
                 self.showAlert(title: APP_NAME, message: "Please enter another Email ID to update .", withTag: 101, button: ["Ok"], isActionRequired: false)
            }
        }
        
        if txtEmail.text?.count == 0 {
            self.showAlert(title: APP_NAME, message: "Please enter Email ID.", withTag: 101, button: ["Ok"], isActionRequired: false)
        }
        else {
            let viewController = UIStoryboard.instantiateOTPForMailOrPhoneViewController()
            viewController.mobileNumber = txtEmail.text!
            viewController.emailOrNumber = "email"
            appDelegate.menuNavController?.pushViewController(viewController, animated: true)
        }
    }
    
    @IBAction func updateMobile(_ sender: Any) {
        
        if let previousMail =  storedPhone {
            
            if previousMail == txtMobile.text {
                
                self.showAlert(title: APP_NAME, message: "Please enter another Number to update .", withTag: 101, button: ["Ok"], isActionRequired: false)
            }
        }
        if txtMobile.text?.count == 0 {
            self.showAlert(title: APP_NAME, message: "Please enter mobile number.", withTag: 101, button: ["Ok"], isActionRequired: false)
        } else {
            let viewController = UIStoryboard.instantiateOTPForMailOrPhoneViewController()
            viewController.mobileNumber = txtMobile.text!
            viewController.emailOrNumber = "mobile"
            appDelegate.menuNavController?.pushViewController(viewController, animated: true)
        }
    }
    
    func getUserDetails()  {
        
        let params : [String:Any] = [:]
        
        ProgressHUD.showProgressHUDWithLabel(label: "Loading...")
        
        
        APICall.callGetUserDetailsAPI(params, header: [ : ]) { (sucess, result, error) in
            
            DispatchQueue.main.async {
                
                weak var weakSelf = self
                
                ProgressHUD.hideProgressHUD()
                
                if (error != nil) {
                    
                    weakSelf?.showAlert(title: "Error", message: (error!.localizedDescription), withTag: 101, button: ["Ok"], isActionRequired: false)
                    return
                }
                if sucess {
                    
                    do {
                        
                        let jsonData = try JSONSerialization.jsonObject(with: result!, options: .mutableContainers)
                        print(jsonData)
                        
                        if (jsonData is NSDictionary){
                            
                            if let statusCode = (jsonData as! NSDictionary).object(forKey: "status"), (statusCode is Int) {
                                
                                if (statusCode as! Int) == 200 {
                                    
                                    //sucessfully retrieved data
                                    if let dataDict = (jsonData as! NSDictionary).object(forKey: "data") as? NSDictionary{
                                        
                                        weakSelf?.userDetailDictionary = dataDict
                                        
                                    }else{
                                        weakSelf?.showAlert(title: APP_NAME, message: AlertMessages.NoDataKeyFoundInResponse, withTag: 101, button: ["Ok"], isActionRequired: false)
                                    }
                                    
                                }else{
                                    
                                    var errStr = ""
                                    if let errormessage = (jsonData as? NSDictionary)?.object(forKey:"error") {
                                        if (errormessage is NSDictionary){
                                            if let errorDescription = (errormessage as? NSDictionary)?.object(forKey: "error_description"),!(errorDescription is NSNull){
                                                if (errorDescription is String){
                                                    errStr = errorDescription as! String
                                                    
                                                }
                                            }else {
                                                if let errorMessage = (errormessage as? NSDictionary)?.object(forKey: "message"),!(errorMessage is NSNull){
                                                    if (errorMessage is String){
                                                        errStr = errorMessage as! String
                                                        
                                                    }
                                                }else{
                                                    errStr = "Something went wrong"
                                                }
                                                
                                            }
                                            weakSelf?.showAlert(title: APP_NAME, message:errStr, withTag: 101, button: ["Ok"], isActionRequired: false)
                                        }
                                        
                                    }else{
                                        weakSelf?.showAlert(title: APP_NAME, message: AlertMessages.UnidetifiedErrorMessage, withTag: 101, button: ["Ok"], isActionRequired: false)
                                    }
                                }
                            }
                            
                        }else{
                            
                            //mot a dict
                            weakSelf?.showAlert(title: "sorry", message: AlertMessages.NOTADICTIONARYMESSAGE , withTag: 101, button:  ["Ok"], isActionRequired: false)
                            return
                        }
                    }catch  let error as NSError {
                        print(error.localizedDescription)
                        
                        weakSelf?.showAlert(title: "sorry", message: AlertMessages.MODELERRORMESSAGE , withTag: 101, button:  ["Ok"], isActionRequired: false)
                        return
                    }
                }
                else{
                    //Alert not sucessfull
                    weakSelf?.showAlert(title: APP_NAME, message: "could not get response from server", withTag: 101, button: ["Ok"], isActionRequired: false)
                }
                
                
            }
        }
        
    }
    
    
    @IBAction func updateProfilePicBtnClicked(_ sender: Any) {
        
        let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        actionSheet.addAction(UIAlertAction(title: "Camera", style: .default, handler: { (alert:UIAlertAction!) -> Void in
            self.camera()
        }))
        
        actionSheet.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { (alert:UIAlertAction!) -> Void in
            self.photoLibrary()
        }))
        
        actionSheet.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        
        self.present(actionSheet, animated: true, completion: nil)
    }
    
    func camera()
    {
        if UIImagePickerController.isSourceTypeAvailable(.camera){
            let myPickerController = UIImagePickerController()
            myPickerController.delegate = self as UIImagePickerControllerDelegate & UINavigationControllerDelegate
            myPickerController.sourceType = .camera
            self.present(myPickerController, animated: true, completion: nil)
        }
        
    }
    
    func photoLibrary()
    {
        
        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary){
            let myPickerController = UIImagePickerController()
            myPickerController.delegate = self as UIImagePickerControllerDelegate & UINavigationControllerDelegate
            myPickerController.sourceType = .photoLibrary
            self.present(myPickerController, animated: true, completion: nil)
        }
    }
    
  func updateDetails(){
        
        
        var params : [String:Any] = [:]
        
        if let userInfoDict = UserModel.getLoginInfo() {
            
            if let  userId = (userInfoDict.object(forKey: "ID") as? String){
                
                params["user_id"]  = userId
                
            }else if let userIded = (userInfoDict.object(forKey: "ID") as? Int){
                
                params["user_id"]  = userIded
            }else{
                
                self.showAlert(title: "Sorry", message: "User details could not be found , Please logout and login again", withTag: 101, button: ["Ok"], isActionRequired: false)
                return
            }
        }
        
        /*
         birthday:1990-07-19
         profile_image ////file data
         title:Mr.
         name:guardian
         student_name:Student7
         student_title:Mr.
         student_email:Student7@gmail.com,
         student_mobile:956898101241
         state_id:38
         city_id:504
         country_id:96
         college_id:15
         */
    if userType == "1"{
        
        //student
    }else if  userType == "2" {
        
        //Guardian
        params["student_title"] = txtStudentName.text ?? ""
        params["student_name"] = txtStudentName.text ?? ""
        params["student_title"] = txtTitle.text ?? ""
        params["student_email"] = txtStudentEmailId.text ?? ""
        params["student_mobile"] = txtStudentMobileNumber.text ?? ""
    }
        params["title"] = txtTitle.text ?? ""
        params["name"] = txtFullName.text ?? ""
        params["state_id"] = stateID!
        params["city_id"] = cityID!
        params["country_id"] = countryID!
        params["college_id"] = collegeID!
    
    if isprofileImageChanged == true {
        if imgProfile.image != nil{
            let imageData = UIImagePNGRepresentation(imgProfile.image!)
            
            
            params["profile_image"] = imageData
        }
    }
        print("params : \(params)")
        
        ProgressHUD.showProgressHUDWithLabel(label: "Loading...")
        
        APICall.callUpdateUserProfilePostAPI(params, header: [ : ]) { (sucess, result, error) in
            
            DispatchQueue.main.async {
                
                ProgressHUD.hideProgressHUD()
                
                weak var weakSelf = self
                
                if (error != nil) {
                    weakSelf?.showAlert(title: "Error", message: (error!.localizedDescription), withTag: 101, button: ["Ok"], isActionRequired: false)
                    return
                }
                if sucess {
                    
                    do {
                        let jsonData = try JSONSerialization.jsonObject(with: result!, options: .mutableContainers)
                        
                        if (jsonData is NSDictionary){
                            
                            if let statusCode = (jsonData as! NSDictionary).object(forKey: "status"), (statusCode is Int) {
                                
                                if let success = (jsonData as! NSDictionary).object(forKey: "success") {
                                    
                                    if (success as! Bool) == true {
                                        
                                        if (statusCode as! Int) == 200 {
                                            if let data = (jsonData as! NSDictionary).object(forKey: "data"), (data is NSDictionary) {
                                                if (data as! NSDictionary).count > 0 {
                                                    
                                                    var msg = "Profile updated successfully"
                                                    if let message = (data as! NSDictionary).object(forKey: "message") as? String {
                                                        
                                                        msg = message
                                                    }
                                                    weakSelf?.showAlert(title: APP_NAME, message: msg, withTag: 110, button: ["Ok"], isActionRequired: true)
                                                    self.navigationController?.popViewController(animated: true)
                                                    
                                                }
                                                
                                            }
                                            
                                            
                                        } else {
                                            
                                            //some other status code
                                            //check error message in the response
                                            var errStr = ""
                                            if let errormessage = (jsonData as? NSDictionary)?.object(forKey:"error") {
                                                if (errormessage is NSDictionary){
                                                    if let errorDescription = (errormessage as? NSDictionary)?.object(forKey: "error_description"),!(errorDescription is NSNull){
                                                        if (errorDescription is String){
                                                            errStr = errorDescription as! String
                                                            
                                                        }
                                                    }else {
                                                        if let errorMessage = (errormessage as? NSDictionary)?.object(forKey: "message"),!(errorMessage is NSNull){
                                                            if (errorMessage is String){
                                                                errStr = errorMessage as! String
                                                                
                                                            }
                                                        }else{
                                                            errStr = AlertMessages.UnidetifiedErrorMessage
                                                        }
                                                        
                                                    }
                                                    weakSelf?.showAlert(title: APP_NAME, message:errStr, withTag: 101, button: ["Ok"], isActionRequired: false)
                                                }
                                            }
                                        }
                                    }else{
                                        
                                        //some other status code
                                        //check error message in the response
                                        var errStr = ""
                                        if let errormessage = (jsonData as? NSDictionary)?.object(forKey:"error") {
                                            if (errormessage is NSDictionary){
                                                if let errorDescription = (errormessage as? NSDictionary)?.object(forKey: "error_description"),!(errorDescription is NSNull){
                                                    if (errorDescription is String){
                                                        errStr = errorDescription as! String
                                                        
                                                    }
                                                }else {
                                                    if let errorMessage = (errormessage as? NSDictionary)?.object(forKey: "message"),!(errorMessage is NSNull){
                                                        if (errorMessage is String){
                                                            errStr = errorMessage as! String
                                                            
                                                        }
                                                    }else{
                                                        errStr = AlertMessages.UnidetifiedErrorMessage
                                                    }
                                                    
                                                }
                                                weakSelf?.showAlert(title: APP_NAME, message:errStr, withTag: 101, button: ["Ok"], isActionRequired: false)
                                            }
                                        }
                                    }
                                    
                                }else{
                                    
                                    weakSelf?.showAlert(title: "sorry", message: AlertMessages.NOTADICTIONARYMESSAGE , withTag: 101, button:  ["Ok"], isActionRequired: false)
                                }
                                
                            }
                        }else{
                            // not a dictionary
                            weakSelf?.showAlert(title: "sorry", message: AlertMessages.NOTADICTIONARYMESSAGE , withTag: 101, button:  ["Ok"], isActionRequired: false)
                        }
                    }catch{
                        weakSelf?.showAlert(title: "sorry", message: AlertMessages.MODELERRORMESSAGE , withTag: 101, button:  ["Ok"], isActionRequired: false)
                        return
                    }
                }
                
            }
        }
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        txtFieldPicker = textField

        if textField == txtCountry {
            txtCountry.text = ""
            txtCountry.tag = 0
            txtCity.text = ""
            txtCity.tag = 0
            txtState.text = ""
            txtState.tag = 0
            DispatchQueue.main.async {
                textField.resignFirstResponder()
                
                self.showCountryView(type: 1)
            }
        }
        if textField == txtState {
            
            txtCity.text = ""
            txtCity.tag = 0
            if (txtCountry.text?.isEmpty)! {
                
                self.showAlert(title: APP_NAME, message: "Please select country first", withTag: 100 , button: ["Ok"], isActionRequired: false)
                return
            }
            DispatchQueue.main.async {
                textField.resignFirstResponder()
                
                self.showCountryView(type: 2)
            }
        }
        if textField == txtCity {
            
            txtCollegeName.text = ""
            
            if (txtCountry.text?.isEmpty)! {
                
                self.showAlert(title: APP_NAME, message: "Please select country first", withTag: 100 , button: ["Ok"], isActionRequired: false)
                return
            } else   if (txtState.text?.isEmpty)! {
                
                self.showAlert(title: APP_NAME, message: "Please select state first",withTag: 101, button: ["Ok"], isActionRequired: false)
                return
            }
            
            DispatchQueue.main.async {
                textField.resignFirstResponder()
                
                self.showCountryView(type: 3)
            }
        }
        if textField == txtCollegeName {
            
            DispatchQueue.main.async {
                textField.resignFirstResponder()
                
                if (self.txtCity.text?.isEmpty)! {
                    
                    self.showAlert(title: APP_NAME, message: "Please select city first",withTag: 101, button: ["Ok"], isActionRequired: false)
                    return
                }
                
                self.showCountryView(type: 4)
            }
        }else if textField == txtTitle  || textField == txtStudentTitle {
            self.showPickerView()
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        if textField == txtTitle  {
            
            if textField.text == "" {
                //nothing to do...
            } else {
                
            }
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField == txtMobile{
            let maxLength = 10
            let currentString: NSString = textField.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
        }
        return true
        
    }
    
    // UIPickerView
    private func showPickerView() {
        
        picker = UIPickerView(frame: CGRect(x: 0, y: 0, width: screenWidth, height: 216))
        picker.backgroundColor = .white
        picker.showsSelectionIndicator = true
        picker.delegate = self as UIPickerViewDelegate
        picker.dataSource = self  as UIPickerViewDataSource
        txtFieldPicker.inputView = picker
        
        let toolBar = UIToolbar()
        toolBar.barStyle = UIBarStyle.default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor.black
        toolBar.sizeToFit()
        
        let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItemStyle.plain, target: self, action: #selector(cancelActionPicker))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.plain, target: self, action: #selector(doneActionPicker))
        
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        txtFieldPicker.inputAccessoryView = toolBar
    }
    
    @objc func doneActionPicker (sender:UIBarButtonItem)
    {
        let tempRow = picker.selectedRow(inComponent: 0)
        if tempRow == 0 {
            self.txtFieldPicker.text = arr_Picker[tempRow]
            registrationModel.studentTitle = self.txtFieldPicker.text
        } else {
            registrationModel.studentTitle = self.txtFieldPicker.text
        }
        
        txtFieldPicker.resignFirstResponder()
        
    }
    
    @objc func cancelActionPicker (sender:UIBarButtonItem)
    {
        if txtFieldPicker != txtTitle
        {
            txtFieldPicker.text = ""
        }
        txtFieldPicker.resignFirstResponder()
    }
  
}
extension EditProfileViewController : UIPickerViewDelegate, UIPickerViewDataSource {
    
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return arr_Picker.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return arr_Picker[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        self.txtFieldPicker.text = arr_Picker[row]
    }
}
extension EditProfileViewController : UIImagePickerControllerDelegate ,UINavigationControllerDelegate {
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let image = info[UIImagePickerControllerOriginalImage] as? UIImage {
           
            imgProfile.image = image
            imgProfileLarge.image = image
            isprofileImageChanged = true
        }else{
            print("Something went wrong")
        }
        self.dismiss(animated: true, completion: nil)
    }
}




extension EditProfileViewController : CountryViewDelegate , UITextFieldDelegate {
    
    func otherEntry(){
        
        self.txtManualCollegeName.isHidden = false
        self.txtCollegeName.isHidden = true
        self.txtManualCollegeName.placeholder = "please enter college name"
        
        registrationModel.collegeID = nil
        txtCollegeName.text = ""
        
        self.btnserchCollegeList.isHidden = false
        self.btnserchCollegeList.backgroundColor = .clear
        self.btnserchCollegeList.setTitleColor(.gray, for:  self.btnserchCollegeList.state)
        self.btnserchCollegeList.borderColor = .gray
        
        
        
        self.txtManualCollegeName.isHidden = false
        self.txtCollegeName.isHidden = true
    }
    func showCountryView(type : Int){
        
        countryView = Bundle.main.loadNibNamed("CountryView", owner: self, options: nil)?[0] as? CountryView
        countryView?.delegate = self
        countryView?.registerNibs()
        countryView?.managerRadius()
        countryView?.tag = type
        appDelegate.window?.addSubview(countryView!)
        countryView?.alpha = 0
        countryView?.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height)
        self.countryView?.btnOther.isHidden = true
        
        UIView.animate(withDuration: 0.3, animations: {
            self.countryView?.alpha = 1
        } ,completion: { (done) in
            // self.countryView?.getAllCountries()     switch (self.countryView?.tag){
            switch (self.countryView?.tag){
                
            case 1:   self.countryView?.lblTitle.text = "Select Country"
            self.countryView?.getAllCountries()
                
            case 2: self.countryView?.lblTitle.text = "Select State"
            self.countryView?.getAllStates(countryId:self.countryID!  )
                
            case 3:  self.countryView?.lblTitle.text = "Select City"
            self.countryView?.getAllCities(stateid: self.stateID!)
                
            case 4:  self.countryView?.lblTitle.text = "Select College"
            //its college name
            //For manuall entry purpose show other button
            self.countryView?.btnOther.isHidden = false
            self.countryView?.getAllColleges(cityid: self.cityID!)
            default: print("not correct tag")
                
            }
        } )
    }
    
    
    func removeCountryView()  {
        self.countryView?.alpha = 1
        UIView.animate(withDuration: 0.3, animations: {
            self.countryView?.alpha = 0
        } ,completion: { (done) in
            self.countryView?.removeFromSuperview()
            self.countryView = nil
        } )
    }
    
    func getCountryCode(data: NSDictionary , type :Int) {
        
        if type == 1{
            let tempStr = String(describing: (data.object(forKey: "id")!))
            txtCountry.text = (data.object(forKey: "name")) as? String
            countryID = tempStr
        }else  if type == 2{
            let tempStr = String(describing: (data.object(forKey: "id")!))
            txtState.text = (data.object(forKey: "name")) as? String
            stateID = tempStr
        }else  if type == 3{
            let tempStr = String(describing: (data.object(forKey: "id")!))
            txtCity.text = (data.object(forKey: "name")) as? String
            cityID = tempStr
        }
        else  if type == 4{
            let tempStr = String(describing: (data.object(forKey: "id")!))
            txtCollegeName.text = (data.object(forKey: "name")) as? String
            collegeID = tempStr
        }
    }
    
}

