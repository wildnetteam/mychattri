//
//  SettingTableViewCell.swift
//  MyChhatri
//
//  Created by Arpana on 21/08/18.
//  Copyright © 2018 Arpana. All rights reserved.
//

import UIKit

class SettingTableViewCell: UITableViewCell {

     @IBOutlet  var lblHeading: UILabel!
     @IBOutlet  var imgView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
