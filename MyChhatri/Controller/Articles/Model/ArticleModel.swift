//
//  ArticleModel.swift
//  MyChhatri
//
//  Created by Arpana on 16/08/18.
//  Copyright © 2018 Anshul. All rights reserved.
//

// To parse the JSON, add this file to your project and do:
//
//   let articleModel = try ArticleModel(json)

import Foundation

class ArticleModel: Codable {
    let status: Int?
    let success: Bool?
    let articleError: ArticleError?
    let articleData: ArticleData?
    
    enum CodingKeys: String, CodingKey {
        case status = "status"
        case success = "success"
        case articleError = "error"
        case articleData = "data"
    }
    
    init(status: Int?, success: Bool?, articleError: ArticleError?, articleData: ArticleData?) {
        self.status = status
        self.success = success
        self.articleError = articleError
        self.articleData = articleData
    }
}
class ArticleData: Codable {
    let categoryDetails: CategoryDetails?
    let articlesData: [ArticlesDatum]?
    
    enum CodingKeys: String, CodingKey {
        case categoryDetails = "Category_details"
        case articlesData = "articles_data"
    }
    
    init(categoryDetails: CategoryDetails?, articlesData: [ArticlesDatum]?) {
        self.categoryDetails = categoryDetails
        self.articlesData = articlesData
    }
}

class ArticlesDatum: Codable {
    let title: String?
    let id: Int?
    let description: String?
    let documentsURL: String?
    
    enum CodingKeys: String, CodingKey {
        case title = "title"
        case id = "id"
        case description = "description"
        case documentsURL = "documents_url"
    }
    
    init(title: String?, id: Int?, description: String?, documentsURL: String?) {
        self.title = title
        self.id = id
        self.description = description
        self.documentsURL = documentsURL
    }
}

class CategoryDetails: Codable {
    let id: Int?
    let title: String?
    let streamID: String?
    let captionText: String?
    let description: String?
    
    enum CodingKeys: String, CodingKey {
        case id = "id"
        case title = "title"
        case streamID = "stream_id"
        case captionText = "caption_text"
        case description = "description"
    }
    
    init(id: Int?, title: String?, streamID: String?, captionText: String?, description: String?) {
        self.id = id
        self.title = title
        self.streamID = streamID
        self.captionText = captionText
        self.description = description
    }
}

class ArticleError: Codable {
    let code: Int?
    let message: String?
    let errorDescription: String?
    
    enum CodingKeys: String, CodingKey {
        case code = "code"
        case message = "message"
        case errorDescription = "error_description"
    }
    
    init(code: Int?, message: String?, errorDescription: String?) {
        self.code = code
        self.message = message
        self.errorDescription = errorDescription
    }
}

// MARK: Convenience initializers and mutators

extension ArticleModel {
    convenience init(data: Data) throws {
        let me = try newJSONDecoder().decode(ArticleModel.self, from: data)
        self.init(status: me.status, success: me.success, articleError: me.articleError, articleData: me.articleData)
    }
    
    convenience init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }
    
    convenience init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }
    
    func with(
        status: Int?? = nil,
        success: Bool?? = nil,
        articleError: ArticleError?? = nil,
        articleData: ArticleData?? = nil
        ) -> ArticleModel {
        return ArticleModel(
            status: status ?? self.status,
            success: success ?? self.success,
            articleError: articleError ?? self.articleError,
            articleData: articleData ?? self.articleData
        )
    }
    
    func jsonData() throws -> Data {
        return try newJSONEncoder().encode(self)
    }
    
    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
        return String(data: try self.jsonData(), encoding: encoding)
    }
}

extension ArticleData {
    convenience init(data: Data) throws {
        let me = try newJSONDecoder().decode(ArticleData.self, from: data)
        self.init(categoryDetails: me.categoryDetails, articlesData: me.articlesData)
    }
    
    convenience init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }
    
    convenience init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }
    
    func with(
        categoryDetails: CategoryDetails?? = nil,
        articlesData: [ArticlesDatum]?? = nil
        ) -> ArticleData {
        return ArticleData(
            categoryDetails: categoryDetails ?? self.categoryDetails,
            articlesData: articlesData ?? self.articlesData
        )
    }
    
    func jsonData() throws -> Data {
        return try newJSONEncoder().encode(self)
    }
    
    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
        return String(data: try self.jsonData(), encoding: encoding)
    }
}

extension ArticlesDatum {
    convenience init(data: Data) throws {
        let me = try newJSONDecoder().decode(ArticlesDatum.self, from: data)
        self.init(title: me.title, id: me.id, description: me.description, documentsURL: me.documentsURL)
    }
    
    convenience init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }
    
    convenience init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }
    
    func with(
        title: String?? = nil,
        id: Int?? = nil,
        description: String?? = nil,
        documentsURL: String?? = nil
        ) -> ArticlesDatum {
        return ArticlesDatum(
            title: title ?? self.title,
            id: id ?? self.id,
            description: description ?? self.description,
            documentsURL: documentsURL ?? self.documentsURL
        )
    }
    
    func jsonData() throws -> Data {
        return try newJSONEncoder().encode(self)
    }
    
    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
        return String(data: try self.jsonData(), encoding: encoding)
    }
}

extension CategoryDetails {
    convenience init(data: Data) throws {
        let me = try newJSONDecoder().decode(CategoryDetails.self, from: data)
        self.init(id: me.id, title: me.title, streamID: me.streamID, captionText: me.captionText, description: me.description)
    }
    
    convenience init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }
    
    convenience init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }
    
    func with(
        id: Int?? = nil,
        title: String?? = nil,
        streamID: String?? = nil,
        captionText: String?? = nil,
        description: String?? = nil
        ) -> CategoryDetails {
        return CategoryDetails(
            id: id ?? self.id,
            title: title ?? self.title,
            streamID: streamID ?? self.streamID,
            captionText: captionText ?? self.captionText,
            description: description ?? self.description
        )
    }
    
    func jsonData() throws -> Data {
        return try newJSONEncoder().encode(self)
    }
    
    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
        return String(data: try self.jsonData(), encoding: encoding)
    }
}

extension ArticleError {
    convenience init(data: Data) throws {
        let me = try newJSONDecoder().decode(ArticleError.self, from: data)
        self.init(code: me.code, message: me.message, errorDescription: me.errorDescription)
    }
    
    convenience init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }
    
    convenience init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }
    
    func with(
        code: Int?? = nil,
        message: String?? = nil,
        errorDescription: String?? = nil
        ) -> ArticleError {
        return ArticleError(
            code: code ?? self.code,
            message: message ?? self.message,
            errorDescription: errorDescription ?? self.errorDescription
        )
    }
    
    func jsonData() throws -> Data {
        return try newJSONEncoder().encode(self)
    }
    
    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
        return String(data: try self.jsonData(), encoding: encoding)
    }
}

func newJSONDecoder() -> JSONDecoder {
    let decoder = JSONDecoder()
    if #available(iOS 10.0, OSX 10.12, tvOS 10.0, watchOS 3.0, *) {
        decoder.dateDecodingStrategy = .iso8601
    }
    return decoder
}

func newJSONEncoder() -> JSONEncoder {
    let encoder = JSONEncoder()
    if #available(iOS 10.0, OSX 10.12, tvOS 10.0, watchOS 3.0, *) {
        encoder.dateEncodingStrategy = .iso8601
    }
    return encoder
}

