//
//  ArticlesViewController.swift
//  MyChhatri
//
//  Created by Arpana on 31/07/18.
//  Copyright © 2018 Anshul. All rights reserved.
//

import UIKit

class ArticlesViewController: UIViewController {
    
    @IBOutlet weak var shadowView: UIView!
    @IBOutlet weak var tableView:UITableView!
    
    // MARK: - MAINTAIN PAGINATION AND WEBINAR LIST
    var pageNumber = 0
    var listlimit = 5 // Define to get maximum data one at a time
    var totalCount : Int?
    var offset : Int?

    var articlesArray : Array<NSDictionary>?
    
    var articleObject :ArticleModel?
    var rootCatID : String?
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action:
            #selector(ArticlesViewController.getArticlesByCategory),
                                 for: UIControlEvents.valueChanged)
        refreshControl.tintColor = .black
        
        return refreshControl
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //Add shdow Below page menu
        shadowView.layer.masksToBounds = false
        shadowView.layer.shadowOpacity = 0.4 ;
        shadowView.layer.shadowColor = UIColor.black.cgColor
        shadowView.layer.shadowOffset = CGSize(width: 0.0, height: 4.0)
        tableView.addSubview(refreshControl)
       offset = 0
        getArticlesByCategory()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = false
        if offset == 0 {
            getArticlesByCategory()
            
        }
    }
    
    
    @objc func getArticlesByCategory()  {
        
        let params : [String:Any] = [:]
        
        if tableView != nil {
            
            ProgressHUD.showProgressHUDWithLabel(label: "Loading...")
        }
        
        if self.refreshControl.isRefreshing{
            offset = 0
        }
        
//        let infoDict = NSDictionary(objects: [rootCatID! , listlimit ,offset ?? 0], forKeys: ["category_id" as NSCopying,"limit" as NSCopying ,"offset" as NSCopying])
        
        
        APICall.callArticlesByCategoryAPI(params, header: [ : ], categoryID: rootCatID!) { (sucess, result, error) in
            
            DispatchQueue.main.async {
                
                weak var weakSelf = self
                
                self.refreshControl.endRefreshing()
                
                ProgressHUD.hideProgressHUD()
                
                if (error != nil) {
                    
                    weakSelf?.showAlert(title: "Error", message: (error!.localizedDescription), withTag: 101, button: ["Ok"], isActionRequired: false)
                    return
                }
                if sucess {
                    
                    do {
                        let jsonData = try JSONSerialization.jsonObject(with: result!, options: .mutableContainers)
                        
                        if let responseDict = (jsonData as? NSDictionary){
                            
                            if let statusCode = responseDict.object(forKey: "status"), (statusCode is Int) {
                                
                                if (statusCode as! Int) == 200 {
                                    
                                    //sucessfully retrieved data
                                    if let dataDict = responseDict.object(forKey: "data") as? NSDictionary{
                                        
                                        if let toptalR = dataDict.value(forKey: "totalCount") as? Int{
                                            weakSelf?.totalCount = toptalR
                                        }else if let toptalR = dataDict.value(forKey: "totalCount") as? String{
                                            weakSelf?.totalCount = Int(toptalR)
                                        }
                                        if let tempArray = dataDict.object(forKey: "articles_data") as? Array<NSDictionary> {
                                            
                                            if tempArray.count > 0 {
                                                //store data
                                                if  weakSelf?.offset == 0 {
                                                    self.articlesArray =  Array()
                                                    
                                                    weakSelf?.articlesArray = tempArray
                                                }else{
                                                    //Just add to previous array
                                                    weakSelf?.articlesArray?.append(contentsOf: tempArray)
                                                }
                                            }else{
                                                //No saved stream found message
                                                weakSelf?.showAlert(title: APP_NAME, message: "You do not have any article yet.", withTag: 101, button: ["Ok"], isActionRequired: false)
                                            }
                                        }else {
                                            //
                                            weakSelf?.showAlert(title: APP_NAME, message: "trends key not found", withTag: 101, button: ["Ok"], isActionRequired: false)
                                        }
                                    }else{
                                        //No data found error
                                        weakSelf?.showAlert(title: APP_NAME, message:  AlertMessages.NoDataKeyFoundInResponse, withTag: 101, button: ["Ok"], isActionRequired: false)
                                    }
                                    
                                } else {
                                    var errStr = ""
                                    if let errormessage = (jsonData as? NSDictionary)?.object(forKey:"error") {
                                        if (errormessage is NSDictionary) {
                                            if let errorDescription = (errormessage as? NSDictionary)?.object(forKey: "error_description"),!(errorDescription is NSNull){
                                                if (errorDescription is String){
                                                    errStr = errorDescription as! String
                                                    
                                                }
                                            }else {
                                                if let errorMessage = (errormessage as? NSDictionary)?.object(forKey: "message"),!(errorMessage is NSNull){
                                                    if (errorMessage is String){
                                                        errStr = errorMessage as! String
                                                        
                                                    }
                                                }else{
                                                    errStr = AlertMessages.UnidetifiedErrorMessage
                                                }
                                                
                                            }
                                            weakSelf?.showAlert(title: APP_NAME, message:errStr, withTag: 101, button: ["Ok"], isActionRequired: false)
                                        }
                                        
                                    }else{
                                        weakSelf?.showAlert(title: APP_NAME, message:  AlertMessages.UnidetifiedErrorMessage, withTag: 101, button: ["Ok"], isActionRequired: false)
                                    }
                                    
                                }
                            }
                        }else{
                            
                            weakSelf?.showAlert(title: "sorry", message: AlertMessages.NOTADICTIONARYMESSAGE , withTag: 101, button:  ["Ok"], isActionRequired: false)
                            return
                        }
                        
                        
                        
                    } catch  {
                        
                        weakSelf?.showAlert(title: "sorry", message: AlertMessages.MODELERRORMESSAGE , withTag: 101, button:  ["Ok"], isActionRequired: false)
                        return
                    }
                }
                else{
                    //Alert not sucessfull
                    weakSelf?.showAlert(title: APP_NAME, message: "could not get response from server", withTag: 101, button: ["Ok"], isActionRequired: false)
                }
                
                if self.refreshControl.isRefreshing{
                    self.refreshControl.endRefreshing()
                }
                
                if weakSelf?.tableView != nil {
                    weakSelf?.tableView.reloadData()
                }
            }
        }
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
    extension ArticlesViewController : UITableViewDelegate, UITableViewDataSource , UIViewControllerTransitioningDelegate{
        
        
        func tableView(_ tableView: UITableView, numberOfRowsInSection section:
            Int) -> Int
        {
            if let  array = articlesArray {
                return array.count
                
            }
            return 0
            
        }
        
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
        {
            // Allocates a Table View Cell
            let cell =
                tableView.dequeueReusableCell(withIdentifier: "ArticleTableViewCellID",
                                              for: indexPath) as! ArticleTableViewCell
            
            cell.imgView.layer.masksToBounds = true
            cell.imgView.layer.cornerRadius = cell.imgView.frame.height * 0.063
            cell.selectionStyle = .none
            
            
            if articlesArray != nil {
                
                if let tempTrendsArray = articlesArray {
                    
                    if let dict = tempTrendsArray[indexPath.row] as?  NSDictionary {
                        
                        if let wTitle = dict.value(forKey: "title") as? String {
                            
                            
                            cell.lblTitle.text =  wTitle
                        }
                        if let wDes = dict.value(forKey: "description") as? String {
                            cell.lblSubTitle.text = wDes.withoutHtml
                        }else{
                            cell.lblSubTitle.text = "No descsription available"
                        }
                        
                        
                        var imageFinalUrl =  ""
                        
                        if let imageURL = dict.value(forKey: "thumbnail_image") {
                            
                            imageFinalUrl = "\(URLS.IMAGE_BASE_URL)" + "\(imageURL)"
                            
                            cell.imgView.sd_setIndicatorStyle(.gray)
                            cell.imgView.sd_setShowActivityIndicatorView(true)
                            
                            cell.imgView.sd_setImage(with: URL(string: imageFinalUrl), completed: { (image, error, type, url) in
                                DispatchQueue.main.async {
                                    if error == nil{
                                        cell.imgView.image = image
                                    }else{
                                        cell.imgView.image = #imageLiteral(resourceName: "Placeholder")
                                    }
                                    cell.imgView.sd_setShowActivityIndicatorView(false)
                                }
                            })
                        }
                    }
                    //Reload the next amount of data
               /*     if(indexPath.row ==  (articlesArray?.count)! - 1 && totalCount! > (articlesArray?.count)! ){
                        //we are at last index of currently displayed cell
                        offset = (articlesArray?.count)!
                        //reload more data
                        getArticlesByCategory()
                    }*/
                }
            }
//            if (articleObject?.articleData?.articlesData) != nil {
//
//                if let articleValue = articleObject?.articleData?.articlesData?[indexPath.row] {
//
//                    if let title = articleValue.title{
//
//                        cell.lblTitle.text = title
//
//                    }
//
//
//                    if let description = articleValue.description{
//
//                        cell.lblSubTitle.text =  description.withoutHtml
//
//                    }
//                    cell.imgView.sd_setIndicatorStyle(.gray)
//                    cell.imgView.sd_setShowActivityIndicatorView(true)
//
//                    var imageFinalUrl =  ""
//
//                    if let imageURL = articleValue.documentsURL {
//
//                        imageFinalUrl = "\(URLS.IMAGE_BASE_URL)" + "\(imageURL)"
//
//                        cell.imgView.sd_setImage(with: URL(string: imageFinalUrl), completed: { (image, error, type, url) in
//                            DispatchQueue.main.async {
//                                if error == nil{
//                                    cell.imgView.image = image
//                                }else{
//                                    cell.imgView.image = #imageLiteral(resourceName: "Placeholder")
//
//                                }
//                                cell.imgView.sd_setShowActivityIndicatorView(false)
//                            }
//                        })
//                    }
//                }
//
//            }
            return cell
        }
        
        
        
        func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
        {
            
//            if (articleObject?.articleData?.articlesData?[indexPath.row]) != nil {
//
//                NavigationManager.moveToDetailedViewControllerIfNotExists(kAppDelegate.menuNavController!, detailsArticle:(articleObject?.articleData?.articlesData?[indexPath.row]) , informationDict: nil)
//
//            }
            
            if articlesArray != nil {
                
                if let dict = articlesArray?[indexPath.row] {
                    NavigationManager.moveToDetailedViewControllerIfNotExists(kAppDelegate.menuNavController!, detailsArticle:nil, informationDict: dict )
                }
                
            }
            
        }
        func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
            
            return  100
        }
        
    }

