//
//  GiftViewController.swift
//  MyChhatri
//
//  Created by Arpana on 24/10/18.
//  Copyright © 2018 Arpana. All rights reserved.
//

import UIKit

class GiftViewController: UIViewController {

    var giftArray : Array<NSDictionary>?
    var userAddressArray : Array<NSDictionary>?
    var resultDictionary: NSDictionary?
    var totalPointsEarned : Int = 0
    var addressID : String?
    @IBOutlet weak var totalPoints: UILabel!

    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        getGiftList()
    }

    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(true)
        self.navigationController?.navigationBar.isHidden = false
        kAppDelegate.menuNavController = self.navigationController
        kAppDelegate.setNavigationAndStatusAppearance()
        let statusBar: UIView = UIApplication.shared.value(forKey: "statusBar") as! UIView
        statusBar.backgroundColor = ColorConstants.appBackGroundCOLOR()
        UIApplication.shared.statusBarStyle = .lightContent
        setNeedsStatusBarAppearanceUpdate()
        
    }
    
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    @IBAction func backButtonClicked(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

  
    func getGiftList() {
        
        
        if tableView != nil {
            
            ProgressHUD.showProgressHUDWithLabel(label: "Loading...")
        }
        
        APICall.callGiftListAPI([:], header: [ : ]) { (sucess, result, error) in
            
            DispatchQueue.main.async {
                
                weak var weakSelf = self
                
                ProgressHUD.hideProgressHUD()
                
                if (error != nil) {
                    weakSelf?.showAlert(title: "Error", message: (error!.localizedDescription), withTag: 101, button: ["Ok"], isActionRequired: false)
                    return
                }
                
                if sucess {
                    
                    do {
                        let jsonData = try JSONSerialization.jsonObject(with: result!, options: .mutableContainers)
                        
                        if let responseDict = (jsonData as? NSDictionary){
                            
                            if let statusCode = responseDict.object(forKey: "status"), (statusCode is Int) {
                                
                                if (statusCode as! Int) == 200 {
                                    
                                    //sucessfully retrieved data
                                    if let dataDict = responseDict.value(forKey: "data") as? NSDictionary {
                                        
                                        if let  totalPoints = dataDict.value(forKey: "total_points") as? String {
                                            
                                            weakSelf?.totalPoints.text = totalPoints
                                             weakSelf?.totalPointsEarned = Int(totalPoints)!
                                        }else if let  totalPoints = dataDict.value(forKey: "total_points") as? Int {
                                            weakSelf?.totalPoints.text = String(totalPoints)
                                             weakSelf?.totalPointsEarned = totalPoints

                                        }
                                        
                                        //sucessfully retrieved data
                                        if let tempArray = dataDict.value(forKey: "gifts") as? Array<NSDictionary> {
                                            
                                            if tempArray.count > 0 {
                                                
                                                weakSelf?.giftArray = Array()
                                                weakSelf?.giftArray =  tempArray
                                                
                                            }else{
                                                //No saved stream found message
                                                weakSelf?.showAlert(title: APP_NAME, message: "No Data found.", withTag: 101, button: ["Ok"], isActionRequired: false)
                                            }
                                        }else {
                                            weakSelf?.showAlert(title: APP_NAME, message:  "gift key is not present in response", withTag: 101, button: ["Ok"], isActionRequired: false)
                                        }
                                        
                                        //sucessfully retrieved data
                                        if let tempArray = dataDict.value(forKey: "user_address") as? Array<NSDictionary> {
                                            
                                            if tempArray.count > 0 {
                                                
                                                weakSelf?.userAddressArray = Array()
                                                weakSelf?.userAddressArray =  tempArray
                                                
                                            }
                                        }
                                    }else {
                                        //
                                        weakSelf?.showAlert(title: APP_NAME, message:  AlertMessages.NoDataKeyFoundInResponse, withTag: 101, button: ["Ok"], isActionRequired: false)
                                    }
                                }else{
                                    var errStr = ""
                                    if let errormessage = (jsonData as? NSDictionary)?.object(forKey:"error") {
                                        if (errormessage is NSDictionary){
                                            if let errorDescription = (errormessage as? NSDictionary)?.object(forKey: "error_description"),!(errorDescription is NSNull){
                                                if (errorDescription is String){
                                                    errStr = errorDescription as! String
                                                    
                                                }
                                            }else {
                                                if let errorMessage = (errormessage as? NSDictionary)?.object(forKey: "message"),!(errorMessage is NSNull){
                                                    if (errorMessage is String){
                                                        errStr = errorMessage as! String
                                                        
                                                    }
                                                }else{
                                                    errStr = AlertMessages.UnidetifiedErrorMessage
                                                }
                                                
                                            }
                                            weakSelf?.showAlert(title: APP_NAME, message:errStr, withTag: 101, button: ["Ok"], isActionRequired: false)
                                        }
                                        
                                    }else{
                                        weakSelf?.showAlert(title: APP_NAME, message:  AlertMessages.UnidetifiedErrorMessage, withTag: 101, button: ["Ok"], isActionRequired: false)
                                    }
                                    
                                }
                                
                            } else {
                                var errStr = ""
                                if let errormessage = (jsonData as? NSDictionary)?.object(forKey:"error") {
                                    if (errormessage is NSDictionary){
                                        if let errorDescription = (errormessage as? NSDictionary)?.object(forKey: "error_description"),!(errorDescription is NSNull){
                                            if (errorDescription is String){
                                                errStr = errorDescription as! String
                                                
                                            }
                                        }else {
                                            if let errorMessage = (errormessage as? NSDictionary)?.object(forKey: "message"),!(errorMessage is NSNull){
                                                if (errorMessage is String){
                                                    errStr = errorMessage as! String
                                                    
                                                }
                                            }else{
                                                errStr = AlertMessages.UnidetifiedErrorMessage
                                            }
                                            
                                        }
                                        weakSelf?.showAlert(title: APP_NAME, message:errStr, withTag: 101, button: ["Ok"], isActionRequired: false)
                                    }
                                    
                                }else{
                                    weakSelf?.showAlert(title: APP_NAME, message:  AlertMessages.UnidetifiedErrorMessage, withTag: 101, button: ["Ok"], isActionRequired: false)
                                }
                                
                            }
                            
                        }else{
                            
                            weakSelf?.showAlert(title: "sorry", message: AlertMessages.NOTADICTIONARYMESSAGE , withTag: 101, button:  ["Ok"], isActionRequired: false)
                            return
                        }
                        
                    }catch{
                        
                        weakSelf?.showAlert(title: "sorry", message: AlertMessages.MODELERRORMESSAGE , withTag: 101, button:  ["Ok"], isActionRequired: false)
                        return
                    }
                    
                } else{
                    
                    //Success false
                    weakSelf?.showAlert(title: APP_NAME, message: AlertMessages.UnidetifiedErrorMessage, withTag: 101, button: ["Ok"], isActionRequired: false)
                    
                }
                
                if weakSelf?.tableView != nil {
                    weakSelf?.tableView.reloadData()
                }
            }
            
        }
    }

}

extension GiftViewController : UITableViewDelegate, UITableViewDataSource , UIViewControllerTransitioningDelegate {
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section:
        Int) -> Int
    {
        if let array = giftArray {
            
            return array.count
            
        }
        return 0
    }
    @objc func redeemButtonTapped(sender: UIButton!) {
        
        
        
        if let tempcompanyArray = giftArray {
            
            
            if let dict = tempcompanyArray[sender.tag] as?  NSDictionary {
                
                var giftID = ""
                if let giftId = dict.value(forKey: "id") as? String {
                    giftID =  giftId //indexPath.row
                }else if let giftId = dict.value(forKey: "id") as? Int {
                    giftID = String(giftId)
                }
                
                var pointForGift: Int = 0
                if let pointsFotThisGift = dict.value(forKey: "points") as? String {
                    pointForGift = Int(pointsFotThisGift)!
                }else if let pointsFotThisGift = dict.value(forKey: "points") as? Int {
                    pointForGift = pointsFotThisGift
                }
                if pointForGift > totalPointsEarned {
                    
                    //you do not have sufficient point
                    //No saved stream found message
                    self.showAlert(title: APP_NAME, message: "You do not have sufficient points for this.", withTag: 101, button: ["Ok"], isActionRequired: false)
                    return
                }else{
                    
                    let popOverVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "AddressPopViewControllerID") as! AddressPopViewController
                    self.addChildViewController(popOverVC)
                    popOverVC.view.frame = self.view.frame
                    self.view.addSubview(popOverVC.view)
                    popOverVC.addressArray = userAddressArray
                    popOverVC.giftID = giftID //String(sender.tag)
                    popOverVC.didMove(toParentViewController: self)
                    popOverVC.tableView.reloadData()
                }
                
            }
            
        }
        
        
        //        //1. Create the alert controller.
        //        let alert = UIAlertController(title: APP_NAME, message: "Enter address", preferredStyle: .alert)
        //
        //        //2. Add the text field. You can configure it however you need.
        //        alert.addTextField(configurationHandler: { (textField) -> Void in
        //            //textField.text = "Some default text."
        //        })
        //
        //        //3. Grab the value from the text field, and print it when the user clicks OK.
        //        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { [weak alert] (action) -> Void in
        //            let textField = alert?.textFields![0]
        //            print("Text field: \(textField?.text ?? "")")
        //        }))
        //
        //        // 4. Present the alert.
        //        self.present(alert, animated: true, completion: nil)
    }
    
    
   
        
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        // Allocates a Table View Cell
        let cell =
            tableView.dequeueReusableCell(withIdentifier: "GiftTableViewCellID",
                                          for: indexPath) as! GiftTableViewCell
        
        cell.selectionStyle = UITableViewCellSelectionStyle.none

        cell.redeemBtn.addTarget(self, action: #selector (GiftViewController.redeemButtonTapped), for: .touchUpInside)
        if giftArray != nil {
            
            if let tempcompanyArray = giftArray {
                
                
                if let dict = tempcompanyArray[indexPath.row] as?  NSDictionary {
                    
                    
                     cell.redeemBtn.tag = indexPath.row
//                    if let giftId = dict.value(forKey: "id") as? String {
//                           cell.redeemBtn.tag  =  Int(giftId)! //indexPath.row
//                    }else if let giftId = dict.value(forKey: "id") as? Int {
//                        cell.redeemBtn.tag = giftId
//                    }
                 

                    if let wTitle = dict.value(forKey: "name") as? String {
                        
                        
                        cell.lblTitle.text =  wTitle
                    }
                    if let wDes = dict.value(forKey: "points") as? String {
                        cell.lblSubTitle.text = wDes
                    }else if let wDes = dict.value(forKey: "points") as? Int {
                        cell.lblSubTitle.text = String(wDes)
                    }
                    
                    
                    var imageFinalUrl =  ""
                    
                    if let imageURL = dict.value(forKey: "image") {
                        
                        imageFinalUrl = "\(URLS.IMAGE_BASE_URL)" + "\(imageURL)"
                        
                        cell.imgView.sd_setIndicatorStyle(.gray)
                        cell.imgView.sd_setShowActivityIndicatorView(true)
                        
                        cell.imgView.sd_setImage(with: URL(string: imageFinalUrl), completed: { (image, error, type, url) in
                            DispatchQueue.main.async {
                                if error == nil{
                                    cell.imgView.image = image
                                }else{
                                    cell.imgView.image = #imageLiteral(resourceName: "Placeholder")
                                }
                                cell.imgView.sd_setShowActivityIndicatorView(false)
                            }
                        })
                    }
                }
         
            }
        }
        cell.selectionStyle = .none
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        
    }
}
