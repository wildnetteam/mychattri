//
//  AddressPopViewController.swift
//  MyChhatri
//
//  Created by Arpana on 24/10/18.
//  Copyright © 2018 Arpana. All rights reserved.
//

import UIKit

class AddressPopViewController: UIViewController {
    
    @IBOutlet weak var addressTxtView: UITextView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet  var tableViewHeader: UIView!
    
    var addressArray:Array<NSDictionary>?
    var addressID : String?
    var giftID : String?
    var streamDecription: String = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        //self.view.backgroundColor = UIColor.black.withAlphaComponent(0.2)
        self.showAnimate()
        self.tableView.tableHeaderView = tableViewHeader
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if addressArray?.count == 0 {
            tableView.isHidden = true
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func closePopUp(_ sender: AnyObject) {
        self.removeAnimate()
        //self.view.removeFromSuperview()
    }
    
    @IBAction func manualAddressAdded(_ sender : Any){
        addAddress(address: addressTxtView.text)
    }
    func showAnimate()
    {
        self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
        self.view.alpha = 0.0;
        UIView.animate(withDuration: 0.25, animations: {
            self.view.alpha = 1.0
            self.view.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
        });
    }
    
    func removeAnimate()
    {
        UIView.animate(withDuration: 0.25, animations: {
            self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
            self.view.alpha = 0.0;
        }, completion:{(finished : Bool)  in
            if (finished)
            {
                self.view.removeFromSuperview()
            }
        });
    }
    
    
}

extension AddressPopViewController: UITableViewDelegate , UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    
        
        if let array = addressArray {
           return array.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell = tableView.dequeueReusableCell(withIdentifier: "Cell") as? UITableViewCell
        
        if cell == nil {
            cell = UITableViewCell(style: UITableViewCellStyle.default, reuseIdentifier: "Cell")
        }
        
        cell?.textLabel?.numberOfLines = 0
        cell?.textLabel?.lineBreakMode = .byWordWrapping
        cell?.textLabel?.font = UIFont(name: "Amble-Regular", size: isIphone5 ? 10.0: 15.0)!
        if let tempcompanyArray = addressArray {
            
            
            if let dict = tempcompanyArray[indexPath.row] as?  NSDictionary{
                
                cell!.textLabel?.text =  dict.object(forKey: "address") as? String
            }
        }
        cell?.selectionStyle = .none
        
    
        
        return cell!
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        tableView.separatorInset = UIEdgeInsets.zero
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if let tempcompanyArray = addressArray {
            
            
            if let dict = tempcompanyArray[indexPath.row] as?  NSDictionary{
                
                if let addressSelected = dict.value(forKey: "id") as? String{
                    
                    addressID = addressSelected
                }else  if let addressSelected = dict.value(forKey: "id") as? Int{
                    
                    addressID = String(addressSelected)
                }
                else{
                    self.showAlert(title:APP_NAME, message:"Address id not found", withTag:101, button:["Ok"], isActionRequired:false)
                    return
                }
            }
            requestGift()
        }

    }
    
    
    
    func addAddress(address: String)  {
        
        
        if address.trim().count ==  0 {
            
            self.showAlert(title:APP_NAME, message:"Please enter address", withTag:101, button:["Ok"], isActionRequired:false)
            return
        }
        
        var params : [String:Any] = [:]
        params["address"] = address
        
        ProgressHUD.showProgressHUDWithLabel(label: "Loading...")
        
        APICall.callAddAdressAPI(params, header: [ : ]) { (sucess, result, error) in
            
            DispatchQueue.main.async {
                
                ProgressHUD.hideProgressHUD()
                
                weak var weakSelf = self
                
                if (error != nil) {
                    weakSelf?.showAlert(title: "Error", message: (error!.localizedDescription), withTag: 101, button: ["Ok"], isActionRequired: false)
                    return
                }
                if sucess {
                    
                    do {
                        let jsonData = try JSONSerialization.jsonObject(with: result!, options: .mutableContainers)
                        
                        if (jsonData is NSDictionary){
                            
                            if let statusCode = (jsonData as! NSDictionary).object(forKey: "status"), (statusCode is Int) {
                                
                                if let success = (jsonData as! NSDictionary).object(forKey: "success") {
                                    
                                    if (success as! Bool) == true {
                                        
                                        if (statusCode as! Int) == 200 {
                                            if let dataDict = (jsonData as! NSDictionary).object(forKey: "data") as? NSDictionary {
                                                if (dataDict).count > 0 {
                                                    
                                                    if let savedAddress = dataDict.object(forKey: "address_id") as? String {
                                                        
                                                        weakSelf?.addressID = savedAddress
                                                    }else if let savedAddress = dataDict.object(forKey: "address_id")  as? Int{
                                                        
                                                        weakSelf?.addressID = String(savedAddress)
                                                    }
                                                    
                                                    weakSelf?.requestGift()
                                                }
                                            }
                                            
                                        } else {
                                            
                                            //some other status code
                                            //check error message in the response
                                            var errStr = ""
                                            if let errormessage = (jsonData as? NSDictionary)?.object(forKey:"error") {
                                                if (errormessage is NSDictionary){
                                                    if let errorDescription = (errormessage as? NSDictionary)?.object(forKey: "error_description"),!(errorDescription is NSNull){
                                                        if (errorDescription is String){
                                                            errStr = errorDescription as! String
                                                            
                                                        }
                                                    }else {
                                                        if let errorMessage = (errormessage as? NSDictionary)?.object(forKey: "message"),!(errorMessage is NSNull){
                                                            if (errorMessage is String){
                                                                errStr = errorMessage as! String
                                                                
                                                            }
                                                        }else{
                                                            errStr = AlertMessages.UnidetifiedErrorMessage
                                                        }
                                                        
                                                    }
                                                    weakSelf?.showAlert(title: APP_NAME, message:errStr, withTag: 101, button: ["Ok"], isActionRequired: false)
                                                }
                                            }
                                        }
                                    }else{
                                        
                                        //some other status code
                                        //check error message in the response
                                        var errStr = ""
                                        if let errormessage = (jsonData as? NSDictionary)?.object(forKey:"error") {
                                            if (errormessage is NSDictionary){
                                                if let errorDescription = (errormessage as? NSDictionary)?.object(forKey: "error_description"),!(errorDescription is NSNull){
                                                    if (errorDescription is String){
                                                        errStr = errorDescription as! String
                                                        
                                                    }
                                                }else {
                                                    if let errorMessage = (errormessage as? NSDictionary)?.object(forKey: "message"),!(errorMessage is NSNull){
                                                        if (errorMessage is String){
                                                            errStr = errorMessage as! String
                                                            
                                                        }
                                                    }else{
                                                        errStr = AlertMessages.UnidetifiedErrorMessage
                                                    }
                                                    
                                                }
                                                weakSelf?.showAlert(title: APP_NAME, message:errStr, withTag: 101, button: ["Ok"], isActionRequired: false)
                                            }
                                        }
                                    }
                                    
                                }
                            }
                        }else{
                            // not a dictionary
                            weakSelf?.showAlert(title: "sorry", message: AlertMessages.NOTADICTIONARYMESSAGE , withTag: 101, button:  ["Ok"], isActionRequired: false)
                        }
                        
                    }catch{
                        weakSelf?.showAlert(title: "sorry", message: AlertMessages.MODELERRORMESSAGE , withTag: 101, button:  ["Ok"], isActionRequired: false)
                        return
                    }
                }
                
            }
        }
    }
    func requestGift()  {
        
        guard let addressId = addressID else{
            
            self.showAlert(title:APP_NAME, message:"No address found to claim gift", withTag:101, button:["Ok"], isActionRequired:false)
            return
        }
        
        guard let giftsId = giftID else{
            
            self.showAlert(title:APP_NAME, message:"No  gift Id found", withTag:101, button:["Ok"], isActionRequired:false)
            return
        }
        var params : [String:Any] = [:]
        params["gift_id"] = giftsId
        params["address_id"] = addressId
        
        
        ProgressHUD.showProgressHUDWithLabel(label: "Loading...")
        
        APICall.callGiftRequestAPI(params, header: [ : ]) { (sucess, result, error) in
            
            DispatchQueue.main.async {
                
                ProgressHUD.hideProgressHUD()
                
                weak var weakSelf = self
                
                if (error != nil) {
                    weakSelf?.showAlert(title: "Error", message: (error!.localizedDescription), withTag: 101, button: ["Ok"], isActionRequired: false)
                    return
                }
                if sucess {
                    
                    do {
                        let jsonData = try JSONSerialization.jsonObject(with: result!, options: .mutableContainers)
                        
                        if (jsonData is NSDictionary){
                            
                            if let statusCode = (jsonData as! NSDictionary).object(forKey: "status"), (statusCode is Int) {
                                
                                if let success = (jsonData as! NSDictionary).object(forKey: "success") {
                                    
                                    if (success as! Bool) == true {
                                        
                                        if (statusCode as! Int) == 200 {
                                            
                                            if let data = (jsonData as! NSDictionary).object(forKey: "data"), (data is NSDictionary) {
                                                if (data as! NSDictionary).count > 0 {
                                                    
                                                    
                                                    var msg = "Thanks"
                                                    if let message = (data as! NSDictionary).object(forKey: "message") as? String {
                                                        
                                                        msg = message
                                                    }
                                                    weakSelf?.showAlert(title: APP_NAME, message: msg, withTag: 111, button: ["Ok"], isActionRequired: true)
                                                    
                                                }
                                                
                                            }
                                            
                                            
                                        } else {
                                            
                                            //some other status code
                                            //check error message in the response
                                            var errStr = ""
                                            if let errormessage = (jsonData as? NSDictionary)?.object(forKey:"error") {
                                                if (errormessage is NSDictionary){
                                                    if let errorDescription = (errormessage as? NSDictionary)?.object(forKey: "error_description"),!(errorDescription is NSNull){
                                                        if (errorDescription is String){
                                                            errStr = errorDescription as! String
                                                            
                                                        }
                                                    }else {
                                                        if let errorMessage = (errormessage as? NSDictionary)?.object(forKey: "message"),!(errorMessage is NSNull){
                                                            if (errorMessage is String){
                                                                errStr = errorMessage as! String
                                                                
                                                            }
                                                        }else{
                                                            errStr = AlertMessages.UnidetifiedErrorMessage
                                                        }
                                                        
                                                    }
                                                    weakSelf?.showAlert(title: APP_NAME, message:errStr, withTag: 101, button: ["Ok"], isActionRequired: false)
                                                }
                                            }
                                        }
                                    }else{
                                        
                                        //some other status code
                                        //check error message in the response
                                        var errStr = ""
                                        if let errormessage = (jsonData as? NSDictionary)?.object(forKey:"error") {
                                            if (errormessage is NSDictionary){
                                                if let errorDescription = (errormessage as? NSDictionary)?.object(forKey: "error_description"),!(errorDescription is NSNull){
                                                    if (errorDescription is String){
                                                        errStr = errorDescription as! String
                                                        
                                                    }
                                                }else {
                                                    if let errorMessage = (errormessage as? NSDictionary)?.object(forKey: "message"),!(errorMessage is NSNull){
                                                        if (errorMessage is String){
                                                            errStr = errorMessage as! String
                                                            
                                                        }
                                                    }else{
                                                        errStr = AlertMessages.UnidetifiedErrorMessage
                                                    }
                                                    
                                                }
                                                weakSelf?.showAlert(title: APP_NAME, message:errStr, withTag: 101, button: ["Ok"], isActionRequired: false)
                                            }
                                        }
                                    }
                                    
                                }else{
                                    
                                    weakSelf?.showAlert(title: "sorry", message: AlertMessages.NOTADICTIONARYMESSAGE , withTag: 101, button:  ["Ok"], isActionRequired: false)
                                }
                                
                            }
                        }else{
                            // not a dictionary
                            weakSelf?.showAlert(title: "sorry", message: AlertMessages.NOTADICTIONARYMESSAGE , withTag: 101, button:  ["Ok"], isActionRequired: false)
                        }
                    }
                    catch{
                        weakSelf?.showAlert(title: "sorry", message: AlertMessages.MODELERRORMESSAGE , withTag: 101, button:  ["Ok"], isActionRequired: false)
                        return
                    }
                }
            }
        }
    }
    
    override func alertButtonAction(alertTag: Int, btnTitle: String) {
        
        if alertTag == 111  {
            if btnTitle == "Ok" {
                if let viewControllers = self.navigationController?.viewControllers {
                    
                    for vc: UIViewController? in viewControllers {
                        
                        if (vc is ContestViewController) {
                            if let aVc = vc {
                                
                                //  popedControllerPageIndex = 1
                                self.navigationController?.popToViewController(aVc, animated: false)
                                return
                            }
                        }
                    }
                    self.navigationController?.popToRootViewController(animated: true)
                    
                }
                self.navigationController?.popViewController(animated: true)
                
            }else{
                
            }
        }
    }
}
