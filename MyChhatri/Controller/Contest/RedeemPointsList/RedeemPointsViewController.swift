//
//  RedeemPointsViewController.swift
//  MyChhatri
//
//  Created by Arpana on 24/10/18.
//  Copyright © 2018 Arpana. All rights reserved.
//

import UIKit

class RedeemPointsViewController: UIViewController {
    
        var playedContest : Array<NSDictionary>?
        var resultDictionary: NSDictionary?

        @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var totalPoints: UILabel!
     var totalPointsEarned : String?
     lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action:
            #selector(RedeemPointsViewController.getPlayedContestList),
                                 for: UIControlEvents.valueChanged)
        refreshControl.tintColor = .black
        
        return refreshControl
    }()
        override func viewDidLoad() {
            super.viewDidLoad()
            tableView.addSubview(refreshControl)

            getPlayedContestList()
        }
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(true)
        self.navigationController?.navigationBar.isHidden = false
        kAppDelegate.menuNavController = self.navigationController
        kAppDelegate.setNavigationAndStatusAppearance()
        let statusBar: UIView = UIApplication.shared.value(forKey: "statusBar") as! UIView
        statusBar.backgroundColor = ColorConstants.appBackGroundCOLOR()
        UIApplication.shared.statusBarStyle = .lightContent
        setNeedsStatusBarAppearanceUpdate()

    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    @IBAction func backButtonClicked(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
        
    }
    
    @IBAction func redeemNowClicked(_ sender: Any) {
        NavigationManager.moveToGiftViewControllerIfNotExists(kAppDelegate.menuNavController!, resultDictDetail: nil)
    }
    
    override func didReceiveMemoryWarning() {
            super.didReceiveMemoryWarning()
            // Dispose of any resources that can be recreated.
        }
    @objc func getPlayedContestList() {
            
            
            if tableView != nil {
                
                ProgressHUD.showProgressHUDWithLabel(label: "Loading...")
            }
            
            APICall.callPlayedContestListAPI([:], header: [ : ] ) { (sucess, result, error) in
                
                DispatchQueue.main.async {
                    
                    weak var weakSelf = self
                    
                    ProgressHUD.hideProgressHUD()
                    
                    if (error != nil) {
                        weakSelf?.showAlert(title: "Error", message: (error!.localizedDescription), withTag: 101, button: ["Ok"], isActionRequired: false)
                        return
                    }
                    
                    if sucess {
                        
                        do {
                            let jsonData = try JSONSerialization.jsonObject(with: result!, options: .mutableContainers)
                            
                            if let responseDict = (jsonData as? NSDictionary){
                                
                                if let statusCode = responseDict.object(forKey: "status"), (statusCode is Int) {
                                    
                                    if (statusCode as! Int) == 200 {
                                        
                                        if let dataDict = responseDict.value(forKey: "data") as? NSDictionary {
                                           
                                            if let  totalPoints = dataDict.value(forKey: "total_points") as? String {
                                            
                                                  weakSelf?.totalPoints.text = totalPoints
                                            }else if let  totalPoints = dataDict.value(forKey: "total_points") as? Int {
                                                weakSelf?.totalPoints.text = String(totalPoints)

                                            }
                                          
                                            //sucessfully retrieved data
                                            if let tempArray = dataDict.value(forKey: "played_contests") as? Array<NSDictionary> {
                                                
                                                if tempArray.count > 0 {
                                                    
                                                    weakSelf?.playedContest = Array()
                                                    weakSelf?.playedContest =  tempArray
                                                    
                                                }else{
                                                    //No saved stream found message
                                                    weakSelf?.showAlert(title: APP_NAME, message: "No Data found.", withTag: 101, button: ["Ok"], isActionRequired: false)
                                                }
                                            }else {
                                             weakSelf?.showAlert(title: APP_NAME, message:  "played_contests key is not present in response", withTag: 101, button: ["Ok"], isActionRequired: false)
                                            }
                                        }else {
                                            //
                                            weakSelf?.showAlert(title: APP_NAME, message:  AlertMessages.NoDataKeyFoundInResponse, withTag: 101, button: ["Ok"], isActionRequired: false)
                                        }
                                    }else{
                                        var errStr = ""
                                        if let errormessage = (jsonData as? NSDictionary)?.object(forKey:"error") {
                                            if (errormessage is NSDictionary){
                                                if let errorDescription = (errormessage as? NSDictionary)?.object(forKey: "error_description"),!(errorDescription is NSNull){
                                                    if (errorDescription is String){
                                                        errStr = errorDescription as! String
                                                        
                                                    }
                                                }else {
                                                    if let errorMessage = (errormessage as? NSDictionary)?.object(forKey: "message"),!(errorMessage is NSNull){
                                                        if (errorMessage is String){
                                                            errStr = errorMessage as! String
                                                            
                                                        }
                                                    }else{
                                                        errStr = AlertMessages.UnidetifiedErrorMessage
                                                    }
                                                    
                                                }
                                                weakSelf?.showAlert(title: APP_NAME, message:errStr, withTag: 101, button: ["Ok"], isActionRequired: false)
                                            }
                                            
                                        }else{
                                            weakSelf?.showAlert(title: APP_NAME, message:  AlertMessages.UnidetifiedErrorMessage, withTag: 101, button: ["Ok"], isActionRequired: false)
                                        }
                                        
                                    }
                                    
                                } else {
                                    var errStr = ""
                                    if let errormessage = (jsonData as? NSDictionary)?.object(forKey:"error") {
                                        if (errormessage is NSDictionary){
                                            if let errorDescription = (errormessage as? NSDictionary)?.object(forKey: "error_description"),!(errorDescription is NSNull){
                                                if (errorDescription is String){
                                                    errStr = errorDescription as! String
                                                    
                                                }
                                            }else {
                                                if let errorMessage = (errormessage as? NSDictionary)?.object(forKey: "message"),!(errorMessage is NSNull){
                                                    if (errorMessage is String){
                                                        errStr = errorMessage as! String
                                                        
                                                    }
                                                }else{
                                                    errStr = AlertMessages.UnidetifiedErrorMessage
                                                }
                                                
                                            }
                                            weakSelf?.showAlert(title: APP_NAME, message:errStr, withTag: 101, button: ["Ok"], isActionRequired: false)
                                        }
                                        
                                    }else{
                                        weakSelf?.showAlert(title: APP_NAME, message:  AlertMessages.UnidetifiedErrorMessage, withTag: 101, button: ["Ok"], isActionRequired: false)
                                    }
                                    
                                }
                                
                            }else{
                                
                                weakSelf?.showAlert(title: "sorry", message: AlertMessages.NOTADICTIONARYMESSAGE , withTag: 101, button:  ["Ok"], isActionRequired: false)
                                return
                            }
                            
                        }catch{
                            
                            weakSelf?.showAlert(title: "sorry", message: AlertMessages.MODELERRORMESSAGE , withTag: 101, button:  ["Ok"], isActionRequired: false)
                            return
                        }
                        
                    } else{
                        
                        //Success false
                        weakSelf?.showAlert(title: APP_NAME, message: AlertMessages.UnidetifiedErrorMessage, withTag: 101, button: ["Ok"], isActionRequired: false)
                        
                    }
                    
                    if self.refreshControl.isRefreshing{
                        self.refreshControl.endRefreshing()
                    }
                    if weakSelf?.tableView != nil {
                        weakSelf?.tableView.reloadData()
                    }
                }
                
            }
        }
        
    }
    extension RedeemPointsViewController : UITableViewDelegate, UITableViewDataSource , UIViewControllerTransitioningDelegate {
        
        
        func tableView(_ tableView: UITableView, numberOfRowsInSection section:
            Int) -> Int
        {
            if let array = playedContest {
                
                return array.count
                
            }
            return 0
        }
        
        
        
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
        {
            // Allocates a Table View Cell
            let cell =
                tableView.dequeueReusableCell(withIdentifier: "RedeemPointTableViewCellID",
                                              for: indexPath) as! RedeemPointTableViewCell
            
            cell.selectionStyle = UITableViewCellSelectionStyle.none
            cell.backGroundWhiteView.layer.cornerRadius =  (cell.backGroundWhiteView.frame.size.width * 0.01506)
            cell.backGroundWhiteView.layer.masksToBounds = true
            // cell.shadowView.backgroundColor =  UIColor.white
            cell.shadowView.layer.shadowColor = UIColor(red:0.8, green:0.8, blue:0.8, alpha:0.5).cgColor
            cell.shadowView.layer.shadowOpacity = 1 ;
            cell.shadowView.layer.shadowRadius = 20
            cell.shadowView.layer.shadowOffset = CGSize(width: 0.0, height: 8)
            cell.shadowView.layer.masksToBounds = false
            
            if playedContest != nil {
                
                if let tempcompanyArray = playedContest {
                    
                    if let dict = tempcompanyArray[indexPath.row] as?  NSDictionary {
                        
                        if let contestDict = dict.value(forKey: "contest") as? NSDictionary {
                            
                            if let wTitle = contestDict.value(forKey: "title") as? String {
                                
                                
                                cell.lblContestName.text = wTitle
                            }
                            //for points
                            if let contestIDAsString = contestDict.value(forKey: "id") as? String {
                                
                                
                                cell.lblContestPoints.text = contestIDAsString
                                
                            }else if let contestIDAsString = contestDict.value(forKey: "id") as? Int {
                                
                                cell.lblContestPoints.text = String(contestIDAsString)
                            }
                            if let playedDate = contestDict.value(forKey: "start_date") as? String {
                                
                                cell.lblContestStartTime.text = CalenderStruct.getFormatedDateStringWithouOrdinal(fromDateUTC:  CalenderStruct.getDateFromDateString(forUTC: playedDate, byFormat: "yyyy-MM-dd"), inFormat: "dd MMM yyyy")
                            }
                        }
                        
                    }
                }
            }
            cell.selectionStyle = .none
            return cell
        }
        
        
        func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
        {
 
        }
    }

