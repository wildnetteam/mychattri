//
//  PlayContestTableViewCell.swift
//  MyChhatri
//
//  Created by Arpana on 22/10/18.
//  Copyright © 2018 Arpana. All rights reserved.
//

import UIKit

class PlayContestTableViewCell: UITableViewCell {

   @IBOutlet weak var shadowView: UIView!
    
    @IBOutlet weak var backGroundWhiteView: UIView!
    
    @IBOutlet weak var lblContestName: UILabel!
    
    @IBOutlet weak var lblContestStartTime: UILabel!
    
    //@IBOutlet weak var imgView: UIImageView!
    
    @IBOutlet weak var lblContestEndTime: UILabel!
    
    @IBOutlet weak var lblContestPlayTime: UILabel!
    
    @IBOutlet weak var btnPlay: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
