//
//  ContestResultViewController.swift
//  MyChhatri
//
//  Created by Arpana on 23/10/18.
//  Copyright © 2018 Arpana. All rights reserved.
//

import UIKit

class ContestResultViewController: UIViewController {

    var resultDictionary: NSDictionary?
        @IBOutlet weak var lblContestName: UILabel!
        @IBOutlet weak var lblQuestionTotalCount: UILabel!
        @IBOutlet weak var lblCorrectAnswerTotalCount: UILabel!
        @IBOutlet weak var lblPointEarned: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        kAppDelegate.tabBarController.tabBar.isHidden = false
        setData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setData()  {
        
        if let showDataDict = resultDictionary  {
            
            if let corectAnsCount = showDataDict.value(forKey: "correct_answer") as? Int{
              lblCorrectAnswerTotalCount.text = String(corectAnsCount)
                lblPointEarned.text =  String(corectAnsCount)

            } else  if let corectAnsCount = showDataDict.value(forKey: "correct_answer") as? String{
                lblCorrectAnswerTotalCount.text = corectAnsCount
                lblPointEarned.text = corectAnsCount
            }
            
              if let showDataDetailDict = resultDictionary?.value(forKey: "contest_details") as? NSDictionary {
                
                if showDataDetailDict.count > 0{
                    
                if let totalQues =  showDataDetailDict.value(forKey: "no_of_question") as? Int {
                    
                    lblQuestionTotalCount.text = String(totalQues)

                }else  if let totalQues =  showDataDetailDict.value(forKey: "no_of_question") as? String {
                    
                    lblQuestionTotalCount.text = totalQues
                    
                    }
                }
            }
            
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.navigationBar.isHidden = false
        self.navigationItem.hidesBackButton = true
        
         self.navigationController?.navigationBar.topItem?.title =  "Contest Name"
        kAppDelegate.menuNavController = self.navigationController
        
        kAppDelegate.setNavigationAndStatusAppearance()
        let statusBar: UIView = UIApplication.shared.value(forKey: "statusBar") as! UIView
        statusBar.backgroundColor = ColorConstants.appBackGroundCOLOR()
        setNeedsStatusBarAppearanceUpdate()
    }

}
