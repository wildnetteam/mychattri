//
//  ContestViewController.swift
//  MyChhatri
//
//  Created by Arpana on 12/10/18.
//  Copyright © 2018 Arpana. All rights reserved.
//

import UIKit

class ContestViewController: UIViewController {

    var contestArray : Array<NSDictionary>?
    @IBOutlet weak var tableView: UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()
        getContestList()
    }

    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.navigationItem.leftBarButtonItem = nil
        self.navigationController?.navigationItem.hidesBackButton = true
        let statusBar: UIView = UIApplication.shared.value(forKey: "statusBar") as! UIView
        statusBar.backgroundColor = ColorConstants.appBackGroundCOLOR()
        kAppDelegate.tabBarController.tabBar.isHidden = false

    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func redeemClicked (_ sender: Any){
        
        NavigationManager.moveToRedeemPointsViewControllerIfNotExists(kAppDelegate.menuNavController!, resultDictDetail: nil)
    }
    func getContestList() {

        
        if tableView != nil {
            
            ProgressHUD.showProgressHUDWithLabel(label: "Loading...")
        }
        
        APICall.callRatingQuestionsAPI([:], header: [ : ], typeID: "2" ) { (sucess, result, error) in
            
            DispatchQueue.main.async {
                
                weak var weakSelf = self
                
                ProgressHUD.hideProgressHUD()
                
                if (error != nil) {
                    weakSelf?.showAlert(title: "Error", message: (error!.localizedDescription), withTag: 101, button: ["Ok"], isActionRequired: false)
                    return
                }
                
                if sucess {
                    
                    do {
                        let jsonData = try JSONSerialization.jsonObject(with: result!, options: .mutableContainers)
                        
                        if let responseDict = (jsonData as? NSDictionary){
                            
                            if let statusCode = responseDict.object(forKey: "status"), (statusCode is Int) {
                                
                                if (statusCode as! Int) == 200 {
                                    
                                    //sucessfully retrieved data
                                    if let tempArray = responseDict.object(forKey: "data") as? Array<NSDictionary> {
                                        
                                        if tempArray.count > 0 {
                                            
                                            weakSelf?.contestArray = Array()
                                            weakSelf?.contestArray =  tempArray
                                            
                                        }else{
                                            //No saved stream found message
                                            weakSelf?.showAlert(title: APP_NAME, message: "No Data found.", withTag: 101, button: ["Ok"], isActionRequired: false)
                                        }
                                    }else {
                                        //
                                        weakSelf?.showAlert(title: APP_NAME, message:  AlertMessages.NoDataKeyFoundInResponse, withTag: 101, button: ["Ok"], isActionRequired: false)
                                    }
                                }else{
                                    var errStr = ""
                                    if let errormessage = (jsonData as? NSDictionary)?.object(forKey:"error") {
                                        if (errormessage is NSDictionary){
                                            if let errorDescription = (errormessage as? NSDictionary)?.object(forKey: "error_description"),!(errorDescription is NSNull){
                                                if (errorDescription is String){
                                                    errStr = errorDescription as! String
                                                    
                                                }
                                            }else {
                                                if let errorMessage = (errormessage as? NSDictionary)?.object(forKey: "message"),!(errorMessage is NSNull){
                                                    if (errorMessage is String){
                                                        errStr = errorMessage as! String
                                                        
                                                    }
                                                }else{
                                                    errStr = AlertMessages.UnidetifiedErrorMessage
                                                }
                                                
                                            }
                                            weakSelf?.showAlert(title: APP_NAME, message:errStr, withTag: 101, button: ["Ok"], isActionRequired: false)
                                        }
                                        
                                    }else{
                                        weakSelf?.showAlert(title: APP_NAME, message:  AlertMessages.UnidetifiedErrorMessage, withTag: 101, button: ["Ok"], isActionRequired: false)
                                    }
                                    
                                }
                                
                            } else {
                                var errStr = ""
                                if let errormessage = (jsonData as? NSDictionary)?.object(forKey:"error") {
                                    if (errormessage is NSDictionary){
                                        if let errorDescription = (errormessage as? NSDictionary)?.object(forKey: "error_description"),!(errorDescription is NSNull){
                                            if (errorDescription is String){
                                                errStr = errorDescription as! String
                                                
                                            }
                                        }else {
                                            if let errorMessage = (errormessage as? NSDictionary)?.object(forKey: "message"),!(errorMessage is NSNull){
                                                if (errorMessage is String){
                                                    errStr = errorMessage as! String
                                                    
                                                }
                                            }else{
                                                errStr = AlertMessages.UnidetifiedErrorMessage
                                            }
                                            
                                        }
                                        weakSelf?.showAlert(title: APP_NAME, message:errStr, withTag: 101, button: ["Ok"], isActionRequired: false)
                                    }
                                    
                                }else{
                                    weakSelf?.showAlert(title: APP_NAME, message:  AlertMessages.UnidetifiedErrorMessage, withTag: 101, button: ["Ok"], isActionRequired: false)
                                }
                                
                            }
                            
                        }else{
                            
                            weakSelf?.showAlert(title: "sorry", message: AlertMessages.NOTADICTIONARYMESSAGE , withTag: 101, button:  ["Ok"], isActionRequired: false)
                            return
                        }
                        
                    }catch{
                        
                        weakSelf?.showAlert(title: "sorry", message: AlertMessages.MODELERRORMESSAGE , withTag: 101, button:  ["Ok"], isActionRequired: false)
                        return
                    }
                    
                } else{
                    
                    //Success false
                    weakSelf?.showAlert(title: APP_NAME, message: AlertMessages.UnidetifiedErrorMessage, withTag: 101, button: ["Ok"], isActionRequired: false)
                    
                }
                
                if weakSelf?.tableView != nil {
                    weakSelf?.tableView.reloadData()
                }
            }
            
        }
    }
    
}
    extension ContestViewController : UITableViewDelegate, UITableViewDataSource , UIViewControllerTransitioningDelegate {
        
        
        func tableView(_ tableView: UITableView, numberOfRowsInSection section:
            Int) -> Int
        {
            if let array = contestArray {
                
                return array.count
                
            }
            return 0
        }
        
        
        
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
        {
            // Allocates a Table View Cell
            let cell =
                tableView.dequeueReusableCell(withIdentifier: "PlayContestTableViewCellID",
                                              for: indexPath) as! PlayContestTableViewCell
            
            cell.selectionStyle = UITableViewCellSelectionStyle.none
            cell.backGroundWhiteView.layer.cornerRadius =  (cell.backGroundWhiteView.frame.size.width * 0.01506)
            cell.backGroundWhiteView.layer.masksToBounds = true
            cell.btnPlay.tag = indexPath.row
            // cell.shadowView.backgroundColor =  UIColor.white
            cell.shadowView.layer.shadowColor = UIColor(red:0.8, green:0.8, blue:0.8, alpha:0.5).cgColor
            cell.shadowView.layer.shadowOpacity = 1 ;
            cell.shadowView.layer.shadowRadius = 20
            cell.shadowView.layer.shadowOffset = CGSize(width: 0.0, height: 8)
            cell.shadowView.layer.masksToBounds = false
          
            if contestArray != nil {
                
                if let tempcompanyArray = contestArray {
                    
                    if let dict = tempcompanyArray[indexPath.row] as?  NSDictionary {
                        
                        
                       var playStatus : Int = 0
                        
                        if (dict.value(forKey: "played") as? NSDictionary) != nil{
                            
                            playStatus = 1
                            
                        }
                        
                        if playStatus == 0 {
                            //you can pay the game
                            cell.btnPlay.setTitle("Play", for: .normal)
                        }else{
                            //already played
                            cell.btnPlay.setTitle("Played", for: .normal)
                        }
                        
                        if let contestIDAsString = dict.value(forKey: "id") as? String {
                            
                            
                            cell.btnPlay.tag = Int(contestIDAsString)!
                            
                        }else if let contestIDAsString = dict.value(forKey: "id") as? Int {
                            
                            cell.btnPlay.tag = contestIDAsString
                        }
                        
                        
                        if let wTitle = dict.value(forKey: "title") as? String {
                            
                            
                            cell.lblContestName.text = wTitle
                        }
                        
                        
                        if let startDate = dict.value(forKey: "start_date") as? String {
                            
                           cell.lblContestStartTime.text = CalenderStruct.getFormatedDateStringWithouOrdinal(fromDateUTC:  CalenderStruct.getDateFromDateString(forUTC: startDate, byFormat: "yyyy-MM-dd"), inFormat: "dd MMM yyyy")
                        }
                        
                        if let endDate = dict.value(forKey: "end_date") as? String {
                            
                            cell.lblContestEndTime.text  = CalenderStruct.getFormatedDateStringWithouOrdinal(fromDateUTC:  CalenderStruct.getDateFromDateString(forUTC: endDate, byFormat: "yyyy-MM-dd"), inFormat: "dd MMM yyyy")
                        }
                        
                        
                        if let wDes = dict.value(forKey: "contest_time") as? String {
                            cell.lblContestPlayTime.text = wDes == "" ?"No descsription available":wDes
                        }else  if let wDes = dict.value(forKey: "contest_time") as? Int {
                            cell.lblContestPlayTime.text = "\( String(wDes)) min"
                        }
                    }
                }
            }
            cell.selectionStyle = .none
            return cell
        }
        
        
        func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
        {
            

            let currentCell = tableView.cellForRow(at: indexPath) as! PlayContestTableViewCell
            
            if currentCell.btnPlay.title(for: .normal) == "Played"{
                
                return
            }

            var playTime : Int = 1
            
            if let tempcompanyArray = contestArray {
                
                if let dict = tempcompanyArray[indexPath.row] as?  NSDictionary {
                    if let tm =  dict.value(forKey: "contest_time") as? String{
                        playTime = Int(tm)!
                    }else if let tm =  dict.value(forKey: "contest_time") as? Int{
                        playTime = tm
                    }
                }
            }
            if let Id = currentCell.btnPlay.tag as? Int {
                NavigationManager.moveToContestQuestionsViewControllerIfNotExists(kAppDelegate.menuNavController!, contestId: String(Id) , time : playTime * 60)
            }else{
                
                self.showAlert(title: APP_NAME, message: "Contest ID not found for this", withTag: 101, button: ["OK"], isActionRequired: false)
            }
        }
    }


