//
//  ContestQuestionsViewController.swift
//  MyChhatri
//
//  Created by Arpana on 22/10/18.
//  Copyright © 2018 Arpana. All rights reserved.
//

import UIKit

import SRCountdownTimer

class ContestQuestionsViewController: UIViewController {

    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var contestTimer: SRCountdownTimer!

    var contestID : String?
    var contestTime : Int?
    var questionArray : NSMutableArray?

    override func viewDidLoad() {
        super.viewDidLoad()
        kAppDelegate.tabBarController.tabBar.isHidden = true
         startTimer()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.navigationItem.leftBarButtonItem = nil
        self.navigationItem.leftBarButtonItems = []

        let backButton = UIBarButtonItem(title: "", style: .plain, target: navigationController, action: nil)
        navigationItem.leftBarButtonItem = backButton
        
        
        kAppDelegate.tabBarController?.navigationItem.hidesBackButton = true
        self.navigationController?.navigationItem.hidesBackButton = true
        kAppDelegate.menuNavController = self.navigationController
        let statusBar: UIView = UIApplication.shared.value(forKey: "statusBar") as! UIView
        statusBar.backgroundColor = ColorConstants.appBackGroundCOLOR()
        getQuestionList()

    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        contestTimer.end()
    }

    func startTimer(){
        contestTimer.labelTextColor  = UIColor.init(red: 0.990, green: 0.610, blue: 0.220, alpha: 1)
        contestTimer.lineColor  = UIColor.init(red: 0.990, green: 0.610, blue: 0.220, alpha: 1)
        contestTimer.lineWidth = 2.0
        contestTimer.useMinutesAndSecondsRepresentation = true
        contestTimer.start(beginingValue: contestTime! , interval: 1)
        contestTimer.isHidden = false
        contestTimer.delegate = self
    }

    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    

     func getQuestionList()  {
        
                guard (contestID) != nil else{
        
                    self.showAlert(title: "Sorry", message: "Contest ID is not available please try again", withTag: 101, button: ["Ok"], isActionRequired: false)
                    return
                }
        
        
        if collectionView != nil {
            
            ProgressHUD.showProgressHUDWithLabel(label: "Loading...")
        }
        
        APICall.callContestQuestionListQuestionsAPI([:], header: [ : ], contestID: contestID! ) { (sucess, result, error) in
            
            DispatchQueue.main.async {
                
                weak var weakSelf = self
                
                ProgressHUD.hideProgressHUD()
                
                if (error != nil) {
                    weakSelf?.showAlert(title: "Error", message: (error!.localizedDescription), withTag: 101, button: ["Ok"], isActionRequired: false)
                    return
                }
                
                if sucess {
                    
                    do {
                        let jsonData = try JSONSerialization.jsonObject(with: result!, options: .mutableContainers)
                        
                        if let responseDict = (jsonData as? NSDictionary){
                            
                            if let statusCode = responseDict.object(forKey: "status"), (statusCode is Int) {
                                
                                if (statusCode as! Int) == 200 {
                                    
                                    //sucessfully retrieved data
                                    if let tempDict = responseDict.object(forKey: "data") as? NSDictionary {
                                        
                                        if let tempArray = tempDict.object(forKey: "questions") as? NSArray {
                                            
                                            if tempArray.count > 0 {
                                                
                                                weakSelf?.questionArray = NSMutableArray()
                                                weakSelf?.questionArray =  tempArray as? NSMutableArray
                                                
                                            }else{
                                                weakSelf?.showAlert(title: APP_NAME, message:  "No question found ", withTag: 101, button: ["Ok"], isActionRequired: false)
                                            }
                                        }else {
                                            //
                                            weakSelf?.showAlert(title: APP_NAME, message: "questions key not found", withTag: 101, button: ["Ok"], isActionRequired: false)
                                        }
                                    }
                                    else{
                                        //No data found error
                                        weakSelf?.showAlert(title: APP_NAME, message:  AlertMessages.NoDataKeyFoundInResponse, withTag: 101, button: ["Ok"], isActionRequired: false)
                                    }
                                }else {
                                    var errStr = ""
                                    if let errormessage = (jsonData as? NSDictionary)?.object(forKey:"error") {
                                        if (errormessage is NSDictionary){
                                            if let errorDescription = (errormessage as? NSDictionary)?.object(forKey: "error_description"),!(errorDescription is NSNull){
                                                if (errorDescription is String){
                                                    errStr = errorDescription as! String
                                                    
                                                }
                                            }else {
                                                if let errorMessage = (errormessage as? NSDictionary)?.object(forKey: "message"),!(errorMessage is NSNull){
                                                    if (errorMessage is String){
                                                        errStr = errorMessage as! String
                                                        
                                                    }
                                                }else{
                                                    errStr = AlertMessages.UnidetifiedErrorMessage
                                                }
                                                
                                            }
                                            weakSelf?.showAlert(title: APP_NAME, message:errStr, withTag: 101, button: ["Ok"], isActionRequired: false)
                                        }
                                        
                                    }else{
                                        weakSelf?.showAlert(title: APP_NAME, message:  AlertMessages.UnidetifiedErrorMessage, withTag: 101, button: ["Ok"], isActionRequired: false)
                                    }
                                    
                                }
                                
                            } else {
                                var errStr = ""
                                if let errormessage = (jsonData as? NSDictionary)?.object(forKey:"error") {
                                    if (errormessage is NSDictionary){
                                        if let errorDescription = (errormessage as? NSDictionary)?.object(forKey: "error_description"),!(errorDescription is NSNull){
                                            if (errorDescription is String){
                                                errStr = errorDescription as! String
                                                
                                            }
                                        }else {
                                            if let errorMessage = (errormessage as? NSDictionary)?.object(forKey: "message"),!(errorMessage is NSNull){
                                                if (errorMessage is String){
                                                    errStr = errorMessage as! String
                                                    
                                                }
                                            }else{
                                                errStr = AlertMessages.UnidetifiedErrorMessage
                                            }
                                            
                                        }
                                        weakSelf?.showAlert(title: APP_NAME, message:errStr, withTag: 101, button: ["Ok"], isActionRequired: false)
                                    }
                                    
                                }else{
                                    weakSelf?.showAlert(title: APP_NAME, message:  AlertMessages.UnidetifiedErrorMessage, withTag: 101, button: ["Ok"], isActionRequired: false)
                                }
                                
                            }
                            
                        }else{
                            
                            weakSelf?.showAlert(title: "sorry", message: AlertMessages.NOTADICTIONARYMESSAGE , withTag: 101, button:  ["Ok"], isActionRequired: false)
                            return
                        }
                        
                    }catch{
                        
                        weakSelf?.showAlert(title: "sorry", message: AlertMessages.MODELERRORMESSAGE , withTag: 101, button:  ["Ok"], isActionRequired: false)
                        return
                    }
                    
                } else{
                    
                    //Success false
                    weakSelf?.showAlert(title: APP_NAME, message: AlertMessages.UnidetifiedErrorMessage, withTag: 101, button: ["Ok"], isActionRequired: false)
                    
                }
                
                if weakSelf?.collectionView != nil {
                    weakSelf?.collectionView.reloadData()
                }
            }
            
        }
    }
    
    @IBAction func backButtonClicked(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
        
    }
    
    func submitContest(){
        
        var params : [String:Any] = [:]
        
        guard let orderNo = contestID else{
            
            self.showAlert(title: "Error", message: "Contest ID not found.", withTag: 101, button: ["Ok"], isActionRequired: false)
            return
        }
        params["contest_id"] =  orderNo
//      /  params["questions"] = questionArray!
/*
         
         contest_id:1
         questions[0][id]:1
         questions[0][answer]:option_3
         questions[1][id]:2
         questions[1][answer]:option_2
 */
        for i in 0..<(self.questionArray?.count)! {
            let dic = self.questionArray?.object(at: i)as?  NSDictionary

            if dic?.value(forKey: "answer") != nil {
            params["questions[\(i)][id]"] = dic?.value(forKey: "id")
            params["questions[\(i)][answer]"] = dic?.value(forKey: "answer")
            }

        }
        
        print("Param is :\(params)")
        
        
        
//        let tempDict = NSDictionary(object: params, forKey: "data" as NSCopying)
      //  print("params to send : \(params)")
        

        ProgressHUD.showProgressHUDWithLabel(label: "Loading...")
        
        APICall.submitContestAPI(params, header: [ : ]) { (sucess, result, error) in

            DispatchQueue.main.async {

                ProgressHUD.hideProgressHUD()

                weak var weakSelf = self

                if (error != nil) {
                    weakSelf?.showAlert(title: "Error", message: (error!.localizedDescription), withTag: 101, button: ["Ok"], isActionRequired: false)
                    return
                }
                
                if sucess {

                    do {
                        let jsonData = try JSONSerialization.jsonObject(with: result!, options: .mutableContainers)

                        if (jsonData is NSDictionary){

                            if let statusCode = (jsonData as! NSDictionary).object(forKey: "status"), (statusCode is Int) {

                                if let success = (jsonData as! NSDictionary).object(forKey: "success") {

                                    if (success as! Bool) == true {

                                        if (statusCode as! Int) == 200 {

                                            if let data = (jsonData as! NSDictionary).object(forKey: "data"), (data is NSDictionary) {
                                                if (data as! NSDictionary).count > 0 {
                                                    NavigationManager.moveToContestResultViewControllerIfNotExists(kAppDelegate.menuNavController!, resultDictDetail: data as? NSDictionary)

                                                }

                                            }


                                        } else {

                                            //some other status code
                                            //check error message in the response
                                            var errStr = ""
                                            if let errormessage = (jsonData as? NSDictionary)?.object(forKey:"error") {
                                                if (errormessage is NSDictionary){
                                                    if let errorDescription = (errormessage as? NSDictionary)?.object(forKey: "error_description"),!(errorDescription is NSNull){
                                                        if (errorDescription is String){
                                                            errStr = errorDescription as! String

                                                        }
                                                    }else {
                                                        if let errorMessage = (errormessage as? NSDictionary)?.object(forKey: "message"),!(errorMessage is NSNull){
                                                            if (errorMessage is String){
                                                                errStr = errorMessage as! String

                                                            }
                                                        }else{
                                                            errStr = AlertMessages.UnidetifiedErrorMessage
                                                        }

                                                    }
                                                    weakSelf?.showAlert(title: APP_NAME, message:errStr, withTag: 101, button: ["Ok"], isActionRequired: false)
                                                }
                                            }
                                        }
                                    }else{

                                        //some other status code
                                        //check error message in the response
                                        var errStr = ""
                                        if let errormessage = (jsonData as? NSDictionary)?.object(forKey:"error") {
                                            if (errormessage is NSDictionary){
                                                if let errorDescription = (errormessage as? NSDictionary)?.object(forKey: "error_description"),!(errorDescription is NSNull){
                                                    if (errorDescription is String){
                                                        errStr = errorDescription as! String

                                                    }
                                                }else {
                                                    if let errorMessage = (errormessage as? NSDictionary)?.object(forKey: "message"),!(errorMessage is NSNull){
                                                        if (errorMessage is String){
                                                            errStr = errorMessage as! String

                                                        }
                                                    }else{
                                                        errStr = AlertMessages.UnidetifiedErrorMessage
                                                    }

                                                }
                                                weakSelf?.showAlert(title: APP_NAME, message:errStr, withTag: 101, button: ["Ok"], isActionRequired: false)
                                            }
                                        }
                                    }

                                }else{

                                    weakSelf?.showAlert(title: "sorry", message: AlertMessages.NOTADICTIONARYMESSAGE , withTag: 101, button:  ["Ok"], isActionRequired: false)
                                }

                            }
                        }else{
                            // not a dictionary
                            weakSelf?.showAlert(title: "sorry", message: AlertMessages.NOTADICTIONARYMESSAGE , withTag: 101, button:  ["Ok"], isActionRequired: false)
                        }
                    }catch{
                        weakSelf?.showAlert(title: "sorry", message: AlertMessages.MODELERRORMESSAGE , withTag: 101, button:  ["Ok"], isActionRequired: false)
                        return
                    }
                }

            }
        }
    }

}
extension ContestQuestionsViewController : SRCountdownTimerDelegate {
    
    //Timer Delegate
    func timerDidEnd() {

        contestTimer.isHidden = true
        submitContest()
    }
}
extension ContestQuestionsViewController : UICollectionViewDelegateFlowLayout {
    //1
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        //2.4
        let   size   =  CGSize(width: screenWidth , height: collectionView.frame.size.height)
        
        return size
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat{
        
        return 0.5
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat{
        return 0.5
    }
    
    
}
extension ContestQuestionsViewController: UICollectionViewDelegate , UICollectionViewDataSource , actionCompletedDelegate {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if let array = questionArray {
            return array.count
        }
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ContestQuestionCollectionViewCellID", for: indexPath) as! ContestQuestionCollectionViewCell
        
        
        cell.updateConstraintsIfNeeded()
        cell.layoutIfNeeded()
        
        cell.btnPrevious.tag = indexPath.row
        cell.btnNext.tag = indexPath.row

        cell.btnPrevious.addTarget(self, action: #selector (ContestQuestionsViewController.previousButtonTapped), for: .touchUpInside)
        cell.btnNext.addTarget(self, action: #selector (ContestQuestionsViewController.nextButtonTapped ), for: .touchUpInside)
        
        
        if indexPath.row == 0 {
            cell.btnPrevious.isHidden = true
        }else{
            cell.btnPrevious.isHidden = false
        }
        
        if indexPath.row == (questionArray?.count)! - 1{
            cell.btnNext.setTitle("Finish", for: .normal)
        }else{
            cell.btnNext.setTitle("Next", for: .normal)
        }
       // cell.updateUIOnDoneDelegate = self
        if let tempbatchArray = questionArray {
            
            if let dict = tempbatchArray[indexPath.row] as?  NSDictionary {
                cell.setCellData(dict: dict)
            }
            
        }
        return cell
    }
    
    func updateUI(_ i: Int){
        
        self.navigationController?.popToRootViewController(animated: true)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        
    }
    
    @objc func previousButtonTapped(sender: UIButton!) {
        if sender.tag == 0  {
            //first element in array
            //do nothing
        }else{
            // scroll to  previous item
            collectionView.scrollToItem(at: IndexPath(row: sender.tag - 1, section: 0), at: .left, animated: true)
        }
        
        
    }
    
    @objc func nextButtonTapped(sender: UIButton!) {
        
        
        //get the dictionary at given index
        //update its answer value with selected option
        
        var updatedDict : NSMutableDictionary?
        
        if let questionDict = questionArray?[sender.tag] as? NSDictionary{
            
            let currentCell = collectionView.cellForItem(at: IndexPath(row: sender.tag , section: 0)) as! ContestQuestionCollectionViewCell
            var answer: String?
            for element in currentCell.backGroundWhiteView.subviews {
                if element.isKind(of: UIButton.self){
                    let btn :  UIButton = element as! UIButton
                    if btn.isSelected == true{
                        answer = "option_" + String(btn.tag)
                        //add this option as answer in current dictionary
                        break
                    }
                }
            }
            updatedDict = NSMutableDictionary(dictionary: questionDict)

            if let optionSelected = answer {
                updatedDict?.setObject(optionSelected, forKey: "answer" as NSCopying)
            }
            
        }
        var idToRemove = ""
        
        if let idAsString = (updatedDict?.value(forKey: "id")) as? String   {
           idToRemove = idAsString
        }else  if let idAsInt = (updatedDict?.value(forKey: "id")) as? Int   {
            idToRemove = String(idAsInt)
        }
        let questionIdPredicate = NSPredicate(format: "id = \(idToRemove)")
        
        let resultArray = (questionArray)?.filtered(using: questionIdPredicate)
        
        if (resultArray?.count)! > 0{
            
            let dictToRemove = resultArray?.first as? NSDictionary
            //This questionId is already added to the array, remove first
            questionArray?.remove(dictToRemove!)
            
        }
        questionArray?.insert(updatedDict, at: sender.tag)
        
        if  sender.title(for: .normal) == "Finish" {
            //submit the answer
            
            self.showAlert(title: APP_NAME, message: "Do you want to submit contest", withTag: 101, button: ["Ok"], isActionRequired: true)
            return
        }
        else {
            
            if sender.tag == (questionArray?.count)! - 1  {
                //last element in array
                //do nothing
            }else{
                // scroll to  previous item
                collectionView.scrollToItem(at: IndexPath(row: sender.tag + 1, section: 0), at: .right, animated: true)
            }
        }
    }
    
    override func alertButtonAction(alertTag: Int, btnTitle: String) {

        if alertTag == 101  {
            if btnTitle == "Ok" {
                contestTimer.pause()
                submitContest()
                
            }
        }
    }
    
}
