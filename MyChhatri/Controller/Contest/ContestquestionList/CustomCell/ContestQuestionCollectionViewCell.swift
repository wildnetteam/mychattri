//
//  ContestQuestionCollectionViewCell.swift
//  MyChhatri
//
//  Created by Arpana on 22/10/18.
//  Copyright © 2018 Arpana. All rights reserved.
//

import UIKit

class ContestQuestionCollectionViewCell: UICollectionViewCell {
    
    
    @IBOutlet weak var backGroundWhiteView: UIView!
    
    @IBOutlet weak var lblQuestion: UILabel!
    
    @IBOutlet weak var lbloption1: UILabel!
    @IBOutlet weak var lbloption2: UILabel!
    @IBOutlet weak var lbloption3: UILabel!
    @IBOutlet weak var lbloption4: UILabel!
    
    @IBOutlet weak var btnoption1: UIButton!
    @IBOutlet weak var btnoption2: UIButton!
    @IBOutlet weak var btnoption3: UIButton!
    @IBOutlet weak var btnoption4: UIButton!

    @IBOutlet weak var btnPrevious: UIButton!
    @IBOutlet weak var btnNext: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    @IBAction func optionBtnClicked (_ sender : UIButton){
        
            for element in self.backGroundWhiteView.subviews {
                if element.isKind(of: UIButton.self){
                    let btn :  UIButton = element as! UIButton
                    
                    if(element.tag == sender.tag){
                        sender.isSelected = true
                        sender.setImage(#imageLiteral(resourceName: "Group"), for: .selected)
                    }
                    else{
                      //  if(btn.isSelected == true){
                        btn.isSelected = false
                        sender.setImage(#imageLiteral(resourceName: "Rectangle"), for: .normal)


                       // }
                    }
                }
            }
        }
    
    @IBAction func previousBtnClicked (_ sender : Any){

    }

    func  setCellData(dict : NSDictionary)  {
        
        if let title = dict.value(forKey: "question") as? String{
            lblQuestion.text = title
        }
        if let title = dict.value(forKey: "option_1") as? String{
            lbloption1.text = title
        }
        if let title = dict.value(forKey: "option_2") as? String{
            lbloption2.text = title
        }
        if let title = dict.value(forKey: "option_3") as? String{
            lbloption3.text = title
        }
        if let title = dict.value(forKey: "option_4") as? String{
            lbloption4.text = title
        }
    }
    
}
