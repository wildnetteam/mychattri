//
//  LoginModel.swift
//  MyChhatri
//
//  Created by Anshul on 19/07/18.
//  Copyright © 2018 Anshul. All rights reserved.
//

// To parse the JSON, add this file to your project and do:
//
//   let loginModel = try? JSONDecoder().decode(LoginModel.self, from: jsonData)


import Foundation

class LoginModel: NSObject, Codable  {
    
    var status: Int?
    var success: Bool?
    var data: DataClass?
    var error: ErrorLogin?
    var user_id: Int?
    var mobile: String?
    init(status: Int?, success: Bool?, data: DataClass? , error: ErrorLogin? , user_id: Int? , mobile: String? ) {
        self.status = status
        self.success = success
        self.data = data
        self.error = error
        self.user_id = user_id
        self.mobile = mobile
    }
    
    init( data: DataClass? ) {
        self.data = data
    }
}



class Token:  NSObject, Codable {
    
    let tokenType: String?
    let expiresIn: Int?
    let accessToken, refreshToken: String?
    
    enum CodingKeys: String, CodingKey {
        case tokenType = "token_type"
        case expiresIn = "expires_in"
        case accessToken = "access_token"
        case refreshToken = "refresh_token"
    }
    
    init(tokenType: String?, expiresIn: Int?, accessToken: String?, refreshToken: String?) {
        self.tokenType = tokenType
        self.expiresIn = expiresIn
        self.accessToken = accessToken
        self.refreshToken = refreshToken
    }
}

@objc class DataClass: NSObject ,Codable {
    let token: Token?
    let user: UserDetails?
    
    enum CodingKeys: String, CodingKey {
        case token = "token"
        case user = "user"
    }
    init(token: Token?, user: UserDetails?) {
        self.token = token
        self.user = user
    }
    func encode(with aCoder: NSCoder) {
        aCoder.encode(token, forKey: "token")
        aCoder.encode(user, forKey: "user")
    }
}

class ErrorLogin:  NSObject, Codable {
    let code: Int?
    let message, errorDescription: String?
    
    enum CodingKeys: String, CodingKey {
        case code, message
        case errorDescription = "error_description"
    }
    init(code: Int?, message: String?, errorDescription: String?) {
        self.code = code
        self.message = message
        self.errorDescription = errorDescription
    }
    
}

class UserDetails:  NSObject, Codable {
    
    let id: Int?
    let name: String?
    let email: String?
    let mobile: String?
    let role: Int?
    let institute_type : String?
    let is_verified : String?

    enum CodingKeys: String, CodingKey {
        case id = "id"
        case name = "name"
        case email = "email"
        case mobile = "mobile"
        case role = "role"
        case institute_type = "institute_type"
        case is_verified = "is_verified"
    }
    
    init(id: Int?, name: String?, email: String?, mobile: String?, role: Int? ,  institute_type: String? ,  is_verified: String?) {
        self.id = id
        self.name = name
        self.email = email
        self.mobile = mobile
        self.role = role
        self.institute_type = institute_type
        self.is_verified = is_verified
    }
}


// MARK: Convenience initializers and mutators

extension LoginModel {
    convenience init(data: Data) throws {
        let me = try JSONDecoder().decode(LoginModel.self, from: data)
        self.init(status: me.status, success: me.success, data: me.data, error: me.error , user_id: me.user_id , mobile: me.mobile ) //, institute_type: me.institute_type  , is_verified: me.is_verified
    }
    
    convenience init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }
    
    convenience init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }
    
    func with(
        status: Int?? = nil,
        success: Bool?? = nil,
        data: DataClass?? = nil,
        error: ErrorLogin?? = nil,
        userId: Int?? = nil,
        mobile:String?? = nil
        ) -> LoginModel {
        return LoginModel(
            status: status ?? self.status,
            success: success ?? self.success,
            data: data ?? self.data,
            error: error ?? self.error ,
            user_id: userId ?? self.user_id ,
            mobile: mobile ?? self.mobile

        )
    }
    
    func jsonData() throws -> Data {
        return try JSONEncoder().encode(self)
    }
    
    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
        return String(data: try self.jsonData(), encoding: encoding)
    }
}

extension DataClass {
    
    convenience init(data: Data) throws {
        let me = try JSONDecoder().decode(DataClass.self, from: data)
        self.init(token: me.token, user: me.user)
    }
    
    convenience init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }
    
    convenience init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }
    
    func with(
        token: Token?? = nil,
        user: UserDetails?? = nil
        ) -> DataClass {
        return DataClass(
            token: token ?? self.token,
            user: user ?? self.user
        )
    }
    
    func jsonData() throws -> Data {
        return try JSONEncoder().encode(self)
    }
    
    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
        return String(data: try self.jsonData(), encoding: encoding)
    }
}

extension Token {
    convenience init(data: Data) throws {
        let me = try JSONDecoder().decode(Token.self, from: data)
        self.init(tokenType: me.tokenType, expiresIn: me.expiresIn, accessToken: me.accessToken, refreshToken: me.refreshToken)
    }
    
    convenience init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }
    
    convenience init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }
    
    func with(
        tokenType: String?? = nil,
        expiresIn: Int?? = nil,
        accessToken: String?? = nil,
        refreshToken: String?? = nil
        ) -> Token {
        return Token(
            tokenType: tokenType ?? self.tokenType,
            expiresIn: expiresIn ?? self.expiresIn,
            accessToken: accessToken ?? self.accessToken,
            refreshToken: refreshToken ?? self.refreshToken
        )
    }
    
    func jsonData() throws -> Data {
        return try JSONEncoder().encode(self)
    }
    
    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
        return String(data: try self.jsonData(), encoding: encoding)
    }
}

extension UserDetails {
    convenience init(data: Data) throws {
        let me = try JSONDecoder().decode(UserDetails.self, from: data)
        self.init(id: me.id, name: me.name, email: me.email, mobile: me.mobile, role: me.role , institute_type: me.institute_type  , is_verified: me.is_verified )
    }
    
    convenience init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }
    
    convenience init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }
    
    func with(
        id: Int?? = nil,
        name: String?? = nil,
        email: String?? = nil,
        mobile: String?? = nil,
        role: Int?? = nil,
        institute_type: String?? = nil,
        is_verified: String?? = nil
        ) -> UserDetails {
        return UserDetails(
            id: id ?? self.id,
            name: name ?? self.name,
            email: email ?? self.email,
            mobile: mobile ?? self.mobile,
            role: role ?? self.role,
            institute_type: institute_type ?? self.institute_type,
            is_verified: is_verified ?? self.is_verified
            
        )
    }
    
    func jsonData() throws -> Data {
        return try JSONEncoder().encode(self)
    }
    
    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
        return String(data: try self.jsonData(), encoding: encoding)
    }
}

extension ErrorLogin {
    convenience init(data: Data) throws {
        let me = try JSONDecoder().decode(ErrorLogin.self, from: data)
        self.init(code: me.code, message: me.message, errorDescription: me.errorDescription)
    }
    
    convenience init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }
    
    convenience init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }
    
    func with(
        code: Int?? = nil,
        message: String?? = nil,
        errorDescription: String?? = nil
        ) -> ErrorLogin {
        return ErrorLogin(
            code: code ?? self.code,
            message: message ?? self.message,
            errorDescription: errorDescription ?? self.errorDescription
        )
    }
    
    func jsonData() throws -> Data {
        return try JSONEncoder().encode(self)
    }
    
    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
        return String(data: try self.jsonData(), encoding: encoding)
    }
}


