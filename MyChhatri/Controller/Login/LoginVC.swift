//
//  LoginVC.swift
//  MyChhatri
//
//  Created by Anshul on 06/07/18.
//  Copyright © 2018 Anshul. All rights reserved.
//

import UIKit
import Crashlytics

class LoginVC: UIViewController {

    @IBOutlet weak var txtEmail:UITextField!
    @IBOutlet weak var txtPassword:UITextField!
    @IBOutlet weak var btnLogin:UIButton!
    @IBOutlet weak var textFieldPicker:UITextField!
    
    @IBOutlet weak var btnRememberMe: UIButton!
    //  @IBOutlet weak var viewContainerUserType:UIView!
    
    //*********Picker Setup*********//
   // private var picker = UIPickerView()
   // private let arr_Picker = ["Student/Guardian","Freelancer","Organization"]
    //*********Picker Setup*********//
    
    private var loginObject: LoginModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.initialViewSetup()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setNavigationAndStatusAppearance()
        setNeedsStatusBarAppearanceUpdate()
        
        if let userNamePassword = UserModel.getUserCredentials() as? NSDictionary {
            
            if let username = userNamePassword.value(forKey: "userName"){
              //
                txtEmail.text = username as? String
            }
            if let pass = userNamePassword.value(forKey: "Password"){
                //  Password
                txtPassword.text = pass as? String
            }
        }
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    private func initialViewSetup() {

        btnLogin.addGrediant()
        
        //let viewLeftPicker = UIView(frame: CGRect(x: 0, y: 0, width: 8, height: 40))
        //textFieldPicker.leftView = viewLeftPicker
       // textFieldPicker.leftViewMode = .always
        let viewLeftEmail = UIView(frame: CGRect(x: 0, y: 0, width: 8, height: 40))
        txtEmail.leftView = viewLeftEmail
        txtEmail.leftViewMode = .always
        let viewLeftPassword = UIView(frame: CGRect(x: 0, y: 0, width: 8, height: 40))
        txtPassword.leftView = viewLeftPassword
        txtPassword.leftViewMode = .always
        
    }
    
    func setNavigationAndStatusAppearance()  {
        let statusBar: UIView = UIApplication.shared.value(forKey: "statusBar") as! UIView
        statusBar.backgroundColor = .white
        
    }

    //MARK: IBAction
    @IBAction func btnActionLogin(sender:UIButton) {
        
        self.view.endEditing(true)
        
        if (btnRememberMe.isSelected == false){
            UserModel.saveUserCredentials(userNameAndPassword: nil)

        }
        if validateData() {
            self.callLoginAPI()
        }
    }
   
    @IBAction func btnActionSkip(sender:UIButton) {
        appDelegate.loadStudentHomeViewController()
    }
    
    @IBAction func rememberMeClicked(_ sender: Any) {
        
        //Save user login credentials in user defaults
        if  btnRememberMe.isSelected == true{
            btnRememberMe.isSelected = false
            btnRememberMe.setImage(#imageLiteral(resourceName: "radio-unselect"), for: .normal)
            UserModel.saveUserCredentials(userNameAndPassword: nil)
        }else{
            btnRememberMe.isSelected = true
            btnRememberMe.setImage(#imageLiteral(resourceName: "radio-select"), for: .selected)
            
            //Keep user name and password in default for next time autofil
            UserModel.saveUserCredentials(userNameAndPassword:  ["userName": txtEmail.text ?? "", "Password": txtPassword.text ?? ""])
        }
        
    }
    @IBAction func btnActionForgotPassword(sender:UIButton) {
        let viewController = UIStoryboard.instantiateForgotPasswordViewController()
        appDelegate.menuNavController?.pushViewController(viewController, animated: true)
    }
    
    @IBAction func btnActionRegister(sender:UIButton) {
        let viewController = UIStoryboard.instantiateRegisterStepOneViewController()
        appDelegate.menuNavController?.pushViewController(viewController, animated: true)
    }
    
    // Validate data...
    private func validateData() -> Bool {
        
//        if textFieldPicker.text?.trim().count == 0 {
//            self.showAlert(title:APP_NAME, message:"Please select User type", withTag:101, button:["Ok"], isActionRequired:false)
//            return false
//        } else
//        else if !(txtEmail.text?.isValidEmail())! {self.showAlert(title:APP_NAME, message:"Please enter valid email", withTag:101, button:["Ok"], isActionRequired:false)
//            return false
//        }
            if txtEmail.text?.trim().count == 0 {
            self.showAlert(title:APP_NAME, message:"Please enter registered number", withTag:101, button:["Ok"], isActionRequired:false)
            return false
        }  else if txtPassword.text?.trim().count == 0 {self.showAlert(title:APP_NAME, message:"Please enter password", withTag:101, button:["Ok"], isActionRequired:false)
            return false
        } else if txtPassword.text?.trim().count == 0 {self.showAlert(title:APP_NAME, message:"Password must contain minimum 8 characters at least 1 Alphabet, 1 Number and 1 Special Character.", withTag:101, button:["Ok"], isActionRequired:false)
            return false
        } else {
            return true
        }
    }
    
    private func validatePassword() -> Bool {
        
        if (txtPassword.text?.count)! < 8 {
            return false
        }
        
        let decimalCharacters = CharacterSet.decimalDigits
        let decimalRange = txtPassword.text?.rangeOfCharacter(from: decimalCharacters)
        if decimalRange == nil {
            //print("Numbers not found")
            return false
        }
        
        let letters = NSCharacterSet.letters
        let range = txtPassword.text?.rangeOfCharacter(from: letters)
        // range will be nil if no letters is found
        if range == nil {
            //print("letters not found")
            return false
        }
        
        let characterset = CharacterSet(charactersIn: "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789")
        if txtPassword.text?.rangeOfCharacter(from: characterset.inverted) != nil {
            //print("string contains special characters")
            return false
        }
        
        return true
    }
    
    //MARK: API
    
    private func callLoginAPI()  {

        
        var params : [String:Any] = [:]
        params["email"] = txtEmail.text
        params["password"] = txtPassword.text
        params["grant_type"] = "password"
        params["client_id"] = kAppDelegate.kBOOL_LIVE ?"9":"16"
            params["client_secret"] = kAppDelegate.kBOOL_LIVE ? "BqpRsBdQhfSWNnK94NcCj3R5wBuoC0NNI6OB8zur" : "k87ciDnEFUr89TDL52YsjfRLu0vpgTCBZp3Qkj2Y"
        params["scope"] = "*"
        
     //   print("params : \(params)")
        
        ProgressHUD.showProgressHUDWithLabel(label: "Loading...")
        
        APIManager.callPost("\(URLS.BASE_URL)\(URLS.LOGIN)", param: params, header: ["":""]) { (isSuccess, result, error) in
            
            DispatchQueue.main.async {
                
               UserModel.setAutoLogin(isSucessfull:false)
                ProgressHUD.hideProgressHUD()
                
                weak var weakSelf = self
                
                if (error != nil) {
                    weakSelf?.showAlert(title: "Error", message: (error!.localizedDescription), withTag: 101, button: ["Ok"], isActionRequired: false)
                    return
                }
                
                    if isSuccess {
                        do {
                            
                            let jsonData = try JSONSerialization.jsonObject(with: result!, options: .mutableContainers)
                            
                            if let responseDict = (jsonData as? NSDictionary){
                                
                                if let statusCode = responseDict.object(forKey: "status"), (statusCode is Int) {
                                    
                                    var idToSave : String = ""
                                    var mobileNoToSave  : String = ""
                                    if (statusCode as! Int) == 200 {
                                        
                                        
                                        if let dataDict = responseDict.object(forKey: "data") as? NSDictionary {
                                            // save data and land to home view controller...
                                            
                                            var acessTokeToSave : String = ""
                                            var acessRefreshTokeToSave : String = ""

                                            if let tokenDictDict = dataDict.object(forKey: "token") as? NSDictionary {
                                                
                                                if let roleType = tokenDictDict.value(forKey:"access_token") as? String{
                                                    
                                                    acessTokeToSave = roleType
                                                }
                                                if let roleType = tokenDictDict.value(forKey:"refresh_token") as? String{
                                                    
                                                    acessRefreshTokeToSave = roleType
                                                }
                                            }
                                            
                                            if let userDict = dataDict.object(forKey: "user") as? NSDictionary {
                                                
                                                if let roleType = userDict.value(forKey:"id") as? String{
                                                    
                                                    idToSave = roleType
                                                }else  if let roleType = userDict.value(forKey:"id") as? Int{
                                                    idToSave = String(roleType)

                                                }
                                                if let mob = userDict.value(forKey:"mobile") as? String{
                                                    
                                                    mobileNoToSave = mob
                                                }else  if let mobInt = userDict.value(forKey:"id") as? Int{
                                                    mobileNoToSave = String(mobInt)
                                                    
                                                }
                                                
                                                
                                                UserModel.setLoginInfo(userData:   NSDictionary(objects: [ idToSave , mobileNoToSave , acessTokeToSave ,  acessRefreshTokeToSave
                                                    ],   forKeys: [ "ID" as NSCopying ,   "mobile" as NSCopying ,
                                                                    "token" as NSCopying,
                                                                    "refreshToken" as NSCopying]))
                                                
                                               
                                                if let issType = userDict.value(forKey:"institute_type") as? String{
                                                    
                                                    UserModel.setUserType(uType: issType)
                                                }else  if let issType = userDict.value(forKey:"institute_type") as? Int{
                                                    UserModel.setUserType(uType: String(issType))

                                                }
                                                
                                                UserModel.setAutoLogin(isSucessfull:true)
                                                appDelegate.loadStudentHomeViewController()
                                            }
                                        }
                                    }else if  (statusCode as! Int) == 201 {
                                        
                                        //Otp not verify Got to Otp Screen
                                        
                                        let viewController = UIStoryboard.instantiateVerifyOTPViewController()
                                        viewController.isCommingFromRegistration = 3
                                        
                                        if let userI = idToSave as? String  {
                                            
                                            viewController.userIDReceived = String(userI)
                                        }
                                        if let phoneNumber = mobileNoToSave as? String  {
                                            viewController.mobileNumber = phoneNumber//.replace(string: "+91", replacement: "")
                                        }
                                        
                                        appDelegate.menuNavController?.pushViewController(viewController, animated: true)
                                        // save data and land to home view controller...
                                        
                                        
                                    } else  {
                                        var errStr = ""
                                        if let errormessage = (jsonData as? NSDictionary)?.object(forKey:"error") {
                                            if (errormessage is NSDictionary) {
                                                if let errorDescription = (errormessage as? NSDictionary)?.object(forKey: "error_description"),!(errorDescription is NSNull){
                                                    if (errorDescription is String){
                                                        errStr = errorDescription as! String
                                                        
                                                    }
                                                }else {
                                                    if let errorMessage = (errormessage as? NSDictionary)?.object(forKey: "message"),!(errorMessage is NSNull){
                                                        if (errorMessage is String){
                                                            errStr = errorMessage as! String
                                                            
                                                        }
                                                    }else{
                                                        errStr = AlertMessages.UnidetifiedErrorMessage
                                                    }
                                                    
                                                }
                                                weakSelf?.showAlert(title: APP_NAME, message:errStr, withTag: 101, button: ["Ok"], isActionRequired: false)
                                            }
                                            
                                        }else{
                                            weakSelf?.showAlert(title: APP_NAME, message:  AlertMessages.UnidetifiedErrorMessage, withTag: 101, button: ["Ok"], isActionRequired: false)
                                        }
                                        
                                    }
                                }
                            }
                        }catch  {
                            weakSelf?.showAlert(title: "sorry", message: AlertMessages.MODELERRORMESSAGE , withTag: 101, button:  ["Ok"], isActionRequired: false)
                            return
                        }
                    } else{
                        weakSelf?.showAlert(title: APP_NAME, message: "Something went wrong!!", withTag: 101, button: ["Ok"], isActionRequired: false)
                    }
                
            }
        }
    }
    
}

extension LoginVC : UITextFieldDelegate  {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if textField == txtEmail {
            txtPassword.becomeFirstResponder()
        } else {
            textField.resignFirstResponder()
        }
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
       if textField == txtPassword {
            let maxLength = 8
            let currentString: NSString = textField.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
        }
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
//        if textField == textFieldPicker {
//           // self.showPickerView()
//        }
    }
    
    // Picker Setup
    // UIPickerView
 /*   func showPickerView() {
        
        picker = UIPickerView(frame: CGRect(x: 0, y: 0, width: screenWidth, height: 216))
        picker.backgroundColor = .white
        picker.showsSelectionIndicator = true
        picker.delegate = self
        picker.dataSource = self
        textFieldPicker.inputView = picker
        
        let toolBar = UIToolbar()
        toolBar.barStyle = UIBarStyle.default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor.black
        toolBar.sizeToFit()
        
        let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItemStyle.plain, target: self, action: #selector(cancelActionPicker))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.plain, target: self, action: #selector(doneActionPicker))
        
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        textFieldPicker.inputAccessoryView = toolBar
    }
    
    @objc func doneActionPicker (sender:UIBarButtonItem)
    {
        let tempRow = picker.selectedRow(inComponent: 0)
        
        if tempRow == 0 {
            self.textFieldPicker.text = arr_Picker[tempRow]
        }
        
        textFieldPicker.resignFirstResponder()
    }
    
    @objc func cancelActionPicker (sender:UIBarButtonItem)
    {
        textFieldPicker.text = ""
        textFieldPicker.resignFirstResponder()
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return arr_Picker.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return arr_Picker[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        self.textFieldPicker.text = arr_Picker[row]
    }
    */
}
