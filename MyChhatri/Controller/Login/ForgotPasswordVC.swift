//
//  ForgotPasswordVC.swift
//  MyChhatri
//
//  Created by Anshul on 09/07/18.
//  Copyright © 2018 Anshul. All rights reserved.
//

import UIKit

class ForgotPasswordVC: UIViewController {

    @IBOutlet weak var textMobileNumber:UITextField!
    @IBOutlet weak var btnSubmit:UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        btnSubmit.addGrediant()
        let view1 = UIView(frame: CGRect(x: 0, y: 0, width: 8, height: 40))
        textMobileNumber.leftView = view1
        textMobileNumber.leftViewMode = .always
        kAppDelegate.menuNavController = self.navigationController
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: IBAction
    @IBAction func btnActionBack(sender:UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnActionSubmit(sender:UIButton) {
        
        if textMobileNumber.text?.count == 0 {
            self.showAlert(title: APP_NAME, message: "Please enter mobile number.", withTag: 101, button: ["Ok"], isActionRequired: false)
        } else {
            let viewController = UIStoryboard.instantiateVerifyOTPViewController()
            viewController.isCommingFromRegistration = 2
            viewController.mobileNumber = textMobileNumber.text!
            appDelegate.menuNavController?.pushViewController(viewController, animated: true)
        }
        
    }
    
}

extension ForgotPasswordVC : UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        var sucess = true
        
        if textField == textMobileNumber{
            let maxLength = 10
            let currentString: NSString = textField.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            sucess = newString.length <= maxLength
        }
        if textField == textMobileNumber && sucess == true {
            let allowedCharacters = CharacterSet(charactersIn:"0123456789 ")//Here change this characters based on your requirement
            let characterSet = CharacterSet(charactersIn: string)
            sucess = allowedCharacters.isSuperset(of: characterSet)
        }
        return sucess
        
    }
    
}
