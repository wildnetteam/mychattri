//
//  WishListTableViewCell.swift
//  MyChhatri
//
//  Created by Arpana on 13/09/18.
//  Copyright © 2018 Arpana. All rights reserved.
//

import UIKit

@objc protocol DeleteWishCellDelegate  {
    
    @objc optional func removefromWish(_ i: Int)
}


class WishListTableViewCell: UITableViewCell {

    @IBOutlet weak var lblStartDate: UILabel!
    @IBOutlet weak var lblEndDate: UILabel!
    @IBOutlet weak var lblCompanyName: UILabel!
    @IBOutlet weak var lblProgramName: UILabel!
    @IBOutlet weak var viewDetailButton: UIButton!
    @IBOutlet weak var DetailView: UIView!
     @IBOutlet weak var removeButton:  UIButton!
    
    weak var delegate:DeleteWishCellDelegate?

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func  setCellData(dict : NSDictionary)  {
        
        
        if let batchDict = dict.value(forKey: "batch") as? NSDictionary {
            if let endDate = batchDict.value(forKey: "end_date") as? String {
                
                lblEndDate.text = CalenderStruct.getFormatedDateStringWithouOrdinal(fromDateUTC:  CalenderStruct.getDateFromDateString(forUTC: endDate, byFormat: "yyyy-MM-dd"), inFormat: "dd MMM yyyy")
            }
            if let startDate = batchDict.value(forKey: "start_date") as? String {
                
                lblStartDate.text = CalenderStruct.getFormatedDateStringWithouOrdinal(fromDateUTC:  CalenderStruct.getDateFromDateString(forUTC: startDate, byFormat: "yyyy-MM-dd"), inFormat: "dd MMM yyyy")
            }
        }
                
        if let idToViewDetail = dict.value(forKey: "id") as? String {
            
            if let idAsInt =  Int(idToViewDetail){
                viewDetailButton.tag = idAsInt
            }
        }else  if let idToViewDetailAsInt = dict.value(forKey: "id") as? Int {
            
            viewDetailButton.tag = idToViewDetailAsInt
            
        }
        
   
      
        if let courseDict = dict.value(forKey: "course") as? NSDictionary {
            if let title = courseDict.value(forKey: "title") as? String{
                lblProgramName.text = title
            }
        }
        if let vendorDict = dict.value(forKey: "vendor") as? NSDictionary {
            if let titlename = vendorDict.value(forKey: "name") as? String{
                lblCompanyName.text = titlename
            }
        }
    }
    
    @IBAction func viewDetails(_ sender: Any){
        
        if let Id = String(viewDetailButton.tag) as? String {
            NavigationManager.moveToWishListDetailViewControllerIfNotExists(kAppDelegate.menuNavController!, enrolmentID: Id)
        }
    }
    
    @IBAction func removeFromWishList(_ sender: Any){
        
        delete()
        
    }
    
    func delete(){
        
        var params : [String:Any] = [:]
        
        
        guard let IdTodelete = String(viewDetailButton.tag) as? String else{
            
            self.showAlert(title: "Error", message: "Id to delete is not found. Try again", withTag: 101, button: ["Ok"], isActionRequired: false)
            return
        }
        
        
        APICall.callRemoveFromWishListAPI(params, header: [ : ], batchID: IdTodelete) { (sucess, result, error) in
            
            DispatchQueue.main.async {
                
                weak var weakSelf = self
                
                ProgressHUD.hideProgressHUD()
                
                if (error != nil) {
                    weakSelf?.showAlert(title: "Error", message: (error!.localizedDescription), withTag: 101, button: ["Ok"], isActionRequired: false)
                    return
                }
                
                if sucess {
                    
                    do {
                        let jsonData = try JSONSerialization.jsonObject(with: result!, options: .mutableContainers)
                        
                        if let responseDict = (jsonData as? NSDictionary){
                            
                            if let statusCode = responseDict.object(forKey: "status"), (statusCode is Int) {
                                
                                if (statusCode as! Int) == 200 {
                                    
                                    var msg = "Removed successfully"
                                    
                                    if let message = responseDict.object(forKey: "message") as? String {
                                        msg = message
                                    }
                                    weakSelf?.showAlert(title: APP_NAME, message: msg, withTag: 110, button: ["Ok"], isActionRequired: true)
                                    
                                     weakSelf?.removeButton.isHidden = true
                                    
                                    //Deleted sucesfully , update the collectionview
                                    if weakSelf?.delegate != nil{
                                        weakSelf?.delegate?.removefromWish!((weakSelf?.removeButton?.tag)!)
                                    }
                                    
                                    
                                } else {
                                    var errStr = ""
                                    if let errormessage = (jsonData as? NSDictionary)?.object(forKey:"error") {
                                        if (errormessage is NSDictionary){
                                            if let errorDescription = (errormessage as? NSDictionary)?.object(forKey: "error_description"),!(errorDescription is NSNull){
                                                if (errorDescription is String){
                                                    errStr = errorDescription as! String
                                                    
                                                }
                                            }else {
                                                if let errorMessage = (errormessage as? NSDictionary)?.object(forKey: "message"),!(errorMessage is NSNull){
                                                    if (errorMessage is String){
                                                        errStr = errorMessage as! String
                                                        
                                                    }
                                                }else{
                                                    errStr = AlertMessages.UnidetifiedErrorMessage
                                                }
                                                
                                            }
                                            weakSelf?.showAlert(title: APP_NAME, message:errStr, withTag: 101, button: ["Ok"], isActionRequired: false)
                                        }
                                        
                                    }else{
                                        weakSelf?.showAlert(title: APP_NAME, message:  AlertMessages.UnidetifiedErrorMessage, withTag: 101, button: ["Ok"], isActionRequired: false)
                                    }
                                    
                                }
                            }
                        }else{
                            
                            weakSelf?.showAlert(title: "sorry", message: AlertMessages.NOTADICTIONARYMESSAGE , withTag: 101, button:  ["Ok"], isActionRequired: false)
                            return
                        }
                        
                    }catch{
                        
                        weakSelf?.showAlert(title: "sorry", message: AlertMessages.MODELERRORMESSAGE , withTag: 101, button:  ["Ok"], isActionRequired: false)
                        return
                    }
                    
                } else{
                    
                    //Success false
                    weakSelf?.showAlert(title: APP_NAME, message: AlertMessages.UnidetifiedErrorMessage, withTag: 101, button: ["Ok"], isActionRequired: false)
                    
                }
                
                
            }
            
        }
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
