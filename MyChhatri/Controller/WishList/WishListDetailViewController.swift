//
//  WishListDetailViewController.swift
//  MyChhatri
//
//  Created by Arpana on 13/09/18.
//  Copyright © 2018 Arpana. All rights reserved.
//

import UIKit

class WishListDetailViewController: UIViewController {

    
    var courseID :String?
    var batchID: String?
    var deleteID: String?
  //  var courseId: String?
    var dataDictionary : NSDictionary?
    
    @IBOutlet weak var lblBatchStartDate: UILabel!
    @IBOutlet weak var lblBatchEndDate: UILabel!
    @IBOutlet weak var lblBatchCompanyName: UILabel!
    @IBOutlet weak var lblBatchProgramName: UILabel!
    @IBOutlet weak var lblBatchDays: UILabel!
    @IBOutlet weak var lblBatchDuration: UILabel!
    @IBOutlet weak var lblBatchTiming: UILabel!
    @IBOutlet weak var lblBatchNo: UILabel!
    @IBOutlet weak var viewBatchView: UIView!
    var batchArray : Array<NSDictionary>?
    var enrollmentId: String?

    @IBOutlet weak var viewFee: UIView!
    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBOutlet weak var lblProgramName: UILabel!
    
    @IBOutlet weak var lblCourseFee: UILabel!
    @IBOutlet weak var lblRegistrationFee: UILabel!
    
    @IBOutlet weak var lblDescription: UILabel!
    
    @IBOutlet weak var lblTotalPrice: UILabel!
    
  //  @IBOutlet weak var collectionView: UICollectionView!
     @IBOutlet weak var payButton: UIButton!
    @IBOutlet weak var removeButton: UIButton!
    
    
    var acceptTerms:String = "0"
    @IBOutlet weak var btnTermCheck : UIButton!
    @IBOutlet weak var btnTermLink : UIButton!
    
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action:
            #selector(CourseDetailViewController.getprogramDetail),
                                 for: UIControlEvents.valueChanged)
        refreshControl.tintColor = .black
        
        return refreshControl
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUI()
        getprogramDetail()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        self.navigationController?.navigationBar.isHidden = false
        self.navigationItem.hidesBackButton = true
        
       // self.navigationController?.navigationBar.topItem?.title =  "My Registeration Details"
        kAppDelegate.menuNavController = self.navigationController
        
        kAppDelegate.setNavigationAndStatusAppearance()
        let statusBar: UIView = UIApplication.shared.value(forKey: "statusBar") as! UIView
        statusBar.backgroundColor = ColorConstants.appBackGroundCOLOR()
        setNeedsStatusBarAppearanceUpdate()
        
        
    }
    
    func setUI() {
        
        viewFee.layer.shadowOffset = CGSize(width: 0, height: 8)
        viewFee.layer.shadowColor = UIColor(red:0.8, green:0.8, blue:0.8, alpha:0.5).cgColor
        viewFee.layer.shadowOpacity = 1
        viewFee.layer.shadowRadius = 20
        
        viewBatchView.layer.shadowOffset = CGSize(width: 0, height: 8)
        viewBatchView.layer.shadowColor = UIColor(red:0.8, green:0.8, blue:0.8, alpha:0.5).cgColor
        viewBatchView.layer.shadowOpacity = 1
        viewBatchView.layer.shadowRadius = 20
        
        lblDescription.layer.shadowOffset = CGSize(width: 0, height: 8)
        lblDescription.layer.shadowColor = UIColor(red:0.8, green:0.8, blue:0.8, alpha:0.5).cgColor
        lblDescription.layer.shadowOpacity = 1
        lblDescription.layer.shadowRadius = 20
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setData() {
        
        guard let dictionary = dataDictionary else{
            
            self.showAlert(title: "", message: "No data found", withTag: 101, button: ["OK"], isActionRequired: false)
            return
        }
        
        if let batch = dictionary.value(forKey: "batch_id") as? String {
            batchID = batch
        }else  if let batch = dictionary.value(forKey: "batch_id") as? Int {
            batchID = String(batch)
        }
        
        if let idDel = dictionary.value(forKey: "id") as? Int {
            deleteID = String(idDel)
        } else  if let idDel = dictionary.value(forKey: "id") as? String {
            deleteID = idDel
        }
        
        if let cou = dictionary.value(forKey: "course_id") as? String {
            courseID = cou
        }else if let cou = dictionary.value(forKey: "course_id") as? Int {
            courseID = String(cou)
        }
        
        if let courseDict = dictionary.value(forKey: "course") as? NSDictionary {
            
            if let title = courseDict.value(forKey: "title") as? String{
                lblProgramName.text = title
            }
            
            if let description = courseDict.value(forKey: "description") as? String {
                
                lblDescription.text = description.withoutHtml
            }
            
            var regisFee = ""
            if let registrationFees = courseDict.value(forKey: "registration_fees") as? String {
                regisFee = registrationFees
            }else  if let registrationFees = courseDict.value(forKey: "registration_fees") as? Int {
                regisFee = String(registrationFees)
            }
            
            if  regisFee != nil {
                
                let headingString = NSMutableAttributedString(
                    string: "Registration Fees :",
                    attributes: [NSAttributedStringKey(rawValue: NSAttributedStringKey.foregroundColor.rawValue):  UIColor(red:0.34, green:0.36, blue:0.4, alpha:1) , NSAttributedStringKey(rawValue: NSAttributedStringKey.font.rawValue): UIFont(name: "Amble-Regular", size: isIphone5 ? 10.0: 12.0)!])
                
                let subHeadingString = NSMutableAttributedString(
                    string: "INR \(regisFee).",
                    attributes: [NSAttributedStringKey(rawValue: NSAttributedStringKey.foregroundColor.rawValue): UIColor(red:0.34, green:0.36, blue:0.4, alpha:1) , NSAttributedStringKey(rawValue: NSAttributedStringKey.font.rawValue): UIFont(name: "Amble-Bold", size: isIphone5 ? 10.0 :12.0)!])
                
                let attrString1 = NSMutableAttributedString(string: "")
                attrString1.append(headingString)
                attrString1.append( NSMutableAttributedString(string: "  "))
                attrString1.append(subHeadingString)
                
                lblRegistrationFee.attributedText = attrString1
                
            }
            
            var courseFees = ""
            if let courseFee = courseDict.value(forKey: "course_fees") as? String {
                courseFees = courseFee
            }else  if let courseFee = courseDict.value(forKey: "course_fees")  as? Int {
                courseFees = String(courseFee)
            }
            
            if  courseFees != nil {
                
                
                let headingString = NSMutableAttributedString(
                    string: "Course Fees :",
                    attributes: [NSAttributedStringKey(rawValue: NSAttributedStringKey.foregroundColor.rawValue):  UIColor(red:0.34, green:0.36, blue:0.4, alpha:1) , NSAttributedStringKey(rawValue: NSAttributedStringKey.font.rawValue): UIFont(name: "Amble-Regular", size: isIphone5 ? 10.0: 12.0)!])
                
                let subHeadingString = NSMutableAttributedString(
                    string: "INR \(courseFees).",
                    attributes: [NSAttributedStringKey(rawValue: NSAttributedStringKey.foregroundColor.rawValue): UIColor(red:0.34, green:0.36, blue:0.4, alpha:1) , NSAttributedStringKey(rawValue: NSAttributedStringKey.font.rawValue): UIFont(name: "Amble-Bold", size: isIphone5 ? 10.0 :12.0)!])
                
                let attrString1 = NSMutableAttributedString(string: "")
                attrString1.append(headingString)
                attrString1.append( NSMutableAttributedString(string: "  "))
                attrString1.append(subHeadingString)
                
                lblCourseFee.attributedText = attrString1
            }
            
            var totalFees : Float = 0
            //calculate total price
            if let registrationFees = regisFee as? String {
                
                totalFees = Float(registrationFees)!
            }
            
            if let courseFees = courseFees  as? String {
                totalFees = totalFees + Float(courseFees)!
                
            }
            if let totalFeesCalculated = totalFees as? Float{
                
                let headingString = NSMutableAttributedString(
                    string: "Total Price : ",
                    attributes: [NSAttributedStringKey(rawValue: NSAttributedStringKey.foregroundColor.rawValue):  UIColor(red:0.34, green:0.36, blue:0.4, alpha:1) , NSAttributedStringKey(rawValue: NSAttributedStringKey.font.rawValue): UIFont(name: "Amble-Regular", size: isIphone5 ? 10.0: 12.0)!])
                
                let subHeadingString = NSMutableAttributedString(
                    string: "INR \(totalFeesCalculated) ",
                    attributes: [NSAttributedStringKey(rawValue: NSAttributedStringKey.foregroundColor.rawValue): UIColor(red:0.01, green:0.51, blue:0.73, alpha:1) , NSAttributedStringKey(rawValue: NSAttributedStringKey.font.rawValue): UIFont(name: "Amble-Bold", size: isIphone5 ? 10.0 :12.0)!])
                
                let attrString1 = NSMutableAttributedString(string: "")
                attrString1.append(headingString)
                attrString1.append( NSMutableAttributedString(string: "  "))
                attrString1.append(subHeadingString)
                
                lblTotalPrice.attributedText = attrString1
            }
        }
        //sucessfully retrieved data
        if let batchDict =  dictionary.object(forKey: "batch") as? NSDictionary {
//            batchArray = Array()
//            batchArray?.append(batchDict)
//            collectionView.reloadData()
            
            setBatchData(dict: batchDict)
        } else{
            
        }
        
    }
    
    func  getDaysArray(combinedDays:String) -> [String] {
        return combinedDays.components(separatedBy: ",")
        
    }
    
    func  setBatchData(dict : NSDictionary)  {
        
        
//
//        if let wishCount = dict.value(forKey: "wishlist_count") as? String {
//            if let  isDispayButton = Int(wishCount) {
//                if isDispayButton != 0 {
//                    wishListButton.isHidden = true
//                }else{
//                    wishListButton.isHidden = false
//                }
//            }
//        }
//
//        if let bookingCount = dict.value(forKey: "booking_count") as? String {
//            if let  isDispayButton = Int(bookingCount) {
//                if isDispayButton != 0 {
//                    enrollButton.isHidden = true
//                    wishListButton.isHidden = true
//
//                }else{
//                    enrollButton.isHidden = false
//                }
//            }
//        }
        
        
        
        if let startDate = dict.value(forKey: "start_date") as? String {
            
            
            let headingString = NSMutableAttributedString(
                string: "Start Date :",
                attributes: [NSAttributedStringKey(rawValue: NSAttributedStringKey.foregroundColor.rawValue):  UIColor(red:0.34, green:0.36, blue:0.4, alpha:1) , NSAttributedStringKey(rawValue: NSAttributedStringKey.font.rawValue): UIFont(name: "Amble-Regular", size: isIphone5 ? 10.0: 12.0)!])
            
            let startDateString = CalenderStruct.getFormatedDateStringWithouOrdinal(fromDateUTC:  CalenderStruct.getDateFromDateString(forUTC: startDate, byFormat: "yyyy-MM-dd"), inFormat: "dd MMM yyyy")
            var subHeadingString = NSMutableAttributedString(string: "")
            if let formatedDate = startDateString {
                
                subHeadingString = NSMutableAttributedString(
                    string: "\(formatedDate).",
                    attributes: [NSAttributedStringKey(rawValue: NSAttributedStringKey.foregroundColor.rawValue): UIColor(red:0.34, green:0.36, blue:0.4, alpha:1) , NSAttributedStringKey(rawValue: NSAttributedStringKey.font.rawValue): UIFont(name: "Amble-Bold", size: isIphone5 ? 10.0 :12.0)!])
            }else {
                subHeadingString = NSMutableAttributedString(
                    string: "\(startDate).",
                    attributes: [NSAttributedStringKey(rawValue: NSAttributedStringKey.foregroundColor.rawValue): UIColor(red:0.34, green:0.36, blue:0.4, alpha:1) , NSAttributedStringKey(rawValue: NSAttributedStringKey.font.rawValue): UIFont(name: "Amble-Bold", size: isIphone5 ? 10.0 :12.0)!])
            }
            let attrString1 = NSMutableAttributedString(string: "")
            attrString1.append(headingString)
            attrString1.append( NSMutableAttributedString(string: "  "))
            attrString1.append(subHeadingString)
            
            lblBatchStartDate.attributedText = attrString1
        }
        if let endDate = dict.value(forKey: "end_date") as? String {
            
            
            let headingString = NSMutableAttributedString(
                string: "End Date :",
                attributes: [NSAttributedStringKey(rawValue: NSAttributedStringKey.foregroundColor.rawValue):  UIColor(red:0.34, green:0.36, blue:0.4, alpha:1) , NSAttributedStringKey(rawValue: NSAttributedStringKey.font.rawValue): UIFont(name: "Amble-Regular", size: isIphone5 ? 10.0: 12.0)!])
            
            let startDateString = CalenderStruct.getFormatedDateStringWithouOrdinal(fromDateUTC:  CalenderStruct.getDateFromDateString(forUTC: endDate, byFormat: "yyyy-MM-dd"), inFormat: "dd MMM yyyy")
            var subHeadingString = NSMutableAttributedString(string: "")
            if let formatedDate = startDateString {
                
                subHeadingString = NSMutableAttributedString(
                    string: "\(formatedDate).",
                    attributes: [NSAttributedStringKey(rawValue: NSAttributedStringKey.foregroundColor.rawValue): UIColor(red:0.34, green:0.36, blue:0.4, alpha:1) , NSAttributedStringKey(rawValue: NSAttributedStringKey.font.rawValue): UIFont(name: "Amble-Bold", size: isIphone5 ? 10.0 :12.0)!])
            }else {
                subHeadingString = NSMutableAttributedString(
                    string: "\(endDate).",
                    attributes: [NSAttributedStringKey(rawValue: NSAttributedStringKey.foregroundColor.rawValue): UIColor(red:0.34, green:0.36, blue:0.4, alpha:1) , NSAttributedStringKey(rawValue: NSAttributedStringKey.font.rawValue): UIFont(name: "Amble-Bold", size: isIphone5 ? 10.0 :12.0)!])
            }
            
            let attrString1 = NSMutableAttributedString(string: "")
            attrString1.append(headingString)
            attrString1.append( NSMutableAttributedString(string: "  "))
            attrString1.append(subHeadingString)
            
            lblBatchEndDate.attributedText = attrString1
        }
        if let batchNo = dict.value(forKey: "batch_no") as? String {
            
            
            let headingString = NSMutableAttributedString(
                string: "Batch Number :",
                attributes: [NSAttributedStringKey(rawValue: NSAttributedStringKey.foregroundColor.rawValue):  UIColor(red:0.34, green:0.36, blue:0.4, alpha:1) , NSAttributedStringKey(rawValue: NSAttributedStringKey.font.rawValue): UIFont(name: "Amble-Regular", size: isIphone5 ? 10.0: 12.0)!])
            
            let subHeadingString = NSMutableAttributedString(
                string: "\(batchNo).",
                attributes: [NSAttributedStringKey(rawValue: NSAttributedStringKey.foregroundColor.rawValue): UIColor(red:0.34, green:0.36, blue:0.4, alpha:1) , NSAttributedStringKey(rawValue: NSAttributedStringKey.font.rawValue): UIFont(name: "Amble-Bold", size: isIphone5 ? 10.0 :12.0)!])
            
            let attrString1 = NSMutableAttributedString(string: "")
            attrString1.append(headingString)
            attrString1.append( NSMutableAttributedString(string: "  "))
            attrString1.append(subHeadingString)
            
            lblBatchNo.attributedText = attrString1
        }
        if let duration = dict.value(forKey: "duration") as? String {
            
            
            let headingString = NSMutableAttributedString(
                string: "Duration :",
                attributes: [NSAttributedStringKey(rawValue: NSAttributedStringKey.foregroundColor.rawValue):  UIColor(red:0.34, green:0.36, blue:0.4, alpha:1) , NSAttributedStringKey(rawValue: NSAttributedStringKey.font.rawValue): UIFont(name: "Amble-Regular", size: isIphone5 ? 10.0: 12.0)!])
            
            let subHeadingString = NSMutableAttributedString(
                string: "\(duration).",
                attributes: [NSAttributedStringKey(rawValue: NSAttributedStringKey.foregroundColor.rawValue): UIColor(red:0.34, green:0.36, blue:0.4, alpha:1) , NSAttributedStringKey(rawValue: NSAttributedStringKey.font.rawValue): UIFont(name: "Amble-Bold", size: isIphone5 ? 10.0 :12.0)!])
            
            let attrString1 = NSMutableAttributedString(string: "")
            attrString1.append(headingString)
            attrString1.append( NSMutableAttributedString(string: "  "))
            attrString1.append(subHeadingString)
            
            lblBatchDuration.attributedText = attrString1
        }
        
        var time1Str = ""
        var time2Str = ""
        if let startTime = dict.value(forKey: "start_time") as? String {
            
            time1Str = startTime
        }
        if let endTime = dict.value(forKey: "end_time") as? String {
            time2Str = endTime
            
        }
        
        let formatter = DateFormatter()
        formatter.dateFormat = "HH:mm:ss"
        
        let startTime = formatter.date(from: time1Str)
        
        let endTime = formatter.date(from: time2Str)
        
        
        // let formatter = DateFormatter()
        formatter.dateStyle = DateFormatter.Style.long
        formatter.timeStyle = DateFormatter.Style.medium
        formatter.dateFormat = "hh:mm a"
        formatter.amSymbol = "am"
        formatter.pmSymbol = "pm"
        
        var  fromTime = ""
        var  toTime = ""
        if startTime != nil {
            
            fromTime = formatter.string(from: startTime!)
            
        }
        if endTime != nil {
            toTime = formatter.string(from: endTime! )
        }
        //Now format will be ..To..
        
        
        let headingString = NSMutableAttributedString(
            string: "Timing :",
            attributes: [NSAttributedStringKey(rawValue: NSAttributedStringKey.foregroundColor.rawValue):  UIColor(red:0.34, green:0.36, blue:0.4, alpha:1) , NSAttributedStringKey(rawValue: NSAttributedStringKey.font.rawValue): UIFont(name: "Amble-Regular", size: isIphone5 ? 10.0: 12.0)!])
        
        let subHeadingString = NSMutableAttributedString(
            string: "\(fromTime) To \(toTime)",
            attributes: [NSAttributedStringKey(rawValue: NSAttributedStringKey.foregroundColor.rawValue): UIColor(red:0.34, green:0.36, blue:0.4, alpha:1) , NSAttributedStringKey(rawValue: NSAttributedStringKey.font.rawValue): UIFont(name: "Amble-Bold", size: isIphone5 ? 10.0 :12.0)!])
        
        let attrString1 = NSMutableAttributedString(string: "")
        attrString1.append(headingString)
        attrString1.append( NSMutableAttributedString(string: "  "))
        attrString1.append(subHeadingString)
        lblBatchTiming.text = String("\(fromTime) To \(toTime)")
        
        
        
        if let weekdays = dict.value(forKey: "weekdays") as? String {
            
            
            let headingString = NSMutableAttributedString(
                string: "Days :",
                attributes: [NSAttributedStringKey(rawValue: NSAttributedStringKey.foregroundColor.rawValue):  UIColor(red:0.34, green:0.36, blue:0.4, alpha:1) , NSAttributedStringKey(rawValue: NSAttributedStringKey.font.rawValue): UIFont(name: "Amble-Regular", size: isIphone5 ? 10.0: 12.0)!])
            
            
            let dayStri: NSMutableString = ""
            for d in (getDaysArray(combinedDays: weekdays) as? [String])!{
                
                let val = loadDayName(forDay: Int(d)!)
                dayStri.append(val)
                dayStri.append(", ")
            }
            //delete last ", " so index 2
            dayStri.deleteCharacters(in:NSRange(location: dayStri.length - 2, length: 1))
            
            let subHeadingString = NSMutableAttributedString(
                string: "\(dayStri)",
                attributes: [NSAttributedStringKey(rawValue: NSAttributedStringKey.foregroundColor.rawValue): UIColor(red:0.34, green:0.36, blue:0.4, alpha:1) , NSAttributedStringKey(rawValue: NSAttributedStringKey.font.rawValue): UIFont(name: "Amble-Bold", size: isIphone5 ? 10.0 :12.0)!])
            let attrString1 = NSMutableAttributedString(string: "")
            attrString1.append(headingString)
            attrString1.append( NSMutableAttributedString(string: "  "))
            attrString1.append(subHeadingString)
            lblBatchDays.text = String(dayStri)
        }
        //start_time
 
    
      /*
        if let batchDict = dict.value(forKey: "batch") as? NSDictionary {
            if let endDate = batchDict.value(forKey: "end_date") as? String {
                
                lblBatchEndDate.text = CalenderStruct.getFormatedDateStringWithouOrdinal(fromDateUTC:  CalenderStruct.getDateFromDateString(forUTC: endDate, byFormat: "yyyy-MM-dd"), inFormat: "dd MMM yyyy")
            }
            if let startDate = batchDict.value(forKey: "start_date") as? String {
                
                lblBatchStartDate.text = CalenderStruct.getFormatedDateStringWithouOrdinal(fromDateUTC:  CalenderStruct.getDateFromDateString(forUTC: startDate, byFormat: "yyyy-MM-dd"), inFormat: "dd MMM yyyy")
            }
        }

        
        if let courseDict = dict.value(forKey: "course") as? NSDictionary {
            if let title = courseDict.value(forKey: "title") as? String{
                lblBatchProgramName.text = title
            }
        }
        if let vendorDict = dict.value(forKey: "vendor") as? NSDictionary {
            if let titlename = vendorDict.value(forKey: "name") as? String{
                lblBatchCompanyName.text = titlename
            }
        }
    }
    
    @objc func getprogramDetail()  {
        
        if self.refreshControl.isRefreshing{
        }
        
        guard (enrollmentId) != nil else{
            
            self.showAlert(title: "Sorry", message: "CategoryID is not available please try again", withTag: 101, button: ["Ok"], isActionRequired: false)
            return
        }
        
        ProgressHUD.showProgressHUDWithLabel(label: "Loading...")
        
        
        APICall.callBatchesWishLisDetailsAPI([:], header: [ : ], enID: enrollmentId! ) { (sucess, result, error) in
            
            DispatchQueue.main.async {
                
                weak var weakSelf = self
                
                ProgressHUD.hideProgressHUD()
                
                if (error != nil) {
                    weakSelf?.showAlert(title: "Error", message: (error!.localizedDescription), withTag: 101, button: ["Ok"], isActionRequired: false)
                    return
                }
                
                if sucess {
                    
                    do {
                        let jsonData = try JSONSerialization.jsonObject(with: result!, options: .mutableContainers)
                        
                        if let responseDict = (jsonData as? NSDictionary){
                            
                            if let statusCode = responseDict.object(forKey: "status"), (statusCode is Int) {
                                
                                if (statusCode as! Int) == 200 {
                                    
                                    //sucessfully retrieved data
                                    if let dataDict = responseDict.object(forKey: "data") as? NSDictionary{
                                        
                                        weakSelf?.dataDictionary = dataDict
                                        
                                        weakSelf?.setData()
                                        
                                    }else{
                                        //No data found error
                                        weakSelf?.showAlert(title: APP_NAME, message:  AlertMessages.NoDataKeyFoundInResponse, withTag: 101, button: ["Ok"], isActionRequired: false)
                                    }
                                    
                                } else {
                                    var errStr = ""
                                    if let errormessage = (jsonData as? NSDictionary)?.object(forKey:"error") {
                                        if (errormessage is NSDictionary){
                                            if let errorDescription = (errormessage as? NSDictionary)?.object(forKey: "error_description"),!(errorDescription is NSNull){
                                                if (errorDescription is String){
                                                    errStr = errorDescription as! String
                                                    
                                                }
                                            }else {
                                                if let errorMessage = (errormessage as? NSDictionary)?.object(forKey: "message"),!(errorMessage is NSNull){
                                                    if (errorMessage is String){
                                                        errStr = errorMessage as! String
                                                        
                                                    }
                                                }else{
                                                    errStr = AlertMessages.UnidetifiedErrorMessage
                                                }
                                                
                                            }
                                            weakSelf?.showAlert(title: APP_NAME, message:errStr, withTag: 101, button: ["Ok"], isActionRequired: false)
                                        }
                                        
                                    }else{
                                        weakSelf?.showAlert(title: APP_NAME, message:  AlertMessages.UnidetifiedErrorMessage, withTag: 101, button: ["Ok"], isActionRequired: false)
                                    }
                                    
                                }
                            }
                        }else{
                            
                            weakSelf?.showAlert(title: "sorry", message: AlertMessages.NOTADICTIONARYMESSAGE , withTag: 101, button:  ["Ok"], isActionRequired: false)
                            return
                        }
                        
                    }catch{
                        
                        weakSelf?.showAlert(title: "sorry", message: AlertMessages.MODELERRORMESSAGE , withTag: 101, button:  ["Ok"], isActionRequired: false)
                        return
                    }
                    
                } else{
                    
                    //Success false
                    weakSelf?.showAlert(title: APP_NAME, message: AlertMessages.UnidetifiedErrorMessage, withTag: 101, button: ["Ok"], isActionRequired: false)
                    
                }
                
                if self.refreshControl.isRefreshing{
                    self.refreshControl.endRefreshing()
                }
                
                //                if weakSelf?.tableView != nil {
                //                    weakSelf?.tableView.reloadData()
                //                }
            }
            
        }*/
    }
    
    @IBAction func enrollNow (_ sender : Any){
        
        
        if acceptTerms == "0" {
            self.showAlert(title:APP_NAME, message:"Please accept Terms and Condition", withTag:101, button:["Ok"], isActionRequired:false)
            return
        }
        
        var params : [String:Any] = [:]
        
        
        guard let courseIdToSave = courseID else{
            
            self.showAlert(title: "Error", message: "CourseId to SAVE is not found. Try again", withTag: 101, button: ["Ok"], isActionRequired: false)
            return
        }
        
        
        params["course_id"] = courseIdToSave
        
        guard let batchIdToSave = batchID else{
            
            self.showAlert(title: "Error", message: "Batch Id to SAVE is not found. Try again", withTag: 101, button: ["Ok"], isActionRequired: false)
            return
            
        }
        params["batch_id"] = batchIdToSave
        
        print("params : \(params)")
        
        ProgressHUD.showProgressHUDWithLabel(label: "Loading...")
        
        APICall.callEnrollPostAPI(params, header: [ : ]) { (sucess, result, error) in
            
            DispatchQueue.main.async {
                
                ProgressHUD.hideProgressHUD()
                
                weak var weakSelf = self
                
                if (error != nil) {
                    weakSelf?.showAlert(title: "Error", message: (error!.localizedDescription), withTag: 101, button: ["Ok"], isActionRequired: false)
                    return
                }
                if sucess {
                    
                    do {
                        let jsonData = try JSONSerialization.jsonObject(with: result!, options: .mutableContainers)
                        
                        if (jsonData is NSDictionary){
                            
                            if let statusCode = (jsonData as! NSDictionary).object(forKey: "status"), (statusCode is Int) {
                                
                                if let success = (jsonData as! NSDictionary).object(forKey: "success") {
                                    
                                    if (success as! Bool) == true {
                                        
                                        if (statusCode as! Int) == 200 {
                                            if let data = (jsonData as! NSDictionary).object(forKey: "data"), (data is NSDictionary) {
                                                if (data as! NSDictionary).count > 0 {
                                                    
                                                    if let message = (data as! NSDictionary).object(forKey: "message") as? String {
                                                        
                                                        weakSelf?.showAlert(title: APP_NAME, message: message, withTag: 400, button: ["Ok"], isActionRequired: false)
                                                        
                                                          weakSelf?.payButton.isHidden = true
                                                        weakSelf?.removeButton.isHidden = true
                                                        weakSelf?.btnTermLink.isHidden = true
                                                        weakSelf?.btnTermCheck.isHidden = true
                                                     
                                                    }
                                                    
                                                }
                                            }
                                            
                                        } else {
                                            
                                            var errStr = ""
                                            if let errormessage = (jsonData as? NSDictionary)?.object(forKey:"error") {
                                                if (errormessage is NSDictionary){
                                                    if let errorDescription = (errormessage as? NSDictionary)?.object(forKey: "error_description"),!(errorDescription is NSNull){
                                                        if (errorDescription is String){
                                                            errStr = errorDescription as! String
                                                            
                                                        }
                                                    }else {
                                                        if let errorMessage = (errormessage as? NSDictionary)?.object(forKey: "message"),!(errorMessage is NSNull){
                                                            if (errorMessage is String){
                                                                errStr = errorMessage as! String
                                                                
                                                            }
                                                        }else{
                                                            errStr = AlertMessages.UnidetifiedErrorMessage
                                                        }
                                                        
                                                    }
                                                    weakSelf?.showAlert(title: APP_NAME, message:errStr, withTag: 101, button: ["Ok"], isActionRequired: false)
                                                }
                                            }
                                        }
                                    }else{
                                        
                                        //some other status code
                                        //check error message in the response
                                        var errStr = ""
                                        if let errormessage = (jsonData as? NSDictionary)?.object(forKey:"error") {
                                            if (errormessage is NSDictionary){
                                                if let errorDescription = (errormessage as? NSDictionary)?.object(forKey: "error_description"),!(errorDescription is NSNull){
                                                    if (errorDescription is String){
                                                        errStr = errorDescription as! String
                                                        
                                                    }
                                                }else {
                                                    if let errorMessage = (errormessage as? NSDictionary)?.object(forKey: "message"),!(errorMessage is NSNull){
                                                        if (errorMessage is String){
                                                            errStr = errorMessage as! String
                                                            
                                                        }
                                                    }else{
                                                        errStr = AlertMessages.UnidetifiedErrorMessage
                                                    }
                                                    
                                                }
                                                weakSelf?.showAlert(title: APP_NAME, message:errStr, withTag: 101, button: ["Ok"], isActionRequired: false)
                                            }
                                        }
                                    }
                                    
                                }else{
                                    
                                    weakSelf?.showAlert(title: "sorry", message: AlertMessages.NOTADICTIONARYMESSAGE , withTag: 101, button:  ["Ok"], isActionRequired: false)
                                }
                                
                            }
                        }else{
                            // not a dictionary
                            weakSelf?.showAlert(title: "sorry", message: AlertMessages.NOTADICTIONARYMESSAGE , withTag: 101, button:  ["Ok"], isActionRequired: false)
                        }
                    }catch{
                        weakSelf?.showAlert(title: "sorry", message: AlertMessages.MODELERRORMESSAGE , withTag: 101, button:  ["Ok"], isActionRequired: false)
                        return
                    }
                }
                
            }
        }
    }
    
    @IBAction func backButtonClicked(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
        
    }
    
    
    @IBAction func btnCheckAcceptTerms(sender:UIButton) {
        
        //sender.isSelected = !sender.isSelected
        
        if sender.isSelected {
            acceptTerms = "0"
        } else {
            acceptTerms = "1"
        }
        sender.isSelected = !sender.isSelected
    }
    
    @IBAction func btnAcceptTerms(sender:UIButton) {
        let viewController = UIStoryboard.instantiateAcceptTermsViewController()
        appDelegate.menuNavController?.pushViewController(viewController, animated: true)
    }
    
    @objc func getprogramDetail()  {
        
        if self.refreshControl.isRefreshing{
        }
        
        guard (enrollmentId) != nil else{
            
            self.showAlert(title: "Sorry", message: "CategoryID is not available please try again", withTag: 101, button: ["Ok"], isActionRequired: false)
            return
        }
        
        ProgressHUD.showProgressHUDWithLabel(label: "Loading...")
        
        
        APICall.callBatchesWishLisDetailsAPI([:], header: [ : ], enID: enrollmentId! ) { (sucess, result, error) in
            
            DispatchQueue.main.async {
                
                weak var weakSelf = self
                
                ProgressHUD.hideProgressHUD()
                
                if (error != nil) {
                    weakSelf?.showAlert(title: "Error", message: (error!.localizedDescription), withTag: 101, button: ["Ok"], isActionRequired: false)
                    return
                }
                
                if sucess {
                    
                    do {
                        let jsonData = try JSONSerialization.jsonObject(with: result!, options: .mutableContainers)
                        
                        if let responseDict = (jsonData as? NSDictionary){
                            
                            if let statusCode = responseDict.object(forKey: "status"), (statusCode is Int) {
                                
                                if (statusCode as! Int) == 200 {
                                    
                                    //sucessfully retrieved data
                                    if let dataDict = responseDict.object(forKey: "data") as? NSDictionary{
                                        
                                        weakSelf?.dataDictionary = dataDict
                                        
                                        weakSelf?.setData()
                                        
                                    }else{
                                        //No data found error
                                        weakSelf?.showAlert(title: APP_NAME, message:  AlertMessages.NoDataKeyFoundInResponse, withTag: 101, button: ["Ok"], isActionRequired: false)
                                    }
                                    
                                } else {
                                    var errStr = ""
                                    if let errormessage = (jsonData as? NSDictionary)?.object(forKey:"error") {
                                        if (errormessage is NSDictionary){
                                            if let errorDescription = (errormessage as? NSDictionary)?.object(forKey: "error_description"),!(errorDescription is NSNull){
                                                if (errorDescription is String){
                                                    errStr = errorDescription as! String
                                                    
                                                }
                                            }else {
                                                if let errorMessage = (errormessage as? NSDictionary)?.object(forKey: "message"),!(errorMessage is NSNull){
                                                    if (errorMessage is String){
                                                        errStr = errorMessage as! String
                                                        
                                                    }
                                                }else{
                                                    errStr = AlertMessages.UnidetifiedErrorMessage
                                                }
                                                
                                            }
                                            weakSelf?.showAlert(title: APP_NAME, message:errStr, withTag: 101, button: ["Ok"], isActionRequired: false)
                                        }
                                        
                                    }else{
                                        weakSelf?.showAlert(title: APP_NAME, message:  AlertMessages.UnidetifiedErrorMessage, withTag: 101, button: ["Ok"], isActionRequired: false)
                                    }
                                    
                                }
                            }
                        }else{
                            
                            weakSelf?.showAlert(title: "sorry", message: AlertMessages.NOTADICTIONARYMESSAGE , withTag: 101, button:  ["Ok"], isActionRequired: false)
                            return
                        }
                        
                    }catch{
                        
                        weakSelf?.showAlert(title: "sorry", message: AlertMessages.MODELERRORMESSAGE , withTag: 101, button:  ["Ok"], isActionRequired: false)
                        return
                    }
                    
                } else{
                    
                    //Success false
                    weakSelf?.showAlert(title: APP_NAME, message: AlertMessages.UnidetifiedErrorMessage, withTag: 101, button: ["Ok"], isActionRequired: false)
                    
                }
                
                if self.refreshControl.isRefreshing{
                    self.refreshControl.endRefreshing()
                }
                
                
            }
            
        }
    }
    @IBAction func removeFromWishList(_ sender: Any){
        delete()
    }
    // MARK: - REMOVE API FOR SAVED STREAM
    func delete(){
        
        var params : [String:Any] = [:]
        
        
        guard let IdTodelete = deleteID else{

            self.showAlert(title: "Error", message: "Id to delete is not found. Try again", withTag: 101, button: ["Ok"], isActionRequired: false)
            return
        }
        
    
        APICall.callRemoveFromWishListAPI(params, header: [ : ], batchID: IdTodelete) { (sucess, result, error) in
            
            DispatchQueue.main.async {
                
                weak var weakSelf = self
                
                ProgressHUD.hideProgressHUD()
                
                if (error != nil) {
                    weakSelf?.showAlert(title: "Error", message: (error!.localizedDescription), withTag: 101, button: ["Ok"], isActionRequired: false)
                    return
                }
                
                if sucess {
                    
                    do {
                        let jsonData = try JSONSerialization.jsonObject(with: result!, options: .mutableContainers)
                        
                        if let responseDict = (jsonData as? NSDictionary){
                            
                            if let statusCode = responseDict.object(forKey: "status"), (statusCode is Int) {
                                
                                if (statusCode as! Int) == 200 {
                                    
                                    var msg = "Removed successfully"
                                    
                                    if let message = responseDict.object(forKey: "message") as? String {
                                        msg = message
                                    }
                                    weakSelf?.showAlert(title: APP_NAME, message: msg, withTag: 110, button: ["Ok"], isActionRequired: true)
                                    
                                    weakSelf?.removeButton.isHidden = true
                                    
                                    
                                } else {
                                    var errStr = ""
                                    if let errormessage = (jsonData as? NSDictionary)?.object(forKey:"error") {
                                        if (errormessage is NSDictionary){
                                            if let errorDescription = (errormessage as? NSDictionary)?.object(forKey: "error_description"),!(errorDescription is NSNull){
                                                if (errorDescription is String){
                                                    errStr = errorDescription as! String
                                                    
                                                }
                                            }else {
                                                if let errorMessage = (errormessage as? NSDictionary)?.object(forKey: "message"),!(errorMessage is NSNull){
                                                    if (errorMessage is String){
                                                        errStr = errorMessage as! String
                                                        
                                                    }
                                                }else{
                                                    errStr = AlertMessages.UnidetifiedErrorMessage
                                                }
                                                
                                            }
                                            weakSelf?.showAlert(title: APP_NAME, message:errStr, withTag: 101, button: ["Ok"], isActionRequired: false)
                                        }
                                        
                                    }else{
                                        weakSelf?.showAlert(title: APP_NAME, message:  AlertMessages.UnidetifiedErrorMessage, withTag: 101, button: ["Ok"], isActionRequired: false)
                                    }
                                    
                                }
                            }
                        }else{
                            
                            weakSelf?.showAlert(title: "sorry", message: AlertMessages.NOTADICTIONARYMESSAGE , withTag: 101, button:  ["Ok"], isActionRequired: false)
                            return
                        }
                        
                    }catch{
                        
                        weakSelf?.showAlert(title: "sorry", message: AlertMessages.MODELERRORMESSAGE , withTag: 101, button:  ["Ok"], isActionRequired: false)
                        return
                    }
                    
                } else{
                    
                    //Success false
                    weakSelf?.showAlert(title: APP_NAME, message: AlertMessages.UnidetifiedErrorMessage, withTag: 101, button: ["Ok"], isActionRequired: false)
                    
                }
                
                
            }
            
        }
    }
    override func alertButtonAction(alertTag: Int, btnTitle: String) {
        
        if alertTag == 110  {
            if btnTitle == "Ok" {
                
                self.navigationController?.popViewController(animated: true)
            }
        }
    }
}

/*
 // MARK:- Collection Delegates

extension WishListDetailViewController : UICollectionViewDelegateFlowLayout {
    //1
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let itemSide: CGFloat = (collectionView.bounds.width)
        return CGSize(width: itemSide , height: collectionView.frame.height ) // + ( itemSide * 0.093 ))
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        
        return UIEdgeInsetsMake(0, 0, 0, 0)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat{
        
        
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat{
        
        return 0
    }
}
extension WishListDetailViewController: UICollectionViewDelegate , UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if let array = batchArray {
            return array.count
        }
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CourseDetailCollectionViewCellID", for: indexPath) as! CourseDetailCollectionViewCell
        
        
        cell.updateConstraintsIfNeeded()
        cell.layoutIfNeeded()
        if let tempbatchArray = batchArray {
            
            if let dict = tempbatchArray[indexPath.row] as?  NSDictionary {
                cell.setCellData(dict: dict)
            }
            
        }
        return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        
    }
    
}*/

