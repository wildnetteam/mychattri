//
//  StreamSelectionViewController.swift
//  MyChhatri
//
//  Created by Arpana on 24/07/18.
//  Copyright © 2018 Anshul. All rights reserved.
//

import UIKit
//import TTGTagCollectionView

class StreamSelectionViewController: UIViewController  {
    
    @IBOutlet weak var collectionView: UICollectionView!
    var rowsWhichIsChecked : Int = -1

    @IBOutlet weak var headerImgView: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblSubTitle: UILabel!

    @IBOutlet weak var btnReloadBack: UIButton!
    var controllerHeading : String? // To display which stream is selected, get heading from previous screen
    var controllerSubHeading : String? // To display which stream subtitle
    var controllerHeadingImageURL : String?
//    @IBOutlet weak var textTagCollectionView1: TTGTextTagCollectionView!
    
    var streamId:String?
    var firstCategoryArray: NSArray?
    var storedParentIds : NSMutableArray  = []// To store parent ids to fetch sublisting Ex: 1/2/2
    
    var storedParentIdsToSendToNextScreen : NSArray?
    
    var streamCategoryObject : NSDictionary?
    
    var tempStreamCategoryObject: NSArray? //StreamCategory?  //to store value temprorely
    

    override func viewDidLoad() {
        
        super.viewDidLoad()
        lblTitle.text = controllerHeading
        lblSubTitle.text = controllerSubHeading
        
        
        if let imgUrl = controllerHeadingImageURL as? String {
            
            let imageFinalUrl = "\(URLS.IMAGE_BASE_URL)" + "\(imgUrl)"
            
            headerImgView.sd_setIndicatorStyle(.gray)
            headerImgView.sd_setShowActivityIndicatorView(true)
            headerImgView.sd_setImage(with: URL(string: imageFinalUrl), completed: { (image, error, type, url) in
                if error == nil{
                    self.headerImgView.image = image
                }
                self.headerImgView.sd_setShowActivityIndicatorView(false)
            })
            
        }
        
        
//        if UserModel.getUserType() == 1 {
//
//            lblInstituteType.text = "School"
//        }else if UserModel.getUserType() == 2{
//            lblInstituteType.text = "College"
//
//        }else{
//            print("instituteType:\(String(describing:  UserModel.getUserType()))")
//        }
        
       // self.automaticallyAdjustsScrollViewInsets = false

        
        /* NOt in use now
        // Style1
         textTagCollectionView1.delegate = self
         textTagCollectionView1.showsVerticalScrollIndicator = false
        let config: TTGTextTagConfig? = textTagCollectionView1.defaultConfig
        
        config?.tagTextFont = UIFont(name: "Amble-Regular", size: 16.0)
        config?.tagTextColor = .black
        config?.tagSelectedTextColor = .white
        config?.tagBackgroundColor = .white
        config?.tagSelectedBackgroundColor = UIColor(red:0, green:0.47, blue:0.7, alpha:1)
        textTagCollectionView1.horizontalSpacing = 10.0
        textTagCollectionView1.verticalSpacing = 15.0
        config?.tagBorderColor =  UIColor(red:0.9, green:0.9, blue:0.9, alpha:1)
        config?.tagSelectedBorderColor = .clear
        config?.tagBorderWidth = 1
        config?.tagSelectedBorderWidth = 1
        config?.tagShadowColor = UIColor.black
        config?.tagShadowOffset = CGSize(width: 0, height: 0.3)
        config?.tagShadowOpacity = 0.3
        config?.tagShadowRadius = 0.5
        config?.tagCornerRadius = 3
        // Change alignment
        textTagCollectionView1.alignment = .left*/
        getCategories()
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.navigationBar.isHidden = true
        self.navigationController?.navigationBar.topItem?.title = controllerHeading
        //change the status bar color to clear so that imageview will be hidden 20Px by status bar
        let statusBar: UIView = UIApplication.shared.value(forKey: "statusBar") as! UIView
        statusBar.backgroundColor = .clear
        if(collectionView != nil) {
            rowsWhichIsChecked = -1
            collectionView.reloadData()
        }
       
        if storedParentIds.count >= 1{
            btnReloadBack.isHidden = false
        }else{
          btnReloadBack.isHidden = true
        }

    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
        super.viewWillDisappear(true)
        
        if storedParentIds.count == 1 {
            //In case of navigate from category itself don't keep the id stored, in case of subcategory store values
        storedParentIds.removeAllObjects()
        }

    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(false)
    }
    
    // MARK: - To get listing of Categories -

    func getCategories()  {
        
        let params : [String:Any] = [:]
        
        APICall.callCategoryAPI(params, header: [ : ], streamID: streamId!) { (sucess, result, error) in
            
            DispatchQueue.main.async {
                
                weak var weakSelf = self
                
                if (error != nil) {
                    
                    weakSelf?.showAlert(title: "Error", message: (error!.localizedDescription), withTag: 101, button: ["Ok"], isActionRequired: false)
                    return
                }
                if sucess {
                    
                    do {
                        
                        let jsonData = try JSONSerialization.jsonObject(with: result!, options: .mutableContainers)
                        
                        if (jsonData is NSDictionary){
                            
                            if let statusCode = (jsonData as! NSDictionary).object(forKey: "status"), (statusCode is Int) {
                                
                                if (statusCode as! Int) == 200 {
                                    
                                    //sucessfully retrieved data
                                    if let dataDict = (jsonData as! NSDictionary).object(forKey: "data") as? NSDictionary{
                                        
                                        weakSelf?.streamCategoryObject = NSDictionary(dictionary: dataDict)
                                        //It will have bothn categorie
                                        
//                                        if let StremTitle = dataDict.object(forKey: "description") as? String  {
//
//                                            weakSelf?.lblSubTitle.text = StremTitle
//
//                                        }
                                        if let catTempArray = dataDict.object(forKey: "categories") as? NSArray{
                                            weakSelf?.firstCategoryArray = NSArray(array:catTempArray)
                                        }
                                    }else{
                                        weakSelf?.showAlert(title: "sorry", message: AlertMessages.NoDataKeyFoundInResponse , withTag: 101, button:  ["Ok"], isActionRequired: false)
                                    }
                                }
                                
                            }else{
                                //Some other status code{
                                
                                var errStr = ""
                                if let errormessage = (jsonData as? NSDictionary)?.object(forKey:"error") {
                                    if (errormessage is NSDictionary){
                                        if let errorDescription = (errormessage as? NSDictionary)?.object(forKey: "error_description"),!(errorDescription is NSNull){
                                            if (errorDescription is String){
                                                errStr = errorDescription as! String
                                                
                                            }
                                        }else {
                                            if let errorMessage = (errormessage as? NSDictionary)?.object(forKey: "message"),!(errorMessage is NSNull){
                                                if (errorMessage is String){
                                                    errStr = errorMessage as! String
                                                    
                                                }
                                            }else{
                                                errStr = "Something went wrong"
                                            }
                                            
                                        }
                                        weakSelf?.showAlert(title: APP_NAME, message:errStr, withTag: 101, button: ["Ok"], isActionRequired: false)
                                    }
                                    
                                }else{
                                    weakSelf?.showAlert(title: APP_NAME, message: AlertMessages.UnidetifiedErrorMessage, withTag: 101, button: ["Ok"], isActionRequired: false)
                                }
                            }
                            
                        }else{
                            //not a dictionary message
                            weakSelf?.showAlert(title: "sorry", message: AlertMessages.NOTADICTIONARYMESSAGE , withTag: 101, button:  ["Ok"], isActionRequired: false)
                        }
                        
                    } catch  {
                        
                        weakSelf?.showAlert(title: "sorry", message: AlertMessages.MODELERRORMESSAGE , withTag: 101, button:  ["Ok"], isActionRequired: false)
                        return
                    }
                }
                else{
                    //Alert not sucessfull
                    weakSelf?.showAlert(title: APP_NAME, message: "could not get response from server", withTag: 101, button: ["Ok"], isActionRequired: false)
                }
//                if weakSelf?.streamCategoryObject?.error != nil {
//
//                    //there is error
//                    weakSelf?.showAlert(title: APP_NAME, message: (weakSelf?.streamCategoryObject?.error?.message!)!, withTag: 101, button: ["Ok"], isActionRequired: false)
//                }
//                else{
//                    if weakSelf?.streamCategoryObject?.data?.categories?.count == 0 {
//
//                        //No categories option available
//                        weakSelf?.showAlert(title: APP_NAME, message: "No data found", withTag: 101, button: ["Ok"], isActionRequired: false)
//                        return
//                    }
                
                    //Add titles to tag Array from streamCategoryObject?.data?.categories
                    let titleArray = NSMutableArray()
                    
//                    if let StremTitle = weakSelf?.streamCategoryObject?.data?.description {
//
//                        weakSelf?.lblSubTitle.text = StremTitle
//
//                    }
                
                guard let categoryDetailsArray = weakSelf?.firstCategoryArray as? NSArray else{
                    return
                }
                
                if weakSelf?.firstCategoryArray?.count == 0{
                    weakSelf?.btnReloadBack.isHidden = true

                }
                weakSelf?.collectionView.reloadData()
            
                for categoryDetails in categoryDetailsArray {


                    if let tit = (categoryDetails as? NSDictionary)?.object(forKey: "title")  {
                            titleArray.add(tit)
                        }
                    }
//
//                    // Set tags{
//                    weakSelf?.textTagCollectionView1.addTags(titleArray as! [String])
//                    weakSelf?.textTagCollectionView1.reload()
//                }
            }
        }
        
    
    }
    
    // MARK: - To get sub levels of Categories -

    @IBAction func viewMoreAction(_ sender: Any) {
        
        if controllerSubHeading != nil {
            if((controllerSubHeading?.count)! > 100) {
                let popOverVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "sbPopUpID") as! PopUpViewController
                self.addChildViewController(popOverVC)
                popOverVC.streamDecription = controllerSubHeading ?? ""
                popOverVC.view.frame = self.view.frame
                self.view.addSubview(popOverVC.view)
                popOverVC.didMove(toParentViewController: self)
            }
        }
    }
    
    @IBAction func backButtonPressed(_ sender: Any) {
        
        storedParentIds.removeAllObjects()
        self.navigationController?.popToRootViewController(animated: true)
    }

    @IBAction func reloadBack(_ sender: Any){
        
        if(storedParentIds.count > 0){
            if storedParentIds.count == 1{
                btnReloadBack.isHidden = true
            }
            storedParentIds.removeLastObject()
            getsubCategory(parentId: storedParentIds as! Array<String>)
            
        }
    }
    func getsubCategory(parentId: Array< String > )  {
        
        //"0","6","18"
        let params : [String:Any] = [:]
        
        self.tempStreamCategoryObject = nil
        
        APICall.callSubCategoryAPI( params, header: [ : ], streamID: streamId!, catIdArray: parentId) { (sucess, result, error) in
            
            weak var weakSelf = self
            
            DispatchQueue.main.async{
                
                if (error != nil) {
                    
                    weakSelf?.showAlert(title: "Error", message: (error!.localizedDescription), withTag: 101, button: ["Ok"], isActionRequired: false)
                    weakSelf?.storedParentIds.removeLastObject()
                    
                    return
                }
                if sucess {
                    
                    do {
                        
                        let jsonData = try JSONSerialization.jsonObject(with: result!, options: .mutableContainers)
                        
                        if (jsonData is NSDictionary){
                            
                            if let statusCode = (jsonData as! NSDictionary).object(forKey: "status"), (statusCode is Int) {
                                
                                if (statusCode as! Int) == 200 {
                                    
                                    //sucessfully retrieved data
                                    if let dataDict = (jsonData as! NSDictionary).object(forKey: "data") as? NSDictionary{
                                        
                                        //It will have bothn categories
                                        
                                        
                                        if let catTempArray = dataDict.object(forKey: "categories") as? NSArray{
                                            weakSelf?.tempStreamCategoryObject = NSArray(array:catTempArray)
                                            
                                            
                                        }
                                    }else{
                                        weakSelf?.showAlert(title: "sorry", message: AlertMessages.NoDataKeyFoundInResponse , withTag: 101, button:  ["Ok"], isActionRequired: false)
                                    }
                                    
                                }
                                
                            }else{
                                
                                weakSelf?.storedParentIds.removeLastObject()
                                
                                var errStr = ""
                                if let errormessage = (jsonData as? NSDictionary)?.object(forKey:"error") {
                                    if (errormessage is NSDictionary){
                                        if let errorDescription = (errormessage as? NSDictionary)?.object(forKey: "error_description"),!(errorDescription is NSNull){
                                            if (errorDescription is String){
                                                errStr = errorDescription as! String
                                                
                                            }
                                        }else {
                                            if let errorMessage = (errormessage as? NSDictionary)?.object(forKey: "message"),!(errorMessage is NSNull){
                                                if (errorMessage is String){
                                                    errStr = errorMessage as! String
                                                    
                                                }
                                            }else{
                                                errStr = "Something went wrong"
                                            }
                                            
                                        }
                                        weakSelf?.showAlert(title: APP_NAME, message:errStr, withTag: 101, button: ["Ok"], isActionRequired: false)
                                    }
                                    
                                }else{
                                    weakSelf?.showAlert(title: APP_NAME, message: AlertMessages.UnidetifiedErrorMessage, withTag: 101, button: ["Ok"], isActionRequired: false)
                                }
                            }
                            
                        }else{
                            //not a dict
                        }
                    }
                    catch  {
                        
                        weakSelf?.showAlert(title: "sorry", message: AlertMessages.MODELERRORMESSAGE , withTag: 101, button:  ["Ok"], isActionRequired: false)
                        weakSelf?.storedParentIds.removeLastObject()
                        return
                    }
                }
                else{
                    //Alert not sucessfull
                    weakSelf?.showAlert(title: APP_NAME, message: "could not get response from server", withTag: 101, button: ["Ok"], isActionRequired: false)
                    weakSelf?.storedParentIds.removeLastObject()
                }
                
                
                
                // Clear the previous listing and Reload the next level data
                /*    weakSelf?.streamCategoryObject = nil
                 weakSelf?.textTagCollectionView1.removeAllTags()
                 weakSelf?.streamCategoryObject =   weakSelf?.tempStreamCategoryObject */
                guard let arrayContainedTemp = weakSelf?.tempStreamCategoryObject else{
                    return
                }
                if  arrayContainedTemp.count != 0 {
                    weakSelf?.firstCategoryArray = nil
                  //  weakSelf?.textTagCollectionView1.removeAllTags()
                    weakSelf?.firstCategoryArray =   weakSelf?.tempStreamCategoryObject
                    
                    //Add titles to tag Array from streamCategoryObject?.data?.categories
                    let titleArray = NSMutableArray()
                    
                    
                    guard let categoryDetailsArray = weakSelf?.firstCategoryArray else{
                        return
                    }
                    if categoryDetailsArray.count > 0 &&   weakSelf?.storedParentIds.count == 0 {
                        weakSelf?.btnReloadBack.isHidden = true
                    }else if categoryDetailsArray.count > 0  &&   (weakSelf?.storedParentIds.count)! >= 1 {
                        weakSelf?.btnReloadBack.isHidden = false

                    }
                    weakSelf?.collectionView.reloadData()

                }
                
            }
        }
    }

    func getNextLevelSelection(subjectDetails: NSDictionary )  {
        
        guard let iDArry = storedParentIds as? NSMutableArray else{
            
             self.showAlert(title: "", message: "could not get StreamID ", withTag: 101, button: ["Ok"], isActionRequired: false)
            
        }
        
        if streamId != nil {
            NavigationManager.moveToSavedSteamDetailsBaseViewControllerIfNotExists(kAppDelegate.menuNavController!, rootCategoryID: streamId as? String ,otherIDsArray: iDArry as! [String])
            
        }else{
            
            self.showAlert(title: "", message: "could not steamID", withTag: 101, button: ["Ok"], isActionRequired: false)
        }
        
    }
    
//    func getFinalStream(parentId: Array< String > )  {
//
//        //"0","6","18"
//        let params : [String:Any] = [:]
//
//        var streamId = ""
//        if storedParentIdsToSendToNextScreen?.count  != 0 {
//            if let streamID =  storedParentIdsToSendToNextScreen?[0] as? String{
//                streamId = streamID
//            }
//        }
//
//        APICall.callSubCategoryAPI( params, header: [ : ], streamID: streamId!, catIdArray: [streamId]) { (sucess, result, error) in
//
//            weak var weakSelf = self
//
//            DispatchQueue.main.async{
//
//                if (error != nil) {
//
//                    weakSelf?.showAlert(title: "Error", message: (error!.localizedDescription), withTag: 101, button: ["Ok"], isActionRequired: false)
//
//                    return
//                }
//                if sucess {
//
//                    do {
//
//                        let jsonData = try JSONSerialization.jsonObject(with: result!, options: .mutableContainers)
//                        print(jsonData)
//
//                        if (jsonData is NSDictionary){
//
//                            if let statusCode = (jsonData as! NSDictionary).object(forKey: "status"), (statusCode is Int) {
//
//                                if (statusCode as! Int) == 200 {
//
//                                    //sucessfully retrieved data
//                                    if let dataDict = (jsonData as! NSDictionary).object(forKey: "data") as? NSDictionary{
//
//                                      print(dataDict)
//
//
//                                        if let catTempArray = dataDict.object(forKey: "categories") as? NSArray{
//                                            print(catTempArray)
//
//
//                                        }
//                                        weakSelf?.collectionView.reloadData()
//                                    }else{
//                                        weakSelf?.showAlert(title: "sorry", message: AlertMessages.NoDataKeyFoundInResponse , withTag: 101, button:  ["Ok"], isActionRequired: false)
//                                    }
//
//                                }
//
//                            }else{
//
//
//                                var errStr = ""
//                                if let errormessage = (jsonData as? NSDictionary)?.object(forKey:"error") {
//                                    if (errormessage is NSDictionary){
//                                        if let errorDescription = (errormessage as? NSDictionary)?.object(forKey: "error_description"),!(errorDescription is NSNull){
//                                            if (errorDescription is String){
//                                                errStr = errorDescription as! String
//
//                                            }
//                                        }else {
//                                            if let errorMessage = (errormessage as? NSDictionary)?.object(forKey: "message"),!(errorMessage is NSNull){
//                                                if (errorMessage is String){
//                                                    errStr = errorMessage as! String
//
//                                                }
//                                            }else{
//                                                errStr = "Something went wrong"
//                                            }
//
//                                        }
//                                        weakSelf?.showAlert(title: APP_NAME, message:errStr, withTag: 101, button: ["Ok"], isActionRequired: false)
//                                    }
//
//                                }else{
//                                    weakSelf?.showAlert(title: APP_NAME, message: AlertMessages.UnidetifiedErrorMessage, withTag: 101, button: ["Ok"], isActionRequired: false)
//                                }
//                            }
//
//                        }else{
//                            //not a dict
//                        }
//                    }
//                    catch  {
//
//                        weakSelf?.showAlert(title: "sorry", message: AlertMessages.MODELERRORMESSAGE , withTag: 101, button:  ["Ok"], isActionRequired: false)
//                        return
//                    }
//                }
//                else{
//                    //Alert not sucessfull
//                    weakSelf?.showAlert(title: APP_NAME, message: "could not get response from server", withTag: 101, button: ["Ok"], isActionRequired: false)
//                }
//
//            }
//        }
//    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}

/*
//Selected text color UIColor(red:0.44, green:0.46, blue:0.5, alpha:1)
extension StreamSelectionViewController :  TTGTextTagCollectionViewDelegate{


        func textTagCollectionView(_ textTagCollectionView: TTGTextTagCollectionView!, didTapTag tagText: String!, at index: UInt, selected: Bool, tagConfig config: TTGTextTagConfig!) {
            
            print ("Tap tag: \(tagText ?? ""), at: \(index), selected: \(selected)")
            
            guard let cateArray = firstCategoryArray else{
                return
            }
            
            if let categoryValue = cateArray[Int(index)] as? NSDictionary {
                
                guard categoryValue.object(forKey: "id") != nil else {
                    return
                }
                
                if categoryValue.object(forKey: "child_count") as? Int == 0 {
                    
                    //we need to go to next level
                    if let temId = categoryValue.object(forKey: "id") as? Int{
                        
                        storedParentIds.add(String(temId))
                        
                        
                    }else  if let temId = categoryValue.object(forKey: "id") as? String{
                        
                        storedParentIds.add(temId)
                        
                    }
                    
                    storedParentIdsToSendToNextScreen = NSArray(array: storedParentIds as! [String], copyItems: true)
                    
                    self.getNextLevelSelection(subjectDetails: (self.streamCategoryObject)!)
                    
                    //check if it is type of string in else statement in case of nil
                }else  if categoryValue.object(forKey: "child_count") as? String == "0" {
                    //we need to go to next level
                    if let temId = categoryValue.object(forKey: "id") as? Int{
                        
                        storedParentIds.add(String(temId))
                        
                        
                    }else  if let temId = categoryValue.object(forKey: "id") as? String{
                        
                        storedParentIds.add(temId)
                        
                    }
                  
                    storedParentIdsToSendToNextScreen = NSArray(array: storedParentIds as! [String], copyItems: true)
                    
                    self.getNextLevelSelection(subjectDetails: (self.streamCategoryObject)!)
                    
                }else{
                    
                    if let temId = categoryValue.object(forKey: "id") as? Int{
                        
                        storedParentIds.add(String(temId))
                        
                        
                    }else  if let temId = categoryValue.object(forKey: "id") as? String{
                        
                        storedParentIds.add(temId)
                        
                    }
                    //Check if parent category has child categories or not
                    //If it has child cetegories , reload data else go to next page
                    
                    getsubCategory(parentId: storedParentIds as! Array<String>)
                }
            }else{
                
                print("No data found at this index")
//                //It does not have sub levels go to next screen

            }

        }
    
        func textTagCollectionView(_ textTagCollectionView: TTGTextTagCollectionView?, updateContentSize contentSize: CGSize) {
            if let aView = textTagCollectionView {
                print("text tag collection: \(aView) new content size: \(NSStringFromCGSize(contentSize))")
            }
 }
 
    }*/
    


extension StreamSelectionViewController : UICollectionViewDelegateFlowLayout {
    //1
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let itemSide: CGFloat = (collectionView.bounds.width) / 2
        return CGSize(width: itemSide , height: itemSide - ( itemSide * 0.06 ))
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        
        return UIEdgeInsetsMake(0, 0, 0, 0)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat{
        
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat{
        return 0
    }
}


extension StreamSelectionViewController: UICollectionViewDelegate , UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if let array = firstCategoryArray {
            
            return array.count
            
        }
        return 0
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SavedStreamCollectionViewCellID", for: indexPath) as! SavedStreamCollectionViewCell
        
        
        
        if let val : NSDictionary = self.firstCategoryArray?[indexPath.row] as? NSDictionary {
            
            if let titleText =  val.object(forKey: "title") as? String {
                
                cell.lblTitle.text = titleText
            }
            if let captionText =  val.object(forKey: "caption_text") as? String {
                
                cell.lblDescription.text = captionText
            }
            
            if let imgUrl =  val.object(forKey:"banner_image") as? String {
                
                let imageFinalUrl = "\(URLS.IMAGE_BASE_URL)" + "\(imgUrl)"
                
                cell.imgView.sd_setIndicatorStyle(.gray)
                cell.imgView.sd_setShowActivityIndicatorView(true)
                cell.imgView.sd_setImage(with: URL(string: imageFinalUrl), completed: { (image, error, type, url) in
                    if error == nil{
                        cell.imgView.image = image
                    }else{
                        cell.imgView.image = #imageLiteral(resourceName: "Placeholder")
                    }
                    cell.imgView.sd_setShowActivityIndicatorView(false)
                })
                
            }else{
                //No image url
               // cell.imgView.image = nil
            }
            
            if let idText =  val.object(forKey: "id") as? String {
                cell.btnSelection.setTitle(idText, for: .normal)
                
            } else if let idAsInt =  val.object(forKey: "id") as? Int {
                cell.btnSelection.setTitle(String(idAsInt), for: .normal)
                
            }
            cell.btnSelection.setTitleColor(.clear, for: .normal)
            
            
        }else{
            
        }
        
        
        DispatchQueue.main.async {
            
            if(self.rowsWhichIsChecked == indexPath.row){
                
                // cell.btnSelection.setImage(#imageLiteral(resourceName: "Selected_icon"), for: .normal)
                cell.backGroundWhiteView.backgroundColor = UIColor(red:0, green:0.47, blue:0.7, alpha:1)
                cell.lblDescription.textColor = .white
                cell.lblTitle.textColor = .white
                
            }else{
                
                //   cell.btnSelection.setImage(#imageLiteral(resourceName: "Unselected_icon"), for: .normal)
                cell.backGroundWhiteView.backgroundColor = .white// UIColor(red:0.89, green:0.91, blue:0.94, alpha:1)
                cell.lblDescription.textColor = .black
                // cell.lblTitle.textColor = .black
                
              //  let randomColor: UIColor = .random  // r 0,835 g 0,0 b 1,0 a
                //cell.lblTitle.textColor = randomColor
                
            }
            
            cell.backGroundWhiteView.layer.cornerRadius = (cell.shodowView.frame.size.width * 0.04)
            cell.backGroundWhiteView.layer.masksToBounds = true
            cell.imgView.layer.cornerRadius = (cell.imgView.frame.size.width / 2)
            cell.imgView.layer.masksToBounds = true
            cell.shodowView.layer.shadowOpacity = 1;
            cell.shodowView.layer.masksToBounds = false
            cell.shodowView.layer.shadowRadius = 20
            cell.shodowView.layer.shadowColor = UIColor(red:0.8, green:0.8, blue:0.8, alpha:0.5).cgColor
            cell.shodowView.layer.shadowOffset = CGSize(width: 0.0, height: cell.contentView.frame.height * 0.04)
            cell.updateConstraintsIfNeeded()
            cell.layoutIfNeeded()
        }
        return cell
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        guard let cateArray = firstCategoryArray else{
            return
        }
        
        if let categoryValue = cateArray[indexPath.row] as? NSDictionary {
            
            guard categoryValue.object(forKey: "id") != nil else {
                return
            }
            
            if categoryValue.object(forKey: "child_count") as? Int == 0 {
                
                //we need to go to next level
                if let temId = categoryValue.object(forKey: "id") as? Int{
                    
                    storedParentIds.add(String(temId))
                    
                    
                }else  if let temId = categoryValue.object(forKey: "id") as? String{
                    
                    storedParentIds.add(temId)
                    
                }
                
                storedParentIdsToSendToNextScreen = NSArray(array: storedParentIds as! [String], copyItems: true)
                
                if let titleText =  categoryValue.object(forKey: "title") as? String {
                    
                    //   navigationTitleText = titleText
                    kAppDelegate.menuNavController?.navigationBar.accessibilityLabel = titleText
                }
                self.getNextLevelSelection(subjectDetails: (self.streamCategoryObject)!)
                
                //check if it is type of string in else statement in case of nil
            }else  if categoryValue.object(forKey: "child_count") as? String == "0" {
                //we need to go to next level
                if let temId = categoryValue.object(forKey: "id") as? Int{
                    
                    storedParentIds.add(String(temId))
                    
                    
                }else  if let temId = categoryValue.object(forKey: "id") as? String{
                    
                    storedParentIds.add(temId)
                    
                }
                
                storedParentIdsToSendToNextScreen = NSArray(array: storedParentIds as! [String], copyItems: true)
                
                if let titleText =  categoryValue.object(forKey: "title") as? String {
                    
                    //   navigationTitleText = titleText
                    kAppDelegate.menuNavController?.navigationBar.accessibilityLabel = titleText
                }
                
                self.getNextLevelSelection(subjectDetails: (self.streamCategoryObject)!)
                
            }else{
                
                if let temId = categoryValue.object(forKey: "id") as? Int{
                    
                    storedParentIds.add(String(temId))
                    
                    
                }else  if let temId = categoryValue.object(forKey: "id") as? String{
                    
                    storedParentIds.add(temId)
                    
                }
                //Check if parent category has child categories or not
                //If it has child cetegories , reload data else go to next page
                
                getsubCategory(parentId: storedParentIds as! Array<String>)
            }
        }else{
            
            print("No data found at this index")
            //                //It does not have sub levels go to next screen
            
        }

    }
    
    
}

