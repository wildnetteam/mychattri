//
//  AboutUsViewController.swift
//  MyChhatri
//
//  Created by Arpana on 19/09/18.
//  Copyright © 2018 Arpana. All rights reserved.
//

import UIKit

class AboutUsViewController: UIViewController {

    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var descTxt: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        self.navigationController?.navigationBar.isHidden = false
        kAppDelegate.menuNavController = self.navigationController
        
        // self.navigationController?.navigationBar.topItem?.title = "My Wishlist"
        
        kAppDelegate.setNavigationAndStatusAppearance()
        let statusBar: UIView = UIApplication.shared.value(forKey: "statusBar") as! UIView
        statusBar.backgroundColor = ColorConstants.appBackGroundCOLOR()
        UIApplication.shared.statusBarStyle = .lightContent
        setNeedsStatusBarAppearanceUpdate()
        
        getAboutUsInfo()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func getAboutUsInfo()  {
        
        
        let params : [String:Any] = [:]
        
        
            ProgressHUD.showProgressHUDWithLabel(label: "Loading...")
      
        
        APICall.callAboutUsAPI(params, header: [ : ]) { (sucess, result, error) in
            
            DispatchQueue.main.async {
                
                weak var weakSelf = self
                
                
                ProgressHUD.hideProgressHUD()
                
                if (error != nil) {
                    
                    weakSelf?.showAlert(title: "Error", message: (error!.localizedDescription), withTag: 101, button: ["Ok"], isActionRequired: false)
                    return
                }
                if sucess {
                    
                    do {
                        
                        let jsonData = try JSONSerialization.jsonObject(with: result!, options: .mutableContainers)
                        print(jsonData)
                        
                        if (jsonData is NSDictionary){
                            
                            if let statusCode = (jsonData as! NSDictionary).object(forKey: "status"), (statusCode is Int) {
                                
                                if (statusCode as! Int) == 200 {
                                    
                                    //sucessfully retrieved data
                                    if let dataDict = (jsonData as! NSDictionary).object(forKey: "data") as? NSDictionary{
                                        print(dataDict)
                                        
                                      //  if (dataDict.value(forKey: "description") as? String) != nil {
                                            
                                            let headingString = NSMutableAttributedString(
                                                string: "Unlocking the Power of Potential",
                                                attributes: [NSAttributedStringKey(rawValue: NSAttributedStringKey.foregroundColor.rawValue):  UIColor(red:0.34, green:0.36, blue:0.4, alpha:1) , NSAttributedStringKey(rawValue: NSAttributedStringKey.font.rawValue): UIFont(name: "Amble-Bold", size: isIphone5 ? 20.0: 22.0)!])
                                            
                                            let subHeadingString = NSMutableAttributedString(
                                                string: "mychatri, ",
                                                attributes: [NSAttributedStringKey(rawValue: NSAttributedStringKey.foregroundColor.rawValue): UIColor(red:0.34, green:0.36, blue:0.4, alpha:1) , NSAttributedStringKey(rawValue: NSAttributedStringKey.font.rawValue): UIFont(name: "Amble-Bold", size: isIphone5 ? 15.0 :18.0)!])
                                            
                                            let Descrption = NSMutableAttributedString(
                                                string: " an initiative by LCPL (Letz Dream4u Consultants Pvt. Ltd.) a company driven by professionals working with a vision to make a tangible impact when it comes to nurturing skills in a positive and meaningful manner. We are a team of entrepreneurs with a diversified experience in Education, Computer, Automation, Electrical and various other domains. Our mission is to enhance capabilities and inspire growth opportunities through innovative ideas amongst students. ",
                                                attributes: [NSAttributedStringKey(rawValue: NSAttributedStringKey.foregroundColor.rawValue): UIColor(red:0.34, green:0.36, blue:0.4, alpha:1) , NSAttributedStringKey(rawValue: NSAttributedStringKey.font.rawValue): UIFont(name: "Amble-Regular", size: isIphone5 ? 12.0 :14.0)!])
                                            
                                            let attrString1 = NSMutableAttributedString(string: "")
                                            attrString1.append(headingString)
                                            attrString1.append(NSAttributedString(string:"\n\n\n"))
                                            attrString1.append(subHeadingString)
                                            attrString1.append(Descrption)
                                            
                                            self.descTxt.attributedText =  attrString1
                                            
                                      //  }
                                        
                                        
                                        var imageFinalUrl =  ""
                                        
                                        if let imageURL = dataDict.value(forKey: "banner_image") {
                                            
                                            weak var weakSelf = self
                                            
                                            imageFinalUrl = "\(URLS.IMAGE_BASE_URL)" + "\(imageURL)"
                                            
                                            weakSelf?.imgView.sd_setImage(with: URL(string: imageFinalUrl), completed: { (image, error, type, url) in
                                                DispatchQueue.main.async {
                                                    if error == nil{
                                                        weakSelf?.imgView.image = image
                                                        weakSelf?.imgView.transform = CGAffineTransform(scaleX: 0, y: 0)
                                                        UIView.animate(withDuration: 0.50) {
                                                            self.imgView.transform = .identity
                                                        }
                                                    }else{
                                                      weakSelf?.imgView.image = #imageLiteral(resourceName: "Placeholder")
                                                    }
                                                    weakSelf?.imgView.sd_setShowActivityIndicatorView(false)
                                                }
                                            })
                                        }
                                    }
                                }else{
                                    
                                    var errStr = ""
                                    if let errormessage = (jsonData as? NSDictionary)?.object(forKey:"error") {
                                        if (errormessage is NSDictionary){
                                            if let errorDescription = (errormessage as? NSDictionary)?.object(forKey: "error_description"),!(errorDescription is NSNull){
                                                if (errorDescription is String){
                                                    errStr = errorDescription as! String
                                                    
                                                }
                                            }else {
                                                if let errorMessage = (errormessage as? NSDictionary)?.object(forKey: "message"),!(errorMessage is NSNull){
                                                    if (errorMessage is String){
                                                        errStr = errorMessage as! String
                                                        
                                                    }
                                                }else{
                                                    errStr = "Something went wrong"
                                                }
                                                
                                            }
                                            weakSelf?.showAlert(title: APP_NAME, message:errStr, withTag: 101, button: ["Ok"], isActionRequired: false)
                                        }
                                        
                                    }else{
                                        weakSelf?.showAlert(title: APP_NAME, message: AlertMessages.UnidetifiedErrorMessage, withTag: 101, button: ["Ok"], isActionRequired: false)
                                    }
                                }
                            }
                        }else{
                            
                            //mot a dict
                            weakSelf?.showAlert(title: "sorry", message: AlertMessages.NOTADICTIONARYMESSAGE , withTag: 101, button:  ["Ok"], isActionRequired: false)
                            return
                        }
                    }catch  let error as NSError {
                        print(error.localizedDescription)
                        
                        weakSelf?.showAlert(title: "sorry", message: AlertMessages.MODELERRORMESSAGE , withTag: 101, button:  ["Ok"], isActionRequired: false)
                        return
                    }
                }
                else{
                    //Alert not sucessfull
                    weakSelf?.showAlert(title: APP_NAME, message: "could not get response from server", withTag: 101, button: ["Ok"], isActionRequired: false)
                }
                ProgressHUD.hideProgressHUD()
            }
            }
        }

    
    @IBAction func backButtonClicked(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
        
    }
    
}
