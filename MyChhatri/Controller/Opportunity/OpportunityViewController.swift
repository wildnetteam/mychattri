//
//  OpportunityViewController.swift
//  MyChhatri
//
//  Created by Arpana on 08/08/18.
//  Copyright © 2018 Anshul. All rights reserved.
//

import UIKit

class OpportunityViewController: UIViewController {
    
    
    @IBOutlet weak var shadowView: UIView!
    @IBOutlet weak var tableView:UITableView!
    var rootCatID : String?

    // MARK: - MAINTAIN PAGINATION AND WEBINAR LIST
    var pageNumber = 0
    var listlimit = 10 // Define to get maximum data one at a time
    var totalCount : Int?
    
    var opportunityArray : Array<NSDictionary>?
    var offset : Int?
    
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action:
            #selector(OpportunityViewController.getOpportunityList),
                                 for: UIControlEvents.valueChanged)
        refreshControl.tintColor = .black
        
        return refreshControl
    }()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //Add shdow Below page menu
        shadowView.layer.masksToBounds = false
        shadowView.layer.shadowOpacity = 0.4 ;
        shadowView.layer.shadowColor = UIColor.black.cgColor
        shadowView.layer.shadowOffset = CGSize(width: 0.0, height: 4.0)
        offset =  0
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if offset == 0 {
            getOpportunityList()
        }
        self.navigationController?.navigationBar.isHidden = false
        TableViewAnimationKit.show(with: XSTableViewAnimationType(rawValue: 3)!, tableView: self.tableView)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    // MARK:- GET WEB DATA FROM API
    
    @objc func getOpportunityList()  {
        
        if self.refreshControl.isRefreshing{
            offset = 0
        }
        
        let infoDict = NSDictionary(objects: [rootCatID! ,10 ,offset ?? 0], forKeys: ["category_id" as NSCopying,"limit" as NSCopying ,"offset" as NSCopying])
        
        if let userInfoDict = UserModel.getLoginInfo() {
            
            guard ((userInfoDict.object(forKey: "ID")) != nil) else{
                
                self.showAlert(title: "Sorry", message: "User details could not be found , Please logout and login again", withTag: 101, button: ["Ok"], isActionRequired: false)
                return
            }
            
            guard (userInfoDict.object(forKey: "token") as? String) != nil else{
                
                self.showAlert(title: "Sorry", message: "User accessToken could not be found , Please logout and login again", withTag: 101, button: ["Ok"], isActionRequired: false)
                return
            }
        }
        if tableView != nil {
            
            ProgressHUD.showProgressHUDWithLabel(label: "Loading...")
        }
        
        APICall.callOpportunityListAPI([:], header: [ : ], information:infoDict ) { (sucess, result, error) in
            
            DispatchQueue.main.async {
                
                weak var weakSelf = self
                
                ProgressHUD.hideProgressHUD()
                
                if (error != nil) {
                    weakSelf?.showAlert(title: "Error", message: (error!.localizedDescription), withTag: 101, button: ["Ok"], isActionRequired: false)
                    return
                }
                
                if sucess {
                    
                    do {
                        let jsonData = try JSONSerialization.jsonObject(with: result!, options: .mutableContainers)
                        
                        if let responseDict = (jsonData as? NSDictionary){
                            
                            if let statusCode = responseDict.object(forKey: "status"), (statusCode is Int) {
                                
                                if (statusCode as! Int) == 200 {
                                    
                                    //sucessfully retrieved data
                                    if let dataDict = responseDict.object(forKey: "data") as? NSDictionary{
                                        
                                        if let toptalR = dataDict.value(forKey: "totalCount") as? Int{
                                            weakSelf?.totalCount = toptalR
                                        }else if let toptalR = dataDict.value(forKey: "totalCount") as? String{
                                            weakSelf?.totalCount = Int(toptalR)
                                        }
                                        if let tempArray = dataDict.object(forKey: "opportunities") as? Array<NSDictionary> {
                                            
                                            if tempArray.count > 0 {
                                                //store data
                                                if  weakSelf?.offset == 0 {
                                                    self.opportunityArray =  Array()
                                                    
                                                    weakSelf?.opportunityArray = tempArray
                                                }else{
                                                    //Just add to previous array
                                                    weakSelf?.opportunityArray?.append(contentsOf: tempArray)
                                                }
                                                self.pageNumber = self.pageNumber + 1
                                            }else{
                                                //No saved stream found message
                                                weakSelf?.showAlert(title: APP_NAME, message: "You do not have any seminars yet.", withTag: 101, button: ["Ok"], isActionRequired: false)
                                            }
                                        }else {
                                            //
                                            weakSelf?.showAlert(title: APP_NAME, message: "opportunities key not found", withTag: 101, button: ["Ok"], isActionRequired: false)
                                        }
                                    }else{
                                        //No data found error
                                        weakSelf?.showAlert(title: APP_NAME, message:  AlertMessages.NoDataKeyFoundInResponse, withTag: 101, button: ["Ok"], isActionRequired: false)
                                    }
                                    
                                } else {
                                    var errStr = ""
                                    if let errormessage = (jsonData as? NSDictionary)?.object(forKey:"error") {
                                        if (errormessage is NSDictionary) {
                                            if let errorDescription = (errormessage as? NSDictionary)?.object(forKey: "error_description"),!(errorDescription is NSNull){
                                                if (errorDescription is String){
                                                    errStr = errorDescription as! String
                                                    
                                                }
                                            }else {
                                                if let errorMessage = (errormessage as? NSDictionary)?.object(forKey: "message"),!(errorMessage is NSNull){
                                                    if (errorMessage is String){
                                                        errStr = errorMessage as! String
                                                        
                                                    }
                                                }else{
                                                    errStr = AlertMessages.UnidetifiedErrorMessage
                                                }
                                                
                                            }
                                            weakSelf?.showAlert(title: APP_NAME, message:errStr, withTag: 101, button: ["Ok"], isActionRequired: false)
                                        }
                                        
                                    }else{
                                        weakSelf?.showAlert(title: APP_NAME, message:  AlertMessages.UnidetifiedErrorMessage, withTag: 101, button: ["Ok"], isActionRequired: false)
                                    }
                                    
                                }
                            }
                        }else{
                            
                            weakSelf?.showAlert(title: "sorry", message: AlertMessages.NOTADICTIONARYMESSAGE , withTag: 101, button:  ["Ok"], isActionRequired: false)
                            return
                        }
                        
                    }catch{
                        
                        weakSelf?.showAlert(title: "sorry", message: AlertMessages.MODELERRORMESSAGE , withTag: 101, button:  ["Ok"], isActionRequired: false)
                        return
                    }
                    
                } else{
                    
                    //Success false
                    weakSelf?.showAlert(title: APP_NAME, message: AlertMessages.UnidetifiedErrorMessage, withTag: 101, button: ["Ok"], isActionRequired: false)
                    
                }
                
                if self.refreshControl.isRefreshing{
                    self.refreshControl.endRefreshing()
                }
                
                if weakSelf?.tableView != nil {
                    weakSelf?.tableView.reloadData()
                }
            }
            
        }
    }
}
extension OpportunityViewController : UITableViewDelegate, UITableViewDataSource , UIViewControllerTransitioningDelegate{
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section:
        Int) -> Int
    {
        
        if let mArray = opportunityArray {
            return mArray.count
        }else {
            return 0
        }
        
    }
    
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        // Allocates a Table View Cell
        let cell =
            tableView.dequeueReusableCell(withIdentifier: "OpportunityTableViewCellID",
                                          for: indexPath) as! OpportunityTableViewCell
        
        cell.imgView.layer.masksToBounds = true
        cell.imgView.layer.cornerRadius = cell.imgView.frame.height * 0.063
        
        if opportunityArray != nil {
            
            if let tempOpportunityArray = opportunityArray {
                
                if let dict = tempOpportunityArray[indexPath.row] as?  NSDictionary {
                    
                  /*  if let wtime = dict.value(forKey: "created_at") as? String {
                        
                        
                        cell.lblTitle.text =   CalenderStruct.getFormatedDateString(fromDateUTC:  CalenderStruct.getDateFromDateString(forUTC: wTitle, byFormat: CalenderStruct.DATE_TIME_ZONE_FORMAT_LOCAL), inFormat: "dd MMM yyyy")
                    }*/
                    
                    if let wTitle = dict.value(forKey: "title") as? String {
                        
                        cell.lblTitle.text =  wTitle
                        
                    }
                    cell.imgView.sd_setIndicatorStyle(.gray)
                    cell.imgView.sd_setShowActivityIndicatorView(true)
                    
                    var imageFinalUrl =  ""
                    
                    if let imageURL = dict.value(forKey: "banner_image") {
                        
                        imageFinalUrl = "\(URLS.IMAGE_BASE_URL)" + "\(imageURL)"
                        
                        cell.imgView.sd_setImage(with: URL(string: imageFinalUrl), completed: { (image, error, type, url) in
                            DispatchQueue.main.async {
                                if error == nil{
                                    cell.imgView.image = image
                                }else{
                                    cell.imgView.image = #imageLiteral(resourceName: "Placeholder")
                                }
                                cell.imgView.sd_setShowActivityIndicatorView(false)
                            }
                        })
                    }
                    if let wDes = dict.value(forKey: "description") as? String {
                        cell.lblSubTitle.text = wDes.withoutHtml
                    }
                    
                }
                //Reload the next amount of data
                if(indexPath.row ==  (opportunityArray?.count)! - 1 && totalCount! > (opportunityArray?.count)! ){
                    //we are at last index of currently displayed cell
                    offset = (opportunityArray?.count)!
                    //reload more data
                    getOpportunityList()
                }
            }
        }
        cell.selectionStyle = .none
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        
        if opportunityArray != nil {
            
            if let dict = opportunityArray?[indexPath.row] {
                NavigationManager.moveToDetailedViewControllerIfNotExists(kAppDelegate.menuNavController!, detailsArticle:nil, informationDict: dict )
            }
            
        }
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return  100
    }
    
}


