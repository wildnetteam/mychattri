//
//  OpportunityTableViewCell.swift
//  MyChhatri
//
//  Created by Arpana on 08/08/18.
//  Copyright © 2018 Anshul. All rights reserved.
//

import UIKit

class OpportunityTableViewCell: UITableViewCell {
    
    @IBOutlet weak var lblTitle: UILabel!
    
    @IBOutlet weak var imgView: UIImageView!

    @IBOutlet weak var lblSubTitle: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
