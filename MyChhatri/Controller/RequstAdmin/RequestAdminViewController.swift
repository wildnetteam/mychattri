//
//  RequestAdminViewController.swift
//  MyChhatri
//
//  Created by Arpana on 21/09/18.
//  Copyright © 2018 Arpana. All rights reserved.
//

import UIKit

class RequestAdminViewController: UIViewController {
    
    
    @IBOutlet weak var txtSubject:UITextField!
    @IBOutlet weak var txtTopic:UITextField!
    @IBOutlet weak var txtMsg: UITextView!
    @IBOutlet weak var btnSave:UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        btnSave.addGrediant()
        setUPUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        self.navigationController?.navigationBar.isHidden = false
        kAppDelegate.menuNavController = self.navigationController
        
        // self.navigationController?.navigationBar.topItem?.title = "My Wishlist"
        
        kAppDelegate.setNavigationAndStatusAppearance()
        let statusBar: UIView = UIApplication.shared.value(forKey: "statusBar") as! UIView
        statusBar.backgroundColor = ColorConstants.appBackGroundCOLOR()
        UIApplication.shared.statusBarStyle = .lightContent
        setNeedsStatusBarAppearanceUpdate()
        
    }
    
    func setUPUI() {
        
        let viewLefttxtSubject = UIView(frame: CGRect(x: 0, y: 0, width: 8, height: 40))
        txtSubject.leftView = viewLefttxtSubject
        txtSubject.leftViewMode = .always
        
        let viewLefttxtTopic = UIView(frame: CGRect(x: 0, y: 0, width: 8, height: 40))
        txtTopic.leftView = viewLefttxtTopic
        txtTopic.leftViewMode = .always
        
        txtMsg.textContainerInset = UIEdgeInsets(top: 0, left: 5, bottom: 0, right: 0);
    }
    
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func backButtonClicked(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
        
    }
    @IBAction func SubmitButtonClicked(_ sender: Any) {
        
    }
}

