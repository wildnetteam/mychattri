//
//  AllCoursesTableViewCell.swift
//  MyChhatri
//
//  Created by Arpana on 10/09/18.
//  Copyright © 2018 Arpana. All rights reserved.
//

import UIKit

class AllCoursesTableViewCell: UITableViewCell {

    @IBOutlet weak var lblTitle: UILabel!
    
    @IBOutlet weak var buttonViewAll: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
