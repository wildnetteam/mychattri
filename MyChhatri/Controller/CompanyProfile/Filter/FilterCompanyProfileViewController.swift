//
//  FilterCompanyProfileViewController.swift
//  MyChhatri
//
//  Created by Arpana on 28/09/18.
//  Copyright © 2018 Arpana. All rights reserved.
//

import UIKit

@objc  protocol FilterParamDelegate {
    
    @objc optional func passfilterparams(data: [String: Any] )
 
}

class FilterCompanyProfileViewController: UIViewController {
    
    @IBOutlet weak var btnApply : UIButton!
    @IBOutlet weak var  txtRegNo  : UITextField!
    @IBOutlet weak var txtCompanyName : UITextField!
    @IBOutlet weak var txtCourseName : UITextField!
    @IBOutlet weak var txtStatus : UITextField!
    
    
    @IBOutlet weak var txtDatePicker: UITextField!
    @IBOutlet weak var txtEndDatePicker: UITextField!
    let datePicker = UIDatePicker()
    
    
    //*********Picker Setup*********//
    private var picker = UIPickerView()
    private var txtFieldPicker = UITextField()
    private var arr_Picker = [""]
    
    
    fileprivate lazy var arrStudentType = ["enrolled", "completed" , "cancelled"]
    
    weak var filterDelegate: FilterParamDelegate?

    override func viewDidLoad() {
        super.viewDidLoad()
        kAppDelegate.menuNavController = self.navigationController
        btnApply.addGrediant()
        
        // Do any additional setup after loading the view.
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    @IBAction func backButtonClicked(_ sender: Any) {
        
        if filterDelegate != nil {
            
            filterDelegate?.passfilterparams!(data: [:])
            
        }
        
        self.navigationController?.popViewController(animated: true)
        
    }
    
    @IBAction func applyBtnClicked (_ sender: Any) {
        
        
        var params : [String:Any] = [:]
        
        if let userInfoDict = UserModel.getLoginInfo() {
            
            if let  userId = (userInfoDict.object(forKey: "ID") as? String){
                
                params["user_id"]  = userId
                
            }else if let userIded = (userInfoDict.object(forKey: "ID") as? Int){
                
                params["user_id"]  = userIded
            }else{
                
                self.showAlert(title: "Sorry", message: "User details could not be found , Please logout and login again", withTag: 101, button: ["Ok"], isActionRequired: false)
                return
            }
        }
        
        params["order_no"] = txtRegNo.text ?? ""
        params["organization"] = txtCompanyName.text ?? ""
        params["keyword"] = txtCourseName.text ?? ""
        params["status"] = txtStatus.text ?? ""
        params["to_date"] = txtEndDatePicker.text ?? ""
        params["from_date"] = txtDatePicker.text ?? ""

        if filterDelegate != nil {
            
            filterDelegate?.passfilterparams!(data: params)
            
        }
        
        self.navigationController?.popViewController(animated: true)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
extension FilterCompanyProfileViewController : UITextFieldDelegate, UIPickerViewDelegate, UIPickerViewDataSource {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        
        txtFieldPicker = textField

        if textField == txtStatus {

            self.arr_Picker = arrStudentType
            self.showPickerView()
            
        }
        else if  textField == txtDatePicker || textField == txtEndDatePicker {
            self.showDatePicker()
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        if textField == txtStatus {
            
            if textField.text == "" {
                //nothing to do...
            } else {
                
            }
        }
    }
    
    func showDatePicker(){
        //Formate Date
        datePicker.datePickerMode = .date
        
        //ToolBar
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(donedatePicker));
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelDatePicker));
        
        toolbar.setItems([cancelButton,spaceButton,doneButton], animated: false)
        
        txtDatePicker.inputAccessoryView = toolbar
        txtDatePicker.inputView = datePicker
        
        txtEndDatePicker.inputAccessoryView = toolbar
        txtEndDatePicker.inputView = datePicker
        
    }
    
    @objc func donedatePicker(){
        
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        
        if   txtFieldPicker == txtDatePicker {
            txtDatePicker.text = formatter.string(from: datePicker.date)

        }else  if   txtFieldPicker == txtEndDatePicker {
            txtEndDatePicker.text = formatter.string(from: datePicker.date)
            
        }
        self.view.endEditing(true)
    }
    
    @objc func cancelDatePicker(){
        self.view.endEditing(true)
    }

    // Picker Setup
    // UIPickerView
     private func showPickerView() {
        
        picker = UIPickerView(frame: CGRect(x: 0, y: 0, width: screenWidth, height: 216))
        picker.backgroundColor = .white
        picker.showsSelectionIndicator = true
        picker.delegate = self
        picker.dataSource = self
        txtFieldPicker.inputView = picker
        
        let toolBar = UIToolbar()
        toolBar.barStyle = UIBarStyle.default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor.black
        toolBar.sizeToFit()
        
        let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItemStyle.plain, target: self, action: #selector(cancelActionPicker))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.plain, target: self, action: #selector(doneActionPicker))
        
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        txtFieldPicker.inputAccessoryView = toolBar
    }
    
    @objc func doneActionPicker (sender:UIBarButtonItem)
    {
        let tempRow = picker.selectedRow(inComponent: 0)
        if tempRow == 0 {
            self.txtFieldPicker.text = arr_Picker[tempRow]
            //            if self.txtFieldPicker == self.txtGuardianTitle {
            //                registrationModel.guardianTitle = self.txtGuardianTitle.text
            //            }
        }
        txtFieldPicker.resignFirstResponder()
        
        if self.txtFieldPicker == self.txtStatus {
            
            print(self.txtStatus)
            
        }
    }
    
    @objc func cancelActionPicker (sender:UIBarButtonItem)
    {
      //  if txtFieldPicker != txtGuardianTitle {
            txtFieldPicker.text = ""
       // }
        txtFieldPicker.resignFirstResponder()
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return arr_Picker.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return arr_Picker[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        self.txtFieldPicker.text = arr_Picker[row]
    }
    
    
    
}
