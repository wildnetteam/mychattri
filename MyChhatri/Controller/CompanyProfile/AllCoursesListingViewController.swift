//
//  AllCoursesListingViewController.swift
//  MyChhatri
//
//  Created by Arpana on 10/09/18.
//  Copyright © 2018 Arpana. All rights reserved.
//

import UIKit

class AllCoursesListingViewController: UIViewController {

    var courseDetailDictionary: NSDictionary?
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var lblSubTitle: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var imgView: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = false        
        kAppDelegate.menuNavController = self.navigationController
        
        kAppDelegate.setNavigationAndStatusAppearance()
        let statusBar: UIView = UIApplication.shared.value(forKey: "statusBar") as! UIView
        statusBar.backgroundColor = ColorConstants.appBackGroundCOLOR()
        setNeedsStatusBarAppearanceUpdate()
        
        tableView.addObserver(self, forKeyPath: "contentSize", options: .new, context: nil)

        setData()
       }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewWillLayoutSubviews() {
        
             scrollView.contentSize.height =   lblTitle.frame.height + lblSubTitle.frame.height + tableView.contentSize.height + 30
        
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if let obj = object as? UITableView {
            if obj == self.tableView && keyPath == "contentSize" {
                scrollView.contentSize.height =   lblTitle.frame.height + lblSubTitle.frame.height + tableView.contentSize.height + 30
            }
        }
        viewWillLayoutSubviews()
    }
    
    @IBAction func backButtonClicked(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
        
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
//        courseDetailDictionary = nil
        tableView.removeObserver(self, forKeyPath: "contentSize")

    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    func setData(){
        
        guard let detail = courseDetailDictionary else{
            return
        }
        if let userDetailDict = detail.value(forKey: "userdetails") as? NSDictionary{
            
            if let wTitle = userDetailDict.value(forKey: "organization_name") as? String {
                
                
                lblTitle.text = wTitle
            }
            if let wDes = userDetailDict.value(forKey: "description") as? String {
                lblSubTitle.text = wDes == "" ?"No descsription available":wDes.withoutHtml
            }else{
                lblSubTitle.text = "No descsription available"
            }
            
            lblSubTitle.layoutIfNeeded()
            var imageFinalUrl =  ""
            
            if let imageURL = detail.value(forKey: "profile_image") {
                
                weak var weakSelf = self
                
                imageFinalUrl = "\(URLS.IMAGE_BASE_URL)" + "\(imageURL)"
                
                weakSelf?.imgView.sd_setImage(with: URL(string: imageFinalUrl), completed: { (image, error, type, url) in
                    DispatchQueue.main.async {
                        if error == nil{
                            weakSelf?.imgView.image = image
                            
                        }else{
                            weakSelf?.imgView.image = #imageLiteral(resourceName: "Placeholder")
                        }
                        weakSelf?.imgView.sd_setShowActivityIndicatorView(false)
                    }
                })
            }
        }

    }
}
extension AllCoursesListingViewController : UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section:
        Int) -> Int
    {
        if let courseDetail = courseDetailDictionary{
            
            if let courseArray = courseDetail.value(forKey: "course") as? NSArray {
                
                return courseArray.count
                
            }
        }
        return 0
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        // Allocates a Table View Cell
        let cell =
            tableView.dequeueReusableCell(withIdentifier: "AllCoursesTableViewCellID",
                                          for: indexPath) as! AllCoursesTableViewCell
        if courseDetailDictionary != nil {
            
            if let dict = courseDetailDictionary {
                
                
                if let courseArray = dict.value(forKey: "course") as? NSArray {
                    
                    if courseArray.count > 0 {
                        
                        if let courseDetailDict = courseArray.object(at: indexPath.row) as? NSDictionary {
                            
                            if let  title = courseDetailDict.value(forKey: "title")  as? String{
                                
                                
                                cell.lblTitle.text = title
                            }
                        }
                    }                    }
            }
            
        }
        cell.buttonViewAll.cornerRadius = 4
        cell.buttonViewAll.layer.masksToBounds = true
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        if courseDetailDictionary != nil {

            if let dict = courseDetailDictionary {


                if let courseArray = dict.value(forKey: "course") as? NSArray {

                    if courseArray.count > 0 {

                        if let courseDetailDict = courseArray.object(at: indexPath.row) as? NSDictionary {
                           
                            if let cId =  courseDetailDict.value(forKey: "id") as? String{
                                NavigationManager.moveToCourseDetailViewControllerIfNotExists(kAppDelegate.menuNavController!, courseId: cId)
                                
                            }else if let cIdInt =  courseDetailDict.value(forKey: "id") as? Int{
                                NavigationManager.moveToCourseDetailViewControllerIfNotExists(kAppDelegate.menuNavController!, courseId: String(cIdInt))
                            }else{
                                self.showAlert(title: "", message: "courseId could not be found", withTag: 111, button: ["OK"], isActionRequired: false)
                            }
                        }
                    }
                }
            }
        }
    }

    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        
    }
    
}
