//
//  MyPageViewController.swift
//  MyChhatri
//
//  Created by Arpana on 24/07/18.
//  Copyright © 2018 Anshul. All rights reserved.
//

import UIKit

class CompanyProfileListingViewController: UIViewController {

    var countryView:CountryView?

    @IBOutlet weak var tableView: UITableView!
    var rootCatID : String?
    var cityID : String?
    // Declare a variable which stores checked rows. UITableViewCell gets dequeued and restored as you scroll up and down, so it is best to store a reference of rows which has been checked
     var rowsWhichIsChecked : Int = -1
    
    // MARK: - MAINTAIN PAGINATION AND WEBINAR LIST
    // MARK: - MAINTAIN PAGINATION AND WEBINAR LIST
    var pageNumber = 0
    var listlimit = 10 // Define to get maximum data one at a time
    var totalCount : Int?
    
    var companyArray : Array<NSDictionary>?
    
    var offset : Int?
    
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action:
            #selector(CompanyProfileListingViewController.getCompanyList),
                                 for: UIControlEvents.valueChanged)
        refreshControl.tintColor = .black
        
        return refreshControl
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        offset = 0
        tableView.addSubview(refreshControl)
        // Do any additional setup after loading the view.
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if offset == 0 {
            showCountryView(type: 0)
        }

    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func getCompanyList()  {
        
        if self.refreshControl.isRefreshing{
            offset = 0
        }
        
        guard (rootCatID) != nil else{
            
            self.showAlert(title: "Sorry", message: "CategoryID is not available please try again", withTag: 101, button: ["Ok"], isActionRequired: false)
            return
        }
        guard (cityID) != nil else{
            
            self.showAlert(title: "Sorry", message: "Please select city", withTag: 101, button: ["Ok"], isActionRequired: false)
            return
        }
        
        
        let infoDict = NSDictionary(objects: [rootCatID! ,10 , offset ?? 0 , cityID! ,"1"], forKeys: ["category_id" as NSCopying,"limit" as NSCopying ,"offset" as NSCopying, "city_id" as NSCopying, "type" as NSCopying])
        
        if tableView != nil {
            
            ProgressHUD.showProgressHUDWithLabel(label: "Loading...")
        }
        
        APICall.callCompanyListAPI([:], header: [ : ], information:infoDict ) { (sucess, result, error) in
            
            DispatchQueue.main.async {
                
                weak var weakSelf = self
                
                ProgressHUD.hideProgressHUD()
                
                if (error != nil) {
                    weakSelf?.showAlert(title: "Error", message: (error!.localizedDescription), withTag: 101, button: ["Ok"], isActionRequired: false)
                    return
                }
                
                if sucess {
                    
                    do {
                        let jsonData = try JSONSerialization.jsonObject(with: result!, options: .mutableContainers)
                        
                        if let responseDict = (jsonData as? NSDictionary){
                            
                            if let statusCode = responseDict.object(forKey: "status"), (statusCode is Int) {
                                
                                if (statusCode as! Int) == 200 {
                                    
                                    //sucessfully retrieved data
                                    if let dataDict = responseDict.object(forKey: "data") as? NSDictionary{
                                        
                                        if let toptalR = dataDict.value(forKey: "totalCount") as? Int{
                                            weakSelf?.totalCount = toptalR
                                        }else if let toptalR = dataDict.value(forKey: "totalCount") as? String{
                                            weakSelf?.totalCount = Int(toptalR)
                                        }
                                        
                                        if let tempArray = dataDict.object(forKey: "vendors") as? Array<NSDictionary> {
                                            
                                            if tempArray.count > 0 {
                                                //store data
                                                if  weakSelf?.pageNumber == 0 {
                                                    self.companyArray =  Array()
                                                    
                                                    weakSelf?.companyArray = tempArray
                                                }else{
                                                    //Just add to previous array
                                                    weakSelf?.companyArray?.append(contentsOf: tempArray)
                                                }
                                               
                                            }else{
                                                //No saved stream found message
                                                weakSelf?.showAlert(title: APP_NAME, message: "You do not have any vendors yet.", withTag: 101, button: ["Ok"], isActionRequired: false)
                                            }
                                        }else {
                                            //
                                            weakSelf?.showAlert(title: APP_NAME, message: "vendors key not found", withTag: 101, button: ["Ok"], isActionRequired: false)
                                        }
                                    }else{
                                        //No data found error
                                        weakSelf?.showAlert(title: APP_NAME, message:  AlertMessages.NoDataKeyFoundInResponse, withTag: 101, button: ["Ok"], isActionRequired: false)
                                    }
                                    
                                } else {
                                    var errStr = ""
                                    if let errormessage = (jsonData as? NSDictionary)?.object(forKey:"error") {
                                        if (errormessage is NSDictionary){
                                            if let errorDescription = (errormessage as? NSDictionary)?.object(forKey: "error_description"),!(errorDescription is NSNull){
                                                if (errorDescription is String){
                                                    errStr = errorDescription as! String
                                                    
                                                }
                                            }else {
                                                if let errorMessage = (errormessage as? NSDictionary)?.object(forKey: "message"),!(errorMessage is NSNull){
                                                    if (errorMessage is String){
                                                        errStr = errorMessage as! String
                                                        
                                                    }
                                                }else{
                                                    errStr = AlertMessages.UnidetifiedErrorMessage
                                                }
                                                
                                            }
                                            weakSelf?.showAlert(title: APP_NAME, message:errStr, withTag: 101, button: ["Ok"], isActionRequired: false)
                                        }
                                        
                                    }else{
                                        weakSelf?.showAlert(title: APP_NAME, message:  AlertMessages.UnidetifiedErrorMessage, withTag: 101, button: ["Ok"], isActionRequired: false)
                                    }
                                    
                                }
                            }
                        }else{
                            
                            weakSelf?.showAlert(title: "sorry", message: AlertMessages.NOTADICTIONARYMESSAGE , withTag: 101, button:  ["Ok"], isActionRequired: false)
                            return
                        }
                        
                    }catch{
                        
                        weakSelf?.showAlert(title: "sorry", message: AlertMessages.MODELERRORMESSAGE , withTag: 101, button:  ["Ok"], isActionRequired: false)
                        return
                    }
                    
                } else{
                    
                    //Success false
                    weakSelf?.showAlert(title: APP_NAME, message: AlertMessages.UnidetifiedErrorMessage, withTag: 101, button: ["Ok"], isActionRequired: false)
                    
                }
                
                if self.refreshControl.isRefreshing{
                    self.refreshControl.endRefreshing()
                }
                
                if weakSelf?.tableView != nil {
                    weakSelf?.tableView.reloadData()
                }
            }
            
        }
    }
    
    func showAlertForSelection(selectedIndex: Int){
        
        
        var message = ""
        var alerttag = -5
        
        if selectedIndex == 1 {
            
            message = "You have selected college to proceed."
            
        }
        else if selectedIndex == 0 {
            
            message = "You have selected school to proceed."
            
        }
        else{
            
            message = "You have selected wrong option."
            print("School part is not yet implemented")
            
        }
        alerttag = selectedIndex
        self.showAlert(title: "", message: message, withTag: alerttag, button: ["Ok"], isActionRequired: true)
    }
    
    override func alertButtonAction(alertTag: Int, btnTitle: String) {
        
        switch alertTag {
            
        case 0:
            self.showAlert(title: "", message: "School part is not yet implemented", withTag: 101, button: ["Ok"], isActionRequired: false)
         
        case 1:  kAppDelegate.loadStudentHomeViewController()
            
        default:
            self.showAlert(title: "", message: "You have selected wrong option.", withTag: 101, button: ["Ok"], isActionRequired: false)
            
        }
    }
}
extension CompanyProfileListingViewController : CountryViewDelegate {
    
    func showCountryView(type : Int){
        
        countryView = Bundle.main.loadNibNamed("CountryView", owner: self, options: nil)?[0] as? CountryView
        countryView?.delegate = self
        countryView?.registerNibs()
        countryView?.managerRadius()
        countryView?.tag = type
        appDelegate.window?.addSubview(countryView!)
        countryView?.alpha = 0
        countryView?.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height)
        self.countryView?.btnOther.isHidden = true
        
        UIView.animate(withDuration: 0.3, animations: {
            self.countryView?.alpha = 1
        } ,completion: { (done) in
            self.countryView?.lblTitle.text = "Select City"
            self.countryView?.getAllCitiesHavingCompany()
        })
    }
    
    func getCountryCode(data: NSDictionary , type :Int) {
        
        let tempStr = String(describing: (data.object(forKey: "id")!))
        print((data.object(forKey: "name")) as? String)
        print(tempStr)
        cityID = tempStr
        if offset == 0 {
            getCompanyList()
        }
        
    }
    func removeCountryView()  {
        self.countryView?.alpha = 1
        UIView.animate(withDuration: 0.3, animations: {
            self.countryView?.alpha = 0
        } ,completion: { (done) in
            self.countryView?.removeFromSuperview()
            self.countryView = nil
        } )
    }
    
}
extension CompanyProfileListingViewController : UITableViewDelegate, UITableViewDataSource , UIViewControllerTransitioningDelegate , ViewCoursesDelegate{
    
    func viewCourseList(_ i: Int) {
        
        if companyArray != nil {
            
            if let tempcompanyArray = companyArray {
                
                if let dict = tempcompanyArray[i]  as? NSDictionary{
             NavigationManager.moveToAllCoursesListingViewControllerIfNotExists(kAppDelegate.menuNavController!, courseDictDetail: dict)
                }
            }
        }
    }

    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section:
        Int) -> Int
    {
        if let array = companyArray {
            
            return array.count
            
        }
        return 0
    }
    

    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        // Allocates a Table View Cell
        let cell =
            tableView.dequeueReusableCell(withIdentifier: "MyPageRegisterTableViewCellID",
                                          for: indexPath) as! MyPageRegisterTableViewCell
        
        cell.selectionStyle = UITableViewCellSelectionStyle.none
        cell.backGroundWhiteView.layer.cornerRadius =  (cell.backGroundWhiteView.frame.size.width * 0.01506)
        cell.backGroundWhiteView.layer.masksToBounds = true
        cell.buttonViewAll.tag = indexPath.row
        cell.viewCoursesDelegate = self
       // cell.shadowView.backgroundColor =  UIColor.white
        cell.shadowView.layer.shadowColor = UIColor(red:0.8, green:0.8, blue:0.8, alpha:0.5).cgColor
        cell.shadowView.layer.shadowOpacity = 1 ;
        cell.shadowView.layer.shadowRadius = 20
        cell.shadowView.layer.shadowOffset = CGSize(width: 0.0, height: cell.contentView.frame.height * 0.04)
        cell.shadowView.layer.masksToBounds = false
        cell.imgView.layer.cornerRadius =  (cell.imgView.frame.size.width * 0.18)
        cell.imgView.layer.masksToBounds = true
        if companyArray != nil {
            
            if let tempcompanyArray = companyArray {
                
                if let dict = tempcompanyArray[indexPath.row] as?  NSDictionary {
                    
                    if let userDetailDict = dict.value(forKey: "userdetails") as? NSDictionary{
                        
                        if let wTitle = userDetailDict.value(forKey: "organization_name") as? String {
                            
                            
                            cell.lblHeading.text = wTitle
                        }
                        if let wDes = userDetailDict.value(forKey: "description") as? String {
                            cell.lblDescription.text = wDes == "" ?"No descsription available":wDes.withoutHtml
                        }else{
                            cell.lblDescription.text = "No descsription available"
                        }
                    }
                    
                        if let courseArray = dict.value(forKey: "course") as? NSArray {
                             cell.lblCourse.text = ""
                            if courseArray.count > 0 {
                                
                                for i in 0..<courseArray.count {
                                  //Display only 2 course here, if more click on view All
                                    if i <= 1 {
                                        if let courseDetailDict = courseArray.object(at: i) as? NSDictionary {
                                            
                                            if let  title = courseDetailDict.value(forKey: "title")  as? String{
                                                
                                                if i == 1{
                                                    
                                                    cell.lblCourse.text?.append("    |    ")
                                                }
                                                cell.lblCourse.text?.append(title)
                                            }
                                        }
                                        
                                    }
                                }
                            }
                    }
                    var imageFinalUrl =  ""
                    
                    if let imageURL = dict.value(forKey: "profile_image") {
                        
                        imageFinalUrl = "\(URLS.IMAGE_BASE_URL)" + "\(imageURL)"
                        
                        cell.imgView.sd_setIndicatorStyle(.gray)
                        cell.imgView.sd_setShowActivityIndicatorView(true)
                        
                        cell.imgView.sd_setImage(with: URL(string: imageFinalUrl), completed: { (image, error, type, url) in
                            DispatchQueue.main.async {
                                if error == nil{
                                    cell.imgView.image = image
                                }else{
                                     cell.imgView.image = #imageLiteral(resourceName: "Placeholder")
                                }
                                cell.imgView.sd_setShowActivityIndicatorView(false)
                            }
                        })
                    }
                        
                }
                //Reload the next amount of data
                if(indexPath.row ==  (companyArray?.count)! - 1 && totalCount! > (companyArray?.count)! ){
                    //we are at last index of currently displayed cell
                    //reload more data
                    offset = (companyArray?.count)!
                    getCompanyList()
                }
            }
        }
        cell.selectionStyle = .none
        return cell
    }


    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        
//        // cross checking for checked rows
//        if(rowsWhichIsChecked == indexPath.row){
//            rowsWhichIsChecked = -1
//
//        }else {
//            rowsWhichIsChecked = indexPath.row
//        }
//        tableView.reloadData()
//
//        showAlertForSelection(selectedIndex: indexPath.row)
    }
    
}
