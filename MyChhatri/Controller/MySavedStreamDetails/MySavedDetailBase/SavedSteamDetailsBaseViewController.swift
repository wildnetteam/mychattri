//
//  SavedSteamDetailsBaseViewController.swift
//  MyChhatri
//
//  Created by Arpana on 31/07/18.
//  Copyright © 2018 Anshul. All rights reserved.
//

import UIKit

class SavedSteamDetailsBaseViewController: UIViewController  , CAPSPageMenuDelegate   {
  
    var pageMenu : CAPSPageMenu?
    var StreamID : String?
    var isFromCarrerOption: Int?
    var topicID :String?
    var CatregoryID: String?
    var storedIds : [String]?

    @IBOutlet weak var saveOfRemoveBuuton: UIBarButtonItem!
    
    @IBOutlet weak var viewHeading: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        assignTopicAndCategoryID()
 
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        var topSafeArea: CGFloat
        
        if #available(iOS 11.0, *) {
            topSafeArea = view.safeAreaInsets.top
        } else {
            topSafeArea = topLayoutGuide.length
        }
        let  frame = CGRect(x:0.0,y: topSafeArea  , width:self.view.frame.size.width,height:self.view.frame.size.height)
        
        pageMenu?.view.frame = frame
        
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        self.navigationController?.navigationBar.isHidden = false
        kAppDelegate.menuNavController = self.navigationController
        
        self.navigationController?.navigationBar.topItem?.title = kAppDelegate.menuNavController?.navigationBar.accessibilityLabel
        kAppDelegate.setNavigationAndStatusAppearance()
        let statusBar: UIView = UIApplication.shared.value(forKey: "statusBar") as! UIView
        statusBar.backgroundColor = ColorConstants.appBackGroundCOLOR()
        UIApplication.shared.statusBarStyle = .lightContent
        setNeedsStatusBarAppearanceUpdate()
      

    }
    
    func assignTopicAndCategoryID() {
        
        guard let allRetrivedIDS = storedIds else {
            self.showAlert(title: "Error", message: "could not retrieved rootId and StreamID", withTag: 101, button: ["Ok"], isActionRequired: false)
            return
        }
        if allRetrivedIDS.count == 1 {
            topicID = allRetrivedIDS.first
            CatregoryID = allRetrivedIDS.first
        }else{
            for _ in allRetrivedIDS {
                
                topicID = allRetrivedIDS.last
                CatregoryID = allRetrivedIDS.first
            }
        }
        getTabCount()
        isStreamAlreadySaved()
        
    }
    // MARK: - GET STATUS IF STREAM IS ALREADY SAVED OR NOT
    func isStreamAlreadySaved()  {

        let params : [String:Any] = [:]
        
        var streamId = ""
        if let streamID = CatregoryID {
                streamId = streamID
            }
        

        APICall.callIsStreamSavedAPI( params, header: [ : ], topicID: streamId) { (sucess, result, error) in
            
            weak var weakSelf = self
            
            
            DispatchQueue.main.async {
                
                var isStreamSaved : Int = 0

                if (error != nil) {
                    
                    weakSelf?.showAlert(title: "Error", message: (error!.localizedDescription), withTag: 101, button: ["Ok"], isActionRequired: false)
                    
                    return
                }
                if sucess {
                    
                    do {
                        
                        let jsonData = try JSONSerialization.jsonObject(with: result!, options: .mutableContainers)
                        
                        if (jsonData is NSDictionary){
                            
                            if let statusCode = (jsonData as! NSDictionary).object(forKey: "status"), (statusCode is Int) {
                                
                                if (statusCode as! Int) == 200 {
                                    
                                    //sucessfully retrieved data
                                    if let dataDict = (jsonData as! NSDictionary).object(forKey: "data") as? NSDictionary{
                                        
                                        
                                        if let statusVal = dataDict.object(forKey: "status") as? Int{
                                            isStreamSaved = statusVal
                                            
                                        }else  if let statusVal = dataDict.object(forKey: "status") as? String{
                                            isStreamSaved = Int(statusVal)!
                                            
                                        }
                                    }else{
                                        weakSelf?.showAlert(title: "sorry", message: AlertMessages.NoDataKeyFoundInResponse , withTag: 101, button:  ["Ok"], isActionRequired: false)
                                    }
                                  
                                    weakSelf?.isFromCarrerOption = isStreamSaved
                                }
                                
                            }else{
                                
                                
                                var errStr = ""
                                if let errormessage = (jsonData as? NSDictionary)?.object(forKey:"error") {
                                    if (errormessage is NSDictionary){
                                        if let errorDescription = (errormessage as? NSDictionary)?.object(forKey: "error_description"),!(errorDescription is NSNull){
                                            if (errorDescription is String){
                                                errStr = errorDescription as! String
                                                
                                            }
                                        }else {
                                            if let errorMessage = (errormessage as? NSDictionary)?.object(forKey: "message"),!(errorMessage is NSNull){
                                                if (errorMessage is String){
                                                    errStr = errorMessage as! String
                                                    
                                                }
                                            }else{
                                                errStr = "Something went wrong"
                                            }
                                            
                                        }
                                        weakSelf?.showAlert(title: APP_NAME, message:errStr, withTag: 101, button: ["Ok"], isActionRequired: false)
                                    }
                                    
                                }else{
                                    weakSelf?.showAlert(title: APP_NAME, message: AlertMessages.UnidetifiedErrorMessage, withTag: 101, button: ["Ok"], isActionRequired: false)
                                }
                            }
                            
                        }else{
                            //not a dict
                        }
                    }
                    catch  {
                        
                        weakSelf?.showAlert(title: "sorry", message: AlertMessages.MODELERRORMESSAGE , withTag: 101, button:  ["Ok"], isActionRequired: false)
                        return
                    }
                }
                else{
                    //Alert not sucessfull
                    weakSelf?.showAlert(title: APP_NAME, message: "could not get response from server", withTag: 101, button: ["Ok"], isActionRequired: false)
                }
             

                        if  weakSelf?.isFromCarrerOption == 1 {
                            //came from carreser screen, It will have save button
                //          /  saveOfRemoveBuuton.title = "Adadfad"
                            self.saveOfRemoveBuuton.title =  "remove"  //\u{2699}"
                
                        }else{
                            //came from saved stream screen, It will have remove button
                             weakSelf?.saveOfRemoveBuuton.title = "save"
                
                        }
            }
        }
    }
    
    func setTabs(infoDict: NSDictionary?) {
        
 
    }
    // MARK: - ADD PAGE MENU PRIMARY
    func addPageMenuForPrimaryManager(infoDict: NSDictionary? ) {
        
        var controllerArray : [UIViewController] = []
        
        let articleByCategoriesViewController = UIStoryboard.instantiatArticlesViewController()
        articleByCategoriesViewController.title = "Articles".localized(lang: getLanguageKey(lang:  LocalDB.shared.currentLanguage!))
        
        if let cID = topicID {
            articleByCategoriesViewController.rootCatID = cID
            
        } else  {
            showAlert(title: APP_NAME, message: "Category Id is missing", withTag: 109, button: ["OK"], isActionRequired: false)
        }
        
        var totalArticleCount = 1
        
        if let articleCount =  infoDict?.value(forKey: "tota_articles") as? String {
            
            totalArticleCount = Int(articleCount)!
            
        }else if let articleCount =  infoDict?.value(forKey: "total_articles") as? Int {
            
            totalArticleCount = articleCount

        }
        
        if totalArticleCount > 0 {
            
            controllerArray.append(articleByCategoriesViewController)
            
        }
        
        
        let  videoListViewController = UIStoryboard.instantiatVideoListingViewController()
        videoListViewController.title =  "Interview Videos".localized(lang: getLanguageKey(lang:  LocalDB.shared.currentLanguage!))
        
        if let cID = topicID {
            videoListViewController.rootCatID = cID
            
        } else  {
            showAlert(title: APP_NAME, message: "Category Id is missing", withTag: 109, button: ["OK"], isActionRequired: false)
        }
        
        
        var totalVideoCount = 1
        
        if let videoCount =  infoDict?.value(forKey: "total_interviews") as? String {
            
            totalVideoCount = Int(videoCount)!
            
        }else if let videoCount =  infoDict?.value(forKey: "total_interviews") as? Int {
            
            totalVideoCount = videoCount
            
        }
        
        if totalVideoCount > 0 {
            
            controllerArray.append(videoListViewController)
        }

        let  webinarViewController = UIStoryboard.instantiatWebinarViewController()
        if let cID = topicID {
            webinarViewController.rootCatID = cID
            
        } else  {
            showAlert(title: APP_NAME, message: "Category Id is missing", withTag: 109, button: ["OK"], isActionRequired: false)
        }
        webinarViewController.title = "Webinar".localized(lang: getLanguageKey(lang:  LocalDB.shared.currentLanguage!))
        
        
        
        var totalWebinarCount = 1
        
        if let videoCount =  infoDict?.value(forKey: "total_webinars") as? String {
            
            totalWebinarCount = Int(videoCount)!
            
        }else if let videoCount =  infoDict?.value(forKey: "total_webinars") as? Int {
            
            totalWebinarCount = videoCount
            
        }
        
        if totalWebinarCount > 0 {
            
        controllerArray.append(webinarViewController)
            
        }
        
        
        let  seminarViewController = UIStoryboard.instantiateSeminarViewController()
        if let cID = topicID {
            seminarViewController.rootCatID = cID
            
        } else  {
            showAlert(title: APP_NAME, message: "Category Id is missing", withTag: 109, button: ["OK"], isActionRequired: false)
        }
        seminarViewController.title = "Seminar".localized(lang: getLanguageKey(lang:  LocalDB.shared.currentLanguage!))
        
        
        var totalSeminarCount = 1
        
        if let videoCount =  infoDict?.value(forKey: "total_seminars") as? String {
            
            totalSeminarCount = Int(videoCount)!
            
        }else if let videoCount =  infoDict?.value(forKey: "total_seminars") as? Int {
            
            totalSeminarCount = videoCount
            
        }
        
        if totalSeminarCount > 0 {
        controllerArray.append(seminarViewController)
        
        }
        let trendsViewController = UIStoryboard.instantiatInterviewsViewController()
        if let cID = topicID {
            trendsViewController.rootCatID = cID
            
        } else  {
            showAlert(title: APP_NAME, message: "Category Id is missing", withTag: 109, button: ["OK"], isActionRequired: false)
        }
        trendsViewController.title =  "Latest Trends".localized(lang: getLanguageKey(lang:  LocalDB.shared.currentLanguage!))
        
          var totalTrendsCount = 1
        
        if let videoCount =  infoDict?.value(forKey: "total_trends") as? String {
            
            totalTrendsCount = Int(videoCount)!
            
        }else if let videoCount =  infoDict?.value(forKey: "total_trends") as? Int {
            
            totalTrendsCount = videoCount
            
        }
        
        if totalTrendsCount > 0 {
        controllerArray.append(trendsViewController)
        
        }
        
     
         let innovationController = UIStoryboard.instantiateInnovationViewController()
        if let cID = topicID {
            innovationController.rootCatID = cID
            
        } else  {
            showAlert(title: APP_NAME, message: "Category Id is missing", withTag: 109, button: ["OK"], isActionRequired: false)
        }
        innovationController.title =  "Latest Innovation".localized(lang: getLanguageKey(lang:  LocalDB.shared.currentLanguage!))
        
        var totalInnovationsCount = 1
        
        if let videoCount =  infoDict?.value(forKey: "total_innovations") as? String {
            
            totalInnovationsCount = Int(videoCount)!
            
        }else if let videoCount =  infoDict?.value(forKey: "total_innovations") as? Int {
            
            totalInnovationsCount = videoCount
            
        }
        
        if totalInnovationsCount > 0 {
            
            controllerArray.append(innovationController)
        }
        
        let opportunityViewController = UIStoryboard.instantiatOpportunityViewController()
        if let cID = topicID {
            opportunityViewController.rootCatID = cID
            
        } else  {
            showAlert(title: APP_NAME, message: "Category Id is missing", withTag: 109, button: ["OK"], isActionRequired: false)
        }
        opportunityViewController.title =  "Opportunity".localized(lang: getLanguageKey(lang:  LocalDB.shared.currentLanguage!))
        
        /*
         
         "data": {
         "total_advertisements": 0
         }
         */
        var totalOpportunitiesCount = 1
        
        if let videoCount =  infoDict?.value(forKey: "total_opportunities") as? String {
            
            totalOpportunitiesCount = Int(videoCount)!
            
        }else if let videoCount =  infoDict?.value(forKey: "total_opportunities") as? Int {
            
            totalOpportunitiesCount = videoCount
            
        }
        
        if totalOpportunitiesCount > 0 {
            controllerArray.append(opportunityViewController)
        }
        let companyListVC = UIStoryboard.instantiateCompanyListViewController()
        if let cID = topicID {
            companyListVC.rootCatID = cID
            
        } else  {
            showAlert(title: APP_NAME, message: "Category Id is missing", withTag: 109, button: ["OK"], isActionRequired: false)
        }
        
        companyListVC.title =  "Training".localized(lang: getLanguageKey(lang:  LocalDB.shared.currentLanguage!))
        controllerArray.append(companyListVC)
        
        
        let internshipListVC = UIStoryboard.instantiateInternshipListingViewController()
        if let cID = topicID {
            internshipListVC.rootCatID = cID
            
        } else  {
            showAlert(title: APP_NAME, message: "Category Id is missing", withTag: 109, button: ["OK"], isActionRequired: false)
        }
        
        internshipListVC.title =  "Internship".localized(lang: getLanguageKey(lang:  LocalDB.shared.currentLanguage!))
        controllerArray.append(internshipListVC)
        
        
        let dictionary:NSDictionary = [
            CAPSPageMenuOptionScrollMenuBackgroundColor : ColorConstants.appBackGroundCOLOR()
            ,   CAPSPageMenuOptionViewBackgroundColor : UIColor.clear,CAPSPageMenuOptionMenuItemWidth : self.view.frame.size.width/3 - 12 ,CAPSPageMenuOptionMenuHeight:  50.0 , CAPSPageMenuOptionCenterMenuItems: true ,CAPSPageMenuOptionMenuItemFont: UIFont(name: "Amble-Regular", size: 16.0)!
        ]
        
        if (pageMenu != nil) {
            pageMenu?.view.removeFromSuperview()
        }
        
        let frame = CGRect(x:0.0,y: 64 , width:self.view.frame.size.width,height:self.view.frame.size.height - 60)
        
        pageMenu = CAPSPageMenu.init(viewControllers: controllerArray, frame: frame, options: dictionary as? [AnyHashable: Any] ?? [:])
        
        pageMenu!.delegate = self
        pageMenu?.viewBackgroundColor = .red
        self.view.addSubview(pageMenu!.view)
    }
    

    func didMove(toPage controller: UIViewController!, index: Int) {
        
        kAppDelegate.menuNavController = self.navigationController
        
        
        switch index {
        case 0:print("Need to implement")
            
        case 1: print("Need to implement")
            //   ((controller as! MustBuyViewController).tableView as UIScrollView).delegate = self
            
        case 2: print("Need to implement")
        default: print("Wrong index")
            
        }
        viewDidLayoutSubviews()
    }

   
    
    
    @IBAction func saveOrRemoveBtnClicked(_ sender: Any) {
        if  self.saveOfRemoveBuuton.title == "save"{
            
            //Save  api will be called
            //reove api will be called
            self.showAlert(title: APP_NAME, message: "Are you sure, you want to save this stream?", withTag: 112, button: ["Cancel" , "Ok"], isActionRequired: true)
        }else{
            
            //reove api will be called
            self.showAlert(title: APP_NAME, message: "Are you sure, you want to remove saved stream?", withTag: 111, button: ["Cancel" , "Ok"], isActionRequired: true)
            
        }
    
}

    override func alertButtonAction(alertTag: Int, btnTitle: String) {
        
        if alertTag == 111  {
            if btnTitle == "Ok" {
                
                delete()
                
            }
        }else  if alertTag == 112  {
            if btnTitle == "Ok" {
                
                saveSubject()
            }
        }else  if alertTag == 110  {
            if btnTitle == "Ok" {
                
                //Go to saved carrer screen
                GoToSaveCarrerScreen()
            }
        }else{
            
        }
    }
    
        // MARK: - REMOVE API FOR SAVED STREAM
    func delete(){
        
        let params : [String:Any] = [:]
        
        guard let idTodelete = CatregoryID else{
            
            self.showAlert(title: "Error", message: "Id to delete is not found. Try again", withTag: 101, button: ["Ok"], isActionRequired: false)
            return
        }
        APICall.callRemoveSubjectAPI(params, header: [ : ], subjectID: idTodelete) { (sucess, result, error) in
            
            DispatchQueue.main.async {
                
                weak var weakSelf = self
                
                ProgressHUD.hideProgressHUD()
                
                if (error != nil) {
                    weakSelf?.showAlert(title: "Error", message: (error!.localizedDescription), withTag: 101, button: ["Ok"], isActionRequired: false)
                    return
                }
                
                if sucess {
                    
                    do {
                        let jsonData = try JSONSerialization.jsonObject(with: result!, options: .mutableContainers)
                        
                        if let responseDict = (jsonData as? NSDictionary){
                            
                            if let statusCode = responseDict.object(forKey: "status"), (statusCode is Int) {
                                
                                if (statusCode as! Int) == 200 {
                                    
                                    var msg = "Deleted successfully"
                                    if let message = (responseDict as! NSDictionary).object(forKey: "message") as? String {
                                        msg = message
                                    }
                                    weakSelf?.showAlert(title: APP_NAME, message: msg, withTag: 110, button: ["Ok"], isActionRequired: true)
                                    
                                    
                                } else {
                                    var errStr = ""
                                    if let errormessage = (jsonData as? NSDictionary)?.object(forKey:"error") {
                                        if (errormessage is NSDictionary){
                                            if let errorDescription = (errormessage as? NSDictionary)?.object(forKey: "error_description"),!(errorDescription is NSNull){
                                                if (errorDescription is String){
                                                    errStr = errorDescription as! String
                                                    
                                                }
                                            }else {
                                                if let errorMessage = (errormessage as? NSDictionary)?.object(forKey: "message"),!(errorMessage is NSNull){
                                                    if (errorMessage is String){
                                                        errStr = errorMessage as! String
                                                        
                                                    }
                                                }else{
                                                    errStr = AlertMessages.UnidetifiedErrorMessage
                                                }
                                                
                                            }
                                            weakSelf?.showAlert(title: APP_NAME, message:errStr, withTag: 101, button: ["Ok"], isActionRequired: false)
                                        }
                                        
                                    }else{
                                        weakSelf?.showAlert(title: APP_NAME, message:  AlertMessages.UnidetifiedErrorMessage, withTag: 101, button: ["Ok"], isActionRequired: false)
                                    }
                                    
                                }
                            }
                        }else{
                            
                            weakSelf?.showAlert(title: "sorry", message: AlertMessages.NOTADICTIONARYMESSAGE , withTag: 101, button:  ["Ok"], isActionRequired: false)
                            return
                        }
                        
                    }catch{
                        
                        weakSelf?.showAlert(title: "sorry", message: AlertMessages.MODELERRORMESSAGE , withTag: 101, button:  ["Ok"], isActionRequired: false)
                        return
                    }
                    
                } else{
                    
                    //Success false
                    weakSelf?.showAlert(title: APP_NAME, message: AlertMessages.UnidetifiedErrorMessage, withTag: 101, button: ["Ok"], isActionRequired: false)
                    
                }
                
                
            }
            
        }
    }

    // MARK: -  API FOR SAVE STREAM

    func saveSubject(){
        
        /* user_id:1
         topic_id:10
         stream_id:1
         root_category_id:1*/
        
        var params : [String:Any] = [:]
        
        if let userInfoDict = UserModel.getLoginInfo() {
            
            if let  userId = (userInfoDict.object(forKey: "ID") as? String){
                
                params["user_id"]  = userId
                
            }else if let userIded = (userInfoDict.object(forKey: "ID") as? Int){
                
                params["user_id"]  = userIded
            }else{
                
                self.showAlert(title: "Sorry", message: "User details could not be found , Please logout and login again", withTag: 101, button: ["Ok"], isActionRequired: false)
                return
            }
        }
        
        guard let topiIdToSave = topicID else{
            
            self.showAlert(title: "Error", message: "Id to SAVE is not found. Try again", withTag: 101, button: ["Ok"], isActionRequired: false)
            return
        }
        
        
        params["topic_id"] = topiIdToSave
        
        guard let streamIdToSave = StreamID else{
            
            self.showAlert(title: "Error", message: "Id to SAVE is not found. Try again", withTag: 101, button: ["Ok"], isActionRequired: false)
            return
            
        }
        params["stream_id"] = streamIdToSave
        
        guard let rootCategoryIdToSave = CatregoryID else{
            
            self.showAlert(title: "Error", message: "Id to SAVE is not found. Try again", withTag: 101, button: ["Ok"], isActionRequired: false)
            return
        }
        
        params["root_category_id"] = rootCategoryIdToSave
        
      //  print("params : \(params)")
        
        ProgressHUD.showProgressHUDWithLabel(label: "Loading...")
        
        APICall.callSaveSubjectPostAPI(params, header: [ : ]) { (sucess, result, error) in
            
            DispatchQueue.main.async {
                
                ProgressHUD.hideProgressHUD()
                
                weak var weakSelf = self
                
                if (error != nil) {
                    weakSelf?.showAlert(title: "Error", message: (error!.localizedDescription), withTag: 101, button: ["Ok"], isActionRequired: false)
                    return
                }
                if sucess {
                    
                    do {
                        let jsonData = try JSONSerialization.jsonObject(with: result!, options: .mutableContainers)
                        
                        if (jsonData is NSDictionary){
                            
                            if let statusCode = (jsonData as! NSDictionary).object(forKey: "status"), (statusCode is Int) {
                                
                                if let success = (jsonData as! NSDictionary).object(forKey: "success") {
                                    
                                    if (success as! Bool) == true {
                                        
                                        if (statusCode as! Int) == 200 {
                                            if let data = (jsonData as! NSDictionary).object(forKey: "data"), (data is NSDictionary) {
                                                if (data as! NSDictionary).count > 0 {
                                                    
                                                    if let userId = (data as! NSDictionary).object(forKey: "user_id") as? String {
                                                        print("\(userId)")
                                                    }
                                                    var msg = "Saved successfully"
                                                    if let message = (data as! NSDictionary).object(forKey: "message") as? String {
                                                        msg = message
                                                    }
                                                    weakSelf?.showAlert(title: APP_NAME, message: msg, withTag: 110, button: ["Ok"], isActionRequired: true)
                                                    
                                                }
                                                
                                            }
                                            
                                            
                                        } else {
                                            
                                            //some other status code
                                            //check error message in the response
                                            var errStr = ""
                                            if let errormessage = (jsonData as? NSDictionary)?.object(forKey:"error") {
                                                if (errormessage is NSDictionary){
                                                    if let errorDescription = (errormessage as? NSDictionary)?.object(forKey: "error_description"),!(errorDescription is NSNull){
                                                        if (errorDescription is String){
                                                            errStr = errorDescription as! String
                                                            
                                                        }
                                                    }else {
                                                        if let errorMessage = (errormessage as? NSDictionary)?.object(forKey: "message"),!(errorMessage is NSNull){
                                                            if (errorMessage is String){
                                                                errStr = errorMessage as! String
                                                                
                                                            }
                                                        }else{
                                                            errStr = AlertMessages.UnidetifiedErrorMessage
                                                        }
                                                        
                                                    }
                                                    weakSelf?.showAlert(title: APP_NAME, message:errStr, withTag: 101, button: ["Ok"], isActionRequired: false)
                                                }
                                            }
                                        }
                                    }else{
                                        
                                        //some other status code
                                        //check error message in the response
                                        var errStr = ""
                                        if let errormessage = (jsonData as? NSDictionary)?.object(forKey:"error") {
                                            if (errormessage is NSDictionary){
                                                if let errorDescription = (errormessage as? NSDictionary)?.object(forKey: "error_description"),!(errorDescription is NSNull){
                                                    if (errorDescription is String){
                                                        errStr = errorDescription as! String
                                                        
                                                    }
                                                }else {
                                                    if let errorMessage = (errormessage as? NSDictionary)?.object(forKey: "message"),!(errorMessage is NSNull){
                                                        if (errorMessage is String){
                                                            errStr = errorMessage as! String
                                                            
                                                        }
                                                    }else{
                                                        errStr = AlertMessages.UnidetifiedErrorMessage
                                                    }
                                                    
                                                }
                                                weakSelf?.showAlert(title: APP_NAME, message:errStr, withTag: 101, button: ["Ok"], isActionRequired: false)
                                            }
                                        }
                                    }
                                    
                                }else{
                                    
                                    weakSelf?.showAlert(title: "sorry", message: AlertMessages.NOTADICTIONARYMESSAGE , withTag: 101, button:  ["Ok"], isActionRequired: false)
                                }
                                
                            }
                        }else{
                            // not a dictionary
                            weakSelf?.showAlert(title: "sorry", message: AlertMessages.NOTADICTIONARYMESSAGE , withTag: 101, button:  ["Ok"], isActionRequired: false)
                        }
                    }catch{
                        weakSelf?.showAlert(title: "sorry", message: AlertMessages.MODELERRORMESSAGE , withTag: 101, button:  ["Ok"], isActionRequired: false)
                        return
                    }
                }
                
            }
        }
    }
    
    func getTabCount()  {
        
        
        guard (topicID) != nil else{

            self.showAlert(title: "Sorry", message: "Topic ID is not available please try again", withTag: 101, button: ["Ok"], isActionRequired: false)
            return
        }
        
        ProgressHUD.showProgressHUDWithLabel(label: "Loading...")
        
        
        APICall.callTabCountAPI([:], header: [ : ], topicID: topicID! ) { (sucess, result, error) in
            
            DispatchQueue.main.async {
                
                weak var weakSelf = self
                
                ProgressHUD.hideProgressHUD()
                
                if (error != nil) {
                   //show all the tabs
                    weakSelf?.addPageMenuForPrimaryManager(infoDict:nil)

                }
                
                if sucess {
                    
                    do {
                        let jsonData = try JSONSerialization.jsonObject(with: result!, options: .mutableContainers)
                        
                        if let responseDict = (jsonData as? NSDictionary){
                            
                            if let statusCode = responseDict.object(forKey: "status"), (statusCode is Int) {
                                
                                if (statusCode as! Int) == 200 {
                                    
                                    //sucessfully retrieved data
                                    if let dataDict = responseDict.object(forKey: "data") as? NSDictionary{
                                         weakSelf?.addPageMenuForPrimaryManager(infoDict: dataDict)
                                        
                                    }else{
                                        //No data found error
                                        weakSelf?.showAlert(title: APP_NAME, message:  AlertMessages.NoDataKeyFoundInResponse, withTag: 101, button: ["Ok"], isActionRequired: false)
                                    }
                                    
                                } else {
                                     weakSelf?.addPageMenuForPrimaryManager(infoDict:nil)

                                }
                            }
                        }else{
                            
                            weakSelf?.showAlert(title: "sorry", message: AlertMessages.NOTADICTIONARYMESSAGE , withTag: 101, button:  ["Ok"], isActionRequired: false)
                            return
                        }
                        
                    }catch{
                        
                        weakSelf?.showAlert(title: "sorry", message: AlertMessages.MODELERRORMESSAGE , withTag: 101, button:  ["Ok"], isActionRequired: false)
                        return
                    }
                    
                } else{
                    
                    //Success false
                    weakSelf?.showAlert(title: APP_NAME, message: AlertMessages.UnidetifiedErrorMessage, withTag: 101, button: ["Ok"], isActionRequired: false)
                    
                }
            
            }
            
        }
    }
    @IBAction func detailedInformativeButtonClicked(_ sender: Any) {
        NavigationManager.moveToInformativeViewControllerIfNotExists(kAppDelegate.menuNavController!)
    }
    
    func GoToSaveCarrerScreen()  {
        //pop to saved Stream Page
        
        if let viewControllers = self.navigationController?.viewControllers {
            
            for vc: UIViewController? in viewControllers {
                
                if (vc is MyPageBaseViewController) {
                    if let aVc = vc {
                        
                        popedControllerPageIndex = 1
                        self.navigationController?.popToViewController(aVc, animated: false)
                        return
                    }
                }
            }
            self.navigationController?.popToRootViewController(animated: true)
            
        }
    }
    @IBAction func backButtonClicked(_ sender: Any) {
        
        //pop to saved Stream Page
        
  /*      if let viewControllers = self.navigationController?.viewControllers {
            
            for vc: UIViewController? in viewControllers {
                
                if (vc is MyPageBaseViewController) {
                    if let aVc = vc {
                        
                      //  popedControllerPageIndex = 1
                         self.navigationController?.popToViewController(aVc, animated: false)
                         return
                    }
                }
            }
            self.navigationController?.popToRootViewController(animated: true)

        }*/
        
        self.navigationController?.popViewController(animated: true)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
