//
//  RegisterStepTwoVC.swift
//  MyChhatri
//
//  Created by Anshul on 10/07/18.
//  Copyright © 2018 Anshul. All rights reserved.
//

import UIKit

class RegisterStepTwoVC: UIViewController {
    
    @IBOutlet weak var txtStudentTitle:UITextField!
    @IBOutlet weak var txtStudentName:UITextField!
    @IBOutlet weak var txtStudentEmail:UITextField!
    @IBOutlet weak var txtCountry:UITextField!
    @IBOutlet weak var txtState:UITextField!
    @IBOutlet weak var txtCity:UITextField!
    @IBOutlet weak var txtMobile:UITextField!
    @IBOutlet weak var txtPassword:UITextField!
    
    
    @IBOutlet weak var txtCollegeName:UITextField!
    @IBOutlet weak var txtManualCollegeName:UITextField!
    @IBOutlet weak var btnserchCollegeList:UIButton!
 

    @IBOutlet weak var txtrePassword:UITextField!
    @IBOutlet weak var btnNext:UIButton!
    
    @IBOutlet weak var adViewHightConstant: NSLayoutConstraint!
    @IBOutlet weak var adView : CustomAdvertiseView!
    
    //*********Picker Setup*********//
    private var picker = UIPickerView()
    private var txtFieldPicker = UITextField()
    private var arr_Picker = ["Mr.","Mrs.","Ms.","Miss."]
    //*********Picker Setup*********//
    
    var countryView:CountryView?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.initialViewSetup()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if(adView != nil) {
            adView.isHidden = false
            adView.hideAdViewDel = self
            adViewHightConstant.constant = 60
            adView.categoryIDForAdvertise = "8"
            adView.typeFoAdvertiseData = "1"
        }
        adView.awakeFromNib()
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        adView.hideAdViewDel = nil
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    private func initialViewSetup() {
        btnNext.addGrediant()
        
        registrationModel.acceptTerms = "0"
        registrationModel.studentTitle = "Mr."
        
        let viewGuardianTitle = UIView(frame: CGRect(x: 0, y: 0, width: 8, height: 40))
        txtStudentTitle.leftView = viewGuardianTitle
        txtStudentTitle.leftViewMode = .always
        let view1 = UIView(frame: CGRect(x: 0, y: 0, width: 8, height: 40))
        txtStudentName.leftView = view1
        txtStudentName.leftViewMode = .always
        let view2 = UIView(frame: CGRect(x: 0, y: 0, width: 8, height: 40))
        txtStudentEmail.leftView = view2
        txtStudentEmail.leftViewMode = .always
        let view3 = UIView(frame: CGRect(x: 0, y: 0, width: 8, height: 40))
        txtCountry.leftView = view3
        txtCountry.leftViewMode = .always
        let view7 = UIView(frame: CGRect(x: 0, y: 0, width: 8, height: 40))
        txtState.leftView = view7
        txtState.leftViewMode = .always
        let view8 = UIView(frame: CGRect(x: 0, y: 0, width: 8, height: 40))
        txtCity.leftView = view8
        txtCity.leftViewMode = .always
        
        let view4 = UIView(frame: CGRect(x: 0, y: 0, width: 8, height: 40))
        txtMobile.leftView = view4
        txtMobile.leftViewMode = .always
        let view5 = UIView(frame: CGRect(x: 0, y: 0, width: 8, height: 40))
        txtPassword.leftView = view5
        txtPassword.leftViewMode = .always
        let view6 = UIView(frame: CGRect(x: 0, y: 0, width: 8, height: 40))
        txtrePassword.leftView = view6
        txtrePassword.leftViewMode = .always
        let view9 = UIView(frame: CGRect(x: 0, y: 0, width: 8, height: 40))
        txtCollegeName.leftView = view9
        txtCollegeName.leftViewMode = .always
        let viewRight9 = UIView(frame: CGRect(x: 0, y: 0, width: 30, height: 40))
        txtCollegeName.rightView = viewRight9
        txtCollegeName.rightViewMode = .always
        let view10 = UIView(frame: CGRect(x: 0, y: 0, width: 8, height: 40))
        txtManualCollegeName.leftView = view10
        txtManualCollegeName.leftViewMode = .always
        

    }
    
    //MARK: IBAction
    @IBAction func searchCollegeList(_ sender : Any) {
        
        self.txtManualCollegeName.isHidden = true
        self.txtCollegeName.isHidden = false
        self.txtManualCollegeName.resignFirstResponder()
        self.txtCollegeName.becomeFirstResponder()
    }
    
    
    @IBAction func btnActionBack(sender:UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnCheckAcceptTerms(sender:UIButton) {
        
        if sender.isSelected {
            registrationModel.acceptTerms = "0"
        } else {
            registrationModel.acceptTerms = "1"
        }
        sender.isSelected = !sender.isSelected
    }
    
    @IBAction func btnAcceptTerms(sender:UIButton) {
       NavigationManager.moveToTermsViewControllerIfNotExists(kAppDelegate.menuNavController!)
    }
    
    @IBAction func btnActionNext(sender:UIButton) {
        
        self.view.endEditing(true)
        
        if validateData() {
            self.callRegisterAPI()
        }
        
    }
    
    private func callRegisterAPI() {
        
        var params : [String:Any] = [:]
        
        params["user_type"] = registrationModel.userType
        
        // userType == 3 student, userType == 2 guardian
        
        if registrationModel.userType == 3 {
            
            params["title"] = registrationModel.studentTitle
            params["name"] = registrationModel.studentName
            params["email"] = registrationModel.studentEmail
            
        } else {
            params["title"] = registrationModel.guardianTitle
            params["name"] = registrationModel.guardianName
            params["email"] = registrationModel.guardianEmail
            /////////////For student under guardian
            params["student_email"] = registrationModel.studentEmail
            params["student_title"] = registrationModel.studentTitle
            params["student_name"] = registrationModel.studentName
        }
        
        params["country_id"] = registrationModel.countryID
        params["state_id"] = registrationModel.stateID
        params["city_id"] = registrationModel.cityID
        params["password"] = registrationModel.password
        params["c_password"] = registrationModel.rePassword
        params["is_accepted_terms"] = "1"
        params["grant_type"] = "password"
        params["client_id"] = kAppDelegate.kBOOL_LIVE ?"9":"16" //8
        params["client_secret"] = kAppDelegate.kBOOL_LIVE ? "BqpRsBdQhfSWNnK94NcCj3R5wBuoC0NNI6OB8zur" : "k87ciDnEFUr89TDL52YsjfRLu0vpgTCBZp3Qkj2Y"
       //P0kVPtAyWTG3UpnG6wIW1GkhdzcgOdUsEOcleKiQ"
        params["scope"] = "*"
        
        if let streamId = registrationModel.streamID {
            params["stream_id"] = streamId
        } else {
            params["stream_id"] = ""
        }
        if let specializationId = registrationModel.specializationID {
            params["category_id"] = specializationId
        } else {
            params["category_id"] = ""
        }
        
        params["mobile"] = registrationModel.mobileNumber
        
        if let studentType = registrationModel.studentType {
            
            if studentType == "School" {
                params["institute_id"] = registrationModel.schoolID
                
            } else {
                params["institute_id"] = registrationModel.yearID
            }
            params["institute_type"] = registrationModel.instituteType
            
        }
        if registrationModel.collegeID == nil{
            
             params["college_name"] = txtManualCollegeName.text
        }else{
            params["college_id"] = registrationModel.collegeID

        }
        print("params : \(params)")
        
        ProgressHUD.showProgressHUDWithLabel(label: "Loading...")
        
        APIManager.callPost("\(URLS.BASE_URL)\(URLS.REGISTER)", param: params, header: ["":""]) { (isSuccess, result, error) in
            
            DispatchQueue.main.async {
                
                ProgressHUD.hideProgressHUD()
                
                weak var weakSelf = self
                
                if (error != nil) {
                    weakSelf?.showAlert(title: "Error", message: (error!.localizedDescription), withTag: 101, button: ["Ok"], isActionRequired: false)
                    return
                }
                if isSuccess {
                    
                    do {
                        let jsonData = try JSONSerialization.jsonObject(with: result!, options: .mutableContainers)
                        
                        if (jsonData is NSDictionary){
                            
                            if let statusCode = (jsonData as! NSDictionary).object(forKey: "status"), (statusCode is Int) {
                                
                                if (statusCode as! Int) == 200 {
                                    
                                    let viewController = UIStoryboard.instantiateVerifyOTPViewController()
                                    viewController.isCommingFromRegistration = 1 //While registering
                                    
                                    if let userI = ((jsonData as! NSDictionary).object(forKey: "data") as? NSDictionary)?.object(forKey: "user_id") as? Int {
                                        viewController.userIDReceived = String(userI)
                                        
                                        UserModel.setLoginInfo(userData:   NSDictionary(objects: [String(userI) ], forKeys: [ "ID" as NSCopying  ]))
                                    }
                                  
                                   
                                    appDelegate.menuNavController?.pushViewController(viewController, animated: true)
                                    
                                } else if (statusCode as! Int) == 447 {
                                    var errStr = ""
                                    if let errormessage = (jsonData as? NSDictionary)?.object(forKey:"error") {
                                        if (errormessage is NSDictionary){
                                            if let errorDescription = (errormessage as? NSDictionary)?.object(forKey: "error_description"),!(errorDescription is NSNull){
                                                if (errorDescription is String){
                                                    errStr = errorDescription as! String
                                                    
                                                }
                                            }else {
                                                if let errorMessage = (errormessage as? NSDictionary)?.object(forKey: "message"),!(errorMessage is NSNull){
                                                    if (errorMessage is String){
                                                        errStr = errorMessage as! String
                                                        
                                                    }
                                                }else{
                                                    errStr = "Something went wrong"
                                                }
                                                
                                            }
                                            weakSelf?.showAlert(title: APP_NAME, message:errStr, withTag: 101, button: ["Ok"], isActionRequired: false)
                                        }
                                    }
                                    
                                    
                                } else {
                                        var errStr = ""
                                        if let errormessage = (jsonData as? NSDictionary)?.object(forKey:"error") {
                                            if (errormessage is NSDictionary){
                                                if let errorDescription = (errormessage as? NSDictionary)?.object(forKey: "error_description"),!(errorDescription is NSNull){
                                                    if (errorDescription is String){
                                                        errStr = errorDescription as! String
                                                        
                                                    }
                                                }else {
                                                    if let errorMessage = (errormessage as? NSDictionary)?.object(forKey: "message"),!(errorMessage is NSNull){
                                                        if (errorMessage is String){
                                                            errStr = errorMessage as! String
                                                            
                                                        }
                                                    }else{
                                                        errStr = "Something went wrong"
                                                    }
                                                    
                                                }
                                                weakSelf?.showAlert(title: APP_NAME, message:errStr, withTag: 101, button: ["Ok"], isActionRequired: false)
                                            }
                                        
                                    }else{
                                        weakSelf?.showAlert(title: APP_NAME, message: "Something went wrong!!", withTag: 101, button: ["Ok"], isActionRequired: false)
                                    }
                                    
                                }
                            }
                        }else{
                            
                            weakSelf?.showAlert(title: "sorry", message: AlertMessages.NOTADICTIONARYMESSAGE , withTag: 101, button:  ["Ok"], isActionRequired: false)
                            return
                        }
                        
                    }catch{
                        
                        weakSelf?.showAlert(title: "sorry", message: AlertMessages.MODELERRORMESSAGE , withTag: 101, button:  ["Ok"], isActionRequired: false)
                        return
                    }
                    
                } else{
                        
                        //Success false
                            weakSelf?.showAlert(title: APP_NAME, message: AlertMessages.UnidetifiedErrorMessage, withTag: 101, button: ["Ok"], isActionRequired: false)
                        
                    }
                
            }
        }
    }
    
    private func validateData() -> Bool {
        
        if registrationModel.studentTitle == nil {
            self.showAlert(title:APP_NAME, message:"Please select Student Title", withTag:101, button:["Ok"], isActionRequired:false)
            return false
        }
            //Its optional now*******************************
//            else if txtStudentName.text?.trim().count == 0 {
//            self.showAlert(title:APP_NAME, message:"Please enter Name", withTag:101, button:["Ok"], isActionRequired:false)
//            return false
//        } else if txtStudentEmail.text?.trim().count == 0 {
//            self.showAlert(title:APP_NAME, message:"Please enter Email", withTag:101, button:["Ok"], isActionRequired:false)
//            return false
//        } else
        if  (txtStudentEmail.text?.trim().count)! > 0 && !(txtStudentEmail.text?.isValidEmail())! {
            self.showAlert(title:APP_NAME, message:"Please enter valid Email", withTag:101, button:["Ok"], isActionRequired:false)
            return false
        }
        else if registrationModel.countryID == nil || (txtCountry.text?.count == 0) {
            self.showAlert(title:APP_NAME, message:"Please select Country", withTag:101, button:["Ok"], isActionRequired:false)
            return false
        } /* else if registrationModel.stateID == nil || (txtCountry.text?.count == 0) {
            self.showAlert(title:APP_NAME, message:"Please select State", withTag:101, button:["Ok"], isActionRequired:false)
            return false
        }  else if registrationModel.cityID == nil || (txtCountry.text?.count == 0) {
            self.showAlert(title:APP_NAME, message:"Please select City", withTag:101, button:["Ok"], isActionRequired:false)
            return false
        }*/ else if txtMobile.text?.trim().count == 0 {
            self.showAlert(title:APP_NAME, message:"Please enter Mobile Number", withTag:101, button:["Ok"], isActionRequired:false)
            return false
        } else if txtPassword.text?.trim().count == 0 {
            self.showAlert(title:APP_NAME, message:"Please enter Password", withTag:101, button:["Ok"], isActionRequired:false)
            return false
        }else if (txtPassword.text?.trim().count)! < 4 {
            self.showAlert(title:APP_NAME, message:"Please enter minimum 4 character Password", withTag:101, button:["Ok"], isActionRequired:false)
            return false
        } else if txtrePassword.text?.trim().count == 0 {
            self.showAlert(title:APP_NAME, message:"Please re-enter Password", withTag:101, button:["Ok"], isActionRequired:false)
            return false
        } else if txtPassword.text != txtrePassword.text {
            self.showAlert(title:APP_NAME, message:"Password and Confirm Password should be same", withTag:101, button:["Ok"], isActionRequired:false)
            return false
        } else if registrationModel.acceptTerms == "0" {
            self.showAlert(title:APP_NAME, message:"Please accept Terms and Condition", withTag:101, button:["Ok"], isActionRequired:false)
            return false
        } else {
            return true
        }
        
    }
}
extension RegisterStepTwoVC : HideADUIDelegate {
    //update add UI if no data available hide the view
    func removeAdcollectionViewUI()  {
        
        adView.isHidden = true
        adViewHightConstant.constant = 0
    }
}

extension RegisterStepTwoVC : UITextFieldDelegate, UIPickerViewDelegate, UIPickerViewDataSource {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if textField == txtStudentName {
            textField.becomeFirstResponder()
        } else if textField == txtStudentEmail {
            textField.becomeFirstResponder()
        } else if textField == txtCountry {
            textField.becomeFirstResponder()
        } else if textField == txtState {
            textField.becomeFirstResponder()
        }else if textField == txtCity {
            textField.becomeFirstResponder()
        }else if textField == txtMobile {
            textField.becomeFirstResponder()
        } else if textField == txtPassword {
            textField.becomeFirstResponder()
        } else if textField == txtrePassword {
            textField.becomeFirstResponder()
        } else {
            textField.resignFirstResponder()
        }
        
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        txtFieldPicker = textField
//        if textField == txtCountry {
//            DispatchQueue.main.async {
//                textField.resignFirstResponder()
//                self.showCountryView()
//            }
//        }
        if textField == txtCountry {
            txtCountry.text = ""
            txtCountry.tag = 0
            txtCity.text = ""
            txtCity.tag = 0
            txtState.text = ""
            txtState.tag = 0
            DispatchQueue.main.async {
                textField.resignFirstResponder()
                
                self.showCountryView(type: 1)
            }
        }
        if textField == txtState {
            
            txtCity.text = ""
            txtCity.tag = 0
            if (txtCountry.text?.isEmpty)! {
                
                self.showAlert(title: APP_NAME, message: "Please select country first", withTag: 100 , button: ["Ok"], isActionRequired: false)
                return
            }
            DispatchQueue.main.async {
                textField.resignFirstResponder()
                
                self.showCountryView(type: 2)
            }
        }
        if textField == txtCity {
            
            txtCollegeName.text = ""
            
            if (txtCountry.text?.isEmpty)! {
                
                self.showAlert(title: APP_NAME, message: "Please select country first", withTag: 100 , button: ["Ok"], isActionRequired: false)
                return
            } else   if (txtState.text?.isEmpty)! {
                
                self.showAlert(title: APP_NAME, message: "Please select state first",withTag: 101, button: ["Ok"], isActionRequired: false)
                return
            }
            
            DispatchQueue.main.async {
                textField.resignFirstResponder()
                
                self.showCountryView(type: 3)
            }
        }
        if textField == txtCollegeName {
            
            DispatchQueue.main.async {
                textField.resignFirstResponder()
                
                if (self.txtCity.text?.isEmpty)! {
                    
                    self.showAlert(title: APP_NAME, message: "Please select city first",withTag: 101, button: ["Ok"], isActionRequired: false)
                    return
                }
                
                self.showCountryView(type: 4)
            }
        }else if textField == txtStudentTitle {
            self.showPickerView()
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        if textField == txtStudentName {
            registrationModel.studentName = textField.text
        } else if textField == txtStudentEmail {
            registrationModel.studentEmail = textField.text
        } else if textField == txtMobile {
            registrationModel.mobileNumber = textField.text
        } else if textField == txtPassword {
            registrationModel.password = textField.text
        } else if textField == txtrePassword {
            registrationModel.rePassword = textField.text
        }
        
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField == txtMobile{
            let maxLength = 10
            let currentString: NSString = textField.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
        }else if textField == txtPassword {
            let maxLength = 8
            let currentString: NSString = textField.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
        }else if textField == txtrePassword {
            let maxLength = 8
            let currentString: NSString = textField.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
        }
        return true
       
    }
    
    // UIPickerView
    private func showPickerView() {
        
        picker = UIPickerView(frame: CGRect(x: 0, y: 0, width: screenWidth, height: 216))
        picker.backgroundColor = .white
        picker.showsSelectionIndicator = true
        picker.delegate = self
        picker.dataSource = self
        txtFieldPicker.inputView = picker
        
        let toolBar = UIToolbar()
        toolBar.barStyle = UIBarStyle.default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor.black
        toolBar.sizeToFit()
        
        let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItemStyle.plain, target: self, action: #selector(cancelActionPicker))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.plain, target: self, action: #selector(doneActionPicker))
        
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        txtFieldPicker.inputAccessoryView = toolBar
    }
    
    @objc func doneActionPicker (sender:UIBarButtonItem)
    {
        let tempRow = picker.selectedRow(inComponent: 0)
        if tempRow == 0 {
            self.txtFieldPicker.text = arr_Picker[tempRow]
            registrationModel.studentTitle = self.txtFieldPicker.text
        } else {
            registrationModel.studentTitle = self.txtFieldPicker.text
        }
        
        txtFieldPicker.resignFirstResponder()
        
    }
    
    @objc func cancelActionPicker (sender:UIBarButtonItem)
    {
        if txtFieldPicker != txtStudentTitle {
            txtFieldPicker.text = ""
        }
        txtFieldPicker.resignFirstResponder()
        registrationModel.studentTitle = nil
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return arr_Picker.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return arr_Picker[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        self.txtFieldPicker.text = arr_Picker[row]
    }
    
}

extension RegisterStepTwoVC : CountryViewDelegate {
    
    func otherEntry(){
        
        self.txtManualCollegeName.isHidden = false
        self.txtCollegeName.isHidden = true
        self.txtManualCollegeName.placeholder = "please enter college name"
        
        registrationModel.collegeID = nil
        txtCollegeName.text = ""
        
        self.btnserchCollegeList.isHidden = false
        self.btnserchCollegeList.backgroundColor = .clear
        self.btnserchCollegeList.setTitleColor(.gray, for:  self.btnserchCollegeList.state)
        self.btnserchCollegeList.borderColor = .gray
        
        
        
        self.txtManualCollegeName.isHidden = false
        self.txtCollegeName.isHidden = true
    }
     func showCountryView(type : Int){
        
        countryView = Bundle.main.loadNibNamed("CountryView", owner: self, options: nil)?[0] as? CountryView
        countryView?.delegate = self
        countryView?.registerNibs()
        countryView?.managerRadius()
        countryView?.tag = type
        appDelegate.window?.addSubview(countryView!)
        countryView?.alpha = 0
        countryView?.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height)
        self.countryView?.btnOther.isHidden = true

        UIView.animate(withDuration: 0.3, animations: {
            self.countryView?.alpha = 1
        } ,completion: { (done) in
            // self.countryView?.getAllCountries()     switch (self.countryView?.tag){
            switch (self.countryView?.tag){

            case 1:   self.countryView?.lblTitle.text = "Select Country"
            self.countryView?.getAllCountries()
            
            case 2: self.countryView?.lblTitle.text = "Select State"
            self.countryView?.getAllStates(countryId: registrationModel.countryID!  )
            
            case 3:  self.countryView?.lblTitle.text = "Select City"
            self.countryView?.getAllCities(stateid: registrationModel.stateID!)
                
            case 4:  self.countryView?.lblTitle.text = "Select College"
            //its college name
            //For manuall entry purpose show other button
            self.countryView?.btnOther.isHidden = false
            self.countryView?.getAllColleges(cityid: registrationModel.cityID!)
            default: print("not correct tag")
            
        }
        } )
    }
    
   
    func removeCountryView()  {
        self.countryView?.alpha = 1
        UIView.animate(withDuration: 0.3, animations: {
            self.countryView?.alpha = 0
        } ,completion: { (done) in
            self.countryView?.removeFromSuperview()
            self.countryView = nil
        } )
    }
    
    func getCountryCode(data: NSDictionary , type :Int) {
        
        if type == 1{
            let tempStr = String(describing: (data.object(forKey: "id")!))
            txtCountry.text = (data.object(forKey: "name")) as? String
            registrationModel.countryID = tempStr
        }else  if type == 2{
            let tempStr = String(describing: (data.object(forKey: "id")!))
            txtState.text = (data.object(forKey: "name")) as? String
            registrationModel.stateID = tempStr
        }else  if type == 3{
            let tempStr = String(describing: (data.object(forKey: "id")!))
            txtCity.text = (data.object(forKey: "name")) as? String
            registrationModel.cityID = tempStr
        }
        else  if type == 4{
            let tempStr = String(describing: (data.object(forKey: "id")!))
            txtCollegeName.text = (data.object(forKey: "name")) as? String
            registrationModel.collegeID = tempStr
        }
    }
    
}


