//
//  RegisterStepOneVC.swift
//  MyChhatri
//
//  Created by Anshul on 10/07/18.
//  Copyright © 2018 Anshul. All rights reserved.
//

import UIKit

class RegisterStepOneVC: UIViewController {

    @IBOutlet weak var viewGuardianDetailBGHeightConstraint:NSLayoutConstraint!
    
    @IBOutlet weak var btnUserGuardianType:UIButton!
    @IBOutlet weak var btnUserStudentType:UIButton!
    @IBOutlet weak var imgGuardianType:UIImageView!
    @IBOutlet weak var imgStudentType:UIImageView!
    @IBOutlet weak var viewGuardianDetailBG:UIView!
    @IBOutlet weak var txtGuardianTitle:UITextField!
    @IBOutlet weak var txtGuardianName:UITextField!
    @IBOutlet weak var txtGuardianEmail:UITextField!
    @IBOutlet weak var txtStudentType:UITextField!
    @IBOutlet weak var txtClassOrYear:UITextField!
    @IBOutlet weak var txtStream:UITextField!
    @IBOutlet weak var txtSpecialization:UITextField!
    @IBOutlet weak var btnNext:UIButton!
    
    
    @IBOutlet weak var viewSpecialization:UIView!
    @IBOutlet weak var viewStream:UIView!
    @IBOutlet weak var imgStreamDropdown:UIImageView!
    @IBOutlet weak var imgSpecializationDropdown:UIImageView!
    @IBOutlet weak var lblStream:UILabel!
    @IBOutlet weak var lblSpecialization:UILabel!
    
    fileprivate lazy var arrTitle = ["Mr.","Mrs.","Ms.","Miss."]
    fileprivate lazy var arrStudentType = ["College","School"]
    fileprivate lazy var arrClass = NSArray()
    fileprivate lazy var arrYear = NSArray()
    fileprivate lazy var arrStream = ["Stream 1","Stream 2","Stream 3","Stream 4","Stream 5"]
    fileprivate lazy var arrSpecialization = ["Specialization 1","Specialization 2","Specialization 3","Specialization 4","Specialization 5"]
    
    //*********Picker Setup*********//
    private var picker = UIPickerView()
    private var txtFieldPicker = UITextField()
    private var arr_Picker = [""]
    //*********Picker Setup*********//
    
    var arrStreamSchool = NSArray()
    var arrStreamCollege = NSArray()
    var arrSpecializationServer = NSArray()
    
    @IBOutlet weak var adViewHightConstant: NSLayoutConstraint!
    @IBOutlet weak var adView : CustomAdvertiseView!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.initialViewSetup()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if(adView != nil) {
            adView.isHidden = false
            adView.hideAdViewDel = self
            adViewHightConstant.constant = 60
            adView.categoryIDForAdvertise = "8"
            adView.typeFoAdvertiseData = "1"
        }
        adView.awakeFromNib()
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        adView.hideAdViewDel = nil
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    private func initialViewSetup() {
        btnNext.addGrediant()
        
        registrationModel = RegistrationModel()
        
        registrationModel.userType = 2
        registrationModel.guardianTitle = "Mr."
        
        let viewGuardianTitle = UIView(frame: CGRect(x: 0, y: 0, width: 8, height: 40))
        txtGuardianTitle.leftView = viewGuardianTitle
        txtGuardianTitle.leftViewMode = .always
        let viewGuardianName = UIView(frame: CGRect(x: 0, y: 0, width: 8, height: 40))
        txtGuardianName.leftView = viewGuardianName
        txtGuardianName.leftViewMode = .always
        let viewGuardianEmail = UIView(frame: CGRect(x: 0, y: 0, width: 8, height: 40))
        txtGuardianEmail.leftView = viewGuardianEmail
        txtGuardianEmail.leftViewMode = .always
        let viewStudentType = UIView(frame: CGRect(x: 0, y: 0, width: 8, height: 40))
        txtStudentType.leftView = viewStudentType
        txtStudentType.leftViewMode = .always
        let viewClassOrYear = UIView(frame: CGRect(x: 0, y: 0, width: 8, height: 40))
        txtClassOrYear.leftView = viewClassOrYear
        txtClassOrYear.leftViewMode = .always
        let viewStream = UIView(frame: CGRect(x: 0, y: 0, width: 8, height: 40))
        txtStream.leftView = viewStream
        txtStream.leftViewMode = .always
        let viewSpecialization = UIView(frame: CGRect(x: 0, y: 0, width: 8, height: 40))
        txtSpecialization.leftView = viewSpecialization
        txtSpecialization.leftViewMode = .always

        
    }
    
    //MARK: IBAction
    @IBAction func btnActionBack(sender:UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnActionUserGuardianType(sender:UIButton) {
        
        registrationModel.userType = 2
        
        self.imgGuardianType.image = UIImage(named: "Group")
        self.imgStudentType.image = UIImage(named: "Rectangle")
        
        self.viewGuardianDetailBG.isHidden = false
        self.viewGuardianDetailBGHeightConstraint.constant = 166
        
        self.txtSpecialization.isUserInteractionEnabled = true
        self.txtSpecialization.isHidden = false
        self.lblSpecialization.isHidden = false
        self.viewSpecialization.isHidden = false
        self.txtStream.isUserInteractionEnabled = true
        self.txtStream.isHidden = false
        self.lblStream.isHidden = false
        self.viewStream.isHidden = false
        
        registrationModel.studentType = nil
        registrationModel.classOrYear = nil
        registrationModel.stream = nil
        registrationModel.streamID = nil
        registrationModel.specialization = nil
        registrationModel.specializationID = nil
        
        self.txtStudentType.text = ""
        self.txtClassOrYear.text = ""
        self.txtStream.text = ""
        self.txtSpecialization.text = ""
    }
    
    @IBAction func btnActionUserStudentType(sender:UIButton) {
        
        registrationModel.userType = 3
        //registrationModel.guardianTitle = nil
        registrationModel.guardianName = nil
        registrationModel.guardianEmail = nil
        
        self.imgGuardianType.image = UIImage(named: "Rectangle")
        self.imgStudentType.image = UIImage(named: "Group")
        
        self.viewGuardianDetailBG.isHidden = true
        self.viewGuardianDetailBGHeightConstraint.constant = 0
        self.txtGuardianTitle.text = "Mr."
        self.txtGuardianName.text = ""
        self.txtGuardianEmail.text = ""
        
        self.txtSpecialization.isUserInteractionEnabled = false
        self.txtSpecialization.isHidden = true
        self.lblSpecialization.isHidden = true
        self.viewSpecialization.isHidden = true
        registrationModel.studentType = nil
        registrationModel.classOrYear = nil
        registrationModel.stream = nil
        registrationModel.streamID = nil
        registrationModel.specialization = nil
        registrationModel.specializationID = nil
        
        self.txtStudentType.text = ""
        self.txtClassOrYear.text = ""
        self.txtStream.text = ""
        self.txtSpecialization.text = ""
        
    }
    
    @IBAction func btnActionNext(sender:UIButton) {
        
        if validateData() {
            
            let viewController = UIStoryboard.instantiateRegisterStepTwoViewController()
            appDelegate.menuNavController?.pushViewController(viewController, animated: true)
        }
        
    }
    
    private func validateData() -> Bool {
        
        if registrationModel.userType == 2 {
            
            if registrationModel.guardianTitle == nil {
                self.showAlert(title:APP_NAME, message:"Please select Guardian Title", withTag:101, button:["Ok"], isActionRequired:false)
                return false
            } else if registrationModel.guardianName == nil {
                self.showAlert(title:APP_NAME, message:"Please enter Guardian Name", withTag:101, button:["Ok"], isActionRequired:false)
                return false
            } else if registrationModel.guardianEmail == nil {
                self.showAlert(title:APP_NAME, message:"Please enter Guardian Email", withTag:101, button:["Ok"], isActionRequired:false)
                return false
            } else if !(txtGuardianEmail.text?.isValidEmail())! {
                self.showAlert(title:APP_NAME, message:"Please enter valid Email", withTag:101, button:["Ok"], isActionRequired:false)
                return false
            }
        }
        
        if registrationModel.studentType == nil {
            self.showAlert(title:APP_NAME, message:"Please select Student Type", withTag:101, button:["Ok"], isActionRequired:false)
            return false
        } else {
            
            if registrationModel.classOrYear == nil {
                self.showAlert(title:APP_NAME, message:"Please select Class/Year", withTag:101, button:["Ok"], isActionRequired:false)
                return false
            }
            
            if registrationModel.studentType == "School" {
                
                if Int(registrationModel.classOrYear!)! > 10 {
                    if registrationModel.streamID == nil  {
                        self.showAlert(title:APP_NAME, message:"Please select Stream", withTag:101, button:["Ok"], isActionRequired:false)
                        return false
                    }
                }
            } else {
                if registrationModel.streamID == nil  {
                    self.showAlert(title:APP_NAME, message:"Please select Stream", withTag:101, button:["Ok"], isActionRequired:false)
                    return false
                }
                //else if registrationModel.specializationID == nil {
//                    self.showAlert(title:APP_NAME, message:"Please select Specialization", withTag:101, button:["Ok"], isActionRequired:false)
//                    return false
//                }
            }
        }
        
        return true
    }
}

extension RegisterStepOneVC : HideADUIDelegate {
    //update add UI if no data available hide the view
    func removeAdcollectionViewUI()  {
        
        adView.isHidden = true
        adViewHightConstant.constant = 0
    }
}
extension RegisterStepOneVC : UITextFieldDelegate, UIPickerViewDelegate, UIPickerViewDataSource {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        txtFieldPicker = textField
        if textField == txtGuardianTitle {
            self.arr_Picker = arrTitle
            self.showPickerView()
            
        } else if textField == txtStudentType {
            self.arr_Picker = arrStudentType
            self.showPickerView()
            
        } else if textField == txtClassOrYear {
            
            if registrationModel.studentType == nil || txtStudentType.text?.count == 0 {
                self.showAlert(title: APP_NAME, message: "Please select Student Type first.", withTag: 101, button: ["ok"], isActionRequired: false)
            } else {
                // call api to load stream if its not loaded before
                // Else just reload picker
                
                if let studentSelectedType = txtStudentType.text {
                    
                    loadDataForInstituteType(StudentType: studentSelectedType)
                }
                
            }
        } else if textField == txtStream {
            if registrationModel.classOrYear == nil || txtClassOrYear.text?.count == 0 {
                self.showAlert(title: APP_NAME, message: "Please select Class/Year first.", withTag: 101, button: ["ok"], isActionRequired: false)
            } else {
                // call api to load stream
                self.callStreanAPI()
            }
        } else if textField == txtSpecialization {
            if registrationModel.stream == nil || txtStream.text?.count == 0 {
                self.showAlert(title: APP_NAME, message: "Please select Stream first.", withTag: 101, button: ["ok"], isActionRequired: false)
            } else {
                // call api to load specialization
                self.callSpecializationAPI()
            }
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == txtGuardianTitle {
            registrationModel.guardianTitle = textField.text
        } else if textField == txtGuardianName {
            registrationModel.guardianName = textField.text
        } else if textField == txtGuardianEmail {
            registrationModel.guardianEmail = textField.text
        } else if textField == txtStudentType {
            
            if textField.text == "" {
                //nothing to do...
            } else {
                
                if let studentType = registrationModel.studentType {
                    if studentType == textField.text {
                        //nothing to do...
                    } else {
                        // data is changed.... reset data....
                        registrationModel.studentType = textField.text
                        
                        registrationModel.classOrYear = nil
                        registrationModel.stream = nil
                        registrationModel.streamID = nil
                        registrationModel.specialization = nil
                        registrationModel.specializationID = nil
                        self.txtClassOrYear.text = ""
                        self.txtStream.text = ""
                        self.txtSpecialization.text = ""
                    }
                } else {
                    registrationModel.studentType = textField.text
                }
            }
            
        } else if textField == txtClassOrYear {
            
            if textField.text == "" {
                //nothing to do...
            } else {
                
                if let classOrYear = registrationModel.classOrYear {
                    if classOrYear == textField.text {
                        //nothing to do...
                    } else {
                        // data is changed.... reset data....
                        registrationModel.classOrYear = textField.text
                        
                        registrationModel.stream = nil
                        registrationModel.streamID = nil
                        registrationModel.specialization = nil
                        registrationModel.specializationID = nil
                        self.txtStream.text = ""
                        self.txtSpecialization.text = ""
                    }
                } else {
                    registrationModel.classOrYear = textField.text
                }
            }
            
        } else if textField == txtStream {
            
            if textField.text == "" {
                //nothing to do...
            } else {
                
                if let classOrYear = registrationModel.stream {
                    if classOrYear == textField.text {
                        //nothing to do...
                    } else {
                        // data is changed.... reset data....
                        registrationModel.stream = textField.text
                        
                        registrationModel.specialization = nil
                        registrationModel.specializationID = nil
                        self.txtSpecialization.text = ""
                    }
                } else {
                    registrationModel.stream = textField.text
                }
            }
            
        } else if textField == txtSpecialization {
            registrationModel.specialization = textField.text
        }
    }
    
    // Picker Setup
    // UIPickerView
    private func showPickerView() {
        
        picker = UIPickerView(frame: CGRect(x: 0, y: 0, width: screenWidth, height: 216))
        picker.backgroundColor = .white
        picker.showsSelectionIndicator = true
        picker.delegate = self
        picker.dataSource = self
        txtFieldPicker.inputView = picker
        
        let toolBar = UIToolbar()
        toolBar.barStyle = UIBarStyle.default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor.black
        toolBar.sizeToFit()
        
        let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItemStyle.plain, target: self, action: #selector(cancelActionPicker))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.plain, target: self, action: #selector(doneActionPicker))
        
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        txtFieldPicker.inputAccessoryView = toolBar
    }
    
    @objc func doneActionPicker (sender:UIBarButtonItem)
    {
        let tempRow = picker.selectedRow(inComponent: 0)
        if tempRow == 0 {
            self.txtFieldPicker.text = arr_Picker[tempRow]
            if self.txtFieldPicker == self.txtGuardianTitle {
                registrationModel.guardianTitle = self.txtGuardianTitle.text
            }
        } else {
            if self.txtFieldPicker == self.txtGuardianTitle {
                registrationModel.guardianTitle = self.txtGuardianTitle.text
            }
        }
        txtFieldPicker.resignFirstResponder()
        
        if self.txtFieldPicker == self.txtStudentType {
            if self.txtFieldPicker.text == "School" {
                self.txtSpecialization.isUserInteractionEnabled = false
                self.txtSpecialization.isHidden = true
                self.lblSpecialization.isHidden = true
                self.viewSpecialization.isHidden = true
                registrationModel.instituteType = 1
            } else {
                self.txtSpecialization.isUserInteractionEnabled = true
                self.txtSpecialization.isHidden = false
                self.lblSpecialization.isHidden = false
                self.viewSpecialization.isHidden = false
                self.txtStream.isUserInteractionEnabled = true
                self.txtStream.isHidden = false
                self.lblStream.isHidden = false
                self.viewStream.isHidden = false
                registrationModel.instituteType = 2

            }
        }
        if self.txtFieldPicker == self.txtClassOrYear {
            if self.txtStudentType.text == "School" {
                if let classOrYear = registrationModel.classOrYear {
                    if Int(classOrYear)! > 10 {
                        self.txtStream.isUserInteractionEnabled = true
                        self.txtStream.isHidden = false
                        self.lblStream.isHidden = false
                        self.viewStream.isHidden = false
                    } else {
                        self.txtStream.isUserInteractionEnabled = false
                        self.txtStream.isHidden = true
                        self.lblStream.isHidden = true
                        self.viewStream.isHidden = true
                    }
                } else {
                    self.txtStream.isUserInteractionEnabled = true
                    self.txtStream.isHidden = false
                    self.lblStream.isHidden = false
                    self.viewStream.isHidden = false
                }
            } else {
                self.txtStream.isUserInteractionEnabled = true
                self.txtStream.isHidden = false
                self.lblStream.isHidden = false
                self.viewStream.isHidden = false
            }
            
            if self.txtStudentType.text == "School" {
                
                if let sID = (arrClass[tempRow] as! NSDictionary).object(forKey: "id") as? String {
                    
                     registrationModel.schoolID =  sID
                    
                }else if let sID = (arrClass[tempRow] as! NSDictionary).object(forKey: "id") as? Int{
                    
                    registrationModel.schoolID  = String(sID)
                    
                }

            } else {
                
                if let sID = (arrYear[tempRow] as! NSDictionary).object(forKey: "id") as? String {
                    
                    registrationModel.yearID =  sID
                    
                }else if let sID = (arrYear[tempRow] as! NSDictionary).object(forKey: "id") as? Int{
                    
                    registrationModel.yearID  = String(sID)
                    
                }
                
            }
        }
        if self.txtFieldPicker == self.txtStream {
            
            if self.txtStudentType.text == "School" {
                
                guard let dict = arrStreamSchool[tempRow] as? NSDictionary else{
                    self.showAlert(title: "", message: AlertMessages.NOTADICTIONARYMESSAGE, withTag: 101, button: ["OK"], isActionRequired: false)
                    return
                }
                
                if let sID = dict.object(forKey: "id") as? String{
                    
                    registrationModel.streamID = sID
                    
                }else if let sID = dict.object(forKey: "id") as? Int{
                    registrationModel.streamID = String(sID)
                    
                }
                else{
                    self.showAlert(title: "Server Error", message: "Id could not be fetched by Stream", withTag: 101, button: ["OK"], isActionRequired: false)
                }
                
            } else {
                
                guard let dict = arrStreamCollege[tempRow] as? NSDictionary else{
                    self.showAlert(title: "", message: AlertMessages.NOTADICTIONARYMESSAGE, withTag: 101, button: ["OK"], isActionRequired: false)
                    return
                }
                
                if let sID = dict.object(forKey: "id") as? String{
                    
                    registrationModel.streamID = sID
                    
                }else if let sID = dict.object(forKey: "id") as? Int{
                    registrationModel.streamID = String(sID)
                    
                }
                else{
                    self.showAlert(title: "Server Error", message: "Id could not be fetched by Stream", withTag: 101, button: ["OK"], isActionRequired: false)
                }
            }
        }
        if self.txtFieldPicker == self.txtSpecialization {
            
            if let sID = (arrSpecializationServer[tempRow] as! NSDictionary).object(forKey: "id") as? String{
                
                registrationModel.specializationID = sID
                
            }else if let sID = (arrSpecializationServer[tempRow] as! NSDictionary).object(forKey: "id") as? Int{
                registrationModel.specializationID = String(sID)
                
            }
            
            //
            //            registrationModel.specializationID = (arrSpecializationServer[tempRow] as! NSDictionary).object(forKey: "id") as? String
        }
    }
    
    @objc func cancelActionPicker (sender:UIBarButtonItem)
    {
        if txtFieldPicker != txtGuardianTitle {
            txtFieldPicker.text = ""
        }
        txtFieldPicker.resignFirstResponder()
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return arr_Picker.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return arr_Picker[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        self.txtFieldPicker.text = arr_Picker[row]
    }
    
}

extension RegisterStepOneVC {
    
    
    func loadDataForInstituteType(StudentType: String)  {
        
        if StudentType == "College" {
            
            if (self.arrYear.count) > 0 {
                
                guard (self.arrYear.value(forKey: "standard_value") as? [String]) != nil  else {
                    return
                }
                
                self.arr_Picker = self.arrYear.value(forKey: "standard_value") as! [String]
                
                self.showPickerView()
            } else {
                
               getInstituteType()
            }
        } else if  StudentType == "School"{
            
            if (self.arrClass.count) > 0 {
                
                guard self.arrClass.value(forKey: "standard_value") as? [String] != nil  else {
                    return
                }
                
                self.arr_Picker = self.arrClass.value(forKey: "standard_value") as! [String]
                
                self.showPickerView()
                
            } else {
                
                getInstituteType()
            }
        }
    }
    
    private func getInstituteType() {
        
        ProgressHUD.showProgressHUDWithLabel(label: "Loading...")
        
        APIManager.callGetWithoutAuthantication("\(URLS.BASE_URL)\(URLS.INSTITUTE_TYPE)", param: nil, header: ["":""]) { (isSuccess, result, error) in
            
            DispatchQueue.main.async {
                
                weak var weakSelf = self
                
                ProgressHUD.hideProgressHUD()
                
                if (error != nil) {
                    
                    weakSelf?.showAlert(title: "Error", message: (error!.localizedDescription), withTag: 101, button: ["Ok"], isActionRequired: false)
                    return
                    
                }
                
                if isSuccess {
                    
                    do {
                        let jsonData = try JSONSerialization.jsonObject(with: result!, options: .mutableContainers)
                        
                        if (jsonData is NSDictionary){
                            
                            if let statusCode = (jsonData as! NSDictionary).object(forKey: "status"), (statusCode is Int) {
                                
                                if (statusCode as! Int) == 200 {
                                    if let data = (jsonData as! NSDictionary).object(forKey: "data"), (data is NSDictionary) {
                                        if (data as! NSDictionary).count > 0 {
                                            
                                            if let collegeArr = (data as! NSDictionary).object(forKey: "college"), (collegeArr is NSArray) {
                                                
                                                if (collegeArr as! NSArray).count > 0 {
                                                    
                                                    weakSelf?.arrYear = (collegeArr as! NSArray)
                                                }
                                            }
                                            if let schoolArr = (data as! NSDictionary).object(forKey: "school"), (schoolArr is NSArray) {
                                                if (schoolArr as! NSArray).count > 0 {
                                                    
                                                    weakSelf?.arrClass = (schoolArr as! NSArray)
                                                }
                                            }
                                        }
                                        
                                        if weakSelf?.txtStudentType.text == "College" {
                                            
                                            if (weakSelf?.arrYear.count)! > 0 {
                                                
                                                guard (weakSelf?.arrYear.value(forKey: "standard_value") as? [String]) != nil  else {
                                                    return
                                                }
                                                
                                                weakSelf?.arr_Picker = weakSelf?.arrYear.value(forKey: "standard_value") as! [String]
                                                
                                                weakSelf?.showPickerView()
                                            } else {
                                                self.view.endEditing(true)
                                            }
                                        } else if  weakSelf?.txtStudentType.text == "School"{
                                            
                                            if (weakSelf?.arrClass.count)! > 0 {
                                                
                                                guard weakSelf?.arrClass.value(forKey: "standard_value") as? [String] != nil  else {
                                                    return
                                                }
                                                
                                                weakSelf?.arr_Picker = weakSelf?.arrClass.value(forKey: "standard_value") as! [String]
                                                
                                                weakSelf?.showPickerView()
                                                
                                            } else {
                                                
                                                self.view.endEditing(true)
                                            }
                                        }
                                        else{
                                            
                                            weakSelf?.showAlert(title: APP_NAME, message:"No relevant data found \(String(describing: weakSelf?.txtStudentType.text))", withTag: 101, button: ["Ok"], isActionRequired: false)
                                        }
                                    }
                                } else{
                                    
                                    //some other status code
                                    //check error message in the response
                                    var errStr = ""
                                    if let errormessage = (jsonData as? NSDictionary)?.object(forKey:"error") {
                                        if (errormessage is NSDictionary){
                                            if let errorDescription = (errormessage as? NSDictionary)?.object(forKey: "error_description"),!(errorDescription is NSNull){
                                                if (errorDescription is String){
                                                    errStr = errorDescription as! String
                                                    
                                                }
                                            }else {
                                                if let errorMessage = (errormessage as? NSDictionary)?.object(forKey: "message"),!(errorMessage is NSNull){
                                                    if (errorMessage is String){
                                                        errStr = errorMessage as! String
                                                        
                                                    }
                                                }else{
                                                    errStr = AlertMessages.UnidetifiedErrorMessage
                                                }
                                                
                                            }
                                            weakSelf?.showAlert(title: APP_NAME, message:errStr, withTag: 101, button: ["Ok"], isActionRequired: false)
                                        }
                                        
                                    }
                                }
                            }
                            else{
                                
                                //not a dictionary
                                weakSelf?.showAlert(title: "sorry", message: AlertMessages.NOTADICTIONARYMESSAGE , withTag: 101, button:  ["Ok"], isActionRequired: false)
                                return
                            }
                        }
                    }
                    catch{
                        
                        weakSelf?.showAlert(title: "sorry", message: AlertMessages.MODELERRORMESSAGE , withTag: 101, button:  ["Ok"], isActionRequired: false)
                        return
                    }
                }else{
                    //issucess = false
                    //show alert
                    
                    weakSelf?.showAlert(title: "sorry", message: AlertMessages.UnidetifiedErrorMessage , withTag: 101, button:  ["Ok"], isActionRequired: false)
                    return
                }
                
            }
        }
    }
    
    private func callStreanAPI() {
        
        ProgressHUD.showProgressHUDWithLabel(label: "Loading...")
        
        APIManager.callGetWithoutAuthantication("\(URLS.BASE_URL)\(URLS.STREAM)", param: nil, header: ["":""]) { (isSuccess, result, error) in
            
            DispatchQueue.main.async {
                
                weak var weakSelf = self
                
                ProgressHUD.hideProgressHUD()
                
                if (error != nil) {
                    
                    weakSelf?.showAlert(title: "Error", message: (error!.localizedDescription), withTag: 101, button: ["Ok"], isActionRequired: false)
                    return
                }
                    if isSuccess {
                        
                        do {
                            let jsonData = try JSONSerialization.jsonObject(with: result!, options: .mutableContainers)
                            
                            if (jsonData is NSDictionary){
                                
                                if let statusCode = (jsonData as! NSDictionary).object(forKey: "status"), (statusCode is Int) {
                                    
                                    if (statusCode as! Int) == 200 {
                                        
                                        if let data = (jsonData as! NSDictionary).object(forKey: "data"), (data is NSDictionary) {
                                            
                                            if (data as! NSDictionary).count > 0 {
                                                if let collegeArr = (data as! NSDictionary).object(forKey: "college"), (collegeArr is NSArray) {
                                                    if (collegeArr as! NSArray).count > 0 {
                                                        weakSelf?.arrStreamCollege = (collegeArr as! NSArray)
                                                    }
                                                }
                                                if let schoolArr = (data as! NSDictionary).object(forKey: "school"), (schoolArr is NSArray) {
                                                    if (schoolArr as! NSArray).count > 0 {
                                                        weakSelf?.arrStreamSchool = (schoolArr as! NSArray)
                                                    }
                                                }
                                            }
                                            
                                            if weakSelf?.txtStudentType.text == "College" {
                                                if (weakSelf?.arrStreamCollege.count)! > 0 {
                                                    weakSelf?.arr_Picker = weakSelf?.arrStreamCollege.value(forKey: "title") as! [String]
                                                    weakSelf?.showPickerView()
                                                } else {
                                                    self.view.endEditing(true)
                                                }
                                            } else {
                                                if (weakSelf?.arrStreamSchool.count)! > 0 {
                                                    
                                                    if  weakSelf?.arrStreamSchool.value(forKey: "title") != nil {
                                                        weakSelf?.arr_Picker = (weakSelf?.arrStreamSchool.value(forKey: "title") as?  [String] ?? [""])
                                                        weakSelf?.showPickerView()
                                                    }else{
                                                       self.view.endEditing(true)
                                                    }
                                                }else {
                                                    self.view.endEditing(true)
                                                }
                                            }
                                        } else {
                                            self.view.endEditing(true)
                                        }
                                    } else {
                                        
                                        //some other status code
                                        //check error message in the response
                                        var errStr = ""
                                        if let errormessage = (jsonData as? NSDictionary)?.object(forKey:"error") {
                                            if (errormessage is NSDictionary){
                                                if let errorDescription = (errormessage as? NSDictionary)?.object(forKey: "error_description"),!(errorDescription is NSNull){
                                                    if (errorDescription is String){
                                                        errStr = errorDescription as! String
                                                        
                                                    }
                                                }else {
                                                    if let errorMessage = (errormessage as? NSDictionary)?.object(forKey: "message"),!(errorMessage is NSNull){
                                                        if (errorMessage is String){
                                                            errStr = errorMessage as! String
                                                            
                                                        }
                                                    }else{
                                                        errStr = AlertMessages.UnidetifiedErrorMessage
                                                    }
                                                    
                                                }
                                                weakSelf?.showAlert(title: APP_NAME, message:errStr, withTag: 101, button: ["Ok"], isActionRequired: false)
                                            }
                                            
                                        }
                                        
                                    }
                                }
                            }
                            else {
                                //Not a dictionary
                                weakSelf?.showAlert(title: "sorry", message: AlertMessages.NOTADICTIONARYMESSAGE , withTag: 101, button:  ["Ok"], isActionRequired: false)
                                return
                            }
                        }catch{
                            weakSelf?.showAlert(title: "sorry", message: AlertMessages.MODELERRORMESSAGE , withTag: 101, button:  ["Ok"], isActionRequired: false)
                            return
                        }
                        
                    } else{
                        //issucess = false
                        //show alert
                        
                        weakSelf?.showAlert(title: "sorry", message: AlertMessages.UnidetifiedErrorMessage , withTag: 101, button:  ["Ok"], isActionRequired: false)
                        return
                }
                
            }
        }
    }
    
    private func callSpecializationAPI() {
        
        
        guard let regisID = registrationModel.streamID  else {
            
            self.showAlert(title: "Soory", message: "Registration ID is not selected", withTag: 101, button: ["Ok"], isActionRequired: false)
            return
        }
        ProgressHUD.showProgressHUDWithLabel(label: "Loading...")

        APIManager.callGetWithoutAuthantication("\(URLS.BASE_URL)collection/\(regisID)\(URLS.REGISTER_SPECIALIZATION)", param: nil, header: ["":""]) { (isSuccess, result, error) in
            
            DispatchQueue.main.async {
                
                weak var weakSelf = self
                
                ProgressHUD.hideProgressHUD()
                
                if (error != nil) {
                    weakSelf?.showAlert(title: "Error", message: (error!.localizedDescription), withTag: 101, button: ["Ok"], isActionRequired: false)
                } else {
                    if isSuccess {
                        
                        do {
                            let jsonData = try JSONSerialization.jsonObject(with: result!, options: .mutableContainers)
                            
                            if (jsonData is NSDictionary){
                                
                                if let statusCode = (jsonData as! NSDictionary).object(forKey: "status"), (statusCode is Int) {
                                    
                                    
                                    if (statusCode as! Int) == 200 {
                                        if let data = (jsonData as! NSDictionary).object(forKey: "data"), (data is NSDictionary) {
                                            if (data as! NSDictionary).count > 0 {
                                                if let categories = (data as! NSDictionary).object(forKey: "categories"), (categories is NSArray) {
                                                    if (categories as! NSArray).count > 0 {
                                                        weakSelf?.arrSpecializationServer = (categories as! NSArray)
                                                        weakSelf?.arr_Picker = weakSelf?.arrSpecializationServer.value(forKey: "title") as! [String]
                                                        weakSelf?.showPickerView()
                                                    }else{
                                                        //No specialization under stream is found
                                                        weakSelf?.arrSpecializationServer = NSArray()
                                                        weakSelf?.arr_Picker = []
                                                        
                                                         self.showAlert(title: APP_NAME, message: "No specialization found, please proceed.", withTag: 101, button: ["ok"], isActionRequired: false)
                                                        
                                                        self.view.endEditing(true)
                                                    }
                                                }else{
                                                    
                                                    self.view.endEditing(true)

                                                }
                                            }
                                        }
                                    } else {
                                        
                                        //some other status code
                                        //check error message in the response
                                        var errStr = ""
                                        if let errormessage = (jsonData as? NSDictionary)?.object(forKey:"error") {
                                            if (errormessage is NSDictionary){
                                                if let errorDescription = (errormessage as? NSDictionary)?.object(forKey: "error_description"),!(errorDescription is NSNull){
                                                    if (errorDescription is String){
                                                        errStr = errorDescription as! String
                                                        
                                                    }
                                                }else {
                                                    if let errorMessage = (errormessage as? NSDictionary)?.object(forKey: "message"),!(errorMessage is NSNull){
                                                        if (errorMessage is String){
                                                            errStr = errorMessage as! String
                                                            
                                                        }
                                                    }else{
                                                        errStr = AlertMessages.UnidetifiedErrorMessage
                                                    }
                                                    
                                                }
                                                weakSelf?.showAlert(title: APP_NAME, message:errStr, withTag: 101, button: ["Ok"], isActionRequired: false)
                                            }
                                            
                                        }
                                    }

                                }
                            }else{
                               
                                weakSelf?.showAlert(title: "sorry", message: AlertMessages.NOTADICTIONARYMESSAGE , withTag: 101, button:  ["Ok"], isActionRequired: false)
                            }

                        }catch{
                            
                            weakSelf?.showAlert(title: "sorry", message: AlertMessages.MODELERRORMESSAGE , withTag: 101, button:  ["Ok"], isActionRequired: false)
                            return
                            
                        }
                    } else{
                         weakSelf?.showAlert(title: APP_NAME, message: AlertMessages.UnidetifiedErrorMessage, withTag: 101, button: ["Ok"], isActionRequired: false)
                    }
                }
            }
        }
            
    }
}
