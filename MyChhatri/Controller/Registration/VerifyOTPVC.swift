//
//  VerifyOTPVC.swift
//  MyChhatri
//
//  Created by Anshul on 10/07/18.
//  Copyright © 2018 Anshul. All rights reserved.
//

import UIKit
import SRCountdownTimer

class VerifyOTPVC: UIViewController, SRCountdownTimerDelegate {

    @IBOutlet weak var txtFieldOne:UITextField!
    @IBOutlet weak var txtFieldTwo:UITextField!
    @IBOutlet weak var txtFieldThree:UITextField!
    @IBOutlet weak var txtFieldFour:UITextField!
    
    @IBOutlet weak var lblResendOTP: UILabel!
    @IBOutlet weak var lblResendTimer: UILabel!
    @IBOutlet weak var lblMobileNumber:UILabel!
    @IBOutlet weak var btnSubmit:UIButton!
    @IBOutlet weak var btnResendOTP:UIButton!
    
    @IBOutlet weak var adViewHightConstant: NSLayoutConstraint!
    @IBOutlet weak var adView : CustomAdvertiseView!
    
    @IBOutlet weak var otpTimer: SRCountdownTimer!
    var isCommingFromRegistration: Int = 0 //default= 0,regis = 1, forget password = 2 , while loginwithout Verified = 3
    var userIDReceived = ""
    var mobileNumber = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        startTimer()
        self.initialViewSetup()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if(adView != nil) {
            adView.isHidden = false
            adView.hideAdViewDel = self
            adViewHightConstant.constant = 60
            adView.categoryIDForAdvertise = "8"
            adView.typeFoAdvertiseData = "1"
        }
        adView.awakeFromNib()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        adView.hideAdViewDel = nil
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func startTimer(){
        otpTimer.labelTextColor = UIColor.init(red: 0.990, green: 0.610, blue: 0.220, alpha: 1)
        otpTimer.lineColor = UIColor.init(red: 0.990, green: 0.610, blue: 0.220, alpha: 1)
        otpTimer.lineWidth = 2.0
        otpTimer.start(beginingValue: 30, interval: 1)
        btnResendOTP.isUserInteractionEnabled = false
        lblResendTimer.isHidden = false
        lblResendOTP.isHidden = true
        otpTimer.isHidden = false
        otpTimer.delegate = self
    }

    func initialViewSetup() {
        btnSubmit.addGrediant()
        
        if isCommingFromRegistration == 1 { //register
            if registrationModel.mobileNumber != nil {
                lblMobileNumber.text = registrationModel.mobileNumber
            } else {
                //self.navigationController?.popViewController(animated: true)
            }
        }
        else  { //Forget Password or login
            lblMobileNumber.text = mobileNumber
            self.callResendOTP()
        }
    }
    
    //MARK: IBAction
    @IBAction func btnActionBack(sender:UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnActionResendOTP(sender:UIButton) {
        
        if sendOTPCount < 4 {
            startTimer()
            self.callResendOTP()
            if sendOTPCount == 3 {
                btnResendOTP.isHidden = true
            }
            sendOTPCount += 1
        }
    }
    
    @IBAction func btnActionSubmit(sender:UIButton) {
        if validateOTP() {
            self.verifyOTP()
        }
    }
    
    func validateOTP() -> Bool {
        if txtFieldOne.text?.count == 0 {
            txtFieldOne.becomeFirstResponder()
            self.showAlert(title: APP_NAME, message: "Please enter OTP", withTag: 0, button: ["Ok"], isActionRequired: false)
            return false
        } else if txtFieldTwo.text?.count == 0 {
            txtFieldTwo.becomeFirstResponder()
            self.showAlert(title: APP_NAME, message: "Please enter OTP", withTag: 0, button: ["Ok"], isActionRequired: false)
            return false
        } else if txtFieldThree.text?.count == 0 {
            txtFieldThree.becomeFirstResponder()
            self.showAlert(title: APP_NAME, message: "Please enter OTP", withTag: 0, button: ["Ok"], isActionRequired: false)
            return false
        } else if txtFieldFour.text?.count == 0 {
            txtFieldFour.becomeFirstResponder()
            self.showAlert(title: APP_NAME, message: "Please enter OTP", withTag: 0, button: ["Ok"], isActionRequired: false)
            return false
        } else {
            return true
        }
    }
    
    //MARK: API
    func callResendOTP() {
        
        var params : [String:Any] = [:]
        if isCommingFromRegistration == 1 {
            params["mobile"] = registrationModel.mobileNumber
        } else {
            params["mobile"] = mobileNumber

        }
        params["type"] = isCommingFromRegistration == 2 ? 2 : 1
        print("params : \(params)")
        
        ProgressHUD.showProgressHUDWithLabel(label: "Loading...")
        
        APIManager.callPost("\(URLS.BASE_URL)\(URLS.GET_OTP)", param: params, header: ["":""]) { (isSuccess, result, error) in
            
            DispatchQueue.main.async {
                
                ProgressHUD.hideProgressHUD()
                
                weak var weakSelf = self
                
                if (error != nil) {
                    weakSelf?.showAlert(title: "Error", message: (error!.localizedDescription), withTag: 101, button: ["Ok"], isActionRequired: false)
                    return
                }
                if isSuccess {
                    
                    do {
                        let jsonData = try JSONSerialization.jsonObject(with: result!, options: .mutableContainers)
                        
                        if (jsonData is NSDictionary){
                            
                            if let statusCode = (jsonData as! NSDictionary).object(forKey: "status"), (statusCode is Int) {
                                
                                if let success = (jsonData as! NSDictionary).object(forKey: "success") {
                                    
                                    if (success as! Bool) == true {
                                        
                                        if (statusCode as! Int) == 200 {
                                            if let data = (jsonData as! NSDictionary).object(forKey: "data"), (data is NSDictionary) {
                                                if (data as! NSDictionary).count > 0 {
                                                    
                                                    if let userId = (data as! NSDictionary).object(forKey: "user_id") as? String {
                                                        weakSelf?.userIDReceived = userId
                                                    }
                                                    print("OTP \(data)")
                                                 
                                                    if sendOTPCount == 0 {
                                                        sendOTPCount += 1
                                                    }
                                                    
                                                    weakSelf?.showAlert(title: APP_NAME, message: "OTP send successfully. Please check your registerd mobile number.", withTag: 101, button: ["Ok"], isActionRequired: false)
                                                }
                                            }
                                            
                                        } else {
                                            
                                            //some other status code
                                            //check error message in the response
                                            var errStr = ""
                                            if let errormessage = (jsonData as? NSDictionary)?.object(forKey:"error") {
                                                if (errormessage is NSDictionary){
                                                    if let errorDescription = (errormessage as? NSDictionary)?.object(forKey: "error_description"),!(errorDescription is NSNull){
                                                        if (errorDescription is String){
                                                            errStr = errorDescription as! String
                                                            
                                                        }
                                                    }else {
                                                        if let errorMessage = (errormessage as? NSDictionary)?.object(forKey: "message"),!(errorMessage is NSNull){
                                                            if (errorMessage is String){
                                                                errStr = errorMessage as! String
                                                                
                                                            }
                                                        }else{
                                                            errStr = AlertMessages.UnidetifiedErrorMessage
                                                        }
                                                        
                                                    }
                                                    weakSelf?.showAlert(title: APP_NAME, message:errStr, withTag: 101, button: ["Ok"], isActionRequired: false)
                                                }
                                            }
                                        }
                                    }else{
                                        
                                        //some other status code
                                        //check error message in the response
                                        var errStr = ""
                                        if let errormessage = (jsonData as? NSDictionary)?.object(forKey:"error") {
                                            if (errormessage is NSDictionary){
                                                if let errorDescription = (errormessage as? NSDictionary)?.object(forKey: "error_description"),!(errorDescription is NSNull){
                                                    if (errorDescription is String){
                                                        errStr = errorDescription as! String
                                                        
                                                    }
                                                }else {
                                                    if let errorMessage = (errormessage as? NSDictionary)?.object(forKey: "message"),!(errorMessage is NSNull){
                                                        if (errorMessage is String){
                                                            errStr = errorMessage as! String
                                                            
                                                        }
                                                    }else{
                                                        errStr = AlertMessages.UnidetifiedErrorMessage
                                                    }
                                                    
                                                }
                                                weakSelf?.showAlert(title: APP_NAME, message:errStr, withTag: 101, button: ["Ok"], isActionRequired: false)
                                            }
                                        }
                                    }
                                    
                                }else{
                                    
                                    weakSelf?.showAlert(title: "sorry", message: AlertMessages.NOTADICTIONARYMESSAGE , withTag: 101, button:  ["Ok"], isActionRequired: false)
                                }
                                
                            }
                        }else{
                            // not a dictionary
                            weakSelf?.showAlert(title: "sorry", message: AlertMessages.NOTADICTIONARYMESSAGE , withTag: 101, button:  ["Ok"], isActionRequired: false)
                        }
                    }catch{
                        weakSelf?.showAlert(title: "sorry", message: AlertMessages.MODELERRORMESSAGE , withTag: 101, button:  ["Ok"], isActionRequired: false)
                        return
                    }
                }
                
            }
        }
    }
    
    func verifyOTP() {

        var params : [String:Any] = [:]
        
        params["client_id"] = CLIENTID
        params["client_secret"] = CLIENTSECRET
        
        if isCommingFromRegistration == 1 {
            
            params["mobile"] = registrationModel.mobileNumber
        } else {
            params["mobile"] = mobileNumber
        }
        params["user_id"] = userIDReceived
        
        params["type"] = isCommingFromRegistration == 2 ? 2 : 1 //if 2 means forget password else while log and regis send 1
        params["otp"] = "\(String(describing: txtFieldOne.text!))\(String(describing: txtFieldTwo.text!))\(String(describing: txtFieldThree.text!))\(String(describing: txtFieldFour.text!))"
        
        print("params : \(params)")
        
        ProgressHUD.showProgressHUDWithLabel(label: "Loading...")
        
        APIManager.callPost("\(URLS.BASE_URL)\(URLS.VERIFY_OTP)", param: params, header: ["":""]) { (isSuccess, result, error) in
            
            DispatchQueue.main.async {
                
                ProgressHUD.hideProgressHUD()
                
                weak var weakSelf = self
                
                if (error != nil) {
                    
                    weakSelf?.showAlert(title: "Error", message: (error!.localizedDescription), withTag: 101, button: ["Ok"], isActionRequired: false)
                    return
                }
                    if isSuccess {
                        
                        do {
                            let jsonData = try JSONSerialization.jsonObject(with: result!, options: .mutableContainers)
                            
                            if (jsonData is NSDictionary){
                                
                                if let statusCode = (jsonData as! NSDictionary).object(forKey: "status"), (statusCode is Int) {
                                    
                                    if (statusCode as! Int) == 200 {
                                        
                                        if let data = (jsonData as! NSDictionary).object(forKey: "data"), (data is NSDictionary) {
                                            if (data as! NSDictionary).count > 0 {
                                                
                                                if weakSelf?.isCommingFromRegistration == 1  ||  weakSelf?.isCommingFromRegistration == 3 {
                                                   
                                                    print("\(data)")
                                                   
                                                    
                                                     //email
                                                    var uId , accessToke , refreshToke , mobile : String?
                                                    
                                                    if let userdetail = (data as? NSDictionary)?.object(forKey: "user"){
                                                        
//                                                        email = ((userdetail as? NSDictionary)?.object(forKey: "email") as? String)
                                                        
                                                        if let userIded = ((userdetail as? NSDictionary)?.object(forKey: "id") as? Int){
                                                            
                                                            uId = String(userIded)
                                                        }
                                                        mobile = (userdetail as? NSDictionary)?.object(forKey: "mobile") as? String
                                                        
                                                        UserModel.setUserType(uType: (userdetail as? NSDictionary)?.object(forKey: "institute_type") as? String ?? "")
                                                        
                                                    }
                                                    if let tokendetail = (data as? NSDictionary)?.object(forKey: "token"){
                                                        
                                                        refreshToke = (tokendetail as? NSDictionary)?.object(forKey: "refresh_token") as? String
                                                        accessToke  = (tokendetail as? NSDictionary)?.object(forKey: "access_token") as? String
                                                    }
                                                    
                                                    UserModel.setLoginInfo(userData:   NSDictionary(objects: [ uId ?? "" , mobile ?? "" , accessToke ?? "" , refreshToke ?? "" ],  forKeys: [ "ID" as NSCopying ,  "mobile" as NSCopying ,  "token" as NSCopying,"refreshToken" as NSCopying ]))
                                                    
                                                 
                                                    
                                                    UserModel.setAutoLogin(isSucessfull:true)
                                                     kAppDelegate.loadStudentHomeViewController()
                                                    
                                                    //NavigationManager.moveToSchoolCollegeViewControllerViewControllerIfNotExists(kAppDelegate.menuNavController!)
                                                    
                                                } else {
                                                    //from forget  password
                                                    userDefaults.set(((data as! NSDictionary).object(forKey: "token")as? NSDictionary)?.object(forKey:"access_token"), forKey: "access_token")
                                                    userDefaults.synchronize()
                                                    NavigationManager.moveToResetPasswordViewControllerViewControllerIfNotExists(kAppDelegate.menuNavController! , userID:(weakSelf?.userIDReceived)!)
                                                }
                                                
                                            }
                                        }
                                    } else {
                                        
                                        //some other status code
                                        //check error message in the response
                                        var errStr = ""
                                        if let errormessage = (jsonData as? NSDictionary)?.object(forKey:"error") {
                                            if (errormessage is NSDictionary){
                                                if let errorDescription = (errormessage as? NSDictionary)?.object(forKey: "error_description"),!(errorDescription is NSNull){
                                                    if (errorDescription is String){
                                                        errStr = errorDescription as! String
                                                        
                                                    }
                                                }else {
                                                    if let errorMessage = (errormessage as? NSDictionary)?.object(forKey: "message"),!(errorMessage is NSNull){
                                                        if (errorMessage is String){
                                                            errStr = errorMessage as! String
                                                            
                                                        }
                                                    }else{
                                                        errStr = AlertMessages.UnidetifiedErrorMessage
                                                    }
                                                    
                                                }
                                                weakSelf?.showAlert(title: APP_NAME, message:errStr, withTag: 101, button: ["Ok"], isActionRequired: false)
                                            }
                                            
                                        }
                                    }
                                }
                                
                            } else{
                                weakSelf?.showAlert(title: "sorry", message: AlertMessages.NOTADICTIONARYMESSAGE , withTag: 101, button:  ["Ok"], isActionRequired: false)
                            }
                            
                        }catch{
                            
                            weakSelf?.showAlert(title: "sorry", message: AlertMessages.MODELERRORMESSAGE , withTag: 101, button:  ["Ok"], isActionRequired: false)
                            return
                        }
                    } else{
                         weakSelf?.showAlert(title: APP_NAME, message: AlertMessages.UnidetifiedErrorMessage, withTag: 101, button: ["Ok"], isActionRequired: false)
                    }
            }
        }
    }
    
}

extension VerifyOTPVC : HideADUIDelegate {
    //update add UI if no data available hide the view
    func removeAdcollectionViewUI()  {
        
        adView.isHidden = true
        adViewHightConstant.constant = 0
    }
}
extension VerifyOTPVC : UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField.text?.count == 1 && string == "" {
            
            textField.text = string
            
            if textField == txtFieldTwo {
                txtFieldOne.becomeFirstResponder()
                
            }else  if  textField == txtFieldThree {
                txtFieldTwo.becomeFirstResponder()
                
            }else  if  textField == txtFieldFour {
                txtFieldThree.becomeFirstResponder()
            
        }
            
            return false
        }
        
//        if string == "" {
//
//            return true
//        }

        if textField == txtFieldOne && string.count > 0 {
            txtFieldOne.text = string
            txtFieldTwo.becomeFirstResponder()
        } else if textField == txtFieldTwo && string.count > 0 {
            txtFieldTwo.text = string
            txtFieldThree.becomeFirstResponder()
        } else if textField == txtFieldThree && string.count > 0 {
            txtFieldThree.text = string
            txtFieldFour.becomeFirstResponder()
        } else if textField == txtFieldFour && string.count > 0 {
            txtFieldFour.text = string
            txtFieldFour.resignFirstResponder()
        }
        if (textField.text?.count)! >= 1 && string != "" {

             return false
        }
        return true
    }
    
    //Timer Delegate
    func timerDidEnd() {
        btnResendOTP.isUserInteractionEnabled = true
        lblResendTimer.isHidden = true
        lblResendOTP.isHidden = false
        otpTimer.isHidden = true
    }

}
