//
//  RegistrationModel.swift
//  MyChhatri
//
//  Created by Anshul on 19/07/18.
//  Copyright © 2018 Anshul. All rights reserved.
//

import UIKit

struct RegistrationModel {
    var userType:Int?
    var guardianTitle:String?
    var guardianName:String?
    var guardianEmail:String?
    var studentType:String?
    var classOrYear:String?
    var schoolID:String?
    var yearID:String?
    var stream:String?
    var streamID:String?
    var specialization:String?
    var specializationID:String?
    var studentTitle:String?
    var studentName:String?
    var studentEmail:String?
    var country:String?
    var countryID:String?
    var mobileNumber:String?
    var password:String?
    var rePassword:String?
    var acceptTerms:String?
    var instituteType:Int?
    var stateID:String?
    var cityID:String?
    var collegeID:String?
}
var registrationModel = RegistrationModel()



struct RegisterStreamModel: Codable {
    let status: Int?
    let success: Bool?
    let data: [Datum]?
}

struct Datum: Codable {
    let id, title, instituteTypeID, captionText: String?
    
    enum CodingKeys: String, CodingKey {
        case id, title
        case instituteTypeID = "institute_type_id"
        case captionText = "caption_text"
    }
}

