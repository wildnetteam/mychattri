//
//  MyPageRegisterTableViewCell.swift
//  MyChhatri
//
//  Created by Arpana on 24/07/18.
//  Copyright © 2018 Anshul. All rights reserved.
//

import UIKit

@objc protocol ViewCoursesDelegate  {
    
    @objc optional func viewCourseList(_ i: Int)
}

class MyPageRegisterTableViewCell: UITableViewCell  {

    
    @IBOutlet weak var shadowView: UIView!
    
    @IBOutlet weak var backGroundWhiteView: UIView!
    
    @IBOutlet weak var lblCourse: UILabel!
    
    @IBOutlet weak var lblCourse1: UILabel!
    @IBOutlet weak var imgView: UIImageView!
    
    @IBOutlet weak var lblHeading: UILabel!
    
    @IBOutlet weak var lblDescription: UILabel!
    
    @IBOutlet weak var buttonViewAll: UIButton!
    
    weak var viewCoursesDelegate:ViewCoursesDelegate?

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    @IBAction func viewAllCoursesClicked(_ sender: Any) {
        
        weak var weakSelf = self

        if weakSelf?.viewCoursesDelegate != nil{
            weakSelf?.viewCoursesDelegate?.viewCourseList!((weakSelf?.buttonViewAll?.tag)!)
        }

    }
}
