//
//  AcceptTermsVC.swift
//  MyChhatri
//
//  Created by Anshul on 20/07/18.
//  Copyright © 2018 Anshul. All rights reserved.
//

import UIKit

class AcceptTermsVC: UIViewController {
    
    //@IBOutlet weak var webView:UIWebView!
//    @IBOutlet weak var activityIndicator:UIActivityIndicatorView!
    
    @IBOutlet weak var textView: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
         super.viewWillAppear(animated)
        
//        self.navigationController?.navigationBar.isHidden = false
//        kAppDelegate.menuNavController = self.navigationController
//
//        kAppDelegate.setNavigationAndStatusAppearance()
//        
//        let statusBar: UIView = UIApplication.shared.value(forKey: "statusBar") as! UIView
//        statusBar.backgroundColor = ColorConstants.appBackGroundCOLOR()
//        setNeedsStatusBarAppearanceUpdate()

        getprogramDetail()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
//
//    override func viewDidLayoutSubviews() {
//
//        self.webView.scrollView.contentSize = CGSize(width: self.webView.frame.size.width, height: webView.scrollView.contentSize.height)
//        self.webView.updateConstraintsIfNeeded()
//
//    }
    
    
    @IBAction func backButtonClicked(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    // MARK:- IBAction
    @IBAction func backBtnTapped(_ sender:UIButton){
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
        self.navigationController?.popViewController(animated: true)
    }
    
    
    func getprogramDetail()  {
        
        ProgressHUD.showProgressHUDWithLabel(label: "Loading...")
        
        APIManager.callGetWithoutAuthantication("\(URLS.BASE_URL)collection/cmspage?slug=terms", param: nil, header: nil) { (sucess, result, error) in
            
            DispatchQueue.main.async {
                
                weak var weakSelf = self
                
                ProgressHUD.hideProgressHUD()
                
                if (error != nil) {
                    weakSelf?.showAlert(title: "Error", message: (error!.localizedDescription), withTag: 101, button: ["Ok"], isActionRequired: false)
                    return
                }
                
                if sucess {
                    
                    do {
                        let jsonData = try JSONSerialization.jsonObject(with: result!, options: .mutableContainers)
                        
                        if let responseDict = (jsonData as? NSDictionary){
                            
                            if let statusCode = responseDict.object(forKey: "status"), (statusCode is Int) {
                                
                                if (statusCode as! Int) == 200 {
                                    
                                    //sucessfully retrieved data
                                    if let dataDict = responseDict.object(forKey: "data") as? NSDictionary{
                                        
                                        if let termsTxt =  dataDict.value(forKey: "description") as? String{
                                            
                                            let htmlString = "<html>\(termsTxt)</html>"
                                            // works even without <html><body> </body></html> tags, BTW
                                            let data = htmlString.data(using: String.Encoding.unicode)! // mind "!"
                                            let attrStr = try? NSAttributedString( // do catch
                                                data: data,
                                                options: [NSAttributedString.DocumentReadingOptionKey.documentType: NSAttributedString.DocumentType.html],
                                                documentAttributes: nil)
                                            // suppose we have an UILabel, but any element with NSAttributedString will do
                                            
                                            weakSelf?.textView.attributedText = attrStr
                                        }
                                        
                                    }else{
                                        //No data found error
                                        weakSelf?.showAlert(title: APP_NAME, message:  AlertMessages.NoDataKeyFoundInResponse, withTag: 101, button: ["Ok"], isActionRequired: false)
                                    }
                                    
                                } else {
                                    var errStr = ""
                                    if let errormessage = (jsonData as? NSDictionary)?.object(forKey:"error") {
                                        if (errormessage is NSDictionary){
                                            if let errorDescription = (errormessage as? NSDictionary)?.object(forKey: "error_description"),!(errorDescription is NSNull){
                                                if (errorDescription is String){
                                                    errStr = errorDescription as! String
                                                    
                                                }
                                            }else {
                                                if let errorMessage = (errormessage as? NSDictionary)?.object(forKey: "message"),!(errorMessage is NSNull){
                                                    if (errorMessage is String){
                                                        errStr = errorMessage as! String
                                                        
                                                    }
                                                }else{
                                                    errStr = AlertMessages.UnidetifiedErrorMessage
                                                }
                                                
                                            }
                                            weakSelf?.showAlert(title: APP_NAME, message:errStr, withTag: 101, button: ["Ok"], isActionRequired: false)
                                        }
                                        
                                    }else{
                                        weakSelf?.showAlert(title: APP_NAME, message:  AlertMessages.UnidetifiedErrorMessage, withTag: 101, button: ["Ok"], isActionRequired: false)
                                    }
                                    
                                }
                            }
                        }else{
                            
                            weakSelf?.showAlert(title: "sorry", message: AlertMessages.NOTADICTIONARYMESSAGE , withTag: 101, button:  ["Ok"], isActionRequired: false)
                            return
                        }
                        
                    }catch{
                        
                        weakSelf?.showAlert(title: "sorry", message: AlertMessages.MODELERRORMESSAGE , withTag: 101, button:  ["Ok"], isActionRequired: false)
                        return
                    }
                    
                } else{
                    
                    //Success false
                    weakSelf?.showAlert(title: APP_NAME, message: AlertMessages.UnidetifiedErrorMessage, withTag: 101, button: ["Ok"], isActionRequired: false)
                    
                }
      
            }
            
        }
    }
}
/*
extension AcceptTermsVC:UIWebViewDelegate {
    func webViewDidStartLoad(_ webView: UIWebView) {
        activityIndicator.isHidden = false
        activityIndicator.startAnimating()
        
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    func webViewDidFinishLoad(_ webView: UIWebView) {
        activityIndicator.isHidden = true
        activityIndicator.stopAnimating()
        
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
        //showAlert(with: "Error", message: error.localizedDescription)
        activityIndicator.isHidden = true
        activityIndicator.stopAnimating()
        
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
        
    }
    
}*/
