//
//  CourseDetailCollectionViewCell.swift
//  MyChhatri
//
//  Created by Arpana on 12/09/18.
//  Copyright © 2018 Arpana. All rights reserved.
//

import UIKit

@objc protocol actionCompletedDelegate  {
    
    @objc optional func updateUI(_ i: Int)
}


class CourseDetailCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var lblDuration: UILabel!
    
    @IBOutlet weak var lblBatch: UILabel!
    
    @IBOutlet weak var lblstatrtDate: UILabel!
    @IBOutlet weak var lblEndDate: UILabel!
    @IBOutlet weak var lblDays : UILabel!
    @IBOutlet weak var lblTiming: UILabel!
    @IBOutlet weak var lblDays1 : UILabel!
    @IBOutlet weak var lblTiming1: UILabel!
    @IBOutlet weak var btnTermCheck : UIButton!
    @IBOutlet weak var btnTermLink : UIButton!
    var acceptTerms:String = "0"

    weak var updateUIOnDoneDelegate:actionCompletedDelegate?

    
    @IBOutlet weak var enrollButton : UIButton!
    @IBOutlet weak var wishListButton : UIButton!

    var courseID: String?
    var batchID: String?

    
    func  getDaysArray(combinedDays:String) -> [String] {
        return combinedDays.components(separatedBy: ",")
        
    }
    
    @IBAction func enrollNow (_ sender : Any){
        
        
        /*  course_id:6
         batch_id:13 */
        
        if acceptTerms == "0" {
            self.showAlert(title:APP_NAME, message:"Please accept Terms and Condition", withTag:101, button:["Ok"], isActionRequired:false)
            return
        }
        
        var params : [String:Any] = [:]

        
        guard let courseIdToSave = courseID else{
            
            self.showAlert(title: "Error", message: "CourseId to SAVE is not found. Try again", withTag: 101, button: ["Ok"], isActionRequired: false)
            return
        }
        
        
        params["course_id"] = courseIdToSave
        
        guard let batchIdToSave = batchID else{
            
            self.showAlert(title: "Error", message: "Batch Id to SAVE is not found. Try again", withTag: 101, button: ["Ok"], isActionRequired: false)
            return
            
        }
        params["batch_id"] = batchIdToSave
        
        print("params : \(params)")
        
        ProgressHUD.showProgressHUDWithLabel(label: "Loading...")
        
        APICall.callEnrollPostAPI(params, header: [ : ]) { (sucess, result, error) in
            
            DispatchQueue.main.async {
                
                ProgressHUD.hideProgressHUD()
                
                weak var weakSelf = self
                
                if (error != nil) {
                    weakSelf?.showAlert(title: "Error", message: (error!.localizedDescription), withTag: 101, button: ["Ok"], isActionRequired: false)
                    return
                }
                if sucess {
                    
                    do {
                        let jsonData = try JSONSerialization.jsonObject(with: result!, options: .mutableContainers)
                        
                        if (jsonData is NSDictionary){
                            
                            if let statusCode = (jsonData as! NSDictionary).object(forKey: "status"), (statusCode is Int) {
                                
                                if let success = (jsonData as! NSDictionary).object(forKey: "success") {
                                    
                                    if (success as! Bool) == true {
                                        
                                        if (statusCode as! Int) == 200 {
                                            if let data = (jsonData as! NSDictionary).object(forKey: "data"), (data is NSDictionary) {
                                                if (data as! NSDictionary).count > 0 {
                                                    
                                                    if let message = (data as! NSDictionary).object(forKey: "message") as? String {
                                                        
                                                        weakSelf?.showAlert(title: APP_NAME, message: message, withTag: 400, button: ["Ok"], isActionRequired: false)
                                                       
                                                        //  update the collectionview
                                                        if weakSelf?.updateUIOnDoneDelegate != nil{
                                                            weakSelf?.updateUIOnDoneDelegate?.updateUI!((weakSelf?.enrollButton?.tag)!)
                                                        }
                                                    }
                                                    
                                                }
                                            }
//                                            weakSelf?.enrollButton.isHidden = true
                                             weakSelf?.enrollButton.setTitle("Enrolled", for: .normal)
                                            weakSelf?.btnTermLink.isHidden = true
                                            weakSelf?.btnTermCheck.isHidden = true
                                            weakSelf?.enrollButton.isEnabled = false
                                            weakSelf?.wishListButton.isHidden = true
                                            
                                        } else {

                                            var errStr = ""
                                            if let errormessage = (jsonData as? NSDictionary)?.object(forKey:"error") {
                                                if (errormessage is NSDictionary){
                                                    if let errorDescription = (errormessage as? NSDictionary)?.object(forKey: "error_description"),!(errorDescription is NSNull){
                                                        if (errorDescription is String){
                                                            errStr = errorDescription as! String
                                                            
                                                        }
                                                    }else {
                                                        if let errorMessage = (errormessage as? NSDictionary)?.object(forKey: "message"),!(errorMessage is NSNull){
                                                            if (errorMessage is String){
                                                                errStr = errorMessage as! String
                                                                
                                                            }
                                                        }else{
                                                            errStr = AlertMessages.UnidetifiedErrorMessage
                                                        }
                                                        
                                                    }
                                                    weakSelf?.showAlert(title: APP_NAME, message:errStr, withTag: 101, button: ["Ok"], isActionRequired: false)
                                                }
                                            }
                                        }
                                    }else{
                                        
                                        //some other status code
                                        //check error message in the response
                                        var errStr = ""
                                        if let errormessage = (jsonData as? NSDictionary)?.object(forKey:"error") {
                                            if (errormessage is NSDictionary){
                                                if let errorDescription = (errormessage as? NSDictionary)?.object(forKey: "error_description"),!(errorDescription is NSNull){
                                                    if (errorDescription is String){
                                                        errStr = errorDescription as! String
                                                        
                                                    }
                                                }else {
                                                    if let errorMessage = (errormessage as? NSDictionary)?.object(forKey: "message"),!(errorMessage is NSNull){
                                                        if (errorMessage is String){
                                                            errStr = errorMessage as! String
                                                            
                                                        }
                                                    }else{
                                                        errStr = AlertMessages.UnidetifiedErrorMessage
                                                    }
                                                    
                                                }
                                                weakSelf?.showAlert(title: APP_NAME, message:errStr, withTag: 101, button: ["Ok"], isActionRequired: false)
                                            }
                                        }
                                    }
                                    
                                }else{
                                    
                                    weakSelf?.showAlert(title: "sorry", message: AlertMessages.NOTADICTIONARYMESSAGE , withTag: 101, button:  ["Ok"], isActionRequired: false)
                                }
                                
                            }
                        }else{
                            // not a dictionary
                            weakSelf?.showAlert(title: "sorry", message: AlertMessages.NOTADICTIONARYMESSAGE , withTag: 101, button:  ["Ok"], isActionRequired: false)
                        }
                    }catch{
                        weakSelf?.showAlert(title: "sorry", message: AlertMessages.MODELERRORMESSAGE , withTag: 101, button:  ["Ok"], isActionRequired: false)
                        return
                    }
                }
                
            }
        }
    }
        
    
    @IBAction func btnCheckAcceptTerms(sender:UIButton) {
        
        //sender.isSelected = !sender.isSelected
        
        if sender.isSelected {
            acceptTerms = "0"
        } else {
            acceptTerms = "1"
        }
        sender.isSelected = !sender.isSelected
    }
    
    @IBAction func btnAcceptTerms(sender:UIButton) {
        let viewController = UIStoryboard.instantiateAcceptTermsViewController()
        appDelegate.menuNavController?.pushViewController(viewController, animated: true)
    }
    
    
    @IBAction func addToWishList ( _ sender : Any){
        
        
        /*  course_id:6
         batch_id:13 */
        
        var params : [String:Any] = [:]
        
        
        guard let courseIdToSave = courseID else{
            
            self.showAlert(title: "Error", message: "CourseId to SAVE is not found. Try again", withTag: 101, button: ["Ok"], isActionRequired: false)
            return
        }
        
        
        params["course_id"] = courseIdToSave
        
        guard let batchIdToSave = batchID else{
            
            self.showAlert(title: "Error", message: "Batch Id to SAVE is not found. Try again", withTag: 101, button: ["Ok"], isActionRequired: false)
            return
            
        }
        params["batch_id"] = batchIdToSave
        
        print("params : \(params)")
        
        ProgressHUD.showProgressHUDWithLabel(label: "Loading...")
        
        APICall.callAddBatchToWishListPostAPI(params, header: [ : ]) { (sucess, result, error) in
            
            DispatchQueue.main.async {
                
                ProgressHUD.hideProgressHUD()
                
                weak var weakSelf = self
                
                if (error != nil) {
                    weakSelf?.showAlert(title: "Error", message: (error!.localizedDescription), withTag: 101, button: ["Ok"], isActionRequired: false)
                    return
                }
                if sucess {
                    
                    do {
                        let jsonData = try JSONSerialization.jsonObject(with: result!, options: .mutableContainers)
                        
                        if (jsonData is NSDictionary){
                            
                            if let statusCode = (jsonData as! NSDictionary).object(forKey: "status"), (statusCode is Int) {
                                
                                if let success = (jsonData as! NSDictionary).object(forKey: "success") {
                                    
                                    if (success as! Bool) == true {
                                        
                                        if (statusCode as! Int) == 200 {
                                            if let data = (jsonData as! NSDictionary).object(forKey: "data"), (data is NSDictionary) {
                                                if (data as! NSDictionary).count > 0 {
                                                    
                                                    if let message = (data as! NSDictionary).object(forKey: "message") as? String {
                                                        
                                                        weakSelf?.showAlert(title: APP_NAME, message: message, withTag: 400, button: ["Ok"], isActionRequired: false)
                                                      
                                                        //  update the collectionview
                                                        if weakSelf?.updateUIOnDoneDelegate != nil{
                                                            weakSelf?.updateUIOnDoneDelegate?.updateUI!((weakSelf?.wishListButton?.tag)!)
                                                        }
                                                        
                                                    }
                                                    
                                                }
                                            }
                                            weakSelf?.wishListButton.isHidden = true

                                        } else {
                                            
                                            var errStr = ""
                                            if let errormessage = (jsonData as? NSDictionary)?.object(forKey:"error") {
                                                if (errormessage is NSDictionary){
                                                    if let errorDescription = (errormessage as? NSDictionary)?.object(forKey: "error_description"),!(errorDescription is NSNull){
                                                        if (errorDescription is String){
                                                            errStr = errorDescription as! String
                                                            
                                                        }
                                                    }else {
                                                        if let errorMessage = (errormessage as? NSDictionary)?.object(forKey: "message"),!(errorMessage is NSNull){
                                                            if (errorMessage is String){
                                                                errStr = errorMessage as! String
                                                                
                                                            }
                                                        }else{
                                                            errStr = AlertMessages.UnidetifiedErrorMessage
                                                        }
                                                        
                                                    }
                                                    weakSelf?.showAlert(title: APP_NAME, message:errStr, withTag: 101, button: ["Ok"], isActionRequired: false)
                                                }
                                            }
                                        }
                                    }else{
                                        
                                        //some other status code
                                        //check error message in the response
                                        var errStr = ""
                                        if let errormessage = (jsonData as? NSDictionary)?.object(forKey:"error") {
                                            if (errormessage is NSDictionary){
                                                if let errorDescription = (errormessage as? NSDictionary)?.object(forKey: "error_description"),!(errorDescription is NSNull){
                                                    if (errorDescription is String){
                                                        errStr = errorDescription as! String
                                                        
                                                    }
                                                }else {
                                                    if let errorMessage = (errormessage as? NSDictionary)?.object(forKey: "message"),!(errorMessage is NSNull){
                                                        if (errorMessage is String){
                                                            errStr = errorMessage as! String
                                                            
                                                        }
                                                    }else{
                                                        errStr = AlertMessages.UnidetifiedErrorMessage
                                                    }
                                                    
                                                }
                                                weakSelf?.showAlert(title: APP_NAME, message:errStr, withTag: 101, button: ["Ok"], isActionRequired: false)
                                            }
                                        }
                                    }
                                    
                                }else{
                                    
                                    weakSelf?.showAlert(title: "sorry", message: AlertMessages.NOTADICTIONARYMESSAGE , withTag: 101, button:  ["Ok"], isActionRequired: false)
                                }
                                
                            }
                        }else{
                            // not a dictionary
                            weakSelf?.showAlert(title: "sorry", message: AlertMessages.NOTADICTIONARYMESSAGE , withTag: 101, button:  ["Ok"], isActionRequired: false)
                        }
                    }catch{
                        weakSelf?.showAlert(title: "sorry", message: AlertMessages.MODELERRORMESSAGE , withTag: 101, button:  ["Ok"], isActionRequired: false)
                        return
                    }
                }
                
            }
        }
    }
    
    func  setCellData(dict : NSDictionary)  {
        
     //   wishlist_count == 1 already added to you wishlist
        if let wishCount = dict.value(forKey: "wishlist_count") as? String {
            if let  isDispayButton = Int(wishCount) {
                if isDispayButton != 0 {
                    wishListButton.isHidden = true
                }else{
                    wishListButton.isHidden = false
                }
            }
        } else if let wishCount = dict.value(forKey: "wishlist_count") as? Int {
            if let  isDispayButton = wishCount as? Int {
                if isDispayButton != 0 {
                    wishListButton.isHidden = true
                }else{
                    wishListButton.isHidden = false
                }
            }
        }
        
      //  bookingCount == 1 you have been enrolled for it
        if let bookingCount = dict.value(forKey: "booking_count") as? String {
            if let  isDispayButton = Int(bookingCount) {
                if isDispayButton != 0 {
                   // enrollButton.isHidden = true
                    enrollButton.setTitle("Enrolled", for: .normal)
                    enrollButton.isEnabled = false
                    wishListButton.isHidden = true
                     btnTermLink.isHidden = true
                    btnTermCheck.isHidden = true

                }else{
                    // enrollButton.isHidden = false
                    enrollButton.setTitle("Enroll", for: .normal)
                    enrollButton.isEnabled = true
                    btnTermLink.isHidden = false
                    btnTermCheck.isHidden = false
                }
            }
        }else   if let bookingCount = dict.value(forKey: "booking_count") as? Int {
            if let  isDispayButton = bookingCount as? Int {
                if isDispayButton != 0 {
                   // enrollButton.isHidden = true
                    enrollButton.setTitle("Enrolled", for: .normal)
                    enrollButton.isEnabled = false
                    btnTermLink.isHidden = true
                    btnTermCheck.isHidden = true
                    wishListButton.isHidden = true
                 
                }else{
                    //enrollButton.isHidden = false
                    enrollButton.setTitle("Enroll", for: .normal)
                    enrollButton.isEnabled = true
                    btnTermLink.isHidden = false
                    btnTermCheck.isHidden = false
                }
            }
        }
 
        
        
        if let courseId = dict.value(forKey: "course_id") as? String {
            courseID = courseId
        }else   if let courseId = dict.value(forKey: "course_id") as? Int {
            courseID = String(courseId)
        }
        
        if let batchId = dict.value(forKey: "id") as? Int {
            batchID = String(batchId)
        }else if let batchId = dict.value(forKey: "id") as? String {
            batchID = batchId
        }
        
        if let startDate = dict.value(forKey: "start_date") as? String {
            
            
            let headingString = NSMutableAttributedString(
                string: "Start Date :",
                attributes: [NSAttributedStringKey(rawValue: NSAttributedStringKey.foregroundColor.rawValue):  UIColor(red:0.34, green:0.36, blue:0.4, alpha:1) , NSAttributedStringKey(rawValue: NSAttributedStringKey.font.rawValue): UIFont(name: "Amble-Regular", size: isIphone5 ? 10.0: 12.0)!])
            
           let startDateString = CalenderStruct.getFormatedDateStringWithouOrdinal(fromDateUTC:  CalenderStruct.getDateFromDateString(forUTC: startDate, byFormat: "yyyy-MM-dd"), inFormat: "dd MMM yyyy")
            var subHeadingString = NSMutableAttributedString(string: "")
            if let formatedDate = startDateString {
                
                subHeadingString = NSMutableAttributedString(
                    string: "\(formatedDate)",
                    attributes: [NSAttributedStringKey(rawValue: NSAttributedStringKey.foregroundColor.rawValue): UIColor(red:0.34, green:0.36, blue:0.4, alpha:1) , NSAttributedStringKey(rawValue: NSAttributedStringKey.font.rawValue): UIFont(name: "Amble-Bold", size: isIphone5 ? 10.0 :12.0)!])
            }else {
                subHeadingString = NSMutableAttributedString(
                    string: "\(startDate)",
                    attributes: [NSAttributedStringKey(rawValue: NSAttributedStringKey.foregroundColor.rawValue): UIColor(red:0.34, green:0.36, blue:0.4, alpha:1) , NSAttributedStringKey(rawValue: NSAttributedStringKey.font.rawValue): UIFont(name: "Amble-Bold", size: isIphone5 ? 10.0 :12.0)!])
            }
            let attrString1 = NSMutableAttributedString(string: "")
            attrString1.append(headingString)
            attrString1.append( NSMutableAttributedString(string: "  "))
            attrString1.append(subHeadingString)
            
            lblstatrtDate.attributedText = attrString1
        }
        if let endDate = dict.value(forKey: "end_date") as? String {
            
            
            let headingString = NSMutableAttributedString(
                string: "End Date :",
                attributes: [NSAttributedStringKey(rawValue: NSAttributedStringKey.foregroundColor.rawValue):  UIColor(red:0.34, green:0.36, blue:0.4, alpha:1) , NSAttributedStringKey(rawValue: NSAttributedStringKey.font.rawValue): UIFont(name: "Amble-Regular", size: isIphone5 ? 10.0: 12.0)!])
            
            let startDateString = CalenderStruct.getFormatedDateStringWithouOrdinal(fromDateUTC:  CalenderStruct.getDateFromDateString(forUTC: endDate, byFormat: "yyyy-MM-dd"), inFormat: "dd MMM yyyy")
            var subHeadingString = NSMutableAttributedString(string: "")
            if let formatedDate = startDateString {
                
                subHeadingString = NSMutableAttributedString(
                    string: "\(formatedDate)",
                    attributes: [NSAttributedStringKey(rawValue: NSAttributedStringKey.foregroundColor.rawValue): UIColor(red:0.34, green:0.36, blue:0.4, alpha:1) , NSAttributedStringKey(rawValue: NSAttributedStringKey.font.rawValue): UIFont(name: "Amble-Bold", size: isIphone5 ? 10.0 :12.0)!])
            }else {
                subHeadingString = NSMutableAttributedString(
                    string: "\(endDate)",
                    attributes: [NSAttributedStringKey(rawValue: NSAttributedStringKey.foregroundColor.rawValue): UIColor(red:0.34, green:0.36, blue:0.4, alpha:1) , NSAttributedStringKey(rawValue: NSAttributedStringKey.font.rawValue): UIFont(name: "Amble-Bold", size: isIphone5 ? 10.0 :12.0)!])
            }
            
            let attrString1 = NSMutableAttributedString(string: "")
            attrString1.append(headingString)
            attrString1.append( NSMutableAttributedString(string: "  "))
            attrString1.append(subHeadingString)
            
            lblEndDate.attributedText = attrString1
        }
        if let batchNo = dict.value(forKey: "batch_no") as? String {
            
            
            let headingString = NSMutableAttributedString(
                string: "Batch Number :",
                attributes: [NSAttributedStringKey(rawValue: NSAttributedStringKey.foregroundColor.rawValue):  UIColor(red:0.34, green:0.36, blue:0.4, alpha:1) , NSAttributedStringKey(rawValue: NSAttributedStringKey.font.rawValue): UIFont(name: "Amble-Regular", size: isIphone5 ? 10.0: 12.0)!])
            
            let subHeadingString = NSMutableAttributedString(
                string: "\(batchNo)",
                attributes: [NSAttributedStringKey(rawValue: NSAttributedStringKey.foregroundColor.rawValue): UIColor(red:0.34, green:0.36, blue:0.4, alpha:1) , NSAttributedStringKey(rawValue: NSAttributedStringKey.font.rawValue): UIFont(name: "Amble-Bold", size: isIphone5 ? 10.0 :12.0)!])
            
            let attrString1 = NSMutableAttributedString(string: "")
            attrString1.append(headingString)
            attrString1.append( NSMutableAttributedString(string: "  "))
            attrString1.append(subHeadingString)
            
            lblBatch.attributedText = attrString1
        }
        if let duration = dict.value(forKey: "duration") as? String {
            
            
            let headingString = NSMutableAttributedString(
                string: "Duration :",
                attributes: [NSAttributedStringKey(rawValue: NSAttributedStringKey.foregroundColor.rawValue):  UIColor(red:0.34, green:0.36, blue:0.4, alpha:1) , NSAttributedStringKey(rawValue: NSAttributedStringKey.font.rawValue): UIFont(name: "Amble-Regular", size: isIphone5 ? 10.0: 12.0)!])
            
            let subHeadingString = NSMutableAttributedString(
                string: "\(duration)",
                attributes: [NSAttributedStringKey(rawValue: NSAttributedStringKey.foregroundColor.rawValue): UIColor(red:0.34, green:0.36, blue:0.4, alpha:1) , NSAttributedStringKey(rawValue: NSAttributedStringKey.font.rawValue): UIFont(name: "Amble-Bold", size: isIphone5 ? 10.0 :12.0)!])
            
            let attrString1 = NSMutableAttributedString(string: "")
            attrString1.append(headingString)
            attrString1.append( NSMutableAttributedString(string: "  "))
            attrString1.append(subHeadingString)
            
            lblDuration.attributedText = attrString1
        }
        
        var time1Str = ""
        var time2Str = ""
        if let startTime = dict.value(forKey: "start_time") as? String {
            
              time1Str = startTime
        }
        if let endTime = dict.value(forKey: "end_time") as? String {
             time2Str = endTime

        }
        
        let formatter = DateFormatter()
        formatter.dateFormat = "HH:mm:ss"

        let startTime = formatter.date(from: time1Str)
        
        let endTime = formatter.date(from: time2Str)

        
       // let formatter = DateFormatter()
        formatter.dateStyle = DateFormatter.Style.long
        formatter.timeStyle = DateFormatter.Style.medium
        formatter.dateFormat = "hh:mm a"
        formatter.amSymbol = "am"
        formatter.pmSymbol = "pm"
        
       var  fromTime = ""
        var  toTime = ""
        if startTime != nil {
            
            fromTime = formatter.string(from: startTime!)

        }
        if endTime != nil {
            toTime = formatter.string(from: endTime! )
        }
        //Now format will be ..To..
       
        
            let headingString = NSMutableAttributedString(
                string: "Timing :",
                attributes: [NSAttributedStringKey(rawValue: NSAttributedStringKey.foregroundColor.rawValue):  UIColor(red:0.34, green:0.36, blue:0.4, alpha:1) , NSAttributedStringKey(rawValue: NSAttributedStringKey.font.rawValue): UIFont(name: "Amble-Regular", size: isIphone5 ? 10.0: 12.0)!])

            let subHeadingString = NSMutableAttributedString(
                string: "\(fromTime) To \(toTime)",
                attributes: [NSAttributedStringKey(rawValue: NSAttributedStringKey.foregroundColor.rawValue): UIColor(red:0.34, green:0.36, blue:0.4, alpha:1) , NSAttributedStringKey(rawValue: NSAttributedStringKey.font.rawValue): UIFont(name: "Amble-Bold", size: isIphone5 ? 10.0 :12.0)!])

            let attrString1 = NSMutableAttributedString(string: "")
            attrString1.append(headingString)
            attrString1.append( NSMutableAttributedString(string: "  "))
            attrString1.append(subHeadingString)
        lblTiming1.text = String("\(fromTime) To \(toTime)")
        if let weekdays = dict.value(forKey: "weekdays") as? String {
            
            
            let headingString = NSMutableAttributedString(
                string: "Days :",
                attributes: [NSAttributedStringKey(rawValue: NSAttributedStringKey.foregroundColor.rawValue):  UIColor(red:0.34, green:0.36, blue:0.4, alpha:1) , NSAttributedStringKey(rawValue: NSAttributedStringKey.font.rawValue): UIFont(name: "Amble-Regular", size: isIphone5 ? 10.0: 12.0)!])

     
            let dayStri: NSMutableString = ""
            for d in (getDaysArray(combinedDays: weekdays) as? [String])!{
                
                guard let dayIntVal = Int(d) else{
                    return
                }
                let val = loadDayName(forDay: dayIntVal)
                dayStri.append(val)
                dayStri.append(", ")
            }
//delete last ", " so index 2
            dayStri.deleteCharacters(in:NSRange(location: dayStri.length - 2, length: 1))
           
            let subHeadingString = NSMutableAttributedString(
                string: "\(dayStri)",
                attributes: [NSAttributedStringKey(rawValue: NSAttributedStringKey.foregroundColor.rawValue): UIColor(red:0.34, green:0.36, blue:0.4, alpha:1) , NSAttributedStringKey(rawValue: NSAttributedStringKey.font.rawValue): UIFont(name: "Amble-Bold", size: isIphone5 ? 10.0 :12.0)!])
            let attrString1 = NSMutableAttributedString(string: "")
            attrString1.append(headingString)
            attrString1.append( NSMutableAttributedString(string: "  "))
            attrString1.append(subHeadingString)
          lblDays1.text = String(dayStri)
        }
        //start_time
    }
}
