//
//  CourseDetailViewController.swift
//  MyChhatri
//
//  Created by Arpana on 11/09/18.
//  Copyright © 2018 Arpana. All rights reserved.
//

import UIKit

class CourseDetailViewController: UIViewController {
    
    var courseID :String?
    
    var dataDictionary : NSDictionary?
    
    var batchArray : Array<NSDictionary>?
    
    @IBOutlet weak var viewFee: UIView!
    
    @IBOutlet weak var viewHeightConstant: NSLayoutConstraint!
    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBOutlet weak var lblProgramName: UILabel!
     @IBOutlet weak var lblprerequsite: UILabel!
    @IBOutlet weak var lblCourseFee: UILabel!
    @IBOutlet weak var lblRegistrationFee: UILabel!
    @IBOutlet weak var lblStiphentFee: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    
    @IBOutlet weak var lblTotalPrice: UILabel!
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    @IBOutlet weak var btnterms: UIButton!
    @IBOutlet weak var btnTermLink : UIButton!
    
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action:
            #selector(CourseDetailViewController.getprogramDetail),
                                 for: UIControlEvents.valueChanged)
        refreshControl.tintColor = .black
        
        return refreshControl
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUI()
        getprogramDetail()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        self.navigationController?.navigationBar.isHidden = false
        self.navigationItem.hidesBackButton = true
        
        self.navigationController?.navigationBar.topItem?.title =  "My Registration Details"
        kAppDelegate.menuNavController = self.navigationController

        kAppDelegate.setNavigationAndStatusAppearance()
        let statusBar: UIView = UIApplication.shared.value(forKey: "statusBar") as! UIView
        statusBar.backgroundColor = ColorConstants.appBackGroundCOLOR()
        setNeedsStatusBarAppearanceUpdate()
        
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    func setUI() {
        
        viewFee.layer.shadowOffset = CGSize(width: 0, height: 8)
        viewFee.layer.shadowColor = UIColor(red:0.8, green:0.8, blue:0.8, alpha:0.5).cgColor
        viewFee.layer.shadowOpacity = 1
        viewFee.layer.shadowRadius = 20
        
        collectionView.layer.shadowOffset = CGSize(width:  0, height: collectionView.frame.size.height)
        collectionView.layer.shadowColor = UIColor(red:0.8, green:0.8, blue:0.8, alpha:0.5).cgColor
        collectionView.layer.shadowOpacity = 1
        collectionView.layer.shadowRadius = 20
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setData() {
    
        guard let dictionary = dataDictionary else{
            
            self.showAlert(title: "", message: "No data found", withTag: 101, button: ["OK"], isActionRequired: false)
            return
        }
     
        if let description = dictionary.value(forKey: "description") as? String {
            
            lblDescription.text = description.withoutHtml
        }
        
        if let title = dictionary.value(forKey: "title") as? String {
            
            lblProgramName.text = title
        }
        if let title = dictionary.value(forKey: "prerequisite") as? String {
            
            lblprerequsite.text = title
        }
        
        
        var intershipType : Int = 1
        
        var internOrTrainingType: Int = 1
        
        if let typ =  dictionary.value(forKey: "type") as? String {
            
            internOrTrainingType = Int(typ)!
        }else if let  typ =  dictionary.value(forKey: "type") as? Int {
            
            internOrTrainingType = typ
        }
        
        if internOrTrainingType == 1 {
          //work as its is
            
        }else {
            //manage intern type
          
            //2 -> Stiphend , show only stiphend
            //3 -> free , just show free, no fees at all
            
            if let type = dictionary.value(forKey: "internship_type") as? String {
                intershipType = Int(type)!
            } else if let type = dictionary.value(forKey: "internship_type") as? Int {
                intershipType = type
            }
            
            if intershipType  == 1 {
                
                  //1 -> Paid , (course fee changed to  internship fee , reg fee removed
                
               // lblCourseFee.isHidden = true
                lblStiphentFee.isHidden = true

                
            }else  if intershipType  == 2 {
                
                //stiphend
                lblStiphentFee.isHidden = false
                lblRegistrationFee.isHidden = true
                lblCourseFee.isHidden = true

                
            }else  if intershipType  == 3 {
                
                //free
                lblStiphentFee.isHidden = false
                lblRegistrationFee.isHidden = true
                lblCourseFee.isHidden = true

            }
            
        }
        var regisFee = ""
        if let registrationFees = dictionary.value(forKey: "registration_fees") as? String {
            regisFee = registrationFees
        }else  if let registrationFees = dictionary.value(forKey: "registration_fees") as? Int {
            regisFee = String(registrationFees)
        }
        
        if  regisFee != nil {
            
            let headingString = NSMutableAttributedString(
                string: "Registration Fees :",
                attributes: [NSAttributedStringKey(rawValue: NSAttributedStringKey.foregroundColor.rawValue):  UIColor(red:0.34, green:0.36, blue:0.4, alpha:1) , NSAttributedStringKey(rawValue: NSAttributedStringKey.font.rawValue): UIFont(name: "Amble-Regular", size: isIphone5 ? 10.0: 12.0)!])
            
            let subHeadingString = NSMutableAttributedString(
                string: "INR \(regisFee).",
                attributes: [NSAttributedStringKey(rawValue: NSAttributedStringKey.foregroundColor.rawValue): UIColor(red:0.34, green:0.36, blue:0.4, alpha:1) , NSAttributedStringKey(rawValue: NSAttributedStringKey.font.rawValue): UIFont(name: "Amble-Bold", size: isIphone5 ? 10.0 :12.0)!])
            
            let attrString1 = NSMutableAttributedString(string: "")
            attrString1.append(headingString)
            attrString1.append( NSMutableAttributedString(string: "  "))
            attrString1.append(subHeadingString)
            
            
            lblRegistrationFee.attributedText = attrString1
            
        }
        
        
        var courseFees = ""
        if let courseFee = dictionary.value(forKey: "course_fees") as? String {
            courseFees = courseFee
        }else  if let courseFee = dictionary.value(forKey: "course_fees")  as? Int {
            courseFees = String(courseFee)
        }
        
        if  courseFees != nil {
            
            
            let headingString = NSMutableAttributedString(
                string: internOrTrainingType == 1 ?"Course fee : " :"Fee to pay : ",
                attributes: [NSAttributedStringKey(rawValue: NSAttributedStringKey.foregroundColor.rawValue):  UIColor(red:0.34, green:0.36, blue:0.4, alpha:1) , NSAttributedStringKey(rawValue: NSAttributedStringKey.font.rawValue): UIFont(name: "Amble-Regular", size: isIphone5 ? 10.0: 12.0)!])
            
            let stiphendeString = NSMutableAttributedString(
                string: "Intern Fee : ",
                attributes: [NSAttributedStringKey(rawValue: NSAttributedStringKey.foregroundColor.rawValue):  UIColor(red:0.34, green:0.36, blue:0.4, alpha:1) , NSAttributedStringKey(rawValue: NSAttributedStringKey.font.rawValue): UIFont(name: "Amble-Regular", size: isIphone5 ? 10.0: 12.0)!])
            
            let subHeadingString = NSMutableAttributedString(
                string: "INR \(courseFees).",
                attributes: [NSAttributedStringKey(rawValue: NSAttributedStringKey.foregroundColor.rawValue): UIColor(red:0.34, green:0.36, blue:0.4, alpha:1) , NSAttributedStringKey(rawValue: NSAttributedStringKey.font.rawValue): UIFont(name: "Amble-Bold", size: isIphone5 ? 10.0 :12.0)!])
            
            let attrString1 = NSMutableAttributedString(string: "")
            attrString1.append(headingString)
            attrString1.append( NSMutableAttributedString(string: "  "))
            attrString1.append(subHeadingString)
            
            lblCourseFee.attributedText = attrString1
            

            if intershipType == 3 {
                let headingString = NSMutableAttributedString(
                    string: "  Free",
                    attributes: [NSAttributedStringKey(rawValue: NSAttributedStringKey.foregroundColor.rawValue):  UIColor(red:0.34, green:0.36, blue:0.4, alpha:1) , NSAttributedStringKey(rawValue: NSAttributedStringKey.font.rawValue): UIFont(name: "Amble-Bold", size: isIphone5 ? 14.0: 16.0)!])
                
                lblStiphentFee.attributedText = headingString
                viewHeightConstant.constant = 40
                lblTotalPrice.isHidden = true
            }else if intershipType == 1 {
                
                let attrString12 = NSMutableAttributedString(string: "")
                attrString12.append(stiphendeString)
                attrString12.append( NSMutableAttributedString(string: "  "))
                attrString12.append(subHeadingString)
                lblStiphentFee.attributedText = attrString12
            }
            

        }
        
            var stiphendFees = ""
        if intershipType == 2 {
            
        
            if let stiphiendFee = dictionary.value(forKey: "stipend_price") as? String {
                stiphendFees = stiphiendFee
            }else  if let stiphiendFee = dictionary.value(forKey: "stipend_price")  as? Int {
                stiphendFees = String(stiphiendFee)
            }
            
            if  stiphendFees != nil {
                
                
                let headingString = NSMutableAttributedString(
                    string: "Stiphend : ",
                    attributes: [NSAttributedStringKey(rawValue: NSAttributedStringKey.foregroundColor.rawValue):  UIColor(red:0.34, green:0.36, blue:0.4, alpha:1) , NSAttributedStringKey(rawValue: NSAttributedStringKey.font.rawValue): UIFont(name: "Amble-Regular", size: isIphone5 ? 10.0: 12.0)!])
                
                
                
                let subHeadingString = NSMutableAttributedString(
                    string: "INR \(stiphendFees).",
                    attributes: [NSAttributedStringKey(rawValue: NSAttributedStringKey.foregroundColor.rawValue): UIColor(red:0.34, green:0.36, blue:0.4, alpha:1) , NSAttributedStringKey(rawValue: NSAttributedStringKey.font.rawValue): UIFont(name: "Amble-Bold", size: isIphone5 ? 10.0 :12.0)!])
                
                let attrString1 = NSMutableAttributedString(string: "")
                attrString1.append(headingString)
                attrString1.append( NSMutableAttributedString(string: "  "))
                attrString1.append(subHeadingString)
                
                lblStiphentFee.attributedText = attrString1
            }
        }
        var totalFees : Int = 0
        //calculate total price
          if let registrationFees = regisFee as? String {
            
            totalFees = Int(registrationFees)!
        }
        
        if intershipType == 2
        {
            if let stiphendFeeCalulated = stiphendFees as? String {
                totalFees = totalFees + Int(stiphendFeeCalulated)!
                
            }
        }else {
            if let courseFees = courseFees as? String {
                totalFees = totalFees + Int(courseFees)!
                
            }
        }
        if let totalFeesCalculated = totalFees as? Int{
            
            let headingString = NSMutableAttributedString(
                string: "Total Price : ",
                attributes: [NSAttributedStringKey(rawValue: NSAttributedStringKey.foregroundColor.rawValue):  UIColor(red:0.34, green:0.36, blue:0.4, alpha:1) , NSAttributedStringKey(rawValue: NSAttributedStringKey.font.rawValue): UIFont(name: "Amble-Regular", size: isIphone5 ? 10.0: 12.0)!])
            
            let subHeadingString = NSMutableAttributedString(
                string: "INR \(totalFeesCalculated) ",
                attributes: [NSAttributedStringKey(rawValue: NSAttributedStringKey.foregroundColor.rawValue): UIColor(red:0.01, green:0.51, blue:0.73, alpha:1) , NSAttributedStringKey(rawValue: NSAttributedStringKey.font.rawValue): UIFont(name: "Amble-Bold", size: isIphone5 ? 10.0 :12.0)!])
            
            let attrString1 = NSMutableAttributedString(string: "")
            attrString1.append(headingString)
            attrString1.append( NSMutableAttributedString(string: "  "))
            attrString1.append(subHeadingString)
            
            lblTotalPrice.attributedText = attrString1
        }
        //sucessfully retrieved data
        if let batchTempArray =  dictionary.object(forKey: "batch") as? Array<NSDictionary> {
            batchArray = Array()
            batchArray = batchTempArray
            collectionView.reloadData()
        } else{
            
        }
        
    }
    
    @objc func getprogramDetail()  {
        
        if self.refreshControl.isRefreshing{
        }
        
        guard (courseID) != nil else{
            
            self.showAlert(title: "Sorry", message: "CategoryID is not available please try again", withTag: 101, button: ["Ok"], isActionRequired: false)
            return
        }
        
            ProgressHUD.showProgressHUDWithLabel(label: "Loading...")
 
        
        APICall.callCourseDetailAPI([:], header: [ : ], courseID: courseID! ) { (sucess, result, error) in
            
            DispatchQueue.main.async {
                
                weak var weakSelf = self
                
                ProgressHUD.hideProgressHUD()
                
                if (error != nil) {
                    weakSelf?.showAlert(title: "Error", message: (error!.localizedDescription), withTag: 101, button: ["Ok"], isActionRequired: false)
                    return
                }
                
                if sucess {
                    
                    do {
                        let jsonData = try JSONSerialization.jsonObject(with: result!, options: .mutableContainers)
                        
                        if let responseDict = (jsonData as? NSDictionary){
                            
                            if let statusCode = responseDict.object(forKey: "status"), (statusCode is Int) {
                                
                                if (statusCode as! Int) == 200 {
                                    
                                    //sucessfully retrieved data
                                    if let dataDict = responseDict.object(forKey: "data") as? NSDictionary{
                                    
                                    weakSelf?.dataDictionary = dataDict
                                        
                                         weakSelf?.setData()
                                    
                                    }else{
                                        //No data found error
                                        weakSelf?.showAlert(title: APP_NAME, message:  AlertMessages.NoDataKeyFoundInResponse, withTag: 101, button: ["Ok"], isActionRequired: false)
                                    }
                                    
                                } else {
                                    var errStr = ""
                                    if let errormessage = (jsonData as? NSDictionary)?.object(forKey:"error") {
                                        if (errormessage is NSDictionary){
                                            if let errorDescription = (errormessage as? NSDictionary)?.object(forKey: "error_description"),!(errorDescription is NSNull){
                                                if (errorDescription is String){
                                                    errStr = errorDescription as! String
                                                    
                                                }
                                            }else {
                                                if let errorMessage = (errormessage as? NSDictionary)?.object(forKey: "message"),!(errorMessage is NSNull){
                                                    if (errorMessage is String){
                                                        errStr = errorMessage as! String
                                                        
                                                    }
                                                }else{
                                                    errStr = AlertMessages.UnidetifiedErrorMessage
                                                }
                                                
                                            }
                                            weakSelf?.showAlert(title: APP_NAME, message:errStr, withTag: 101, button: ["Ok"], isActionRequired: false)
                                        }
                                        
                                    }else{
                                        weakSelf?.showAlert(title: APP_NAME, message:  AlertMessages.UnidetifiedErrorMessage, withTag: 101, button: ["Ok"], isActionRequired: false)
                                    }
                                    
                                }
                            }
                        }else{
                            
                            weakSelf?.showAlert(title: "sorry", message: AlertMessages.NOTADICTIONARYMESSAGE , withTag: 101, button:  ["Ok"], isActionRequired: false)
                            return
                        }
                        
                    }catch{
                        
                        weakSelf?.showAlert(title: "sorry", message: AlertMessages.MODELERRORMESSAGE , withTag: 101, button:  ["Ok"], isActionRequired: false)
                        return
                    }
                    
                } else{
                    
                    //Success false
                    weakSelf?.showAlert(title: APP_NAME, message: AlertMessages.UnidetifiedErrorMessage, withTag: 101, button: ["Ok"], isActionRequired: false)
                    
                }
                
                if self.refreshControl.isRefreshing{
                    self.refreshControl.endRefreshing()
                }
                
//                if weakSelf?.tableView != nil {
//                    weakSelf?.tableView.reloadData()
//                }
            }
            
        }
    }
    
    @IBAction func backButtonClicked(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
        
    }
    
}

// MARK:- Collection Delegates

extension CourseDetailViewController : UICollectionViewDelegateFlowLayout {
    //1
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let itemSide: CGFloat = (collectionView.bounds.width)
        return CGSize(width: itemSide , height: collectionView.frame.height ) // + ( itemSide * 0.093 ))
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        
        return UIEdgeInsetsMake(0, 0, 0, 0)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat{
        
        
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat{
        
        return 0
    }
}
extension CourseDetailViewController: UICollectionViewDelegate , UICollectionViewDataSource , actionCompletedDelegate {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if let array = batchArray {
            return array.count
        }
       return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CourseDetailCollectionViewCellID", for: indexPath) as! CourseDetailCollectionViewCell
        
        
        cell.updateConstraintsIfNeeded()
        cell.layoutIfNeeded()
        cell.updateUIOnDoneDelegate = self
        if let tempbatchArray = batchArray {
            
            if let dict = tempbatchArray[indexPath.row] as?  NSDictionary {
                cell.setCellData(dict: dict)
            }
            
        }
        return cell
    }
    
    func updateUI(_ i: Int){
        
        self.navigationController?.popToRootViewController(animated: true)

    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        
    }
    
}
