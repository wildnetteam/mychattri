//
//  SeminarViewController.swift
//  MyChhatri
//
//  Created by Arpana on 30/08/18.
//  Copyright © 2018 Arpana. All rights reserved.
//

import UIKit

class SeminarViewController: UIViewController , UIViewControllerTransitioningDelegate {
        
        
        @IBOutlet weak var dimView: UIView!
        var rootCatID : String?
        
        @IBOutlet weak var shadowView: UIView!
        @IBOutlet weak var collectionView: UICollectionView!
    
     var monthArray: Array<String>?

        // MARK: - OUTLETS ADD MONTH VIEW BELOW
        @IBOutlet weak var calenderButton: UIButton!
        var bottomHeightForTab : CGFloat = 104.0
        var monthCollectionViewHeight : CGFloat = screenHeight * 0.12//isIphone5 ? 100: 160.0
        var circleRation : CGFloat = isIphone5 ? 2: 1.5
        var monthLabel: UILabel?
        var closeButton: UIButton?
        var monthCollectionView: UICollectionView?
        let transition = BubbleTransition()
        let interactiveTransition = BubbleInteractiveTransition()
        var customView = CalenderView()
        
        // MARK: - MAINTAIN PAGINATION AND WEBINAR LIST
        var pageNumber = 0
        var listlimit = 10 // Define to get maximum data one at a time
        var totalCount : Int?
        
        var seminarArray : Array<NSDictionary>?
        var offset : Int?
        
        override func viewDidLoad() {
            super.viewDidLoad()
            shadowView.layer.masksToBounds = false
            shadowView.layer.shadowOpacity = 0.4 ;
            shadowView.layer.shadowColor = UIColor.black.cgColor
            shadowView.layer.shadowOffset = CGSize(width: 0.0, height: 4.0)
            
            offset = 0
            collectionView.addSubview(refreshControl)
            monthArray = CalenderStruct.setUPCalenderMonth()
            getSeminarList()

        }
        
        lazy var refreshControl: UIRefreshControl = {
            let refreshControl = UIRefreshControl()
            refreshControl.addTarget(self, action:
                #selector(SeminarViewController.getSeminarList),
                                     for: UIControlEvents.valueChanged)
            refreshControl.tintColor = .black
            
            return refreshControl
        }()
        
        override func viewWillAppear(_ animated: Bool) {
            super.viewWillAppear(animated)
            if offset == 0 {
                getSeminarList()
            }
        }
        
        override func viewWillDisappear(_ animated: Bool) {
            super.viewWillDisappear(animated)
            self.closeCAlenderView()
            
        }
        
        // MARK:- GET WEB DATA FROM API
        
        
        @objc func getSeminarList()  {
            
            if self.refreshControl.isRefreshing{
                offset = 0
            }

            let infoDict = NSDictionary(objects: [rootCatID! ,10 ,offset ?? 0], forKeys: ["category_id" as NSCopying,"limit" as NSCopying ,"offset" as NSCopying])
            
            if let userInfoDict = UserModel.getLoginInfo() {
                
                guard ((userInfoDict.object(forKey: "ID")) != nil) else{
                    
                    self.showAlert(title: "Sorry", message: "User details could not be found , Please logout and login again", withTag: 101, button: ["Ok"], isActionRequired: false)
                    return
                }
                
                guard (userInfoDict.object(forKey: "token") as? String) != nil else{
                    
                    self.showAlert(title: "Sorry", message: "User accessToken could not be found , Please logout and login again", withTag: 101, button: ["Ok"], isActionRequired: false)
                    return
                }
            }
            if collectionView != nil {
                
                ProgressHUD.showProgressHUDWithLabel(label: "Loading...")
            }
            
            APICall.callSeminarListAPI([:], header: [ : ], information:infoDict ) { (sucess, result, error) in
                
                DispatchQueue.main.async {
                    
                    weak var weakSelf = self
                    
                    ProgressHUD.hideProgressHUD()
                    
                    if (error != nil) {
                        weakSelf?.showAlert(title: "Error", message: (error!.localizedDescription), withTag: 101, button: ["Ok"], isActionRequired: false)
                        return
                    }
                    
                    if sucess {
                        
                        do {
                            let jsonData = try JSONSerialization.jsonObject(with: result!, options: .mutableContainers)
                            
                            if let responseDict = (jsonData as? NSDictionary){
                                
                                if let statusCode = responseDict.object(forKey: "status"), (statusCode is Int) {
                                    
                                    if (statusCode as! Int) == 200 {
                                        
                                        //sucessfully retrieved data
                                        if let dataDict = responseDict.object(forKey: "data") as? NSDictionary{
                                                
                                                if let toptalR = dataDict.value(forKey: "totalCount") as? Int{
                                                    weakSelf?.totalCount = toptalR
                                                }else if let toptalR = dataDict.value(forKey: "totalCount") as? String{
                                                    weakSelf?.totalCount = Int(toptalR)
                                                }
                                            if let tempArray = dataDict.object(forKey: "seminars") as? Array<NSDictionary> {
                                                
                                                if tempArray.count > 0 {
                                                    //store data
                                                  if  weakSelf?.offset == 0 {
                                                        self.seminarArray =  Array()
                                                        
                                                        weakSelf?.seminarArray = tempArray
                                                    }else{
                                                        //Just add to previous array
                                                        weakSelf?.seminarArray?.append(contentsOf: tempArray)
                                                    }
                                                   
                                                }else{
                                                    //No saved stream found message
                                                    weakSelf?.showAlert(title: APP_NAME, message: "You do not have any seminars yet.", withTag: 101, button: ["Ok"], isActionRequired: false)
                                                }
                                            }else {
                                                //
                                                weakSelf?.showAlert(title: APP_NAME, message: "seminars key not found", withTag: 101, button: ["Ok"], isActionRequired: false)
                                            }
                                        }else{
                                            //No data found error
                                            weakSelf?.showAlert(title: APP_NAME, message:  AlertMessages.NoDataKeyFoundInResponse, withTag: 101, button: ["Ok"], isActionRequired: false)
                                        }
                                        
                                    } else {
                                        var errStr = ""
                                        if let errormessage = (jsonData as? NSDictionary)?.object(forKey:"error") {
                                            if (errormessage is NSDictionary) {
                                                if let errorDescription = (errormessage as? NSDictionary)?.object(forKey: "error_description"),!(errorDescription is NSNull){
                                                    if (errorDescription is String){
                                                        errStr = errorDescription as! String
                                                        
                                                    }
                                                }else {
                                                    if let errorMessage = (errormessage as? NSDictionary)?.object(forKey: "message"),!(errorMessage is NSNull){
                                                        if (errorMessage is String){
                                                            errStr = errorMessage as! String
                                                            
                                                        }
                                                    }else{
                                                        errStr = AlertMessages.UnidetifiedErrorMessage
                                                    }
                                                    
                                                }
                                                weakSelf?.showAlert(title: APP_NAME, message:errStr, withTag: 101, button: ["Ok"], isActionRequired: false)
                                            }
                                            
                                        }else{
                                            weakSelf?.showAlert(title: APP_NAME, message:  AlertMessages.UnidetifiedErrorMessage, withTag: 101, button: ["Ok"], isActionRequired: false)
                                        }
                                        
                                    }
                                }
                            }else{
                                
                                weakSelf?.showAlert(title: "sorry", message: AlertMessages.NOTADICTIONARYMESSAGE , withTag: 101, button:  ["Ok"], isActionRequired: false)
                                return
                            }
                            
                        }catch{
                            
                            weakSelf?.showAlert(title: "sorry", message: AlertMessages.MODELERRORMESSAGE , withTag: 101, button:  ["Ok"], isActionRequired: false)
                            return
                        }
                        
                    } else{
                        
                        //Success false
                        weakSelf?.showAlert(title: APP_NAME, message: AlertMessages.UnidetifiedErrorMessage, withTag: 101, button: ["Ok"], isActionRequired: false)
                        
                    }
                    
                    if self.refreshControl.isRefreshing{
                        self.refreshControl.endRefreshing()
                    }
                    
                    if weakSelf?.collectionView != nil {
                        weakSelf?.collectionView.reloadData()
                    }
                }
                
            }
        }
        
        // MARK: - ACTION TO OPEN CALENDER TO SELECT MONTH
        
        @IBAction func setUPUI(){
            
            UIView.animate(withDuration: 0.6, animations: {
                self.dimView.transform = CGAffineTransform.identity
                
                self.dimView.frame = self.frameForBubble(self.calenderButton.center, size: self.dimView.bounds.size, start: self.calenderButton.center)
            }, completion: { (_) in
                
            })
            
            
            customView = (Bundle.main.loadNibNamed("CalenderView", owner: self, options: nil)?[0] as? CalenderView)!
            
            customView.frame = frameForBubble(calenderButton.center, size: CGSize(width: self.view.bounds.width, height: self.view.bounds.height ), start: calenderButton.center)
            customView.layer.cornerRadius = customView.frame.size.height / 2.5
            customView.center = CGPoint(x: self.view.bounds.maxX, y: self.view.frame.maxY)
            customView.transform = CGAffineTransform(scaleX: 0.001, y: 0.001)
            customView.backgroundColor =  UIColor(red: 238 / 255, green: 242 / 255, blue: 245 / 255, alpha: 1)
            customView.layer.masksToBounds = true
            customView.alpha = 1
            self.view.addSubview(customView)
            
            UIView.animate(withDuration: 0.6, animations: {
                self.customView.transform = CGAffineTransform.identity
                
            }, completion: { (_) in
                self.calenderButton.isHidden = true
                self.setUIInCalenderView()
                self.dimView.isHidden = false
                self.monthCollectionView?.isHidden = false
                self.closeButton?.isHidden = false
                self.monthLabel?.isHidden = false
                self.customView.layoutIfNeeded()
                self.customView.updateConstraintsIfNeeded()
            })
            
        }
        
        @IBAction func hideCalenderView(){
            
            self.dimView.isHidden = true
        }
        
        func setUIInCalenderView()  {
            
            self.dimView.layer.cornerRadius = 0
            self.dimView.layer.masksToBounds = false
            
            
            if ( monthCollectionView == nil){
                
                let flowLayout = UICollectionViewFlowLayout()
                flowLayout.itemSize = CGSize(width: 100, height: 100)
                flowLayout.scrollDirection = .horizontal
                
                monthCollectionView = UICollectionView(frame: CGRect(x: 20, y:  self.collectionView.frame.maxY - monthCollectionViewHeight - 20 , width: screenWidth - 40, height: monthCollectionViewHeight ), collectionViewLayout: flowLayout)
                
                self.monthCollectionView?.register(UINib(nibName: "monthCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "monthCollectionViewCellID")
                
                monthCollectionView?.collectionViewLayout = flowLayout
                monthCollectionView?.backgroundColor = .clear
                monthCollectionView?.tag = 999
                monthCollectionView?.delegate = self;
                monthCollectionView?.dataSource = self;
                
            }
            self.view.addSubview(self.monthCollectionView!)
            
            monthCollectionView?.frame =  CGRect(x: 20, y:  self.collectionView.frame.maxY - monthCollectionViewHeight - 40 , width: screenWidth - 40, height: monthCollectionViewHeight )
            
            monthCollectionView?.transform = CGAffineTransform(scaleX: 0, y: 0)
            UIView.animate(withDuration: 0.4) {
                self.monthCollectionView?.transform = .identity
            }
            
            
            if( monthLabel == nil){
                
                
                monthLabel = UILabel(frame:CGRect(x:self.view.bounds.midX , y:  (self.monthCollectionView?.frame.minY)!  + 20 , width: (screenWidth - screenWidth / 2 ) - 20 , height: 0))
                monthLabel?.numberOfLines = 2
                monthLabel?.textAlignment = .right
                
                let headingString = NSMutableAttributedString(
                    string: "Select a Month",
                    attributes: [NSAttributedStringKey(rawValue: NSAttributedStringKey.foregroundColor.rawValue): UIColor.black , NSAttributedStringKey(rawValue: NSAttributedStringKey.font.rawValue): UIFont(name: "Amble-Bold", size: isIphone5 ? 16.0: 18.0)!])
                
                let subHeadingString = NSMutableAttributedString(
                    string: "(choose a month to go ahead).",
                    attributes: [NSAttributedStringKey(rawValue: NSAttributedStringKey.foregroundColor.rawValue): UIColor(red:0.62, green:0.62, blue:0.62, alpha:1) , NSAttributedStringKey(rawValue: NSAttributedStringKey.font.rawValue): UIFont(name: "Amble-Regular", size: isIphone5 ? 12.0 :14.0)!])
                
                let attrString1 = NSMutableAttributedString(string: "")
                attrString1.append(headingString)
                attrString1.append( NSMutableAttributedString(string: "\n"))
                attrString1.append(subHeadingString)
                self.monthLabel?.attributedText = attrString1
                
            }
            monthLabel?.frame = CGRect(x:self.view.bounds.midX - 100 , y: (self.monthCollectionView?.frame.minY)! - ( 20 + 90)  , width: 0 , height: 0)
            //90 height of label
            
            
            UIView.animate(withDuration: 0.5, animations: {
                
                if isIphone5{
                    
                    self.monthLabel?.frame = CGRect(x:self.view.bounds.midX , y:  (self.monthCollectionView?.frame.minY)! - ( 20 + 75) , width: (screenWidth - screenWidth / 2 ) - 10  , height: 75)
                }  else{
                    
                    self.monthLabel?.frame = CGRect(x:self.view.bounds.midX , y:  (self.monthCollectionView?.frame.minY)! - ( 20 + 90) , width: (screenWidth - screenWidth / 2 ) - 10 , height: 90)
                }
                self.monthLabel?.layoutIfNeeded()
                self.monthLabel?.updateConstraintsIfNeeded()
                
            }) { (finished) in
                
                
                if( self.closeButton == nil){
                    
                    self.closeButton = UIButton()
                    
                }
                
                self.closeButton?.frame =  CGRect(x: self.view.bounds.maxX + 25 , y:  (self.monthLabel?.frame.minY)! -  100  , width: 10, height: 10)
                
                UIView.animate(withDuration: 0.4) {
                    
                    self.closeButton?.frame =  CGRect(x: self.view.bounds.maxX -  100 , y:  (self.monthLabel?.frame.minY)! - 80 , width: 100, height: 100)
                    self.closeButton?.setImage(#imageLiteral(resourceName: "calendercross"), for: .normal)
                    
                    self.closeButton?.layoutIfNeeded()
                    self.closeButton?.updateConstraintsIfNeeded()
                }
                self.closeButton?.isHidden = false
                
                self.closeButton?.addTarget(self, action:#selector(self.closeCAlenderView), for: .touchUpInside)
                self.view.addSubview(self.closeButton!)
            }
            self.view.addSubview(monthLabel!)
            monthLabel?.isHidden = false
            
            
        }
        
        // MARK: - ACTION TO CLOSE CALENDER TO SELECT MONTH
        
        @objc func closeCAlenderView() {
            
            UIView.animate(withDuration: 0.6, delay: 0.5, options:
                UIViewAnimationOptions.curveEaseOut, animations: {
                    
                    self.monthLabel?.frame = CGRect(x: (self.monthLabel?.frame.midX)!, y: (self.monthLabel?.frame.midY)!, width: 0, height: 0)
                    
                    self.monthLabel?.layoutIfNeeded()
                    self.monthLabel?.updateConstraintsIfNeeded()
                    
                    self.monthCollectionView?.frame = CGRect(x: (self.monthCollectionView?.frame.midX)!, y: (self.monthCollectionView?.frame.midY)!, width: 0, height: 0)
                    
                    self.monthCollectionView?.layoutIfNeeded()
                    self.monthCollectionView?.updateConstraintsIfNeeded()
                    
                    self.customView.frame = CGRect(x: self.customView.frame.midX, y: self.customView.frame.midY, width: 0, height: 0)
                    
                    self.dimView.layer.cornerRadius = self.dimView.frame.width / 2
                    self.dimView.layer.masksToBounds = true
                    self.dimView.frame = CGRect(x: self.view.bounds.maxX, y: self.view.bounds.maxY, width: 0, height: 0)
                    
                    self.closeButton?.frame = self.calenderButton.frame
                    self.dimView.layoutIfNeeded()
                    self.dimView.updateConstraintsIfNeeded()
                    
            }, completion: { finished in
                
                self.closeButton?.isHidden = true
                self.monthLabel?.isHidden = true
                self.monthCollectionView?.isHidden = true
                self.dimView.isHidden = true
                self.calenderButton.backgroundColor = .clear
                self.calenderButton.isHidden = false
                
                self.customView.layer.cornerRadius = self.customView.frame.size.height / 2.5
                self.customView.center = self.calenderButton.center
                self.customView.transform = CGAffineTransform(scaleX: 0, y: 0)
                self.customView.layer.masksToBounds = true
                self.customView.alpha = 1
                self.customView.removeFromSuperview()
                self.customView.removeFromSuperview()
                self.customView.layoutIfNeeded()
                self.customView.updateConstraintsIfNeeded()
                
            })
            
        }
        
        func frameForBubble(_ originalCenter: CGPoint, size originalSize: CGSize, start: CGPoint) -> CGRect {
            let lengthX = fmax(start.x, originalSize.width - start.x)
            let lengthY = fmax(start.y, originalSize.height - start.y)
            let offset =  sqrt(lengthX * lengthX + lengthY * lengthY) * circleRation // 1.5 ->8p
            let size = CGSize(width: offset, height: offset)
            
            return CGRect(origin: CGPoint.zero, size: size)
        }
        
        override func didReceiveMemoryWarning() {
            super.didReceiveMemoryWarning()
            // Dispose of any resources that can be recreated.
        }
        
        
        
        override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
            let controller = segue.destination
            controller.transitioningDelegate = self
            controller.modalPresentationStyle = .custom
            interactiveTransition.attach(to: controller)
        }
        
        // MARK: UIViewControllerTransitioningDelegate
        
        func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
            transition.transitionMode = .present
            transition.startingPoint = calenderButton.center
            transition.bubbleColor = .green//calenderButton.backgroundColor!
            return transition
        }
        
        func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
            transition.transitionMode = .dismiss
            transition.startingPoint = calenderButton.center
            transition.bubbleColor = .blue//calenderButton.backgroundColor!
            return transition
        }
        
        func interactionControllerForDismissal(using animator: UIViewControllerAnimatedTransitioning) -> UIViewControllerInteractiveTransitioning? {
            return interactiveTransition
        }
        
    }
    // MARK:- Collection Delegates
    
    extension SeminarViewController : UICollectionViewDelegateFlowLayout {
        //1
        func collectionView(_ collectionView: UICollectionView,
                            layout collectionViewLayout: UICollectionViewLayout,
                            sizeForItemAt indexPath: IndexPath) -> CGSize {
            
            if(collectionView.tag ==  999){
                
                return CGSize(width: (collectionView.bounds.width) / 3 , height: collectionView.bounds.height / 2)
            }
            let itemSide: CGFloat = (collectionView.bounds.width) / 2
            return CGSize(width: itemSide , height: itemSide + ( itemSide * 0.093 ))
        }
        
        func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
            
            return UIEdgeInsetsMake(0, 0, 0, 0)
            
        }
        
        func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat{
            
            
            return 0
        }
        
        func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat{
            
            return 0
        }
    }
    extension SeminarViewController: UICollectionViewDelegate , UICollectionViewDataSource {
        
        func numberOfSections(in collectionView: UICollectionView) -> Int {
            
            return 1
        }
        
        func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
            
            if(collectionView.tag ==  999){
                
                if let mArray = monthArray {
                    return mArray.count
                }else {
                    return 0
                }
                
            }
            if let mArray = seminarArray {
                return mArray.count
            }else {
                return 0
            }
        }
        
        func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
        {
            
            if(collectionView.tag ==  999){
                
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "monthCollectionViewCellID", for: indexPath) as! monthCollectionViewCell
                cell.lblTitle.layer.cornerRadius = cell.lblTitle.frame.height * 0.449
                cell.lblTitle.layer.masksToBounds = true
                cell.lblTitle.layer.borderWidth = 1
                cell.lblTitle.layer.borderColor = UIColor(red:0.9, green:0.9, blue:0.9, alpha:1).cgColor
                if let mArray = monthArray {
                    
                    cell.lblTitle.text = mArray[indexPath.row]
                }
                return cell
                
            }else{
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "WebinarCollectionViewCellID", for: indexPath) as! WebinarCollectionViewCell
                
                cell.backGroundWhiteView.layer.cornerRadius = (cell.shodowView.frame.size.width * 0.06)
                cell.backGroundWhiteView.layer.masksToBounds = true
                cell.imgView.layer.cornerRadius = ( cell.imgView.frame.size.width / ( cell.imgView.frame.size.width * 0.4))
                cell.imgView.layer.masksToBounds = true
                cell.shodowView.layer.shadowOpacity = 1;
                cell.shodowView.layer.shadowRadius = 20
                // cell.shodowView.layer.shadowColor = UIColor(red:0, green:0, blue:0, alpha:0.1).cgColor
                cell.shodowView.layer.shadowColor = UIColor(red:0.8, green:0.8, blue:0.8, alpha:0.5).cgColor
                cell.shodowView.layer.shadowOffset = CGSize(width: 0.0, height: cell.contentView.frame.height * 0.04)
                cell.shodowView.layer.masksToBounds = false
                
                if seminarArray != nil {
                    
                    if let tempWebinarArray = seminarArray {
                        
                        if let dict = tempWebinarArray[indexPath.row] as?  NSDictionary {
                            
                            if let wTitle = dict.value(forKey: "created_at") as? String {
                                
                                
                                cell.lblTitle.text =   CalenderStruct.getFormatedDateString(fromDateUTC:  CalenderStruct.getDateFromDateString(forUTC: wTitle, byFormat: CalenderStruct.DATE_TIME_ZONE_FORMAT_LOCAL), inFormat: "dd MMM yyyy")
                            }
                            if let wDes = dict.value(forKey: "description") as? String {
                                cell.lblDescription.text = wDes.withoutHtml
                            }
                            
                            
                            var imageFinalUrl =  ""
                            
                            if let imageURL = dict.value(forKey: "banner_image") {
                                
                                imageFinalUrl = "\(URLS.IMAGE_BASE_URL)" + "\(imageURL)"
                                
                                cell.imgView.sd_setIndicatorStyle(.gray)
                                cell.imgView.sd_setShowActivityIndicatorView(true)
                                
                                cell.imgView.sd_setImage(with: URL(string: imageFinalUrl), completed: { (image, error, type, url) in
                                    DispatchQueue.main.async {
                                        if error == nil{
                                            cell.imgView.image = image
                                        }else{
                                            cell.imgView.image = #imageLiteral(resourceName: "Placeholder")
                                        }
                                        cell.imgView.sd_setShowActivityIndicatorView(false)
                                    }
                                })
                            }
                        }
                        //Reload the next amount of data
                        if(indexPath.row ==  (seminarArray?.count)! - 1 && totalCount! > (seminarArray?.count)! ){
                            //we are at last index of currently displayed cell
                            offset = (seminarArray?.count)!
                            //reload more data
                            getSeminarList()
                        }
                    }
                }
                cell.updateConstraintsIfNeeded()
                cell.layoutIfNeeded()
                
                return cell
            }
        }
        
        func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
            
            print("didSelectItemAt")
            if(collectionView.tag ==  999){
                
                let fmt = DateFormatter()
                fmt.dateFormat = "MMMM"
                let dateString = monthArray![indexPath.row]
                let num =  fmt.monthSymbols.index(of: dateString)
                print("month Number \(num! + 1)")
            }else{
                
                if seminarArray != nil {
                    
                    if let dict = seminarArray?[indexPath.row] {
                        NavigationManager.moveToWebinarDetailedViewControllerIfNotExists(kAppDelegate.menuNavController!,  informationDict: dict )
                    }
                    
                }
            }
        }
}
