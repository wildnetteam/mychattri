//
//  ResetPasswordViewController.swift
//  MyChhatri
//
//  Created by Arpana on 27/07/18.
//  Copyright © 2018 Anshul. All rights reserved.
//

import UIKit

class ResetPasswordViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var txtConfirmPassword:UITextField!
    @IBOutlet weak var txtPassword:UITextField!
    @IBOutlet weak var btnSave:UIButton!
    var userID = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
         setUPUI()
        // Do any additional setup after loading the view.
    }

    func setUPUI() {
        
        let viewLeftEmail = UIView(frame: CGRect(x: 0, y: 0, width: 8, height: 40))
        txtConfirmPassword.leftView = viewLeftEmail
        txtConfirmPassword.leftViewMode = .always
        let viewLeftPassword = UIView(frame: CGRect(x: 0, y: 0, width: 8, height: 40))
        txtPassword.leftView = viewLeftPassword
        txtPassword.leftViewMode = .always
        btnSave.addGrediant()

    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(true)
        let statusBar: UIView = UIApplication.shared.value(forKey: "statusBar") as! UIView
        statusBar.backgroundColor = .clear
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
        super.viewWillDisappear(true)
        
        let statusBar: UIView = UIApplication.shared.value(forKey: "statusBar") as! UIView
        statusBar.backgroundColor = ColorConstants.appBackGroundCOLOR()
        
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
    
    @IBAction func btnActionSave(sender:UIButton) {
        
        self.view.endEditing(true)
        
        if validateData() {
            callResetpasswordAPI()
            
        }
    }
    
    @IBAction func backButtonPressed(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
//        //MARK: API
        func callResetpasswordAPI() {

            var params : [String:Any] = [:]

            params["password"] = txtPassword.text
            params["c_password"] = txtConfirmPassword.text
            params["user_id"] = userID
//            var headerData : [String:Any] = [:]
//
//            headerData["Authorization"] =  "Bearer \(String(describing: userDefaults.value(forKey: "access_token")))"

            print("params : \(params)")

            ProgressHUD.showProgressHUDWithLabel(label: "Loading...")

            APIManager.callPost1("\(URLS.BASE_URL)\(URLS.RESET_PASSWORD)", param: params, header: ["":""]) { (isSuccess, result, error) in

                DispatchQueue.main.async {

                    ProgressHUD.hideProgressHUD()

                    weak var weakSelf = self

                    if (error != nil) {
                        weakSelf?.showAlert(title: "Error", message: (error!.localizedDescription), withTag: 101, button: ["Ok"], isActionRequired: false)
                    } else {
                        if isSuccess {

                            do {
                                let jsonData = try JSONSerialization.jsonObject(with: result!, options: .mutableContainers)

                                if let statusCode = (jsonData as! NSDictionary).object(forKey: "status"), (statusCode is Int) {
                                    if let success = (jsonData as! NSDictionary).object(forKey: "success") {
                                        
                                        if (success as! Bool) == true {
                                            
                                            if (statusCode as! Int) == 200 {
                                                weakSelf?.showAlert(title: APP_NAME, message: "password is set successfully", withTag: 98, button: ["Ok"], isActionRequired: true)
                                                
                                            } else {
                                                var errStr = ""
                                                if let errormessage = (jsonData as? NSDictionary)?.object(forKey:"error") {
                                                    if (errormessage is NSDictionary){
                                                        if let errorDescription = (errormessage as? NSDictionary)?.object(forKey: "error_description"),!(errorDescription is NSNull){
                                                            if (errorDescription is String){
                                                                errStr = errorDescription as! String
                                                                
                                                            }
                                                        }else {
                                                            if let errorMessage = (errormessage as? NSDictionary)?.object(forKey: "message"),!(errorMessage is NSNull){
                                                                if (errorMessage is String){
                                                                    errStr = errorMessage as! String
                                                                    
                                                                }
                                                            }else{
                                                                errStr = "Something went wrong"
                                                            }
                                                            
                                                        }
                                                        weakSelf?.showAlert(title: APP_NAME, message:errStr, withTag: 101, button: ["Ok"], isActionRequired: false)
                                                    }
                                                }
                                            }
                                        } else {
                                            var errStr = ""
                                            if let errormessage = (jsonData as? NSDictionary)?.object(forKey:"error") {
                                                if (errormessage is NSDictionary){
                                                    if let errorDescription = (errormessage as? NSDictionary)?.object(forKey: "error_description"),!(errorDescription is NSNull){
                                                        if (errorDescription is String){
                                                            errStr = errorDescription as! String
                                                            
                                                        }
                                                    }else {
                                                        if let errorMessage = (errormessage as? NSDictionary)?.object(forKey: "message"),!(errorMessage is NSNull){
                                                            if (errorMessage is String){
                                                                errStr = errorMessage as! String
                                                                
                                                            }
                                                        }else{
                                                            errStr = "Something went wrong"
                                                        }
                                                        
                                                    }
                                                    weakSelf?.showAlert(title: APP_NAME, message:errStr, withTag: 101, button: ["Ok"], isActionRequired: false)
                                                }
                                            }
                                        }
                                    }
                                    else{
                                        
                                    }
                                    
                                }  //print(jsonData as! NSDictionary)

                            }catch{
                                weakSelf?.showAlert(title: "sorry", message: AlertMessages.MODELERRORMESSAGE , withTag: 101, button:  ["Ok"], isActionRequired: false)
                                return
                            }
                        } else{
                            self.showAlert(title: APP_NAME, message: AlertMessages.UnidetifiedErrorMessage, withTag: 101, button: ["Ok"], isActionRequired: false)
                        }
                    }
                }
            }
        }

    override func alertButtonAction(alertTag: Int, btnTitle: String) {
        
        if alertTag == 98 {
            
            let viewController = UIStoryboard.instantiateLoginViewController()
            appDelegate.menuNavController?.pushViewController(viewController, animated: true)
           // kAppDelegate.navigateToLoginViewController()

        }
    }

    // Validate data...
    private func validateData() -> Bool {
        
        if txtPassword.text?.trim().count == 0 {self.showAlert(title:APP_NAME, message:"Please enter password", withTag:101, button:["Ok"], isActionRequired:false)
            return false
        }
        if txtPassword.text != txtConfirmPassword.text {
            
            self.showAlert(title:APP_NAME, message:"Password mismatch.", withTag:101, button:["Ok"], isActionRequired:false)
            return false
        }
        else {
            return true
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField == txtPassword || textField == txtConfirmPassword {
            let maxLength = 8
            let currentString: NSString = textField.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
        }
        return true
    }
}
