//
//  termsAndCondistionsViewController.swift
//  MyChhatri
//
//  Created by Arpana on 24/08/18.
//  Copyright © 2018 Arpana. All rights reserved.
//

import UIKit
class termsAndCondistionsViewController: UIViewController, UIWebViewDelegate {

    @IBOutlet weak var webView: UIWebView!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func viewDidLayoutSubviews() {
        
        self.webView.scrollView.contentSize = CGSize(width: self.webView.frame.size.width, height: webView.scrollView.contentSize.height)
        self.webView.updateConstraintsIfNeeded()
        
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        
        ProgressHUD.hideProgressHUD()
        self.view.layoutIfNeeded()
    }
    
    func webViewDidStartLoad(_ webView: UIWebView) {
        ProgressHUD.showProgressHUDWithLabel(label: "Loading...")
    }
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
        ProgressHUD.hideProgressHUD()

    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView.contentOffset.x > 0 {
            scrollView.contentOffset = CGPoint(x: 0, y: scrollView.contentOffset.y)
        }
        self.webView.scrollView.contentSize = CGSize(width: self.webView.frame.size.width, height: webView.frame.size.height)
        
        self.webView.updateConstraintsIfNeeded()
        self.view.layoutIfNeeded()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(true)
        getInformation()
        
    }
    func getInformation()  {
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    


}
