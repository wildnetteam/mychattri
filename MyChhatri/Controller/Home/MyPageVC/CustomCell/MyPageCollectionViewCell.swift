//
//  MyPageCollectionViewCell.swift
//  MyChhatri
//
//  Created by Arpana on 23/07/18.
//  Copyright © 2018 Anshul. All rights reserved.
//

import UIKit

class MyPageCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var backGroundWhiteView: UIView!
    
    @IBOutlet weak var imgView: UIImageView!
    
    @IBOutlet weak var shodowView: UIView!
    
    @IBOutlet weak var btnSelection: UIButton!
    @IBOutlet weak var lblTitle: UILabel!
    
    @IBOutlet weak var lblDescription: UILabel!
}
