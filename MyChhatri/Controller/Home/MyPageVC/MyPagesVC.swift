//
//  MyPagesVC.swift
//  MyChhatri
//
//  Created by Anshul on 18/07/18.
//  Copyright © 2018 Anshul. All rights reserved.
//

import UIKit
class MyPagesVC: UIViewController {
    
    var rowsWhichIsChecked : Int = -1
    var streamCategoryObject :BaseStreamCategory?
    @IBOutlet weak var collectionView: UICollectionView!
    
    @IBOutlet weak var adViewHightConstant: NSLayoutConstraint!
    @IBOutlet weak var shadowView: UIView!
    @IBOutlet weak var adView : CustomAdvertiseView!
    var schoolArray :NSArray?
    var collegeArray :NSArray?
    
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action:
            #selector(MyPagesVC.getCategories),
                                 for: UIControlEvents.valueChanged)
        refreshControl.tintColor = .black
        
        return refreshControl
    }()
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        //Add shdow Below page menu
        shadowView.layer.masksToBounds = false
        shadowView.layer.shadowOpacity = 0.4 ;
        shadowView.layer.shadowColor = UIColor.black.cgColor
        shadowView.layer.shadowOffset = CGSize(width: 0.0, height: 4.0)
        //identify if its school or college student
        getType()
        getCategories()
      //  adView.updelegate = self
        self.collectionView.addSubview(self.refreshControl)
        
    }

    func getType(){
        
        var userInstituteType: String?
        
        if let userInfoDict = UserModel.getLoginInfo() {
            
            if let  userType = (userInfoDict.object(forKey: "instituteType") as? String){
                
                userInstituteType  = userType
                
            }
        }else{
            self.showAlert(title: "Error", message: "could not identify user Type, please login again", withTag: 500, button: ["login"], isActionRequired: false)
            
        }
    }
    
    func setNavigationAndStatusAppearance()  {


        let statusBar: UIView = UIApplication.shared.value(forKey: "statusBar") as! UIView
        statusBar.backgroundColor = ColorConstants.appBackGroundCOLOR()

        UINavigationBar.appearance().barTintColor = ColorConstants.appBackGroundCOLOR()

        UINavigationBar.appearance().setBackgroundImage(UIImage(), for: .default)
        // Sets shadow (line below the bar) to a blank image
        UINavigationBar.appearance().shadowImage = UIImage()
        // Sets the translucent background color
        UINavigationBar.appearance().backgroundColor = ColorConstants.appBackGroundCOLOR()
        // Set translucent. (Default value is already true, so this can be removed if desired.)
        UINavigationBar.appearance().isTranslucent = true

    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.navigationBar.isHidden = false;
        setNavigationAndStatusAppearance()
        if(collectionView != nil) {
            rowsWhichIsChecked = -1
            collectionView.reloadData()
        }
        setNeedsStatusBarAppearanceUpdate()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if(adView != nil) {
            adView.isHidden = false
            adView.hideAdViewDel = self
            adViewHightConstant.constant = 60
            adView.categoryIDForAdvertise = "8"
            adView.typeFoAdvertiseData = "1"
        }
        adView.awakeFromNib()
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        adView.hideAdViewDel = nil

    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    @objc func getCategories()  {
        
        
        let params : [String:Any] = [:]
       
        if collectionView != nil {
            
            ProgressHUD.showProgressHUDWithLabel(label: "Loading...")
        }
        
        APICall.callStreamAPI(params, header: [ : ]) { (sucess, result, error) in
            
            DispatchQueue.main.async {
                
                weak var weakSelf = self
                
                self.refreshControl.endRefreshing()
                
                ProgressHUD.hideProgressHUD()

                if (error != nil) {
                    
                    weakSelf?.showAlert(title: "Error", message: (error!.localizedDescription), withTag: 101, button: ["Ok"], isActionRequired: false)
                    return
                }
                if sucess {
                    
                    do {
                        
                        let jsonData = try JSONSerialization.jsonObject(with: result!, options: .mutableContainers)
                        
                        if (jsonData is NSDictionary){
                            
                            if let statusCode = (jsonData as! NSDictionary).object(forKey: "status"), (statusCode is Int) {
                                
                                if (statusCode as! Int) == 401 {
                                    
                                    //session expired
                                    weakSelf?.showAlert(title: APP_NAME, message:"Session expired, Please login again.", withTag: 500, button: ["login"], isActionRequired: true)
                                    return
                                    
                                }
                                
                                if (statusCode as! Int) == 200 {
                                    
                                    //sucessfully retrieved data
                                    if let dataArray = (jsonData as! NSDictionary).object(forKey: "data") as? NSDictionary{
                                        
                                        //It will have bothn school and college array
                                        
                                        //Fetch both the array's in separate school ang college Array
                                        
                                        if let schoolTempArray = dataArray.object(forKey: "school") as? NSArray{
                                            weakSelf?.schoolArray = NSArray(array:schoolTempArray)
                                        }
                                        if let collegeTempArray = dataArray.object(forKey: "college") as? NSArray{
                                            weakSelf?.collegeArray = NSArray(array:collegeTempArray)
                                            
                                        }
                                        
                                    }
                                    weakSelf?.collectionView.reloadData()
                                }else{
                                    
                                    var errStr = ""
                                    if let errormessage = (jsonData as? NSDictionary)?.object(forKey:"error") {
                                        if (errormessage is NSDictionary){
                                            if let errorDescription = (errormessage as? NSDictionary)?.object(forKey: "error_description"),!(errorDescription is NSNull){
                                                if (errorDescription is String){
                                                    errStr = errorDescription as! String
                                                    
                                                }
                                            }else {
                                                if let errorMessage = (errormessage as? NSDictionary)?.object(forKey: "message"),!(errorMessage is NSNull){
                                                    if (errorMessage is String){
                                                        errStr = errorMessage as! String
                                                        
                                                    }
                                                }else{
                                                    errStr = "Something went wrong"
                                                }
                                                
                                            }
                                            weakSelf?.showAlert(title: APP_NAME, message:errStr, withTag: 101, button: ["Ok"], isActionRequired: false)
                                        }
                                        
                                    }else{
                                        weakSelf?.showAlert(title: APP_NAME, message: AlertMessages.UnidetifiedErrorMessage, withTag: 101, button: ["Ok"], isActionRequired: false)
                                    }
                                }
                            }
                            weakSelf?.collectionView.reloadData()
                            
                        }else{
                            
                            //mot a dict
                            weakSelf?.showAlert(title: "sorry", message: AlertMessages.NOTADICTIONARYMESSAGE , withTag: 101, button:  ["Ok"], isActionRequired: false)
                            return
                        }
                    }catch  let error as NSError {
                        print(error.localizedDescription)
                        
                        weakSelf?.showAlert(title: "sorry", message: AlertMessages.MODELERRORMESSAGE , withTag: 101, button:  ["Ok"], isActionRequired: false)
                        return
                    }
                }
                else{
                    //Alert not sucessfull
                    weakSelf?.showAlert(title: APP_NAME, message: "could not get response from server", withTag: 101, button: ["Ok"], isActionRequired: false)
                }
                
                if weakSelf?.collectionView != nil {
                    ProgressHUD.hideProgressHUD()
                }
            }
        }
        
    }
    
    override func alertButtonAction(alertTag: Int, btnTitle: String) {
        
        if alertTag == 500  {
            if btnTitle == "login" {
                
                UserModel.setLoginInfo(userData: nil)
                UserModel.setAutoLogin(isSucessfull:false)
                //LocalDB.shared.currentLanguage = nil
                appDelegate.navigateToLoginViewController()
                
            }
        }
    }
    
}

extension MyPagesVC : HideADUIDelegate {
  //update add UI if no data available hide the view
    func removeAdcollectionViewUI()  {
        
        adView.isHidden = true
        adViewHightConstant.constant = 0
    }
}

extension MyPagesVC : UICollectionViewDelegateFlowLayout {
    //1
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let itemSide: CGFloat = (collectionView.bounds.width) / 2
        return CGSize(width: itemSide , height: itemSide - ( itemSide * 0.06 ))
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        
        return UIEdgeInsetsMake(0, 0, 0, 0)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat{
        
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat{
        return 0
    }
}


extension MyPagesVC: UICollectionViewDelegate , UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if UserModel.getUserType() == 1 {
           
            if let array = schoolArray {
                
                return array.count
                
            }
        }else{
            if let array = collegeArray {
                
                return array.count
                
            }
        }
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MyPageCollectionViewCellID", for: indexPath) as! MyPageCollectionViewCell
        
       var categorySchoolOrCollegeValue : NSDictionary?
        
        if UserModel.getUserType() == 1 {
            
            if schoolArray != nil {
                if let categoryTempValue = schoolArray![indexPath.row] as? NSDictionary {
                    categorySchoolOrCollegeValue = categoryTempValue
                }
            }
        }else {
           
            if collegeArray != nil {
                if let categoryTempValue = collegeArray![indexPath.row] as? NSDictionary {
                    categorySchoolOrCollegeValue = categoryTempValue
                }
            }
        }
        
        //for now only implemented for college
        //later it will be implemented for both school and college depending on condition
      //  if collegeArray != nil {
        cell.lblDescription.textColor = .black

        if let categoryValue = categorySchoolOrCollegeValue  {
            
            if let title = categoryValue.object(forKey: "title" ) as? String{
                
                cell.lblTitle.text = title
                
            }
            
            
            if let description = categoryValue.object(forKey: "caption_text")  as? String{
                
                cell.lblDescription.text = description
                
            }
            cell.imgView.sd_setIndicatorStyle(.gray)
            cell.imgView.sd_setShowActivityIndicatorView(true)
            
            var imageFinalUrl =  ""
            
            if let imageURL = categoryValue.object(forKey: "thumbnail_image")  as? String {
                
                imageFinalUrl = "\(URLS.IMAGE_BASE_URL)" + "\(imageURL)"
                
                cell.imgView.sd_setImage(with: URL(string: imageFinalUrl), completed: { (image, error, type, url) in
                    DispatchQueue.main.async {
                        if error == nil{
                            cell.imgView.image = image
                        }else{
                            cell.imgView.image = #imageLiteral(resourceName: "Placeholder")
                        }
                        cell.imgView.sd_setShowActivityIndicatorView(false)
                    }
                })
            }
            //     }
            
        }
        if(rowsWhichIsChecked == indexPath.row){
            
            cell.btnSelection.setImage(#imageLiteral(resourceName: "Selected_icon"), for: .normal)
            cell.backGroundWhiteView.backgroundColor = UIColor(red:0, green:0.47, blue:0.7, alpha:1)
            cell.lblDescription.textColor = .white
            cell.lblTitle.textColor = .white
            
        }else{
            
            cell.btnSelection.setImage(#imageLiteral(resourceName: "Unselected_icon"), for: .normal)
            cell.backGroundWhiteView.backgroundColor = .white// UIColor(red:0.89, green:0.91, blue:0.94, alpha:1)
            cell.lblDescription.textColor = .black
            cell.lblTitle.textColor = .black
            
           // let randomColor: UIColor = .random  // r 0,835 g 0,0 b 1,0 a
           // cell.lblTitle.textColor = randomColor
            
        }
        DispatchQueue.main.async {
            
            cell.backGroundWhiteView.layer.cornerRadius = (cell.shodowView.frame.size.width * 0.04)
            cell.backGroundWhiteView.layer.masksToBounds = true
            cell.imgView.layer.cornerRadius = (cell.imgView.frame.size.width / 2)
            cell.imgView.layer.masksToBounds = true
            cell.shodowView.layer.shadowOpacity = 1;
            cell.shodowView.layer.masksToBounds = false
            cell.shodowView.layer.shadowRadius = 20
            // cell.shodowView.layer.shadowColor = UIColor(red:0, green:0, blue:0, alpha:0.1).cgColor
            cell.shodowView.layer.shadowColor = UIColor(red:0.8, green:0.8, blue:0.8, alpha:0.5).cgColor
            cell.shodowView.layer.shadowOffset = CGSize(width: 0.0, height: cell.contentView.frame.height * 0.04)
            cell.updateConstraintsIfNeeded()
            cell.layoutIfNeeded()
        }
        return cell
        
        
    }
    
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {

        
        // cross checking for checked rows
        rowsWhichIsChecked = indexPath.row
        collectionView.reloadData()
    
        
        var categorySchoolOrCollegeValue : NSDictionary?
        
        if UserModel.getUserType() == 1 {
            
            if schoolArray != nil {
                if let categoryTempValue = schoolArray![indexPath.row] as? NSDictionary {
                    categorySchoolOrCollegeValue = categoryTempValue
                }
            }
        }else {
            
            if collegeArray != nil {
                if let categoryTempValue = collegeArray![indexPath.row] as? NSDictionary {
                    categorySchoolOrCollegeValue = categoryTempValue
                }
            }
        }
        
        if let categoryValue = categorySchoolOrCollegeValue {

            if let StreamID = categoryValue.object(forKey: "id") as? String  {
              
                NavigationManager.moveToStreamSelectionViewControllerIfNotExists(kAppDelegate.menuNavController!, categoryId: StreamID , title: (categoryValue.object(forKey: "title") as? String ?? "") , subtitle: (categoryValue.object(forKey: "caption_text") as? String ?? "" ), imageURL: (categoryValue.object(forKey: "banner_image") as? String ?? "" ) )
                
            }else if let StreamID = categoryValue.object(forKey: "id") as? Int{
                NavigationManager.moveToStreamSelectionViewControllerIfNotExists(kAppDelegate.menuNavController!, categoryId: String(StreamID) , title: (categoryValue.object(forKey: "title") as? String ?? "") , subtitle: (categoryValue.object(forKey: "caption_text") as? String ?? "" ), imageURL:(categoryValue.object(forKey: "banner_image") as? String ?? "" )  )
            }

        }
    }
}
