//
//  MyPageBaseViewController.swift
//  MyChhatri
//
//  Created by Arpana on 23/07/18.
//  Copyright © 2018 Anshul. All rights reserved.
//

import UIKit

class MyPageBaseViewController: UIViewController , CAPSPageMenuDelegate {

    var pageMenu : CAPSPageMenu?

   // var controllerIndex = 0 //default value
    @IBOutlet weak var viewHeading: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        kAppDelegate.menuNavController = self.navigationController
        addPageMenuForPrimaryManager()

    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        var topSafeArea: CGFloat
        
        if #available(iOS 11.0, *) {
            topSafeArea = view.safeAreaInsets.top
        } else {
            topSafeArea = topLayoutGuide.length
        }
    let  frame = CGRect(x:0.0,y:viewHeading.frame.size.height + topSafeArea  , width:self.view.frame.size.width,height:self.view.frame.size.height)
        
      pageMenu?.view.frame = frame
        
    }
    
    // MARK: - ADD PAGE MENU PRIMARY
    func addPageMenuForPrimaryManager() {
        
        var controllerArray : [UIViewController] = []
        
        let carrerOptionViewController = UIStoryboard.instantiateMyPagesViewController()
        carrerOptionViewController.title = "Career Options".localized(lang: getLanguageKey(lang:  LocalDB.shared.currentLanguage!))
        controllerArray.append(carrerOptionViewController)
        
        let savedCarrerViewController = UIStoryboard.instantiateMySavedPagesViewController()
        savedCarrerViewController.title = "Saved Stream".localized(lang: getLanguageKey(lang:  LocalDB.shared.currentLanguage!))
        controllerArray.append(savedCarrerViewController)
        
        
        let dictionary:NSDictionary = [
            CAPSPageMenuOptionScrollMenuBackgroundColor : ColorConstants.appBackGroundCOLOR()
            ,   CAPSPageMenuOptionViewBackgroundColor : UIColor.clear,CAPSPageMenuOptionMenuItemWidth : self.view.frame.size.width/2  ,CAPSPageMenuOptionMenuHeight: 50.0 , CAPSPageMenuOptionCenterMenuItems: true ,CAPSPageMenuOptionMenuItemFont: UIFont(name: "Amble-Regular", size: 16.0)!
        ]
        
        if (pageMenu != nil) {
            pageMenu?.view.removeFromSuperview()
        }
     let frame = CGRect(x:0.0,y:viewHeading.frame.maxY + (self.navigationController?.navigationBar.frame.size.height)!  , width:self.view.frame.size.width,height:self.view.frame.size.height)
        
        pageMenu = CAPSPageMenu.init(viewControllers: controllerArray, frame: frame, options: dictionary as? [AnyHashable: Any] ?? [:])
        
        pageMenu!.delegate = self
        self.view.addSubview(pageMenu!.view)
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.navigationItem.leftBarButtonItem = nil
        self.navigationController?.navigationItem.hidesBackButton = true
        
        let statusBar: UIView = UIApplication.shared.value(forKey: "statusBar") as! UIView
        statusBar.backgroundColor = ColorConstants.appBackGroundCOLOR()
        
        kAppDelegate.setNavigationAndStatusAppearance()
        if popedControllerPageIndex != 0 {
            if pageMenu?.currentPageIndex == 1{
                
                //In case we are on saved stream index, then remove stream from navigation
                //pageMenu?.move will not be called as we are on same screen
                //forcefull call viewWillAppear to reload data
               (pageMenu?.controllerArray[1] as? UIViewController)?.viewWillAppear(true)
            }
            // when we are on carrer option index , then save option will mone index to 1
            pageMenu?.move(toPage: Controller.MySavedStream.rawValue)
        }
        
    }

    func willMove(toPage controller: UIViewController!, index: Int) {
        
        if index == 1 {
            controller.viewWillAppear(true)
        }
    }
    func didMove(toPage controller: UIViewController!, index: Int) {
        print(controller)
        switch index {
        case 0: popedControllerPageIndex = 0
            
        case 1: print("Moved to \(controller.nibName)")
            
        case 2: print("Need to implement")
            
        default: print("Wrong index")
            
        }
        viewDidLayoutSubviews()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    


}
