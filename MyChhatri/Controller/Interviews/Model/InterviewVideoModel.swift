//
//  InterviewVideoModel.swift
//  MyChhatri
//
//  Created by Arpana on 16/08/18.
//  Copyright © 2018 Anshul. All rights reserved.
//
// To parse the JSON, add this file to your project and do:
//
//   let interviewVideoModel = try InterviewVideoModel(json)

import Foundation

class InterviewVideoModel: Codable {
    let status: Int?
    let success: Bool?
    let interViewerror: InterViewerror?
    let interViewdata: InterViewdata?
    
    enum CodingKeys: String, CodingKey {
        case status = "status"
        case success = "success"
        case interViewerror = "error"
        case interViewdata = "data"
    }
    
    init(status: Int?, success: Bool?, interViewerror: InterViewerror?, interViewdata: InterViewdata?) {
        self.status = status
        self.success = success
        self.interViewerror = interViewerror
        self.interViewdata = interViewdata
    }
}

class InterViewdata: Codable {
    let categoryDetails: InteviewCategoryDetails?
    let interviews: [InteviewCategoryDetails]?
    
    enum CodingKeys: String, CodingKey {
        case categoryDetails = "category_details"
        case interviews = "interviews"
    }
    
    init(categoryDetails: InteviewCategoryDetails?, interviews: [InteviewCategoryDetails]?) {
        self.categoryDetails = categoryDetails
        self.interviews = interviews
    }
}

class InteviewCategoryDetails: Codable {
    let title: String?
    let id: Int?
    let description: String?
    let captionText: String?
    let interviewvideos: [Interviewvideo]?
    
    enum CodingKeys: String, CodingKey {
        case title = "title"
        case id = "id"
        case description = "description"
        case captionText = "caption_text"
        case interviewvideos = "interviewvideos"
    }
    
    init(title: String?, id: Int?, description: String?, captionText: String?, interviewvideos: [Interviewvideo]?) {
        self.title = title
        self.id = id
        self.description = description
        self.captionText = captionText
        self.interviewvideos = interviewvideos
    }
}

class Interviewvideo: Codable {
    let id: Int?
    let title: String?
    let interviewID: String?
    let path: String?
    let name: String?
    let isOffline: String?
    let onlineURL: String?
    let thumbnail: String?
    
    enum CodingKeys: String, CodingKey {
        case id = "id"
        case title = "title"
        case interviewID = "interview_id"
        case path = "path"
        case name = "name"
        case isOffline = "is_offline"
        case onlineURL = "online_url"
        case thumbnail = "thumbnail"
    }
    
    init(id: Int?, title: String?, interviewID: String?, path: String?, name: String?, isOffline: String?, onlineURL: String?, thumbnail: String?) {
        self.id = id
        self.title = title
        self.interviewID = interviewID
        self.path = path
        self.name = name
        self.isOffline = isOffline
        self.onlineURL = onlineURL
        self.thumbnail = thumbnail
    }
}

class InterViewerror: Codable {
    let code: Int?
    let message: String?
    let errorDescription: String?
    
    enum CodingKeys: String, CodingKey {
        case code = "code"
        case message = "message"
        case errorDescription = "error_description"
    }
    
    init(code: Int?, message: String?, errorDescription: String?) {
        self.code = code
        self.message = message
        self.errorDescription = errorDescription
    }
}

// MARK: Convenience initializers and mutators

extension InterviewVideoModel {
    convenience init(data: Data) throws {
        let me = try newJSONDecoder().decode(InterviewVideoModel.self, from: data)
        self.init(status: me.status, success: me.success, interViewerror: me.interViewerror, interViewdata: me.interViewdata)
    }
    
    convenience init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }
    
    convenience init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }
    
    func with(
        status: Int?? = nil,
        success: Bool?? = nil,
        interViewerror: InterViewerror?? = nil,
        interViewdata: InterViewdata?? = nil
        ) -> InterviewVideoModel {
        return InterviewVideoModel(
            status: status ?? self.status,
            success: success ?? self.success,
            interViewerror: interViewerror ?? self.interViewerror,
            interViewdata: interViewdata ?? self.interViewdata
        )
    }
    
    func jsonData() throws -> Data {
        return try newJSONEncoder().encode(self)
    }
    
    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
        return String(data: try self.jsonData(), encoding: encoding)
    }
}

extension InterViewdata {
    convenience init(data: Data) throws {
        let me = try newJSONDecoder().decode(InterViewdata.self, from: data)
        self.init(categoryDetails: me.categoryDetails, interviews: me.interviews)
    }
    
    convenience init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }
    
    convenience init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }
    
    func with(
        categoryDetails: InteviewCategoryDetails?? = nil,
        interviews: [InteviewCategoryDetails]?? = nil
        ) -> InterViewdata {
        return InterViewdata(
            categoryDetails: categoryDetails ?? self.categoryDetails,
            interviews: interviews ?? self.interviews
        )
    }
    
    func jsonData() throws -> Data {
        return try newJSONEncoder().encode(self)
    }
    
    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
        return String(data: try self.jsonData(), encoding: encoding)
    }
}

extension InteviewCategoryDetails {
    convenience init(data: Data) throws {
        let me = try newJSONDecoder().decode(InteviewCategoryDetails.self, from: data)
        self.init(title: me.title, id: me.id, description: me.description, captionText: me.captionText, interviewvideos: me.interviewvideos)
    }
    
    convenience init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }
    
    convenience init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }
    
    func with(
        title: String?? = nil,
        id: Int?? = nil,
        description: String?? = nil,
        captionText: String?? = nil,
        interviewvideos: [Interviewvideo]?? = nil
        ) -> InteviewCategoryDetails {
        return InteviewCategoryDetails(
            title: title ?? self.title,
            id: id ?? self.id,
            description: description ?? self.description,
            captionText: captionText ?? self.captionText,
            interviewvideos: interviewvideos ?? self.interviewvideos
        )
    }
    
    func jsonData() throws -> Data {
        return try newJSONEncoder().encode(self)
    }
    
    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
        return String(data: try self.jsonData(), encoding: encoding)
    }
}

extension Interviewvideo {
    convenience init(data: Data) throws {
        let me = try newJSONDecoder().decode(Interviewvideo.self, from: data)
        self.init(id: me.id, title: me.title, interviewID: me.interviewID, path: me.path, name: me.name, isOffline: me.isOffline, onlineURL: me.onlineURL, thumbnail: me.thumbnail)
    }
    
    convenience init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }
    
    convenience init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }
    
    func with(
        id: Int?? = nil,
        title: String?? = nil,
        interviewID: String?? = nil,
        path: String?? = nil,
        name: String?? = nil,
        isOffline: String?? = nil,
        onlineURL: String?? = nil,
        thumbnail: String?? = nil
        ) -> Interviewvideo {
        return Interviewvideo(
            id: id ?? self.id,
            title: title ?? self.title,
            interviewID: interviewID ?? self.interviewID,
            path: path ?? self.path,
            name: name ?? self.name,
            isOffline: isOffline ?? self.isOffline,
            onlineURL: onlineURL ?? self.onlineURL,
            thumbnail: thumbnail ?? self.thumbnail
        )
    }
    
    func jsonData() throws -> Data {
        return try newJSONEncoder().encode(self)
    }
    
    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
        return String(data: try self.jsonData(), encoding: encoding)
    }
}

extension InterViewerror {
    convenience init(data: Data) throws {
        let me = try newJSONDecoder().decode(InterViewerror.self, from: data)
        self.init(code: me.code, message: me.message, errorDescription: me.errorDescription)
    }
    
    convenience init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }
    
    convenience init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }
    
    func with(
        code: Int?? = nil,
        message: String?? = nil,
        errorDescription: String?? = nil
        ) -> InterViewerror {
        return InterViewerror(
            code: code ?? self.code,
            message: message ?? self.message,
            errorDescription: errorDescription ?? self.errorDescription
        )
    }
    
    func jsonData() throws -> Data {
        return try newJSONEncoder().encode(self)
    }
    
    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
        return String(data: try self.jsonData(), encoding: encoding)
    }
}



