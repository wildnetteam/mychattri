//
//  InterviewsViewController.swift
//  MyChhatri
//
//  Created by Arpana on 31/07/18.
//  Copyright © 2018 Anshul. All rights reserved.
//

import UIKit

class TrendsViewController: UIViewController {

    @IBOutlet weak var shadowView: UIView!
    @IBOutlet weak var tableView:UITableView!
    var rootCatID : String?
    
    
    // MARK: - MAINTAIN PAGINATION AND WEBINAR LIST
    var pageNumber = 0
    var listlimit = 10 // Define to get maximum data one at a time
    var totalCount : Int?
    
    var trendsArray : Array<NSDictionary>?
    var offset : Int?
    
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action:
            #selector(TrendsViewController.getTrendsList),
                                 for: UIControlEvents.valueChanged)
        refreshControl.tintColor = .black
        
        return refreshControl
    }()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Add shdow Below page menu
        shadowView.layer.masksToBounds = false
        shadowView.layer.shadowOpacity = 0.4 ;
        shadowView.layer.shadowColor = UIColor.black.cgColor
        shadowView.layer.shadowOffset = CGSize(width: 0.0, height: 4.0)
        tableView.addSubview(refreshControl)
        offset = 0
        

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if offset == 0 {
            getTrendsList()

        }
    }
    
    
    // MARK:- GET WEB DATA FROM API
    
    
    @objc func getTrendsList()  {
        
        if self.refreshControl.isRefreshing{
            offset = 0
        }
        let infoDict = NSDictionary(objects: [rootCatID! ,10 ,offset ?? 0], forKeys: ["category_id" as NSCopying,"limit" as NSCopying ,"offset" as NSCopying])
        
        if let userInfoDict = UserModel.getLoginInfo() {
            
            guard ((userInfoDict.object(forKey: "ID")) != nil) else{
                
                self.showAlert(title: "Sorry", message: "User details could not be found , Please logout and login again", withTag: 101, button: ["Ok"], isActionRequired: false)
                return
            }
            
            guard (userInfoDict.object(forKey: "token") as? String) != nil else{
                
                self.showAlert(title: "Sorry", message: "User accessToken could not be found , Please logout and login again", withTag: 101, button: ["Ok"], isActionRequired: false)
                return
            }
        }
        if tableView != nil {
            
            ProgressHUD.showProgressHUDWithLabel(label: "Loading...")
        }
        
        APICall.callTrendsListAPI([:], header: [ : ], information:infoDict ) { (sucess, result, error) in
            
            DispatchQueue.main.async {
                
                weak var weakSelf = self
                
                ProgressHUD.hideProgressHUD()
                
                if (error != nil) {
                    weakSelf?.showAlert(title: "Error", message: (error!.localizedDescription), withTag: 101, button: ["Ok"], isActionRequired: false)
                    return
                }
                
                if sucess {
                    
                    do {
                        let jsonData = try JSONSerialization.jsonObject(with: result!, options: .mutableContainers)
                        
                        if let responseDict = (jsonData as? NSDictionary){
                            
                            if let statusCode = responseDict.object(forKey: "status"), (statusCode is Int) {
                                
                                if (statusCode as! Int) == 200 {
                                    
                                    //sucessfully retrieved data
                                    if let dataDict = responseDict.object(forKey: "data") as? NSDictionary{
                                        
                                        if let toptalR = dataDict.value(forKey: "totalCount") as? Int{
                                            weakSelf?.totalCount = toptalR
                                        }else if let toptalR = dataDict.value(forKey: "totalCount") as? String{
                                            weakSelf?.totalCount = Int(toptalR)
                                        }
                                        if let tempArray = dataDict.object(forKey: "trends") as? Array<NSDictionary> {
                                            
                                            if tempArray.count > 0 {
                                                //store data
                                                if  weakSelf?.offset == 0 {
                                                    self.trendsArray =  Array()
                                                    
                                                    weakSelf?.trendsArray = tempArray
                                                }else{
                                                    //Just add to previous array
                                                    weakSelf?.trendsArray?.append(contentsOf: tempArray)
                                                }
                                            }else{
                                                //No saved stream found message
                                                weakSelf?.showAlert(title: APP_NAME, message: "You do not have any trends yet.", withTag: 101, button: ["Ok"], isActionRequired: false)
                                            }
                                        }else {
                                            //
                                            weakSelf?.showAlert(title: APP_NAME, message: "trends key not found", withTag: 101, button: ["Ok"], isActionRequired: false)
                                        }
                                    }else{
                                        //No data found error
                                        weakSelf?.showAlert(title: APP_NAME, message:  AlertMessages.NoDataKeyFoundInResponse, withTag: 101, button: ["Ok"], isActionRequired: false)
                                    }
                                    
                                } else {
                                    var errStr = ""
                                    if let errormessage = (jsonData as? NSDictionary)?.object(forKey:"error") {
                                        if (errormessage is NSDictionary) {
                                            if let errorDescription = (errormessage as? NSDictionary)?.object(forKey: "error_description"),!(errorDescription is NSNull){
                                                if (errorDescription is String){
                                                    errStr = errorDescription as! String
                                                    
                                                }
                                            }else {
                                                if let errorMessage = (errormessage as? NSDictionary)?.object(forKey: "message"),!(errorMessage is NSNull){
                                                    if (errorMessage is String){
                                                        errStr = errorMessage as! String
                                                        
                                                    }
                                                }else{
                                                    errStr = AlertMessages.UnidetifiedErrorMessage
                                                }
                                                
                                            }
                                            weakSelf?.showAlert(title: APP_NAME, message:errStr, withTag: 101, button: ["Ok"], isActionRequired: false)
                                        }
                                        
                                    }else{
                                        weakSelf?.showAlert(title: APP_NAME, message:  AlertMessages.UnidetifiedErrorMessage, withTag: 101, button: ["Ok"], isActionRequired: false)
                                    }
                                    
                                }
                            }
                        }else{
                            
                            weakSelf?.showAlert(title: "sorry", message: AlertMessages.NOTADICTIONARYMESSAGE , withTag: 101, button:  ["Ok"], isActionRequired: false)
                            return
                        }
                        
                    }catch{
                        
                        weakSelf?.showAlert(title: "sorry", message: AlertMessages.MODELERRORMESSAGE , withTag: 101, button:  ["Ok"], isActionRequired: false)
                        return
                    }
                    
                } else{
                    
                    //Success false
                    weakSelf?.showAlert(title: APP_NAME, message: AlertMessages.UnidetifiedErrorMessage, withTag: 101, button: ["Ok"], isActionRequired: false)
                    
                }
                
                if self.refreshControl.isRefreshing{
                    self.refreshControl.endRefreshing()
                }
                
                if weakSelf?.tableView != nil {
                    weakSelf?.tableView.reloadData()
                }
            }
            
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}

extension TrendsViewController : UITableViewDelegate, UITableViewDataSource , UIViewControllerTransitioningDelegate{
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section:
        Int) -> Int
    {
        if let mArray = trendsArray {
            return mArray.count
        }else {
            return 0
        }
        
    }
    
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        // Allocates a Table View Cell
        let cell =
            tableView.dequeueReusableCell(withIdentifier: "ArticleTableViewCellID",
                                          for: indexPath) as! ArticleTableViewCell
        
        cell.imgView.layer.masksToBounds = true
        cell.imgView.layer.cornerRadius = cell.imgView.frame.height * 0.063
        
        
        if trendsArray != nil {
            
            if let tempTrendsArray = trendsArray {
                
                if let dict = tempTrendsArray[indexPath.row] as?  NSDictionary {
                    
                    if let wTitle = dict.value(forKey: "title") as? String {
                        
                        
                        cell.lblTitle.text =  wTitle
                    }
                    if let wDes = dict.value(forKey: "description") as? String {
                        cell.lblSubTitle.text = wDes.withoutHtml
                    }else{
                         cell.lblSubTitle.text = "No descsription available"
                    }
                    
                    
                    var imageFinalUrl =  ""
                    
                    if let imageURL = dict.value(forKey: "banner_image") {
                        
                        imageFinalUrl = "\(URLS.IMAGE_BASE_URL)" + "\(imageURL)"
                        
                        cell.imgView.sd_setIndicatorStyle(.gray)
                        cell.imgView.sd_setShowActivityIndicatorView(true)
                        
                        cell.imgView.sd_setImage(with: URL(string: imageFinalUrl), completed: { (image, error, type, url) in
                            DispatchQueue.main.async {
                                if error == nil{
                                    cell.imgView.image = image
                                }else{
                                    cell.imgView.image = #imageLiteral(resourceName: "Placeholder")
                                }
                                cell.imgView.sd_setShowActivityIndicatorView(false)
                            }
                        })
                    }
                }
                //Reload the next amount of data
                if(indexPath.row ==  (trendsArray?.count)! - 1 && totalCount! > (trendsArray?.count)! ){
                    //we are at last index of currently displayed cell
                    offset = (trendsArray?.count)!
                    //reload more data
                    getTrendsList()
                }
            }
        }
        cell.selectionStyle = .none

        return cell
    }
    
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        
        if trendsArray != nil {
            
            if let dict = trendsArray?[indexPath.row] {
                NavigationManager.moveToDetailedViewControllerIfNotExists(kAppDelegate.menuNavController!, detailsArticle:nil, informationDict: dict )
            }
            
        }
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return  100
    }
    
}


