//
//  monthCollectionViewCell.swift
//  MyChhatri
//
//  Created by Arpana on 06/08/18.
//  Copyright © 2018 Anshul. All rights reserved.
//

import UIKit

class monthCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var lblTitle : MyLabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
       lblTitle.font =  UIFont(name: "Amble-Bold", size: isIphone5 ? 10.0: 15.0)!
    }

}
