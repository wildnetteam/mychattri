//
//  RatingViewController.swift
//  MyChhatri
//
//  Created by Arpana on 10/10/18.
//  Copyright © 2018 Arpana. All rights reserved.
//

import UIKit

class RatingViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var submitBtn: UIButton!
    @IBOutlet weak var addReviewTextView: UITextView!
    var questionArray : Array<NSDictionary>?
    var ratingListArray : NSMutableArray?
    var courseID : String?
    var isRated: Bool = false
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.estimatedRowHeight = 250.00
        tableView.rowHeight = UITableViewAutomaticDimension
        ratingListArray = NSMutableArray()
        getQuestionList()

    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        submitBtn.addGrediant()

        self.navigationController?.navigationBar.isHidden = false
        kAppDelegate.menuNavController = self.navigationController
        
        self.navigationController?.navigationBar.topItem?.title = "Rate Us"
        
        kAppDelegate.setNavigationAndStatusAppearance()
        let statusBar: UIView = UIApplication.shared.value(forKey: "statusBar") as! UIView
        statusBar.backgroundColor = ColorConstants.appBackGroundCOLOR()
        UIApplication.shared.statusBarStyle = .lightContent
        setNeedsStatusBarAppearanceUpdate()
        
        
    }

    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func alertButtonAction(alertTag: Int, btnTitle: String) {
        
      self.navigationController?.popViewController(animated: true)
    }

    @IBAction func submitReview (_ sender : Any){
    
        var params : [String:Any] = [:]
        
        guard let orderNo = courseID else{
            
            self.showAlert(title: "Error", message: "Order no not found.", withTag: 101, button: ["Ok"], isActionRequired: false)
            return
        }
        params["order_no"] =  orderNo
        params["rating"] = ratingListArray
        params["review"] = addReviewTextView.text
        print("params : \(params)")
        
        ProgressHUD.showProgressHUDWithLabel(label: "Loading...")
        
        APICall.submitReviewAPI(params, header: [ : ]) { (sucess, result, error) in
            
            DispatchQueue.main.async {
                
                ProgressHUD.hideProgressHUD()
                
                weak var weakSelf = self
                
                if (error != nil) {
                    weakSelf?.showAlert(title: "Error", message: (error!.localizedDescription), withTag: 101, button: ["Ok"], isActionRequired: false)
                    return
                }
                if sucess {
                    
                    do {
                        let jsonData = try JSONSerialization.jsonObject(with: result!, options: .mutableContainers)
                        
                        if (jsonData is NSDictionary){
                            
                            if let statusCode = (jsonData as! NSDictionary).object(forKey: "status"), (statusCode is Int) {
                                
                                if let success = (jsonData as! NSDictionary).object(forKey: "success") {
                                    
                                    if (success as! Bool) == true {
                                        
                                        if (statusCode as! Int) == 200 {
                                            if let data = (jsonData as! NSDictionary).object(forKey: "data"), (data is NSDictionary) {
                                                if (data as! NSDictionary).count > 0 {
                                                    
                                                    
                                                    
                                                    var msg = "Thank you."
                                                    if let message = (data as! NSDictionary).object(forKey: "message") as? String {
                                                        
                                                        msg = message
                                                    }
                                                    weakSelf?.showAlert(title: APP_NAME, message: msg, withTag: 110, button: ["Ok"], isActionRequired: true)
                                                    
                                                    self.navigationController?.popViewController(animated: true)
                                                    
                                                }
                                                
                                            }
                                            
                                            
                                        } else {
                                            
                                            //some other status code
                                            //check error message in the response
                                            var errStr = ""
                                            if let errormessage = (jsonData as? NSDictionary)?.object(forKey:"error") {
                                                if (errormessage is NSDictionary){
                                                    if let errorDescription = (errormessage as? NSDictionary)?.object(forKey: "error_description"),!(errorDescription is NSNull){
                                                        if (errorDescription is String){
                                                            errStr = errorDescription as! String
                                                            
                                                        }
                                                    }else {
                                                        if let errorMessage = (errormessage as? NSDictionary)?.object(forKey: "message"),!(errorMessage is NSNull){
                                                            if (errorMessage is String){
                                                                errStr = errorMessage as! String
                                                                
                                                            }
                                                        }else{
                                                            errStr = AlertMessages.UnidetifiedErrorMessage
                                                        }
                                                        
                                                    }
                                                    weakSelf?.showAlert(title: APP_NAME, message:errStr, withTag: 101, button: ["Ok"], isActionRequired: false)
                                                }
                                            }
                                        }
                                    }else{
                                        
                                        //some other status code
                                        //check error message in the response
                                        var errStr = ""
                                        if let errormessage = (jsonData as? NSDictionary)?.object(forKey:"error") {
                                            if (errormessage is NSDictionary){
                                                if let errorDescription = (errormessage as? NSDictionary)?.object(forKey: "error_description"),!(errorDescription is NSNull){
                                                    if (errorDescription is String){
                                                        errStr = errorDescription as! String
                                                        
                                                    }
                                                }else {
                                                    if let errorMessage = (errormessage as? NSDictionary)?.object(forKey: "message"),!(errorMessage is NSNull){
                                                        if (errorMessage is String){
                                                            errStr = errorMessage as! String
                                                            
                                                        }
                                                    }else{
                                                        errStr = AlertMessages.UnidetifiedErrorMessage
                                                    }
                                                    
                                                }
                                                weakSelf?.showAlert(title: APP_NAME, message:errStr, withTag: 101, button: ["Ok"], isActionRequired: false)
                                            }
                                        }
                                    }
                                    
                                }else{
                                    
                                    weakSelf?.showAlert(title: "sorry", message: AlertMessages.NOTADICTIONARYMESSAGE , withTag: 101, button:  ["Ok"], isActionRequired: false)
                                }
                                
                            }
                        }else{
                            // not a dictionary
                            weakSelf?.showAlert(title: "sorry", message: AlertMessages.NOTADICTIONARYMESSAGE , withTag: 101, button:  ["Ok"], isActionRequired: false)
                        }
                    }catch{
                        weakSelf?.showAlert(title: "sorry", message: AlertMessages.MODELERRORMESSAGE , withTag: 101, button:  ["Ok"], isActionRequired: false)
                        return
                    }
                }
                
            }
        }
    }
    
    @objc func getQuestionList()  {

        guard (courseID) != nil else{

            self.showAlert(title: "Sorry", message: "ID is not available please try again", withTag: 101, button: ["Ok"], isActionRequired: false)
            return
        }
        
        
//        let infoDict = NSDictionary(objects: [rootCatID! ,10 , offset ?? 0 , cityID! ,"2"], forKeys: ["category_id" as NSCopying,"limit" as NSCopying ,"offset" as NSCopying, "city_id" as NSCopying, "type" as NSCopying])
        
        if tableView != nil {
            
            ProgressHUD.showProgressHUDWithLabel(label: "Loading...")
        }
        
        APICall.callRatingQuestionsAPI([:], header: [ : ], typeID: courseID! ) { (sucess, result, error) in
            
            DispatchQueue.main.async {
                
                weak var weakSelf = self
                
                ProgressHUD.hideProgressHUD()
                
                if (error != nil) {
                    weakSelf?.showAlert(title: "Error", message: (error!.localizedDescription), withTag: 101, button: ["Ok"], isActionRequired: false)
                    return
                }
                
                if sucess {
                    
                    do {
                        let jsonData = try JSONSerialization.jsonObject(with: result!, options: .mutableContainers)
                        
                        if let responseDict = (jsonData as? NSDictionary){
                            
                            if let statusCode = responseDict.object(forKey: "status"), (statusCode is Int) {
                                
                                if (statusCode as! Int) == 200 {
                                    
                                    //sucessfully retrieved data
                                    if let tempArray = responseDict.object(forKey: "data") as? Array<NSDictionary> {
                                     
                                            if tempArray.count > 0 {
                                              
                                                weakSelf?.questionArray = Array()
                                                weakSelf?.questionArray =  tempArray
                                                
                                            }else{
                                                //No saved stream found message
                                                weakSelf?.showAlert(title: APP_NAME, message: "No Data found.", withTag: 101, button: ["Ok"], isActionRequired: false)
                                            }
                                        }else {
                                            //
                                            weakSelf?.showAlert(title: APP_NAME, message: "vendors key not found", withTag: 101, button: ["Ok"], isActionRequired: false)
                                        }
                                    }else{
                                        //No data found error
                                        weakSelf?.showAlert(title: APP_NAME, message:  AlertMessages.NoDataKeyFoundInResponse, withTag: 101, button: ["Ok"], isActionRequired: false)
                                    }
                                    
                                } else {
                                    var errStr = ""
                                    if let errormessage = (jsonData as? NSDictionary)?.object(forKey:"error") {
                                        if (errormessage is NSDictionary){
                                            if let errorDescription = (errormessage as? NSDictionary)?.object(forKey: "error_description"),!(errorDescription is NSNull){
                                                if (errorDescription is String){
                                                    errStr = errorDescription as! String
                                                    
                                                }
                                            }else {
                                                if let errorMessage = (errormessage as? NSDictionary)?.object(forKey: "message"),!(errorMessage is NSNull){
                                                    if (errorMessage is String){
                                                        errStr = errorMessage as! String
                                                        
                                                    }
                                                }else{
                                                    errStr = AlertMessages.UnidetifiedErrorMessage
                                                }
                                                
                                            }
                                            weakSelf?.showAlert(title: APP_NAME, message:errStr, withTag: 101, button: ["Ok"], isActionRequired: false)
                                        }
                                        
                                    }else{
                                        weakSelf?.showAlert(title: APP_NAME, message:  AlertMessages.UnidetifiedErrorMessage, withTag: 101, button: ["Ok"], isActionRequired: false)
                                    }
                                    
                                }
                            
                        }else{
                            
                            weakSelf?.showAlert(title: "sorry", message: AlertMessages.NOTADICTIONARYMESSAGE , withTag: 101, button:  ["Ok"], isActionRequired: false)
                            return
                        }
                        
                    }catch{
                        
                        weakSelf?.showAlert(title: "sorry", message: AlertMessages.MODELERRORMESSAGE , withTag: 101, button:  ["Ok"], isActionRequired: false)
                        return
                    }
                    
                } else{
                    
                    //Success false
                    weakSelf?.showAlert(title: APP_NAME, message: AlertMessages.UnidetifiedErrorMessage, withTag: 101, button: ["Ok"], isActionRequired: false)
                    
                }
                
                if weakSelf?.tableView != nil {
                    weakSelf?.tableView.reloadData()
                }
            }
            
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func backButtonClicked(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
        
    }
}

extension RatingViewController : UITableViewDelegate, UITableViewDataSource , UIViewControllerTransitioningDelegate , HCStarRatingViewDelegat {
    
    func valueDidChanged(_ value: CGFloat, index: Int) {
        
        print("value selected \(value) at index: \(index)")
        isRated = true
        //before adding dict to array check if same questionId is already added to dict
        
        //create a dictionary like  {
        //  "question_id":4,
        //        "value":3.1}
        
        //append it to ratingListArray
        
        let reviewDict = NSDictionary(objects: [value ,index], forKeys: ["value" as NSCopying, "question_id" as NSCopying ])
        
        
        let questionIdPredicate = NSPredicate(format: "question_id = \(index)")
        
        let resultArray = (ratingListArray)?.filtered(using: questionIdPredicate)
        
        if (resultArray?.count)! > 0{
            
            let dictToRemove = resultArray?.first as? NSDictionary
            //This questionId is already added to the array, remove first
           ratingListArray?.remove(dictToRemove)
            
        }
        ratingListArray?.add(reviewDict)
        print(ratingListArray!)

    }
    
    
    
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section:
        Int) -> Int
    {
        if let array = questionArray {
            
            return array.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 250
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        // Allocates a Table View Cell
        let cell =
            tableView.dequeueReusableCell(withIdentifier: "RatingTableViewCellID",
                                          for: indexPath) as! RatingTableViewCell
        
        cell.selectionStyle = UITableViewCellSelectionStyle.none
       cell.rateView.delegate = self
        cell.rateView.index = indexPath.row
        if questionArray != nil {
            
            if let tempcompanyArray = questionArray {
          
                if let dict = tempcompanyArray[indexPath.row] as?  NSDictionary {
                    
                  print(dict)
                    
                    if let questionText = dict.object(forKey: "title") as? String{
                        
                        cell.lblQuestion.text = questionText
                    }
                }
               
            }
        }
        cell.selectionStyle = .none
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
      
    }
    
}
