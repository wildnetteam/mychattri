//
//  SubjectSelectionViewController.swift
//  MyChhatri
//
//  Created by Arpana on 26/07/18.
//  Copyright © 2018 Anshul. All rights reserved.
//

import UIKit

class SubjectSelectionViewController: UIViewController {
    
    @IBOutlet weak var submitButon: UIButton!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblSubTitle: UILabel!
    
    @IBOutlet weak var lblInstituteType: UILabel!
    var rootID = ""
    var pathIDs : NSArray?
    var subjectObject : NSDictionary?
    
    var rowsWhichIsChecked : Int = -1

    var shoppingList : Array<NSDictionary>?
    
    var controllerHeading : String?
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        submitButon.addGrediant()
        
        if let subject = subjectObject {
            
            print(subject)
            
            if UserModel.getUserType() == 1 {
                
            lblInstituteType.text = "School"
            }else if UserModel.getUserType() == 2{
                lblInstituteType.text = "College"

            }else{
                print("instituteType:\(String(describing:  UserModel.getUserType()))")
            }
            
            
            //  self.lblTitle.text = dataDict.title
            //  self.lblSubTitle.text = dataDict.captionText
            
        }
        collectionView.reloadData()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.navigationBar.isHidden = true
        //change the status bar color to clear so that imageview will be hidden 20Px by status bar
        let statusBar: UIView = UIApplication.shared.value(forKey: "statusBar") as! UIView
        statusBar.backgroundColor = .clear
        UIApplication.shared.statusBarStyle = .lightContent
        getsubCategory(parentId: pathIDs as! Array<String>)
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        let statusBar: UIView = UIApplication.shared.value(forKey: "statusBar") as! UIView
        statusBar.backgroundColor = ColorConstants.appBackGroundCOLOR()
        self.navigationController?.navigationBar.isHidden = false
        pathIDs = nil
        
    }
    
    @IBAction func backButtonClicked(_ sender: Any) {
        
            self.navigationController?.popToRootViewController(animated: true)
  
    }
    
    @IBAction func submitButtonPressed(_ sender: Any) {
        
      saveSubject()
    }
    
    override func alertButtonAction(alertTag: Int, btnTitle: String) {
        
        if(alertTag == 400){
            
//            NavigationManager.moveToSavedSteamDetailsBaseViewControllerIfNotExists(kAppDelegate.menuNavController!, rootCategoryID: rootID ,isFromCarrer: true)
        }
        
    }
    @IBAction func backButtonPressed(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
  
    func saveSubject(){
        
        /* user_id:1
         topic_id:10
         stream_id:1
         root_category_id:1*/

        if rowsWhichIsChecked == -1{
           
             self.showAlert(title: "", message: "Please select a topic first.", withTag: 101, button: ["Ok"], isActionRequired: false)
            return
        }
        var params : [String:Any] = [:]
      
        if let userInfoDict = UserModel.getLoginInfo() {
            
            if let  userId = (userInfoDict.object(forKey: "ID") as? String){
                
                params["user_id"]  = userId
                
            }else if let userIded = (userInfoDict.object(forKey: "ID") as? Int){
                
                params["user_id"]  = userIded
            }else{
                
                self.showAlert(title: "Sorry", message: "User details could not be found , Please logout and login again", withTag: 101, button: ["Ok"], isActionRequired: false)
                return
            }
        }
        
        
        if let catArray = subjectObject?.object(forKey: "categories") as? NSArray{
            
            if let categoryValue = catArray[rowsWhichIsChecked] as? NSDictionary{
                
                
                if let id = categoryValue.object(forKey: "id"){
                    
                    params["topic_id"] = id
                    
                }
            }else{
                
                params["topic_id"] = ""
                
            }
            
        }
        if pathIDs?.count  != 0 {
            if let streamID =  pathIDs?[0] as? String{
                params["stream_id"] = streamID
            }
        }
        params["root_category_id"] = rootID
        
        
        print("params : \(params)")
        
        ProgressHUD.showProgressHUDWithLabel(label: "Loading...")
        
//        APIManager.callPost("\(URLS.BASE_URL)\(URLS.SAVE_TOPIC)", param: params, header: ["":""]) { (isSuccess, result, error) in
        APICall.callSaveSubjectPostAPI(params, header: [ : ]) { (sucess, result, error) in
            
            DispatchQueue.main.async {
                
                ProgressHUD.hideProgressHUD()
                
                weak var weakSelf = self
                
                if (error != nil) {
                    weakSelf?.showAlert(title: "Error", message: (error!.localizedDescription), withTag: 101, button: ["Ok"], isActionRequired: false)
                    return
                }
                if sucess {
                    
                    do {
                        let jsonData = try JSONSerialization.jsonObject(with: result!, options: .mutableContainers)
                        
                        if (jsonData is NSDictionary){
                            
                            if let statusCode = (jsonData as! NSDictionary).object(forKey: "status"), (statusCode is Int) {
                                
                                if let success = (jsonData as! NSDictionary).object(forKey: "success") {
                                    
                                    if (success as! Bool) == true {
                                        
                                        if (statusCode as! Int) == 200 {
                                            if let data = (jsonData as! NSDictionary).object(forKey: "data"), (data is NSDictionary) {
                                                if (data as! NSDictionary).count > 0 {
                                                    
                                                    if let userId = (data as! NSDictionary).object(forKey: "user_id") as? String {
                                                        print("\(userId)")
                                                    }
                                                    
                                                    if let message = (data as! NSDictionary).object(forKey: "message") as? String {
                                                        
                                                        weakSelf?.showAlert(title: APP_NAME, message: message, withTag: 400, button: ["Ok"], isActionRequired: true)
                                                        
                                                    }
                                                    
                                                }
                                            }
                                            
                                        } else {
                                            
                                            //some other status code
                                            //check error message in the response
                                            var errStr = ""
                                            if let errormessage = (jsonData as? NSDictionary)?.object(forKey:"error") {
                                                if (errormessage is NSDictionary){
                                                    if let errorDescription = (errormessage as? NSDictionary)?.object(forKey: "error_description"),!(errorDescription is NSNull){
                                                        if (errorDescription is String){
                                                            errStr = errorDescription as! String
                                                            
                                                        }
                                                    }else {
                                                        if let errorMessage = (errormessage as? NSDictionary)?.object(forKey: "message"),!(errorMessage is NSNull){
                                                            if (errorMessage is String){
                                                                errStr = errorMessage as! String
                                                                
                                                            }
                                                        }else{
                                                            errStr = AlertMessages.UnidetifiedErrorMessage
                                                        }
                                                        
                                                    }
                                                    weakSelf?.showAlert(title: APP_NAME, message:errStr, withTag: 101, button: ["Ok"], isActionRequired: false)
                                                }
                                            }
                                        }
                                    }else{
                                        
                                        //some other status code
                                        //check error message in the response
                                        var errStr = ""
                                        if let errormessage = (jsonData as? NSDictionary)?.object(forKey:"error") {
                                            if (errormessage is NSDictionary){
                                                if let errorDescription = (errormessage as? NSDictionary)?.object(forKey: "error_description"),!(errorDescription is NSNull){
                                                    if (errorDescription is String){
                                                        errStr = errorDescription as! String
                                                        
                                                    }
                                                }else {
                                                    if let errorMessage = (errormessage as? NSDictionary)?.object(forKey: "message"),!(errorMessage is NSNull){
                                                        if (errorMessage is String){
                                                            errStr = errorMessage as! String
                                                            
                                                        }
                                                    }else{
                                                        errStr = AlertMessages.UnidetifiedErrorMessage
                                                    }
                                                    
                                                }
                                                weakSelf?.showAlert(title: APP_NAME, message:errStr, withTag: 101, button: ["Ok"], isActionRequired: false)
                                            }
                                        }
                                    }
                                    
                                }else{
                                    
                                    weakSelf?.showAlert(title: "sorry", message: AlertMessages.NOTADICTIONARYMESSAGE , withTag: 101, button:  ["Ok"], isActionRequired: false)
                                }
                                
                            }
                        }else{
                            // not a dictionary
                            weakSelf?.showAlert(title: "sorry", message: AlertMessages.NOTADICTIONARYMESSAGE , withTag: 101, button:  ["Ok"], isActionRequired: false)
                        }
                    }catch{
                        weakSelf?.showAlert(title: "sorry", message: AlertMessages.MODELERRORMESSAGE , withTag: 101, button:  ["Ok"], isActionRequired: false)
                        return
                    }
                }
                
            }
        }
    }
    
    
    func getsubCategory(parentId: Array< String > )  {
        
        //"0","6","18"
        let params : [String:Any] = [:]
        
       var streamId = ""
        if pathIDs?.count  != 0 {
            if let streamID =  pathIDs?[0] as? String{
                streamId = streamID
            }
        }
        
        APICall.callSubCategoryAPI( params, header: [ : ], streamID: rootID, catIdArray: [streamId]) { (sucess, result, error) in
            
            weak var weakSelf = self
            
            DispatchQueue.main.async{
                
                if (error != nil) {
                    
                    weakSelf?.showAlert(title: "Error", message: (error!.localizedDescription), withTag: 101, button: ["Ok"], isActionRequired: false)
                    
                    return
                }
                if sucess {
                    
                    do {
                        
                        let jsonData = try JSONSerialization.jsonObject(with: result!, options: .mutableContainers)
                        print(jsonData)
                        
                        if (jsonData is NSDictionary){
                            
                            if let statusCode = (jsonData as! NSDictionary).object(forKey: "status"), (statusCode is Int) {
                                
                                if (statusCode as! Int) == 200 {
                                    
                                    //sucessfully retrieved data
                                    if let dataDict = (jsonData as! NSDictionary).object(forKey: "data") as? NSDictionary{
                                        
                                        weakSelf?.subjectObject = dataDict
                                        
                                        
                                        if let catTempArray = dataDict.object(forKey: "categories") as? NSArray{
                                          print(catTempArray)
                                            
                                            
                                        }
                                        weakSelf?.collectionView.reloadData()
                                    }else{
                                        weakSelf?.showAlert(title: "sorry", message: AlertMessages.NoDataKeyFoundInResponse , withTag: 101, button:  ["Ok"], isActionRequired: false)
                                    }
                                    
                                }
                                
                            }else{
                                
                                
                                var errStr = ""
                                if let errormessage = (jsonData as? NSDictionary)?.object(forKey:"error") {
                                    if (errormessage is NSDictionary){
                                        if let errorDescription = (errormessage as? NSDictionary)?.object(forKey: "error_description"),!(errorDescription is NSNull){
                                            if (errorDescription is String){
                                                errStr = errorDescription as! String
                                                
                                            }
                                        }else {
                                            if let errorMessage = (errormessage as? NSDictionary)?.object(forKey: "message"),!(errorMessage is NSNull){
                                                if (errorMessage is String){
                                                    errStr = errorMessage as! String
                                                    
                                                }
                                            }else{
                                                errStr = "Something went wrong"
                                            }
                                            
                                        }
                                        weakSelf?.showAlert(title: APP_NAME, message:errStr, withTag: 101, button: ["Ok"], isActionRequired: false)
                                    }
                                    
                                }else{
                                    weakSelf?.showAlert(title: APP_NAME, message: AlertMessages.UnidetifiedErrorMessage, withTag: 101, button: ["Ok"], isActionRequired: false)
                                }
                            }
                            
                        }else{
                            //not a dict
                        }
                    }
                    catch  {
                        
                        weakSelf?.showAlert(title: "sorry", message: AlertMessages.MODELERRORMESSAGE , withTag: 101, button:  ["Ok"], isActionRequired: false)
                        return
                    }
                }
                else{
                    //Alert not sucessfull
                    weakSelf?.showAlert(title: APP_NAME, message: "could not get response from server", withTag: 101, button: ["Ok"], isActionRequired: false)
                }
                
            }
        }
    }
}


extension SubjectSelectionViewController : UICollectionViewDelegateFlowLayout {
    //1
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let itemSide: CGFloat = (collectionView.bounds.width) / 2
        return CGSize(width: itemSide , height: itemSide - ( itemSide * 0.06 ))
        
    }

    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        
        return UIEdgeInsetsMake(0, 0, 0, 0)
        
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat{
        
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat{
        return 0
    }
}

    extension SubjectSelectionViewController: UICollectionViewDelegate , UICollectionViewDataSource {
        
        func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
            
            if let catArray = subjectObject?.object(forKey: "categories") as? NSArray {
                
                return catArray.count
                
            }
            return 0
        }
        
        func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SavedStreamCollectionViewCellID", for: indexPath) as! SavedStreamCollectionViewCell
            
            if let catArray = subjectObject?.object(forKey: "categories") as? NSArray {
                
                if let val = catArray[indexPath.row] as? NSDictionary{
                    
                    
                    
                    if let titleText =  val.object(forKey: "title") as? String {
                        
                        cell.lblTitle.text = titleText
                    }
                    if let captionText =  val.object(forKey: "caption_text") as? String {
                        
                        cell.lblDescription.text = captionText
                    }
                    
                    if let imgUrl =  val.object(forKey:"banner_image") as? String {
                        
                        let imageFinalUrl = "\(URLS.IMAGE_BASE_URL)" + "\(imgUrl)"
                        
                        cell.imgView.sd_setIndicatorStyle(.gray)
                        cell.imgView.sd_setShowActivityIndicatorView(true)
                        cell.imgView.sd_setImage(with: URL(string: imageFinalUrl), completed: { (image, error, type, url) in
                            if error == nil{
                                cell.imgView.image = image
                            }else{
                                cell.imgView.image = #imageLiteral(resourceName: "Placeholder")
                            }
                            cell.imgView.sd_setShowActivityIndicatorView(false)
                        })
                        
                    }else{
                        //No image url
                        // cell.imgView.image = nil
                    }
                    
                    if let idText =  val.object(forKey: "id") as? String {
                        cell.btnSelection.setTitle(idText, for: .normal)
                        
                    } else if let idAsInt =  val.object(forKey: "id") as? Int {
                        cell.btnSelection.setTitle(String(idAsInt), for: .normal)
                        
                    }
                    cell.btnSelection.setTitleColor(.clear, for: .normal)
                    
                    
                }else{
                    
                }
            }
            
            DispatchQueue.main.async {
                
                if(self.rowsWhichIsChecked == indexPath.row){
                    
                    // cell.btnSelection.setImage(#imageLiteral(resourceName: "Selected_icon"), for: .normal)
                    cell.backGroundWhiteView.backgroundColor = UIColor(red:0, green:0.47, blue:0.7, alpha:1)
                    cell.lblDescription.textColor = .white
                    cell.lblTitle.textColor = .white
                    
                }else{
                    
                    //   cell.btnSelection.setImage(#imageLiteral(resourceName: "Unselected_icon"), for: .normal)
                    cell.backGroundWhiteView.backgroundColor = .white// UIColor(red:0.89, green:0.91, blue:0.94, alpha:1)
                    cell.lblDescription.textColor = .black
                    // cell.lblTitle.textColor = .black
                    
                    let randomColor: UIColor = .random  // r 0,835 g 0,0 b 1,0 a
                    cell.lblTitle.textColor = randomColor
                    
                }
                
                cell.backGroundWhiteView.layer.cornerRadius = (cell.shodowView.frame.size.width * 0.04)
                cell.backGroundWhiteView.layer.masksToBounds = true
                cell.imgView.layer.cornerRadius = (cell.imgView.frame.size.width / 2)
                cell.imgView.layer.masksToBounds = true
                cell.shodowView.layer.shadowOpacity = 1;
                cell.shodowView.layer.masksToBounds = false
                cell.shodowView.layer.shadowRadius = 20
                cell.shodowView.layer.shadowColor = UIColor(red:0.8, green:0.8, blue:0.8, alpha:0.5).cgColor
                cell.shodowView.layer.shadowOffset = CGSize(width: 0.0, height: cell.contentView.frame.height * 0.04)
                cell.updateConstraintsIfNeeded()
                cell.layoutIfNeeded()
            }
            return cell
            
            
        }
        

        
        
        
        func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
//
//            rowsWhichIsChecked = indexPath.row
//            collectionView.reloadData()
//            NavigationManager.moveToSavedSteamDetailsBaseViewControllerIfNotExists(kAppDelegate.menuNavController!, rootCategoryID: rootID , isFromCarrer: true)
        }
    }



