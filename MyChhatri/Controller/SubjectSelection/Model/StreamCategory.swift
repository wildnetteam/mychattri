// To parse the JSON, add this file to your project and do:
//
//   let streamCategory = try StreamCategory(json)

import Foundation

struct StreamCategory: Codable {
    let status: Int?
    let success: Bool?
    let error: StreamCategoryError?
    let data: StreamCategoryDataClass?
    
    enum CodingKeys: String, CodingKey {
        case status = "status"
        case success = "success"
        case error = "error"
        case data = "data"
    }
}

struct StreamCategoryDataClass: Codable {
    let description: String?
    let categories: [Category]?
    let nextLevelChildCount: Bool?
    let categorySelf: CategorySelf?
    
    enum CodingKeys: String, CodingKey {
        case description = "description"
        case categories = "categories"
        case nextLevelChildCount = "next_level_child_count"
        case categorySelf = "category_self"
    }
}

struct Category: Codable {
    
    let id: Int?
    let title: String?
    let streamID: String?
    let captionText: String?
    let description: String?
    let documentsID: String?
    let childCount: String?
    
    enum CodingKeys: String, CodingKey {
        case id = "id"
        case title = "title"
        case streamID = "stream_id"
        case captionText = "caption_text"
        case description = "description"
        case documentsID = "documents_id"
        case childCount = "child_count"
    }

//    init(from decoder: Decoder) throws {
//
//        let container = try decoder.container(keyedBy: CodingKeys.self)
//
//        if let value = try? container.decode(String.self, forKey: .id) {
//            self.id = Int(value)
//        } else {
//            self.id = try container.decode(Int.self, forKey: .id)
//        }
//
//    }
    
}

struct CategorySelf: Codable {
    let id: Int?
    let title: String?
    let streamID: String?
    let captionText: String?
    let description: String?
    let documentsID: String?
    let childCount: String?
    
    enum CodingKeys: String, CodingKey {
        case id = "id"
        case title = "title"
        case streamID = "stream_id"
        case captionText = "caption_text"
        case description = "description"
        case documentsID = "documents_id"
        case childCount = "child_count"
    }
}

struct StreamCategoryError: Codable {
    let message: String?
    let code: Int?
    let errorDescription: String?
    
    enum CodingKeys: String, CodingKey {
        case message = "message"
        case code = "code"
        case errorDescription = "error_description"
    }
}

// MARK: Convenience initializers and mutators

extension StreamCategory {
    init(data: Data) throws {
        self = try JSONDecoder().decode(StreamCategory.self, from: data)
    }
    
    init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }
    
    init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }
    
    func with(
        status: Int?? = nil,
        success: Bool?? = nil,
        error: StreamCategoryError?? = nil,
        data: StreamCategoryDataClass?? = nil
        ) -> StreamCategory {
        return StreamCategory(
            status: status ?? self.status,
            success: success ?? self.success,
            error: error ?? self.error,
            data: data ?? self.data
        )
    }
    
    func jsonData() throws -> Data {
        return try JSONEncoder().encode(self)
    }
    
    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
        return String(data: try self.jsonData(), encoding: encoding)
    }
}

extension StreamCategoryDataClass {
    init(data: Data) throws {
        self = try JSONDecoder().decode(StreamCategoryDataClass.self, from: data)
    }
    
    init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }
    
    init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }
    
    func with(
        description: String?? = nil,
        categories: [Category]?? = nil,
        nextLevelChildCount: Bool?? = nil,
        categorySelf: CategorySelf?? = nil
        ) -> StreamCategoryDataClass {
        return StreamCategoryDataClass(
            description: description ?? self.description,
            categories: categories ?? self.categories,
            nextLevelChildCount: nextLevelChildCount ?? self.nextLevelChildCount,
            categorySelf: categorySelf ?? self.categorySelf
        )
    }
    
    func jsonData() throws -> Data {
        return try JSONEncoder().encode(self)
    }
    
    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
        return String(data: try self.jsonData(), encoding: encoding)
    }
}

extension Category {
    init(data: Data) throws {
        self = try JSONDecoder().decode(Category.self, from: data)
    }
    
    init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }
    

    init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }
    
    func with(
        id: Int?? = nil,
        title: String?? = nil,
        streamID: String?? = nil,
        captionText: String?? = nil,
        description: String?? = nil,
        documentsID: String?? = nil,
        childCount: String?? = nil
        ) -> Category {
        return Category(
            id: id ?? self.id,
            title: title ?? self.title,
            streamID: streamID ?? self.streamID,
            captionText: captionText ?? self.captionText,
            description: description ?? self.description,
            documentsID: documentsID ?? self.documentsID,
            childCount: childCount ?? self.childCount
        )
    }
    
    func jsonData() throws -> Data {
        return try JSONEncoder().encode(self)
    }
    
    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
        return String(data: try self.jsonData(), encoding: encoding)
    }
}

extension CategorySelf {
    init(data: Data) throws {
        self = try JSONDecoder().decode(CategorySelf.self, from: data)
    }
    
    init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }
    
    init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }
    
    func with(
        id: Int?? = nil,
        title: String?? = nil,
        streamID: String?? = nil,
        captionText: String?? = nil,
        description: String?? = nil,
        documentsID: String?? = nil,
        childCount: String?? = nil
        ) -> CategorySelf {
        return CategorySelf(
            id: id ?? self.id,
            title: title ?? self.title,
            streamID: streamID ?? self.streamID,
            captionText: captionText ?? self.captionText,
            description: description ?? self.description,
            documentsID: documentsID ?? self.documentsID,
            childCount: childCount ?? self.childCount
        )
    }
    
    func jsonData() throws -> Data {
        return try JSONEncoder().encode(self)
    }
    
    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
        return String(data: try self.jsonData(), encoding: encoding)
    }
}

extension StreamCategoryError {
    init(data: Data) throws {
        self = try JSONDecoder().decode(StreamCategoryError.self, from: data)
    }
    
    init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }
    
    init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }
    
    func with(
        message: String?? = nil,
        code: Int?? = nil,
        errorDescription: String?? = nil
        ) -> StreamCategoryError {
        return StreamCategoryError(
            message: message ?? self.message,
            code: code ?? self.code,
            errorDescription: errorDescription ?? self.errorDescription
        )
    }
    
    func jsonData() throws -> Data {
        return try JSONEncoder().encode(self)
    }
    
    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
        return String(data: try self.jsonData(), encoding: encoding)
    }
}

// MARK: Encode/decode helpers

class StreamCategoryJSONNull: Codable {
    public init() {}
    
    public required init(from decoder: Decoder) throws {
        let container = try decoder.singleValueContainer()
        if !container.decodeNil() {
            throw DecodingError.typeMismatch(StreamCategoryJSONNull.self, DecodingError.Context(codingPath: decoder.codingPath, debugDescription: "Wrong type for JSONNull"))
        }
    }
    
    public func encode(to encoder: Encoder) throws {
        var container = encoder.singleValueContainer()
        try container.encodeNil()
    }
}
