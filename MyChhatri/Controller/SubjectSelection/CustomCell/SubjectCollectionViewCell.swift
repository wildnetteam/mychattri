//
//  SubjectCollectionViewCell.swift
//  MyChhatri
//
//  Created by Arpana on 26/07/18.
//  Copyright © 2018 Anshul. All rights reserved.
//

import UIKit

class SubjectCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var lblTitle: UILabel!

    @IBOutlet weak var btnCheck: UIButton!

}
