//
//  ContactUsViewController.swift
//  MyChhatri
//
//  Created by Arpana on 19/09/18.
//  Copyright © 2018 Arpana. All rights reserved.
//

import UIKit

class ContactUsViewController: UIViewController {

    @IBOutlet weak var txtStudentFName:UITextField!
    @IBOutlet weak var txtStudentEmail:UITextField!
    @IBOutlet weak var txtStudentLName:UITextField!
    @IBOutlet weak var txtMobile:UITextField!
    @IBOutlet weak var txtMsg: UITextView!
    @IBOutlet weak var btnSave:UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()
        btnSave.addGrediant()
        setUPUI()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        self.navigationController?.navigationBar.isHidden = false
        kAppDelegate.menuNavController = self.navigationController
        
       // self.navigationController?.navigationBar.topItem?.title = "My Wishlist"
        
        kAppDelegate.setNavigationAndStatusAppearance()
        let statusBar: UIView = UIApplication.shared.value(forKey: "statusBar") as! UIView
        statusBar.backgroundColor = ColorConstants.appBackGroundCOLOR()
        UIApplication.shared.statusBarStyle = .lightContent
        setNeedsStatusBarAppearanceUpdate()

    }
    
    func setUPUI() {
        
        let viewLeftStudentFName = UIView(frame: CGRect(x: 0, y: 0, width: 8, height: 40))
        txtStudentFName.leftView = viewLeftStudentFName
        txtStudentFName.leftViewMode = .always
        
        let viewLeftStudentLName = UIView(frame: CGRect(x: 0, y: 0, width: 8, height: 40))
        txtStudentLName.leftView = viewLeftStudentLName
        txtStudentLName.leftViewMode = .always
        
        let viewLeftStudentEmail = UIView(frame: CGRect(x: 0, y: 0, width: 8, height: 40))
        txtStudentEmail.leftView = viewLeftStudentEmail
        txtStudentEmail.leftViewMode = .always
        
        let viewMobile = UIView(frame: CGRect(x: 0, y: 0, width: 8, height: 40))
        txtMobile.leftView = viewMobile
        txtMobile.leftViewMode = .always
        
        let viewLefStudentEmail = UIView(frame: CGRect(x: 0, y: 0, width: 8, height: 40))
        txtStudentEmail.leftView = viewLefStudentEmail
        txtStudentEmail.leftViewMode = .always
        
        txtMsg.textContainerInset = UIEdgeInsets(top: 0, left: 5, bottom: 0, right: 0);
    }
    
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func sentRequest(_ sender: Any){
        
        if txtStudentEmail.text?.trim().count ==  0 {
            
            self.showAlert(title:APP_NAME, message:"Please enter email id", withTag:101, button:["Ok"], isActionRequired:false)
            return
        }
        
        if txtMsg.text?.trim().count ==  0 {
            
            self.showAlert(title:APP_NAME, message:"Please enter message", withTag:101, button:["Ok"], isActionRequired:false)
            return
        }
            
            var params : [String:Any] = [:]
            
            if let userInfoDict = UserModel.getLoginInfo() {
                
                if let  userId = (userInfoDict.object(forKey: "ID") as? String){
                    
                    params["user_id"]  = userId
                    
                }else if let userIded = (userInfoDict.object(forKey: "ID") as? Int){
                    
                    params["user_id"]  = userIded
                }else{
                    
                    self.showAlert(title: "Sorry", message: "User details could not be found , Please logout and login again", withTag: 101, button: ["Ok"], isActionRequired: false)
                    return
                }
            }
        
           params["first_name"] = txtStudentFName.text ?? ""
           params["last_name"] = txtStudentLName.text ?? ""
           params["email"] = txtStudentEmail.text ?? ""
           params["mobile"] = txtMobile.text ?? ""
           params["message"] = txtMsg.text ?? ""

            print("params : \(params)")
            
            ProgressHUD.showProgressHUDWithLabel(label: "Loading...")
            
            APICall.callContactUSPostAPI(params, header: [ : ]) { (sucess, result, error) in
                
                DispatchQueue.main.async {
                    
                    ProgressHUD.hideProgressHUD()
                    
                    weak var weakSelf = self
                    
                    if (error != nil) {
                        weakSelf?.showAlert(title: "Error", message: (error!.localizedDescription), withTag: 101, button: ["Ok"], isActionRequired: false)
                        return
                    }
                    if sucess {
                        
                        do {
                            let jsonData = try JSONSerialization.jsonObject(with: result!, options: .mutableContainers)
                            
                            if (jsonData is NSDictionary){
                                
                                if let statusCode = (jsonData as! NSDictionary).object(forKey: "status"), (statusCode is Int) {
                                    
                                    if let success = (jsonData as! NSDictionary).object(forKey: "success") {
                                        
                                        if (success as! Bool) == true {
                                            
                                            if (statusCode as! Int) == 200 {
                                                if let data = (jsonData as! NSDictionary).object(forKey: "data"), (data is NSDictionary) {
                                                    if (data as! NSDictionary).count > 0 {
                                                        
                                                      weakSelf?.txtStudentFName.text = ""
                                                      weakSelf?.txtStudentLName.text = ""
                                                      weakSelf?.txtStudentEmail.text = ""
                                                      weakSelf?.txtMobile.text = ""
                                                      weakSelf?.txtMsg.text = ""
                                                        
                                                        var msg = "We will get back to you soon."
                                                     if let message = (data as! NSDictionary).object(forKey: "message") as? String {
                                                        
                                                        msg = message
                                                        }
                                                        weakSelf?.showAlert(title: APP_NAME, message: msg, withTag: 110, button: ["Ok"], isActionRequired: true)
                                                        self.navigationController?.popViewController(animated: true)
                                                        
                                                    }
                                                    
                                                }
                                                
                                                
                                            } else {
                                                
                                                //some other status code
                                                //check error message in the response
                                                var errStr = ""
                                                if let errormessage = (jsonData as? NSDictionary)?.object(forKey:"error") {
                                                    if (errormessage is NSDictionary){
                                                        if let errorDescription = (errormessage as? NSDictionary)?.object(forKey: "error_description"),!(errorDescription is NSNull){
                                                            if (errorDescription is String){
                                                                errStr = errorDescription as! String
                                                                
                                                            }
                                                        }else {
                                                            if let errorMessage = (errormessage as? NSDictionary)?.object(forKey: "message"),!(errorMessage is NSNull){
                                                                if (errorMessage is String){
                                                                    errStr = errorMessage as! String
                                                                    
                                                                }
                                                            }else{
                                                                errStr = AlertMessages.UnidetifiedErrorMessage
                                                            }
                                                            
                                                        }
                                                        weakSelf?.showAlert(title: APP_NAME, message:errStr, withTag: 101, button: ["Ok"], isActionRequired: false)
                                                    }
                                                }
                                            }
                                        }else{
                                            
                                            //some other status code
                                            //check error message in the response
                                            var errStr = ""
                                            if let errormessage = (jsonData as? NSDictionary)?.object(forKey:"error") {
                                                if (errormessage is NSDictionary){
                                                    if let errorDescription = (errormessage as? NSDictionary)?.object(forKey: "error_description"),!(errorDescription is NSNull){
                                                        if (errorDescription is String){
                                                            errStr = errorDescription as! String
                                                            
                                                        }
                                                    }else {
                                                        if let errorMessage = (errormessage as? NSDictionary)?.object(forKey: "message"),!(errorMessage is NSNull){
                                                            if (errorMessage is String){
                                                                errStr = errorMessage as! String
                                                                
                                                            }
                                                        }else{
                                                            errStr = AlertMessages.UnidetifiedErrorMessage
                                                        }
                                                        
                                                    }
                                                    weakSelf?.showAlert(title: APP_NAME, message:errStr, withTag: 101, button: ["Ok"], isActionRequired: false)
                                                }
                                            }
                                        }
                                        
                                    }else{
                                        
                                        weakSelf?.showAlert(title: "sorry", message: AlertMessages.NOTADICTIONARYMESSAGE , withTag: 101, button:  ["Ok"], isActionRequired: false)
                                    }
                                    
                                }
                            }else{
                                // not a dictionary
                                weakSelf?.showAlert(title: "sorry", message: AlertMessages.NOTADICTIONARYMESSAGE , withTag: 101, button:  ["Ok"], isActionRequired: false)
                            }
                        }catch{
                            weakSelf?.showAlert(title: "sorry", message: AlertMessages.MODELERRORMESSAGE , withTag: 101, button:  ["Ok"], isActionRequired: false)
                            return
                        }
                    }
                    
                }
            }
        }
    
    @IBAction func backButtonClicked(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
        
    }
}

