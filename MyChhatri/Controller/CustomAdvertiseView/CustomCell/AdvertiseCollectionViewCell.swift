//
//  AirlineLogoCollectionViewCell.swift
//  kannuAirport
//
//  Created by Arpana on 03/05/18.
//  Copyright © 2018 Arpana. All rights reserved.
//

import UIKit

class AdvertiseCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var imageViewLogo: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        imageViewLogo.contentMode = .scaleAspectFill
        imageViewLogo.clipsToBounds = true
        imageViewLogo.cornerRadius = (imageViewLogo.frame.size.width / 6)
        imageViewLogo.layer.masksToBounds = true
        self.layoutIfNeeded()
    }

}
