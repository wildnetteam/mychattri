//
//  AdvertiseCollectionView.swift
//  MyChhatri
//
//  Created by Arpana on 26/09/18.
//  Copyright © 2018 Arpana. All rights reserved.
//

import UIKit

@objc protocol UpdateAddvertiseUIDelegate  {
    
    @objc optional func removeUI()
}

class AdvertiseCollectionView: UICollectionView, UICollectionViewDelegate , UICollectionViewDataSource   {
    
    
    var advertiseArray : Array<NSDictionary>?
    var categoryIDForAdvertiseData : String?
    var typeFoAdvertiseData : String?

    weak var updelegate: UpdateAddvertiseUIDelegate?

    // MARK: - MAINTAIN PAGINATION AND WEBINAR LIST
    var pageNumber = 0
    var listlimit = 3 // Define to get maximum data one at a time
    var totalCount : Int?
    
    var offset : Int?
    
    override func awakeFromNib() {
        
        super.awakeFromNib()
   
    }
    
    
    override init(frame: CGRect, collectionViewLayout layout: UICollectionViewLayout) {
        super.init(frame: frame, collectionViewLayout: layout)
        self.frame =  frame
        self.delegate = self
        self.dataSource = self
        self.register(UINib(nibName: "AdvertiseCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "AdvertiseCollectionViewCell")

        offset = 0
        
    }
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func getadvertiseData()  {
        
        
        guard (categoryIDForAdvertiseData) != nil else{
            
            return
        }
        
        
        let infoDict = NSDictionary(objects: [typeFoAdvertiseData! , categoryIDForAdvertiseData! ,listlimit ,offset ?? 0], forKeys: ["type" as NSCopying ,"category_id" as NSCopying,"limit" as NSCopying ,"offset" as NSCopying])
        
            ProgressHUD.showProgressHUDWithLabel(label: "Loading...")
        
        
        APICall.callAdvertiseListAPI([:], header: [ : ], information:infoDict ) { (sucess, result, error) in
            
            DispatchQueue.main.async {
                
                weak var weakSelf = self
                
                ProgressHUD.hideProgressHUD()
                
                if (error != nil) {
                    weakSelf?.showAlert(title: "Error", message: (error!.localizedDescription), withTag: 101, button: ["Ok"], isActionRequired: false)
                    if weakSelf?.updelegate != nil{
                        weakSelf?.updelegate?.removeUI!()
                    }
                    return
                }
                
                if sucess {
                    
                    do {
                        let jsonData = try JSONSerialization.jsonObject(with: result!, options: .mutableContainers)
                        
                        if let responseDict = (jsonData as? NSDictionary){
                            
                            if let statusCode = responseDict.object(forKey: "status"), (statusCode is Int) {
                                
                                if (statusCode as! Int) == 200 {
                                    
                                    //sucessfully retrieved data
                                    if let dataDict = responseDict.object(forKey: "data") as? NSDictionary{
                                        
                                        if let toptalR = dataDict.value(forKey: "totalCount") as? Int{
                                            weakSelf?.totalCount = toptalR
                                        }else if let toptalR = dataDict.value(forKey: "totalCount") as? String{
                                            weakSelf?.totalCount = Int(toptalR)
                                        }
                                        
                                        if let tempArray = dataDict.object(forKey: "advertisements") as? Array<NSDictionary> {
                                            
                                            if tempArray.count > 0 {
                                                //store data
                                                if  weakSelf?.pageNumber == 0 {
                                                    self.advertiseArray =  Array()
                                                    
                                                    weakSelf?.advertiseArray = tempArray
                                                }else{
                                                    //Just add to previous array
                                                    weakSelf?.advertiseArray?.append(contentsOf: tempArray)
                                                }
                                                
                                            }
                                        }else {
                                            //
                                            weakSelf?.showAlert(title: APP_NAME, message: "webinars key not found", withTag: 101, button: ["Ok"], isActionRequired: false)
                                        }
                                    }else{
                                        //No data found error
                                        weakSelf?.showAlert(title: APP_NAME, message:  AlertMessages.NoDataKeyFoundInResponse, withTag: 101, button: ["Ok"], isActionRequired: false)
                                    }
                                    
                                } else {
                                    
                                    //  update the collectionview
                                    if weakSelf?.updelegate != nil{
                                        weakSelf?.updelegate?.removeUI!()
                                    }
                                    
                                }
                            }
                        }else{
                            
                            weakSelf?.showAlert(title: "sorry", message: AlertMessages.NOTADICTIONARYMESSAGE , withTag: 101, button:  ["Ok"], isActionRequired: false)
                            return
                        }
                        
                    }catch{
                        
                        weakSelf?.showAlert(title: "sorry", message: AlertMessages.MODELERRORMESSAGE , withTag: 101, button:  ["Ok"], isActionRequired: false)
                        return
                    }
                    
                } else{
                    
                    //Success false
                    weakSelf?.showAlert(title: APP_NAME, message: AlertMessages.UnidetifiedErrorMessage, withTag: 101, button: ["Ok"], isActionRequired: false)
                    
                }
                
                if  weakSelf?.advertiseArray?.count == 0 && weakSelf?.offset == 0 {
                    
                    //call a delegate to hide add View
                    
                    //Deleted sucesfully , update the collectionview
                    if weakSelf?.updelegate != nil{
                        weakSelf?.updelegate?.removeUI!()
                    }
                    
                }
                self.reloadData()
                
            }
            
        }
    }
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if let array = advertiseArray {
            return array.count
        }
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "AdvertiseCollectionViewCell", for: indexPath) as! AdvertiseCollectionViewCell
          if let tempbatchArray = advertiseArray {
            
            if let dict = tempbatchArray[indexPath.row] as?  NSDictionary {
                
                var imageFinalUrl =  ""
                
                if let imageURL = dict.value(forKey: "image_url") {
                    
                    imageFinalUrl = "\(URLS.IMAGE_BASE_URL)" + "\(imageURL)"
                    
                    cell.imageViewLogo.sd_setIndicatorStyle(.gray)
                    cell.imageViewLogo.sd_setShowActivityIndicatorView(true)
                    
                    cell.imageViewLogo.sd_setImage(with: URL(string: imageFinalUrl), completed: { (image, error, type, url) in
                        DispatchQueue.main.async {
                            if error == nil{
                                cell.imageViewLogo.image = image
                            }
                            cell.imageViewLogo.sd_setShowActivityIndicatorView(false)
                        }
                    })
                }
            }
            
            //Reload the next amount of data
            if(indexPath.row ==  (advertiseArray?.count)! - 1 && totalCount! > (advertiseArray?.count)! ){
                //we are at last index of currently displayed cell
                offset = (advertiseArray?.count)!
                //reload more data
                getadvertiseData()
            }
            
        }
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        
    }
}
extension AdvertiseCollectionView : UIScrollViewDelegate{
   
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
       
        
    }
    
}
    extension AdvertiseCollectionView : UICollectionViewDelegateFlowLayout {
        //1
        func collectionView(_ collectionView: UICollectionView,
                            layout collectionViewLayout: UICollectionViewLayout,
                            sizeForItemAt indexPath: IndexPath) -> CGSize {
            //2.4
            let   size   =  CGSize(width: (self.frame.width / 3) , height: (self.frame.width / 3))
            
            return size
            
        }
    
        
        func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat{
            
            return 0.5
        }
        
        func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat{
            return 0.5
        }
        
        
    }
    


