//
//  CustomAdvertiseView.swift
//  MyChhatri
//
//  Created by Arpana on 26/09/18.
//  Copyright © 2018 Arpana. All rights reserved.
//

import UIKit

@objc protocol HideADUIDelegate  {
    
    @objc optional func removeAdcollectionViewUI()
}

class CustomAdvertiseView: UIView , UpdateAddvertiseUIDelegate {
    
    var collectionView: AdvertiseCollectionView?
    var crossBtn : UIButton?
    var categoryIDForAdvertise : String?
    var typeFoAdvertiseData : String?
    
    weak var hideAdViewDel : HideADUIDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        let flowLayout = UICollectionViewFlowLayout()
        flowLayout.scrollDirection = .horizontal
     
        if collectionView  == nil  {
            
            collectionView = AdvertiseCollectionView(frame: CGRect(x: 10 , y: 0, width: self.frame.width - 20 , height: self.frame.height - 10 ), collectionViewLayout: flowLayout)
            collectionView?.backgroundColor = .clear
            collectionView?.updelegate = self as UpdateAddvertiseUIDelegate
        }
        
        if crossBtn == nil  {
            
        crossBtn = UIButton(frame: CGRect(x: self.frame.width , y: 0, width: 40 , height: 40 ))
        crossBtn?.addTarget(self, action:#selector(self.crossBtnClicked), for: .touchUpInside)
        crossBtn?.setImage(#imageLiteral(resourceName: "calendercross"), for: .normal)
        
        }
        guard categoryIDForAdvertise != nil else{
            
            if hideAdViewDel != nil{
                hideAdViewDel?.removeAdcollectionViewUI!()
            }
            return
        }
        collectionView?.typeFoAdvertiseData = typeFoAdvertiseData ?? "1"
        collectionView?.categoryIDForAdvertiseData = categoryIDForAdvertise
        collectionView?.getadvertiseData()
     
        if (collectionView?.isDescendant(of: self))! {
        } else {
            self.addSubview(collectionView!)
            
        }
     self.addSubview(crossBtn!)
    }

    @IBAction func crossBtnClicked(_ sender: Any) {
        
        if hideAdViewDel != nil{
            hideAdViewDel?.removeAdcollectionViewUI!()
        }
    }
    func removeUI() {
        print("removeUI")
        if hideAdViewDel != nil{
            hideAdViewDel?.removeAdcollectionViewUI!()
        }
    }
}

