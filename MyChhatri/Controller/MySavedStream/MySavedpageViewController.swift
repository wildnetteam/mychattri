//
//  MySavedpageViewController.swift
//  MyChhatri
//
//  Created by Arpana on 24/07/18.
//  Copyright © 2018 Anshul. All rights reserved.
//

import UIKit

class MySavedpageViewController: UIViewController ,DeleteCellDelegate {
    
    @IBOutlet weak var shadowView: UIView!
    @IBOutlet weak var collectionView: UICollectionView!
    var rowsWhichIsChecked : Int = -1
    var savedStreamArray : NSMutableArray?
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action:
            #selector(MySavedpageViewController.getSavedStreams),
                                 for: UIControlEvents.valueChanged)
        refreshControl.tintColor = .black
        
        return refreshControl
    }()
    
    
        override func viewDidLoad() {
            super.viewDidLoad()
            
            //Add shdow Below page menu
            shadowView.layer.masksToBounds = false
            shadowView.layer.shadowOpacity = 0.4 ;
            shadowView.layer.shadowColor = UIColor.black.cgColor
            shadowView.layer.shadowOffset = CGSize(width: 0.0, height: 4.0)
            self.collectionView.addSubview(self.refreshControl)
            if popedControllerPageIndex == 0 {
                getSavedStreams()
            }

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if popedControllerPageIndex == 1 {
            getSavedStreams()
        }
        if(collectionView != nil) {
            rowsWhichIsChecked = -1
            collectionView.reloadData()
        }

    }
    
        override func didReceiveMemoryWarning() {
            super.didReceiveMemoryWarning()
            // Dispose of any resources that can be recreated.
        }
        

    //PRAGMA MARK:- Api
    
    @objc func getSavedStreams()  {
        
        savedStreamArray = NSMutableArray()
        
        let params : [String:Any] = [:]
        
        if let userInfoDict = UserModel.getLoginInfo() {
            
            guard ((userInfoDict.object(forKey: "ID")) != nil) else{
                
                self.showAlert(title: "Sorry", message: "User details could not be found , Please logout and login again", withTag: 101, button: ["Ok"], isActionRequired: false)
                return
            }
            
            guard (userInfoDict.object(forKey: "token") as? String) != nil else{
                
                self.showAlert(title: "Sorry", message: "User accessToken could not be found , Please logout and login again", withTag: 101, button: ["Ok"], isActionRequired: false)
                return
            }
        }
        if collectionView != nil {
            
            ProgressHUD.showProgressHUDWithLabel(label: "Loading...")
        }
        
        APICall.callSavedStreamAPI(params, header: [ : ]) { (sucess, result, error) in
            
            DispatchQueue.main.async {
                
                weak var weakSelf = self
                
                ProgressHUD.hideProgressHUD()

                if (error != nil) {
                    weakSelf?.showAlert(title: "Error", message: (error!.localizedDescription), withTag: 101, button: ["Ok"], isActionRequired: false)
                    return
                }
                
                if sucess {
                    
                    do {
                        let jsonData = try JSONSerialization.jsonObject(with: result!, options: .mutableContainers)
                        
                        if let responseDict = (jsonData as? NSDictionary){
                            
                            if let statusCode = responseDict.object(forKey: "status"), (statusCode is Int) {
                                
                                if (statusCode as! Int) == 200 {
                                    
                                  //sucessfully retrieved data
                                    if let dataArray = responseDict.object(forKey: "data") as? NSArray{
                                        
                                        if dataArray.count > 0 {
                                            //store data
                                            for dict in dataArray{
                                                
                                                if let elementDetail = dict as? NSDictionary {
                                                    
                                                    weakSelf?.savedStreamArray?.add(elementDetail)
                                                }
                                            }
                                            
                                        }else{
                                            //No saved stream found message
                                             weakSelf?.showAlert(title: APP_NAME, message:  AlertMessages.NoSavedStream, withTag: 101, button: ["Ok"], isActionRequired: false)
                                        }
                                    }else{
                                        //No data found error
                                          weakSelf?.showAlert(title: APP_NAME, message:  AlertMessages.NoDataKeyFoundInResponse, withTag: 101, button: ["Ok"], isActionRequired: false)
                                    }
                                    
                                } else {
                                    var errStr = ""
                                    if let errormessage = (jsonData as? NSDictionary)?.object(forKey:"error") {
                                        if (errormessage is NSDictionary){
                                            if let errorDescription = (errormessage as? NSDictionary)?.object(forKey: "error_description"),!(errorDescription is NSNull){
                                                if (errorDescription is String){
                                                    errStr = errorDescription as! String
                                                    
                                                }
                                            }else {
                                                if let errorMessage = (errormessage as? NSDictionary)?.object(forKey: "message"),!(errorMessage is NSNull){
                                                    if (errorMessage is String){
                                                        errStr = errorMessage as! String
                                                        
                                                    }
                                                }else{
                                                    errStr = AlertMessages.UnidetifiedErrorMessage
                                                }
                                                
                                            }
                                            weakSelf?.showAlert(title: APP_NAME, message:errStr, withTag: 101, button: ["Ok"], isActionRequired: false)
                                        }
                                        
                                    }else{
                                        weakSelf?.showAlert(title: APP_NAME, message:  AlertMessages.UnidetifiedErrorMessage, withTag: 101, button: ["Ok"], isActionRequired: false)
                                    }
                                    
                                }
                            }
                        }else{
                            
                            weakSelf?.showAlert(title: "sorry", message: AlertMessages.NOTADICTIONARYMESSAGE , withTag: 101, button:  ["Ok"], isActionRequired: false)
                            return
                        }
                        
                    }catch{
                        
                        weakSelf?.showAlert(title: "sorry", message: AlertMessages.MODELERRORMESSAGE , withTag: 101, button:  ["Ok"], isActionRequired: false)
                        return
                    }
                    
                } else{
                    
                    //Success false
                    weakSelf?.showAlert(title: APP_NAME, message: AlertMessages.UnidetifiedErrorMessage, withTag: 101, button: ["Ok"], isActionRequired: false)
                    
                }
                
                if self.refreshControl.isRefreshing{
                    self.refreshControl.endRefreshing()
                }
                
                if weakSelf?.collectionView != nil {
                    weakSelf?.collectionView.reloadData()
                }
            }
            
        }
    }
    
    //PRAGMA MARK:- Update collection after removing subject
    func remove(_ i: Int) {
        
        collectionView.performBatchUpdates({
            savedStreamArray?.removeObject(at: i)
            let indexPath = IndexPath(row: i, section: 0)
            self.collectionView.deleteItems(at: [indexPath])
            
        }) { finished in
            
        }
    }
}
    
    extension MySavedpageViewController : UICollectionViewDelegateFlowLayout {
        //1
        func collectionView(_ collectionView: UICollectionView,
                            layout collectionViewLayout: UICollectionViewLayout,
                            sizeForItemAt indexPath: IndexPath) -> CGSize {
            
            let itemSide: CGFloat = (collectionView.bounds.width) / 2
            return CGSize(width: itemSide , height: itemSide - ( itemSide * 0.06 ))
            
        }
        
        func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
            
            return UIEdgeInsetsMake(0, 0, 0, 0)
            
        }
        
        func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat{
            
            return 0
        }
        
        func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat{
            return 0
        }
    }
    
    
    extension MySavedpageViewController: UICollectionViewDelegate , UICollectionViewDataSource {
        
        func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
            
            if let array = savedStreamArray {
                
                return array.count
                
            }
            return 0
            
        }
        
        func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SavedStreamCollectionViewCellID", for: indexPath) as! SavedStreamCollectionViewCell
            
            cell.delegate = self
            cell.btnSelection.tag = indexPath.row
            cell.lblDescription.textColor = .black
            if let val : NSDictionary = self.savedStreamArray?[indexPath.row] as? NSDictionary {
                
                if let titleDict =  val.object(forKey: "topic") as? NSDictionary {
                    
                    if let titleText =  titleDict.object(forKey: "title") as? String {
                        
                        cell.lblTitle.text = titleText
                    }
                    if let captionText =  titleDict.object(forKey: "caption_text") as? String {
                        
                        cell.lblDescription.text = captionText
                    }
                    
                    if let imgUrl =  titleDict.object(forKey: "banner_image") as? String {
                        
                       let imageFinalUrl = "\(URLS.IMAGE_BASE_URL)" + "\(imgUrl)"
                        
                        cell.imgView.sd_setIndicatorStyle(.gray)
                        cell.imgView.sd_setShowActivityIndicatorView(true)
                        cell.imgView.sd_setImage(with: URL(string: imageFinalUrl), completed: { (image, error, type, url) in
                            if error == nil{
                                cell.imgView.image = image
                            }
                            cell.imgView.sd_setShowActivityIndicatorView(false)
                        })
                        
                    }
                    
                    if let idText =  val.object(forKey: "topic_id") as? String {
                     cell.btnSelection.setTitle(idText, for: .normal)
                        
                    } else if let idAsInt =  val.object(forKey: "topic_id") as? Int {
                        cell.btnSelection.setTitle(String(idAsInt), for: .normal)
                        
                    }
                    cell.btnSelection.setTitleColor(.clear, for: .normal)
                }
                
            }
            
            
            DispatchQueue.main.async {
                
                if(self.rowsWhichIsChecked == indexPath.row){
                    
                   // cell.btnSelection.setImage(#imageLiteral(resourceName: "Selected_icon"), for: .normal)
                    cell.backGroundWhiteView.backgroundColor = UIColor(red:0, green:0.47, blue:0.7, alpha:1)
                    cell.lblDescription.textColor = .white
                    cell.lblTitle.textColor = .white
                    
                }else{
                    
                 //   cell.btnSelection.setImage(#imageLiteral(resourceName: "Unselected_icon"), for: .normal)
                    cell.backGroundWhiteView.backgroundColor = .white// UIColor(red:0.89, green:0.91, blue:0.94, alpha:1)
                    cell.lblDescription.textColor = .black
                    // cell.lblTitle.textColor = .black
                    
                   // let randomColor: UIColor = .random  // r 0,835 g 0,0 b 1,0 a
                 //   cell.lblTitle.textColor = randomColor
                    
                }
                
                cell.backGroundWhiteView.layer.cornerRadius = (cell.shodowView.frame.size.width * 0.04)
                cell.backGroundWhiteView.layer.masksToBounds = true
                cell.imgView.layer.cornerRadius = (cell.imgView.frame.size.width / 2)
                cell.imgView.layer.masksToBounds = true
                cell.shodowView.layer.shadowOpacity = 1;
                cell.shodowView.layer.masksToBounds = false
                cell.shodowView.layer.shadowRadius = 20
                cell.shodowView.layer.shadowColor = UIColor(red:0.8, green:0.8, blue:0.8, alpha:0.5).cgColor
                cell.shodowView.layer.shadowOffset = CGSize(width: 0.0, height: cell.contentView.frame.height * 0.04)
                cell.updateConstraintsIfNeeded()
                cell.layoutIfNeeded()
            }
            return cell
            
            
        }
        
        func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
            
            // cross checking for checked rows
            rowsWhichIsChecked = indexPath.row
            collectionView.reloadData()
            
          //  var navigationTitleText : String?
            if let val : NSDictionary = self.savedStreamArray?[indexPath.row] as? NSDictionary {
                
                var iDArry : [String]? = []
                
                if let titleDict =  val.object(forKey: "topic") as? NSDictionary {
                    
                    if let titleText =  titleDict.object(forKey: "title") as? String {
                        
                        //   navigationTitleText = titleText
                        kAppDelegate.menuNavController?.navigationBar.accessibilityLabel = titleText
                    }
                    
                    if let Id =  titleDict.object(forKey: "id")  {
                        
                        
                        if let idAsInt = Id as? Int{
                            
                             iDArry?.append(String(idAsInt))
                            NavigationManager.moveToSavedSteamDetailsBaseViewControllerIfNotExists(kAppDelegate.menuNavController!, rootCategoryID:String(idAsInt)  ,otherIDsArray: iDArry!)
                            
                            
                        } else if let idAsString = Id as? String{
                            
                            iDArry?.append(idAsString)
                            NavigationManager.moveToSavedSteamDetailsBaseViewControllerIfNotExists(kAppDelegate.menuNavController!, rootCategoryID: idAsString , otherIDsArray: iDArry!)
                            
                        }else{
                            
                            self.showAlert(title: APP_NAME, message: "Category Id value missing", withTag: 109, button: ["OK"], isActionRequired: false)
                        }
                        
                    }
                }
                
            }
        }
        
}
