//
//  SavedStreamCollectionViewCell.swift
//  MyChhatri
//
//  Created by Arpana on 08/08/18.
//  Copyright © 2018 Anshul. All rights reserved.
//

import UIKit

@objc protocol DeleteCellDelegate  {
    
    @objc optional func remove(_ i: Int)
}


class SavedStreamCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var backGroundWhiteView: UIView!
    
    @IBOutlet weak var imgView: UIImageView!
    
    @IBOutlet weak var shodowView: UIView!
    
    @IBOutlet weak var btnSelection: UIButton!
    
    @IBOutlet weak var lblTitle: UILabel!
    
    weak var delegate:DeleteCellDelegate?

    
    @IBAction func removeSubject(_ sender: Any) {
        //remove selected subject basis of subject Id
        
         self.showAlert(title: APP_NAME, message: "Are you sure, you want to delete?", withTag: 111, button: ["Cancel" , "Ok"], isActionRequired: true)
    }
    
    override func alertButtonAction(alertTag: Int, btnTitle: String) {
        
        if alertTag == 111  {
            if btnTitle == "Ok" {
                
               delete()
                
            }else{
                
            }
        }
    }
    

    func delete(){
        
        let params : [String:Any] = [:]
        
        guard let idTodelete = btnSelection.title(for: .normal)else{
            
            self.showAlert(title: "Error", message: "Id to delete is not found. Try again", withTag: 101, button: ["Ok"], isActionRequired: false)
            return
        }
        APICall.callRemoveSubjectAPI(params, header: [ : ], subjectID: idTodelete) { (sucess, result, error) in
            
            DispatchQueue.main.async {
                
                weak var weakSelf = self
                
                ProgressHUD.hideProgressHUD()
                
                if (error != nil) {
                    weakSelf?.showAlert(title: "Error", message: (error!.localizedDescription), withTag: 101, button: ["Ok"], isActionRequired: false)
                    return
                }
                
                if sucess {
                    
                    do {
                        let jsonData = try JSONSerialization.jsonObject(with: result!, options: .mutableContainers)
                        
                        if let responseDict = (jsonData as? NSDictionary){
                            
                            if let statusCode = responseDict.object(forKey: "status"), (statusCode is Int) {
                                
                                if (statusCode as! Int) == 200 {
                                    
                                    weakSelf?.showAlert(title: APP_NAME, message:"Deleted successfully", withTag: 101, button: ["Ok"], isActionRequired: false)
                                    
                                    //Deleted sucesfully , update the collectionview
                                    if weakSelf?.delegate != nil{
                                        weakSelf?.delegate?.remove!((weakSelf?.btnSelection?.tag)!)
                                    }
                                    
                                } else {
                                    var errStr = ""
                                    if let errormessage = (jsonData as? NSDictionary)?.object(forKey:"error") {
                                        if (errormessage is NSDictionary){
                                            if let errorDescription = (errormessage as? NSDictionary)?.object(forKey: "error_description"),!(errorDescription is NSNull){
                                                if (errorDescription is String){
                                                    errStr = errorDescription as! String
                                                    
                                                }
                                            }else {
                                                if let errorMessage = (errormessage as? NSDictionary)?.object(forKey: "message"),!(errorMessage is NSNull){
                                                    if (errorMessage is String){
                                                        errStr = errorMessage as! String
                                                        
                                                    }
                                                }else{
                                                    errStr = AlertMessages.UnidetifiedErrorMessage
                                                }
                                                
                                            }
                                            weakSelf?.showAlert(title: APP_NAME, message:errStr, withTag: 101, button: ["Ok"], isActionRequired: false)
                                        }
                                        
                                    }else{
                                        weakSelf?.showAlert(title: APP_NAME, message:  AlertMessages.UnidetifiedErrorMessage, withTag: 101, button: ["Ok"], isActionRequired: false)
                                    }
                                    
                                }
                            }
                        }else{
                            
                            weakSelf?.showAlert(title: "sorry", message: AlertMessages.NOTADICTIONARYMESSAGE , withTag: 101, button:  ["Ok"], isActionRequired: false)
                            return
                        }
                        
                    }catch{
                        
                        weakSelf?.showAlert(title: "sorry", message: AlertMessages.MODELERRORMESSAGE , withTag: 101, button:  ["Ok"], isActionRequired: false)
                        return
                    }
                    
                } else{
                    
                    //Success false
                    weakSelf?.showAlert(title: APP_NAME, message: AlertMessages.UnidetifiedErrorMessage, withTag: 101, button: ["Ok"], isActionRequired: false)
                    
                }
                
                
            }
            
        }
    }

    @IBOutlet weak var lblDescription: UILabel!
}
