//
//  WebinarDetailViewController.swift
//  MyChhatri
//
//  Created by Arpana on 07/09/18.
//  Copyright © 2018 Arpana. All rights reserved.
//

import UIKit

class WebinarDetailViewController: UIViewController {
    
    @IBOutlet weak var imgView: UIImageView!
    var detailedInformation: NSDictionary?
    @IBOutlet weak var lblHeading: UILabel!
    @IBOutlet weak var txtViewSubHeading: UITextView!
    @IBOutlet weak var joinBtn: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        setdata()
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.navigationBar.isHidden = true
        //change the status bar color to clear so that imageview will be hidden 20Px by status bar
        let statusBar: UIView = UIApplication.shared.value(forKey: "statusBar") as! UIView
        statusBar.backgroundColor = .clear
        UIApplication.shared.statusBarStyle = .lightContent
        // test()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
    
    @IBAction func backButtonClicked(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func joinButtonClicked(_ sender: Any) {
        
        self.showAlert(title: "Sorry", message: "Do you want to join the Event?", withTag: 101, button: ["Ok"], isActionRequired: false)
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        let statusBar: UIView = UIApplication.shared.value(forKey: "statusBar") as! UIView
        statusBar.backgroundColor = ColorConstants.appBackGroundCOLOR()
        self.navigationController?.navigationBar.isHidden = false
        detailedInformation = nil
    }
    
    func setdata(){
        
        if detailedInformation != nil{
            //Any other click than Article
            
            if let dict = detailedInformation{
                if let wTitle = dict.value(forKey: "title") as? String {
                    
                    
                    lblHeading.text =  wTitle
                }
                if let wDes = dict.value(forKey: "description") as? String {
                    txtViewSubHeading.text = wDes.withoutHtml
                }
                
                
                var imageFinalUrl =  ""
                
                if let imageURL = dict.value(forKey: "banner_image") {
                    
                    weak var weakSelf = self
                    
                    imageFinalUrl = "\(URLS.IMAGE_BASE_URL)" + "\(imageURL)"
                    
                    weakSelf?.imgView.sd_setImage(with: URL(string: imageFinalUrl), completed: { (image, error, type, url) in
                        DispatchQueue.main.async {
                            if error == nil{
                                weakSelf?.imgView.image = image
                                weakSelf?.imgView.transform = CGAffineTransform(scaleX: 0, y: 0)
                                UIView.animate(withDuration: 1) {
                                    self.imgView.transform = .identity
                                }
                            }else{
                                 weakSelf?.imgView.image = #imageLiteral(resourceName: "Placeholder")
                            }
                            weakSelf?.imgView.sd_setShowActivityIndicatorView(false)
                        }
                    })
                }
            }
            
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func test () {
        
        imgView.transform = CGAffineTransform(scaleX: 0, y: 0)
        UIView.animate(withDuration: 1) {
            self.imgView.transform = .identity
        }
    }
    
}
