//
//  APIManager.swift
//  Template
//
//  Created by Rajeev on 23/01/17.
//  Copyright © 2017 Rajeev. All rights reserved.
//

import UIKit
import SystemConfiguration
import Alamofire

typealias ServiceResponseData = (Bool,  Data?, Error?) -> Void

class APIManager: NSObject {
    
    // MARK: - General API -
    
    static func isInternetAvailable() -> Bool
    {
        var zeroAddress = sockaddr_in()
        zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
        zeroAddress.sin_family = sa_family_t(AF_INET)
        
        let defaultRouteReachability = withUnsafePointer(to: &zeroAddress) {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {zeroSockAddress in
                SCNetworkReachabilityCreateWithAddress(nil, zeroSockAddress)
            }
        }
        
        var flags = SCNetworkReachabilityFlags()
        if !SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) {
            return false
        }
        let isReachable = (flags.rawValue & UInt32(kSCNetworkFlagsReachable)) != 0
        let needsConnection = (flags.rawValue & UInt32(kSCNetworkFlagsConnectionRequired)) != 0
        return (isReachable && !needsConnection)
    }
    
    static func callGetWithoutAuthantication(_ router: String, param: NSDictionary?, header: NSDictionary?, callback:@escaping ServiceResponseData)  -> (){
        
        let plainString = "user:password"
        let plainData = plainString.data(using: .utf8)
        _ = plainData?.base64EncodedString(options:NSData.Base64EncodingOptions(rawValue: 0))
        
    
        // Set in in Authorize header like...
        let headers1: HTTPHeaders = [
            "Accept": "application/json",
            "X-localization": "\(getLanguageKey(lang:  LocalDB.shared.currentLanguage!))"
        ]
        
        Alamofire.request(router, method: .get, parameters: nil,  encoding: JSONEncoding.default, headers: headers1).validate().responseJSON { response in
            
            callback (  response.result.isSuccess ,  response.data , response.error)
            
        }
    }
    
    
    static func callGet(_ router: String, param: [String: Any], header: NSDictionary?, callback:@escaping ServiceResponseData)  -> (){
        
        let plainString = "user:password"
        let plainData = plainString.data(using: .utf8)
        let base64String = plainData?.base64EncodedString(options:NSData.Base64EncodingOptions(rawValue: 0))
        
        _ = "Basic " + base64String!
        
        var accessToken = ""
        if let userDetails = UserModel.getLoginInfo() {
            
            if let  userAcessToken = (userDetails.object(forKey: "token") as? String){
                
                accessToken = userAcessToken
            }
        }
        
        
        // Set in in Authorize header like...
        let headers1: HTTPHeaders = [
            "Accept": "application/json",
            "Authorization" :  "Bearer \(accessToken)" ,
            "X-localization": "\(getLanguageKey(lang:  LocalDB.shared.currentLanguage!))"

        ]
        
        Alamofire.request(router, method: .get, parameters: nil,  encoding: JSONEncoding.default, headers: headers1).validate().responseJSON { response in
            
            callback (  response.result.isSuccess , response.data , response.error  )
            
        }
        
    }
    
    static func callPostForJson(_ router: String, param: [String: Any], header: NSDictionary?, callback:@escaping ServiceResponseData)  -> (){
        
        let plainString = "user:password"
        let plainData = plainString.data(using: .utf8)
        _ = plainData?.base64EncodedString(options:NSData.Base64EncodingOptions(rawValue: 0))
        
        var accessToken = ""
        if let userDetails = UserModel.getLoginInfo() {
            
            if let  userAcessToken = (userDetails.object(forKey: "token") as? String){
                
                accessToken = userAcessToken
            }
        }
        
        
        // Set in in Authorize header like...
        let headers1: HTTPHeaders = [
            "Accept": "application/json",
            "Authorization" :  "Bearer \(accessToken)" ,
            "X-localization": "\(getLanguageKey(lang:  LocalDB.shared.currentLanguage!))"
            
        ]
        var urlComponent = URLComponents(string: router)!
        var request = URLRequest(url: urlComponent.url!)
        request.httpMethod = "POST"
        request.httpBody = try? JSONSerialization.data(withJSONObject: param)
        request.allHTTPHeaderFields = headers1
        
        //Now use this URLRequest with Alamofire to make request
        Alamofire.request(request).responseJSON { response in
            
                callback (  response.result.isSuccess , response.data , response.error  )
                
            }
        
    }
    
    static func callPost(_ router: String, param: [String: Any], header: NSDictionary?, callback:@escaping ServiceResponseData)  -> (){
        
        let plainString = "user:password"
        let plainData = plainString.data(using: .utf8)
        _ = plainData?.base64EncodedString(options:NSData.Base64EncodingOptions(rawValue: 0))
        
        var accessToken = ""
        if let userDetails = UserModel.getLoginInfo() {
            
            if let  userAcessToken = (userDetails.object(forKey: "token") as? String){
                
                accessToken = userAcessToken
            }
        }
        
        
        // Set in in Authorize header like...
        let headers1: HTTPHeaders = [
            "Accept": "application/json",
            "Authorization" :  "Bearer \(accessToken)" ,
            "X-localization": "\(getLanguageKey(lang:  LocalDB.shared.currentLanguage!))"
            
        ]
        Alamofire.request(router, method: .post, parameters: param, encoding: URLEncoding.httpBody, headers: headers1  as HTTPHeaders).validate().responseJSON
            {response in
                
                callback (  response.result.isSuccess , response.data , response.error  )
                
        }
    }
    static func callPost1(_ router: String, param: [String: Any], header: NSDictionary?, callback:@escaping ServiceResponseData)  -> (){
        //For forget password
        let plainString = "user:password"
        let plainData = plainString.data(using: .utf8)
        _ = plainData?.base64EncodedString(options:NSData.Base64EncodingOptions(rawValue: 0))
        
        
        var accessToken = ""
        
        if let userDetails = UserModel.getLoginInfo() {
            
            if let  userAcessToken = (userDetails.object(forKey: "token") as? String){
                
                accessToken = userAcessToken
            }
        }else {
           
            if let  userAcessToken = userDefaults.value(forKey: "access_token") as? String {
                
                accessToken = userAcessToken
            }
        }
        
        // Set in in Authorize header like...
        let headers1: HTTPHeaders = [
            "Accept": "application/json",
            "Authorization" :  "Bearer \(accessToken)" ,
            "X-localization": "\(getLanguageKey(lang:  LocalDB.shared.currentLanguage!))"

        ]
//        print( "Bearer \(String(describing: userDefaults.value(forKey: "access_token")))")
        Alamofire.request(router, method: .post, parameters: param, encoding: URLEncoding.httpBody, headers: headers1  as HTTPHeaders).validate().responseJSON
            {response in
                
                callback (  response.result.isSuccess , response.data , response.error  )
                
        }
    }
    
    static func uploadWithAlamofire(_ router: String, parameters: [String: String], imageData: Data, callback:@escaping (AnyObject?)->()) -> () {
        
        // Begin upload
        Alamofire.upload( multipartFormData: { multipartFormData in
            
            multipartFormData.append(imageData, withName: "file", fileName: "myImage.png", mimeType: "image/png")
            
            // import parameters
            for (key, value) in parameters {
                multipartFormData.append(value.data(using: String.Encoding.utf8)!, withName: key)
            }}
            , to: router, method: .post,
              
              encodingCompletion: { encodingResult in
                
                switch encodingResult {
                    
                case .success(let upload, _, _):
                    
                    upload.responseJSON { response in
                        
                        callback(response.result.value as AnyObject?)
                    }
                case .failure(let encodingError):
                    
                    callback(nil)
                    print("**ERROR")
                    print(encodingError)
                }
        })
    }
}

