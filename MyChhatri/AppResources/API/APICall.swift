//
//  AppDelegate.swift
//  kannuAirport
//
//  Created by Arpana on 03/05/18.
//  Copyright © 2018 Arpana. All rights reserved.
//

import UIKit

class APICall: NSObject {
    
    class func callStreamAPI(_ parameters:[String: Any], header : [String: Any] , onCompletion: @escaping ServiceResponseData) -> Void
    {
        
        let urlToRequset = "\(URLS.BASE_URL)\(URLS.HOMESTREAM)"
        
        APIManager.callGet(urlToRequset, param: parameters , header: [ : ]) { (isSucess, responseDictionary, error) in
            
            onCompletion(isSucess, responseDictionary, error)
        }
    }


    class func callSavedStreamAPI(_ parameters:[String: Any], header : [String: Any] , onCompletion: @escaping ServiceResponseData) -> Void
    {
        
        var userIdentification: String?
        
        if let userInfoDict = UserModel.getLoginInfo() {
            
            if let  userId = (userInfoDict.object(forKey: "ID") as? String){
                
                userIdentification  = userId
                
            }else if let userIded = (userInfoDict.object(forKey: "ID") as? Int){
                
                userIdentification  = String(userIded)
                
            }else{
                
                
                return
            }
        }
        let urlToRequset = "\(URLS.BASE_URL)\(URLS.SAVEDSTREAM)/\(userIdentification!)"
        
        APIManager.callGet(urlToRequset, param: parameters , header: [ : ]) { (isSucess, responseDictionary, error) in
            
            onCompletion(isSucess, responseDictionary, error)
        }
    }
    

    class func callCategoryAPI(_ parameters:[String: Any], header : [String: Any], streamID :  String, onCompletion: @escaping ServiceResponseData) -> Void
    {
        
        let urlToRequset = "\(URLS.BASE_URL)\(URLS.SPECIALIZATION)\(streamID)/categories/"
        
        APIManager.callGet(urlToRequset, param: parameters , header: [ : ]) { (isSucess, responseDictionary, error) in
            
            onCompletion(isSucess, responseDictionary, error)
        }
    }
    
    
    class func callSubCategoryAPI(_ parameters:[String: Any], header : [String: Any] ,streamID :  String, catIdArray : [ String], onCompletion: @escaping ServiceResponseData) -> Void
    {
        var urlToRequset = "\(URLS.BASE_URL)\(URLS.SPECIALIZATION)\(streamID)/categories"
        
        for idCat in catIdArray {
            
            //To get the depth of categorty and append it till all
            urlToRequset = urlToRequset +  "/\(idCat)"
            print(urlToRequset)
            
        }
      //   var urlToRequset  = "http://172.16.16.6/mychhatriweb/api/collection/1/categories/0/6/18a"
        APIManager.callGet(urlToRequset, param: parameters , header: [ : ]) { (isSucess, responseDictionary, error) in
            
            onCompletion(isSucess, responseDictionary, error)
        }
    }
    
    class func callSubStreamAPI(_ parameters:[String: Any], header : [String: Any] ,catId : String, onCompletion: @escaping ServiceResponseData) -> Void
    {
        
        let urlToRequset = "\(URLS.BASE_URL)\(URLS.SPECIALIZATION)\(catId)"
        
        APIManager.callGet(urlToRequset, param: parameters , header: [ : ]) { (isSucess, responseDictionary, error) in
            
            onCompletion(isSucess, responseDictionary, error)
        }
    }
    
    class func callSaveSubjectPostAPI(_ parameters:[String: Any], header : [String: Any] , onCompletion: @escaping ServiceResponseData) -> Void{
        
        let urlToRequset = "\(URLS.BASE_URL)\(URLS.SAVE_TOPIC)"
        
        APIManager.callPost(urlToRequset, param: parameters, header: [:]) { (isSucess, responseDictionary, error) in
            onCompletion(isSucess, responseDictionary, error)
        }
    }
    class func callArticlesByCategoryAPI(_ parameters:[String: Any], header : [String: Any] ,categoryID :  String , onCompletion: @escaping ServiceResponseData) -> Void
    {
        
        let urlToRequset = "\(URLS.BASE_URL)\(URLS.GET_ARTICLES)/\(categoryID)"
        
        APIManager.callGet(urlToRequset, param: parameters , header: [ : ]) { (isSucess, responseDictionary, error) in
            
            onCompletion(isSucess, responseDictionary, error)
        }
    }
    class func callVideosByCategoryAPI(_ parameters:[String: Any], header : [String: Any] ,categoryID :  String , onCompletion: @escaping ServiceResponseData) -> Void
    {
        
        let urlToRequset = "\(URLS.BASE_URL)\(URLS.GET_INTERVIEW_VIDEO)/?category_id=\(categoryID)"
        
        APIManager.callGet(urlToRequset, param: parameters , header: [ : ]) { (isSucess, responseDictionary, error) in
            
            onCompletion(isSucess, responseDictionary, error)
        }
    }
    class func callRemoveSubjectAPI(_ parameters:[String: Any], header : [String: Any] ,subjectID :  String , onCompletion: @escaping ServiceResponseData) -> Void
    {
        
        // Example:   http://172.16.16.6/mychhatridev/api/resource/stream/getusersavedtopic/{user_id}/{topic_id}
        
        var userIdentification: String
        
        if let userInfoDict = UserModel.getLoginInfo() {
            
            if let  userId = (userInfoDict.object(forKey: "ID") as? String){
                
                userIdentification  = userId
                
            }else if let userIded = (userInfoDict.object(forKey: "ID") as? Int){
                
                userIdentification  = String(userIded)
                
            }else{
                
                return
            }
        }else{
            return
        }
        
        let urlToRequset = "\(URLS.BASE_URL)\(URLS.REMOVE_TOPIC)/\(subjectID)/\(userIdentification)"
        
        APIManager.callGet(urlToRequset, param: parameters , header: [ : ]) { (isSucess, responseDictionary, error) in
            
            onCompletion(isSucess, responseDictionary, error)
        }
    }
    
    class func callGetUserDetailsAPI(_ parameters:[String: Any], header : [String: Any]  , onCompletion: @escaping ServiceResponseData) -> Void
    {
                
        let urlToRequset = "\(URLS.BASE_URL)\(URLS.GET_USERDETAILS)"
        
        APIManager.callGet(urlToRequset, param: parameters , header: [ : ]) { (isSucess, responseDictionary, error) in
            
            onCompletion(isSucess, responseDictionary, error)
        }
    }
    class func callWebinarsAPI(_ parameters:[String: Any], header : [String: Any] ,information : NSDictionary , onCompletion: @escaping ServiceResponseData) -> Void
    {
        
        //Ex:     http://172.16.16.6/mychhatridev/api/collection/webinars?category_id=1&limit=10&offset=0

        let paramsToPass = "category_id=\(information.value(forKey: "category_id")!)&limit=\(information.value(forKey: "limit") ?? 10)&offset=\(information.value(forKey: "offset") ?? 0)"
        
        let urlToRequset = "\(URLS.BASE_URL)\(URLS.GET_WEBINARS)?\(paramsToPass)"
        
        APIManager.callGet(urlToRequset, param: parameters , header: [ : ]) { (isSucess, responseDictionary, error) in
            
            onCompletion(isSucess, responseDictionary, error)
        }
    }
    class func callInnovationListAPI(_ parameters:[String: Any], header : [String: Any] ,information : NSDictionary , onCompletion: @escaping ServiceResponseData) -> Void
    {
        
        //Ex:     http://172.16.16.6/mychhatridev/api/collection/webinars?category_id=1&limit=10&offset=0
        
        let paramsToPass = "category_id=\(information.value(forKey: "category_id")!)&limit=\(information.value(forKey: "limit") ?? 10)&offset=\(information.value(forKey: "offset") ?? 0)"
        
        let urlToRequset = "\(URLS.BASE_URL)\(URLS.GET_INNOVATION)?\(paramsToPass)"
        
        APIManager.callGet(urlToRequset, param: parameters , header: [ : ]) { (isSucess, responseDictionary, error) in
            
            onCompletion(isSucess, responseDictionary, error)
        }
    }
    class func callSeminarListAPI(_ parameters:[String: Any], header : [String: Any] ,information : NSDictionary , onCompletion: @escaping ServiceResponseData) -> Void
    {
        
        //Ex:     http://172.16.16.6/mychhatridev/api/collection/webinars?category_id=1&limit=10&offset=0
        
        let paramsToPass = "category_id=\(information.value(forKey: "category_id")!)&limit=\(information.value(forKey: "limit") ?? 10)&offset=\(information.value(forKey: "offset") ?? 0)"
        
        let urlToRequset = "\(URLS.BASE_URL)\(URLS.GET_SEMINAR)?\(paramsToPass)"
        
        APIManager.callGet(urlToRequset, param: parameters , header: [ : ]) { (isSucess, responseDictionary, error) in
            
            onCompletion(isSucess, responseDictionary, error)
        }
    }

    class func callOpportunityListAPI(_ parameters:[String: Any], header : [String: Any] ,information : NSDictionary , onCompletion: @escaping ServiceResponseData) -> Void
    {
        
        let paramsToPass = "category_id=\(information.value(forKey: "category_id")!)&limit=\(information.value(forKey: "limit") ?? 10)&offset=\(information.value(forKey: "offset") ?? 0)"
        
        let urlToRequset = "\(URLS.BASE_URL)\(URLS.GET_OPPORTUNITY)?\(paramsToPass)"
        
        APIManager.callGet(urlToRequset, param: parameters , header: [ : ]) { (isSucess, responseDictionary, error) in
            
            onCompletion(isSucess, responseDictionary, error)
        }
    }
    class func callTrendsListAPI(_ parameters:[String: Any], header : [String: Any] ,information : NSDictionary , onCompletion: @escaping ServiceResponseData) -> Void
    {
        
        let paramsToPass = "category_id=\(information.value(forKey: "category_id")!)&limit=\(information.value(forKey: "limit") ?? 10)&offset=\(information.value(forKey: "offset") ?? 0)"
        
        let urlToRequset = "\(URLS.BASE_URL)\(URLS.GET_TRENDS)?\(paramsToPass)"
        
        APIManager.callGet(urlToRequset, param: parameters , header: [ : ]) { (isSucess, responseDictionary, error) in
            
            onCompletion(isSucess, responseDictionary, error)
        }
    }
    class func callCompanyListAPI(_ parameters:[String: Any], header : [String: Any] ,information : NSDictionary , onCompletion: @escaping ServiceResponseData) -> Void
    {
        
        
        let urlParams : NSMutableString = ""
        // list all values
        for (key, value) in information {
            urlParams.append(key as! String)
            urlParams.append("=")
            
            if value as? String != nil {
                
                urlParams.append((value as? String)!  )
                
            } else if value as? Int != nil{
                
                urlParams.append(String((value as? Int)!)  )
                
            }
            urlParams.append("&")
        }
        
        urlParams.deleteCharacters(in:NSRange(location: urlParams.length - 1, length: 1))
        
      //  let paramsToPass = "category_id=\(information.value(forKey: "category_id")!)&limit=\(information.value(forKey: "limit") ?? 10)&offset=\(information.value(forKey: "offset") ?? 0)"
        
        let urlToRequset = "\(URLS.BASE_URL)\(URLS.GET_Company)?\(urlParams)"
        
        APIManager.callGet(urlToRequset, param: parameters , header: [ : ]) { (isSucess, responseDictionary, error) in
            
            onCompletion(isSucess, responseDictionary, error)
        }
    }
    
    class func callIsStreamSavedAPI(_ parameters:[String: Any], header : [String: Any] ,topicID : String , onCompletion: @escaping ServiceResponseData) -> Void
    {
        //    http://172.16.16.6/mychhatridev/api/resource/stream/getusertopicstatus/1/1
        
        var userIdentification: String
        
        if let userInfoDict = UserModel.getLoginInfo() {
            
            if let  userId = (userInfoDict.object(forKey: "ID") as? String){
                
                userIdentification  = userId
                
            }else if let userIded = (userInfoDict.object(forKey: "ID") as? Int){
                
                userIdentification  = String(userIded)
                
            }else{
                
                return
            }
        }else{
            return
        }
        
        
        let urlToRequset = "\(URLS.BASE_URL)\(URLS.IS_STREAMSAVED)/\(userIdentification)/\(topicID)"
    
        
        APIManager.callGet(urlToRequset, param: parameters , header: [ : ]) { (isSucess, responseDictionary, error) in
            
            onCompletion(isSucess, responseDictionary, error)
        }
    }
    
    class func callCourseDetailAPI(_ parameters:[String: Any], header : [String: Any] ,courseID : String , onCompletion: @escaping ServiceResponseData) -> Void
    {
        //        http://111.93.53.59/mychhatridev/api/resource/coursedetails?course_id=6

        
        let urlToRequset = "\(URLS.BASE_URL)\(URLS.COURSEDETAIL)?course_id=\(courseID)"
        
        
        APIManager.callGet(urlToRequset, param: parameters , header: [ : ]) { (isSucess, responseDictionary, error) in
            
            onCompletion(isSucess, responseDictionary, error)
        }
    }
    class func callEnrollPostAPI(_ parameters:[String: Any], header : [String: Any] , onCompletion: @escaping ServiceResponseData) -> Void{
        
        let urlToRequset = "\(URLS.BASE_URL)\(URLS.ENROLL_BATCH)"
        
        APIManager.callPost(urlToRequset, param: parameters, header: [:]) { (isSucess, responseDictionary, error) in
            onCompletion(isSucess, responseDictionary, error)
        }
    }
    class func callAddBatchToWishListPostAPI(_ parameters:[String: Any], header : [String: Any] , onCompletion: @escaping ServiceResponseData) -> Void{
        
        let urlToRequset = "\(URLS.BASE_URL)\(URLS.ADD_BATCH_WISHLIST)"
        
        APIManager.callPost(urlToRequset, param: parameters, header: [:]) { (isSucess, responseDictionary, error) in
            onCompletion(isSucess, responseDictionary, error)
        }
    }
    class func callUserEnrolledBatchesAPI(_ parameters:[String: Any], header : [String: Any] ,information : NSDictionary , onCompletion: @escaping ServiceResponseData) -> Void
    {
        
        //Ex:    http://172.16.16.6/mychhatridev/api/resource/userenrolledbatches?limit=10&offset=0
        
        let urlParams : NSMutableString = ""
        // list all values
        for (key, value) in information {
            urlParams.append(key as! String)
            urlParams.append("=")

            if value as? String != nil {
                
                urlParams.append((value as? String)!  )

            } else if value as? Int != nil{
                
                urlParams.append(String((value as? Int)!)  )

            }
            urlParams.append("&")
        }
        
        urlParams.deleteCharacters(in:NSRange(location: urlParams.length - 1, length: 1))
       // print(urlParams)

//        let paramsToPass = "limit=\(information.value(forKey: "limit") ?? 10)&offset=\(information.value(forKey: "offset") ?? 0)"
        
        let urlToRequset = "\(URLS.BASE_URL)\(URLS.USER_ENROLLED_BATCHES)?\(urlParams)"
        
        APIManager.callGet(urlToRequset, param: parameters , header: [ : ]) { (isSucess, responseDictionary, error) in
            
            onCompletion(isSucess, responseDictionary, error)
        }
    }
    class func callEnrolledBatchesDetailsAPI(_ parameters:[String: Any], header : [String: Any] ,enID : String , onCompletion: @escaping ServiceResponseData) -> Void
    {
        
        //Ex:    http://172.16.16.6/mychhatridev/api/resource/userenrolledbatches?limit=10&offset=0
        
  
        //
        let urlToRequset = "\(URLS.BASE_URL)\(URLS.USER_ENROLLED_BATCHES_DETAILS)?id=\(enID)"
        
        APIManager.callGet(urlToRequset, param: parameters , header: [ : ]) { (isSucess, responseDictionary, error) in
            
            onCompletion(isSucess, responseDictionary, error)
        }
    }
    class func callUserBatchesWishListAPI(_ parameters:[String: Any], header : [String: Any] ,information : NSDictionary , onCompletion: @escaping ServiceResponseData) -> Void
    {
        
        //Ex:    hhttp://172.16.16.6/mychhatridev/api/resource/userwishlistbatches?limit=10&offset=0
        
        
        let paramsToPass = "limit=\(information.value(forKey: "limit") ?? 10)&offset=\(information.value(forKey: "offset") ?? 0)"
        
        let urlToRequset = "\(URLS.BASE_URL)\(URLS.USER_BATCHES_WISHLIST)?\(paramsToPass)"
        
        APIManager.callGet(urlToRequset, param: parameters , header: [ : ]) { (isSucess, responseDictionary, error) in
            
            onCompletion(isSucess, responseDictionary, error)
        }
    }

    
    class func callBatchesWishLisDetailsAPI(_ parameters:[String: Any], header : [String: Any] ,enID : String , onCompletion: @escaping ServiceResponseData) -> Void
    {
        
        let urlToRequset = "\(URLS.BASE_URL)\(URLS.USER_BATCHES_WISHLIST_DETAILS)?id=\(enID)"
        
        APIManager.callGet(urlToRequset, param: parameters , header: [ : ]) { (isSucess, responseDictionary, error) in
            
            onCompletion(isSucess, responseDictionary, error)
        }
    }
    
    class func callRemoveFromWishListAPI(_ parameters:[String: Any], header : [String: Any] ,batchID :  String , onCompletion: @escaping ServiceResponseData) -> Void
    {
        
        // Example:     http://111.93.53.59/mychhatridev/api/resource/removewishlistbatch?id=1
        
        let urlToRequset = "\(URLS.BASE_URL)\(URLS.REMOVE_FROM_WISHLIST)?id=\(batchID)"
        
        APIManager.callGet(urlToRequset, param: parameters , header: [ : ]) { (isSucess, responseDictionary, error) in
            
            onCompletion(isSucess, responseDictionary, error)
        }
    }
    
    class func callCancelEnrolledBatchAPI(_ parameters:[String: Any], header : [String: Any] ,enrolledID :  String , onCompletion: @escaping ServiceResponseData) -> Void {
        
        // http://172.16.16.6/mychhatridev/api/resource/cancelenrolledbatche?id=1
        
        let urlToRequset = "\(URLS.BASE_URL)\(URLS.CANCEL_ENROLLED_BATCH)?id=\(enrolledID)"
        
        APIManager.callGet(urlToRequset, param: parameters , header: [ : ]) { (isSucess, responseDictionary, error) in
            
            onCompletion(isSucess, responseDictionary, error)
        }
    }
    
    class func callNewsAPI(_ parameters:[String: Any], header : [String: Any] , information : NSDictionary,  onCompletion: @escaping ServiceResponseData) -> Void
    {
        
        let paramsToPass = "limit=\(information.value(forKey: "limit") ?? 10)&offset=\(information.value(forKey: "offset") ?? 0)"
        
        let urlToRequset = "\(URLS.BASE_URL)\(URLS.GET_NEWS)?\(paramsToPass)"
        
        APIManager.callGet(urlToRequset, param: parameters , header: [ : ]) { (isSucess, responseDictionary, error) in
            
            onCompletion(isSucess, responseDictionary, error)
        }
    }

    class func callSearchAPI(_ parameters:[String: Any], header : [String: Any] , information : NSDictionary,  onCompletion: @escaping ServiceResponseData) -> Void
    {
        
//        let paramsToPass = "limit=\(information.value(forKey: "limit") ?? 10)&offset=\(information.value(forKey: "offset") ?? 0)&company_name=\(information.value(forKey: "companyname") ?? "")&course_name=\(information.value(forKey: "coursename") ?? "")&keyword=\(information.value(forKey: "keyword") ?? "")"
        
        let urlParams : NSMutableString = ""
        // list all values
        for (key, value) in information {
            urlParams.append(key as! String)
            urlParams.append("=")
            
            if value as? String != nil {
                
                urlParams.append((value as? String)!  )
                
            } else if value as? Int != nil{
                
                urlParams.append(String((value as? Int)!)  )
                
            }
            urlParams.append("&")
        }
        
        urlParams.deleteCharacters(in:NSRange(location: urlParams.length - 1, length: 1))
        
        let urlToRequsetWithSpace = "\(URLS.BASE_URL)\(URLS.SEARCH)?\(urlParams)"
        
        //Remove spaces inbetween
        let urlToRequset = urlToRequsetWithSpace.addingPercentEncoding(withAllowedCharacters: NSCharacterSet.urlQueryAllowed)
        
        if urlToRequset != nil {
            APIManager.callGet(urlToRequset!, param: parameters , header: [ : ]) { (isSucess, responseDictionary, error) in
                
                onCompletion(isSucess, responseDictionary, error)
            }
        }
    }
    class func callAboutUsAPI(_ parameters:[String: Any], header : [String: Any] , onCompletion: @escaping ServiceResponseData) -> Void
    {
        
        let urlToRequset = "\(URLS.BASE_URL)\(URLS.ABOUTUS)"
        
        APIManager.callGet(urlToRequset, param: parameters , header: [ : ]) { (isSucess, responseDictionary, error) in
            
            onCompletion(isSucess, responseDictionary, error)
        }
    }
    
    class func callContactUSPostAPI(_ parameters:[String: Any], header : [String: Any] , onCompletion: @escaping ServiceResponseData) -> Void{
        
        let urlToRequset = "\(URLS.BASE_URL)\(URLS.CONTACTUS)"
        
        APIManager.callPost(urlToRequset, param: parameters, header: [:]) { (isSucess, responseDictionary, error) in
            onCompletion(isSucess, responseDictionary, error)
        }
    }
    
    class func callAdvertiseListAPI(_ parameters:[String: Any], header : [String: Any] ,information : NSDictionary , onCompletion: @escaping ServiceResponseData) -> Void
    {
        // http://111.93.53.59/mychhatridev/api/collection/advertisement?type=1&category_id=22&limit=10&offset=0
        
        let paramsToPass = "type=1&category_id=\(information.value(forKey: "category_id")!)&limit=\(information.value(forKey: "limit") ?? 10)&offset=\(information.value(forKey: "offset") ?? 0)"
        
        let urlToRequset = "\(URLS.BASE_URL)\(URLS.ADVERTISE)?\(paramsToPass)"
        
        APIManager.callGet(urlToRequset, param: parameters , header: [ : ]) { (isSucess, responseDictionary, error) in
            
            onCompletion(isSucess, responseDictionary, error)
        }
    }
    class func callTabCountAPI(_ parameters:[String: Any], header : [String: Any] ,topicID :  String , onCompletion: @escaping ServiceResponseData) -> Void {
        
        
        let urlToRequset = "\(URLS.BASE_URL)\(URLS.TABCOUNT)?category_id=\(topicID)"
        
        APIManager.callGet(urlToRequset, param: parameters , header: [ : ]) { (isSucess, responseDictionary, error) in
            
            onCompletion(isSucess, responseDictionary, error)
        }
    }

    class func callUpdateUserProfilePostAPI(_ parameters:[String: Any], header : [String: Any] , onCompletion: @escaping ServiceResponseData) -> Void{
        
        let urlToRequset = "\(URLS.BASE_URL)\(URLS.UPDATEUSER)"
        
        APIManager.callPost(urlToRequset, param: parameters, header: [:]) { (isSucess, responseDictionary, error) in
            onCompletion(isSucess, responseDictionary, error)
        }
    }
    
    class func callSessionExpiredAPI(_ parameters:[String: Any], header : [String: Any] , onCompletion: @escaping ServiceResponseData) -> Void {
        
        
        let urlToRequset = "\(URLS.BASE_URL)\(URLS.SESSIONEXP)"
        

        APIManager.callGet(urlToRequset, param: parameters , header: [ : ]) { (isSucess, responseDictionary, error) in

            onCompletion(isSucess, responseDictionary, error)
        }
    }
    
    class func callUpdateEmailOrMobileOnProfileAPI(_ parameters:[String: Any], header : [String: Any] , onCompletion: @escaping ServiceResponseData) -> Void {
        
        //http://ec2-13-126-2-191.ap-south-1.compute.amazonaws.com/api/collection/sendverificationemailormobile

        let urlToRequset = "\(URLS.BASE_URL)\(URLS.UPDATEEMAILORMOBILE)"
        
        
        APIManager.callPost(urlToRequset, param: parameters, header: [:]) { (isSucess, responseDictionary, error) in
            onCompletion(isSucess, responseDictionary, error)
        }
        
    }
    
    //    http://ec2-13-126-2-191.ap-south-1.compute.amazonaws.com/api/collection/verifyemailormobile

    class func callVerifyEmailOrMobileOnProfileAPI(_ parameters:[String: Any], header : [String: Any] , onCompletion: @escaping ServiceResponseData) -> Void {
        
        let urlToRequset = "\(URLS.BASE_URL)\(URLS.VERIFYEMAILORMOBILE)"
        
        
        APIManager.callPost(urlToRequset, param: parameters, header: [:]) { (isSucess, responseDictionary, error) in
            onCompletion(isSucess, responseDictionary, error)
        }
    }
    //http://mychatri.com/testing/api/collection/saveuserratings
    class func submitReviewAPI(_ parameters:[String: Any], header : [String: Any] , onCompletion: @escaping ServiceResponseData) -> Void {
        
        let urlToRequset = "\(URLS.BASE_URL)\(URLS.SUBMITRATING)"
        
        
        APIManager.callPostForJson(urlToRequset, param: parameters, header: [:]) { (isSucess, responseDictionary, error) in
            onCompletion(isSucess, responseDictionary, error)
        }
    }
    //http://mychatri.com/testing/api/collection/getratingquestions?course_type=2
    class func callRatingQuestionsAPI(_ parameters:[String: Any], header : [String: Any] ,typeID :  String , onCompletion: @escaping ServiceResponseData) -> Void {
            
        
        let urlToRequset = "\(URLS.BASE_URL)\(URLS.CONTESTLIST)?course_type=\(typeID)"
        
        APIManager.callGet(urlToRequset, param: parameters, header: [:]) { (isSucess, responseDictionary, error) in
            onCompletion(isSucess, responseDictionary, error)
        }
    }
    //http://mychatri.com/testing/api/online/contests/list
    class func callContestListAPI(_ parameters:[String: Any], header : [String: Any] ,typeID :  String , onCompletion: @escaping ServiceResponseData) -> Void {
        
        let urlToRequset = "\(URLS.BASE_URL)\(URLS.CONTESTLIST)"

        APIManager.callGet(urlToRequset, param: parameters, header: [:]) { (isSucess, responseDictionary, error) in
            onCompletion(isSucess, responseDictionary, error)
        }
    }
    //http://mychatri.com/testing/api/online/contest/questions/list?contest_id=1
    class func callContestQuestionListQuestionsAPI(_ parameters:[String: Any], header : [String: Any] ,contestID :  String , onCompletion: @escaping ServiceResponseData) -> Void {
        
        let urlToRequset = "\(URLS.BASE_URL)\(URLS.CONTESTQUESTIONLIST)?contest_id=\(contestID)"
        
        APIManager.callGet(urlToRequset, param: parameters, header: [:]) { (isSucess, responseDictionary, error) in
            onCompletion(isSucess, responseDictionary, error)
        }
    }
    
   //http://mychatri/testing/api/online/contest/save
    class func submitContestAPI(_ parameters:[String: Any], header : [String: Any] , onCompletion: @escaping ServiceResponseData) -> Void {
        
        let urlToRequset = "\(URLS.BASE_URL)\(URLS.CONTESTQUESTIONSAVE)"

        APIManager.callPost(urlToRequset, param: parameters, header: [:]) { (isSucess, responseDictionary, error) in
            onCompletion(isSucess, responseDictionary, error)
        }
    }
   // http://mychatri.com/testing/api/contest/gifts/list
    class func callGiftListAPI(_ parameters:[String: Any], header : [String: Any] , onCompletion: @escaping ServiceResponseData) -> Void {
        
        let urlToRequset = "\(URLS.BASE_URL)\(URLS.CONTESTGIFTLIST)"
        
        APIManager.callGet(urlToRequset, param: parameters, header: [:]) { (isSucess, responseDictionary, error) in
            onCompletion(isSucess, responseDictionary, error)
        }
    }
    //http://mychatri.com/testing/api/user/address/add
    class func callAddAdressAPI(_ parameters:[String: Any], header : [String: Any] , onCompletion: @escaping ServiceResponseData) -> Void {
        
        let urlToRequset = "\(URLS.BASE_URL)\(URLS.CONTESTADDADDRESS)"
        
        APIManager.callPost(urlToRequset, param: parameters, header: [:]) { (isSucess, responseDictionary, error) in
            onCompletion(isSucess, responseDictionary, error)
        }
    }
    //http://mychatri.com/testing/api/user/gift/request
    class func callGiftRequestAPI(_ parameters:[String: Any], header : [String: Any] , onCompletion: @escaping ServiceResponseData) -> Void {
        
        let urlToRequset = "\(URLS.BASE_URL)\(URLS.CONTESTGIFTREQUEST)"
        
        APIManager.callPost(urlToRequset, param: parameters, header: [:]) { (isSucess, responseDictionary, error) in
            onCompletion(isSucess, responseDictionary, error)
        }
    }
    //http://mychatri.com/testing/api/online/played/contests
    class func callPlayedContestListAPI(_ parameters:[String: Any], header : [String: Any] , onCompletion: @escaping ServiceResponseData) -> Void {
        
        let urlToRequset = "\(URLS.BASE_URL)\(URLS.CONTESTPLAYEDLIST)"
        
        APIManager.callGet(urlToRequset, param: parameters, header: [:]) { (isSucess, responseDictionary, error) in
            onCompletion(isSucess, responseDictionary, error)
        }
    }
}
