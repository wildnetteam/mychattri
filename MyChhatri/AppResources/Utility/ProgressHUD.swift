//
//  ProgressHUD.swift
//  Bizzalley
//
//  Created by Rajeev on 17/11/17.
//  Copyright © 2017 Anuj Jha. All rights reserved.
//

import UIKit
import KRProgressHUD

class ProgressHUD: NSObject {
    open class func showProgressHUDWithLabel(label:String){
        KRProgressHUD.set(style: .black)
        KRProgressHUD.show(withMessage: label, completion: nil)
    }
    
    open class func hideProgressHUD(){
        DispatchQueue.main.async {
            KRProgressHUD.dismiss()
        }
    }
}
