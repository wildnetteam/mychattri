//
//  LocalDB.swift
//  MyChhatri
//
//  Created by Arpana on 24/08/18.
//  Copyright © 2018 Arpana. All rights reserved.
//

import UIKit

class LocalDB {
    
    static let shared = LocalDB()
    
    var currentLanguage: Language? {
        get {
            guard let language = UserDefaults.standard.value(forKey: Local_DB_Key.language) as? String else {
                return nil
            }
            return Language(rawValue: language)
        }
        set(language) {
            UserDefaults.standard.set(language?.rawValue, forKey: Local_DB_Key.language)
        }
    }
}
