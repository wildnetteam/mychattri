//
//  UserModel.swift
//  MyChhatri
//
//  Created by Anshul on 19/07/18.
//  Copyright © 2018 Anshul. All rights reserved.
//

import UIKit

class UserModel: NSObject {

    class func saveUserCredentials(userNameAndPassword :   NSDictionary? ){
        
        
        userDefaults.setValue(userNameAndPassword  , forKey: "LoginCredentials")
        
    }
    
    class func getUserCredentials() -> NSDictionary? {

        if let creden = (userDefaults.value(forKey: "LoginCredentials") as? NSDictionary){
            return creden
        }
        return nil
    }
    
    class func setLoginInfo(userData : NSDictionary? ) {
        
        if let userDetails = userData {
            
            userDefaults.setValue(userDetails  , forKey: "LoginDetails")

        }
        userDefaults.synchronize()
    }
    
    class func getLoginInfo()-> NSDictionary? {
    
        if let userData = userDefaults.value(forKey: "LoginDetails"){
           // let data = NSKeyedUnarchiver.unarchiveObject(with: userData as! Data) as! LoginModel
            return userData as? NSDictionary
        }else{
            return nil
        }
    }
    
    class func setAutoLogin(isSucessfull : Bool? ) {
        
            userDefaults.setValue(isSucessfull  , forKey: "isLoggedIn")
            
        // userDefaults.set(userData?.data?.user, forKey:"UserDetail")
        userDefaults.synchronize()
    }
    
    class func getUserType()-> Int? {
        
        let instituteType = userDefaults.value(forKey: "instituteType") as? String
        return Int(instituteType!)
    }
    
    class func setUserType(uType : String? ) {
        
        userDefaults.setValue(uType  , forKey: "instituteType")
        
        userDefaults.synchronize()
    }
    
    class func getAutoLogin()-> Bool? {
        
        return userDefaults.value(forKey: "isLoggedIn") as? Bool
        
    }
    
}
