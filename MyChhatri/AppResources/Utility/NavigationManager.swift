//
//  NavigationManager.swift
//  my digital nrg
//
//  Created by Tushar Agarwal on 18/01/17.
//  Copyright © 2017 Wildnet Technologies Pvt. Ltd. All rights reserved.
//

import UIKit

class NavigationManager: NSObject {

    class func moveToHomeViewControllerIfNotExists(_ navigationController : UINavigationController) {
        let viewControllers = navigationController.viewControllers
        if viewControllers.count > 0 {
            if !(viewControllers.last is MyPageBaseViewController) {
                navigationController.pushViewController(UIStoryboard.instantiateMyPagesBaseViewController(), animated: false)
            }
        }
    }
    
 
    class func moveToStreamSelectionViewControllerIfNotExists(_ navigationController : UINavigationController ,categoryId :String , title : String, subtitle : String , imageURL : String?) {
        let viewControllers = navigationController.viewControllers
        if viewControllers.count > 0 {
            if !(viewControllers.last is StreamSelectionViewController) {

                let viewStream = UIStoryboard.instantiateStreamSelectionViewController()
                viewStream.streamId = categoryId
                viewStream.controllerHeading = title
                viewStream.controllerSubHeading = subtitle
                viewStream.controllerHeadingImageURL = imageURL
                navigationController.pushViewController(viewStream, animated: false)
            }else {

                //Just change the nodeId reload the page
                (viewControllers.last as! StreamSelectionViewController).streamId = categoryId
                (viewControllers.last as! StreamSelectionViewController).controllerHeading = title
                (viewControllers.last as! StreamSelectionViewController).controllerHeadingImageURL = imageURL
                (viewControllers.last as! StreamSelectionViewController).viewWillAppear(true)

            }
        }
    }
    class func moveToSubjectSelectionViewControllerIfNotExists(_ navigationController : UINavigationController ,nodeId :String , SubjectList: NSDictionary? , rootCategoryID: String ,parentIDs: NSArray) {
        let viewControllers = navigationController.viewControllers
        if viewControllers.count > 0 {
            if !(viewControllers.last is SubjectSelectionViewController) {

                let subjectVC = UIStoryboard.instantiateSubjectSelectionViewController()
                subjectVC.subjectObject = SubjectList
                subjectVC.rootID = rootCategoryID
                subjectVC.pathIDs = parentIDs
                navigationController.pushViewController(subjectVC, animated: false)
            }else {

                //Just change the nodeId reload the page
                (viewControllers.last as! SubjectSelectionViewController).subjectObject =  SubjectList
                (viewControllers.last as! SubjectSelectionViewController).rootID =  rootCategoryID
                (viewControllers.last as! SubjectSelectionViewController).pathIDs =  parentIDs

                (viewControllers.last as! SubjectSelectionViewController).viewWillAppear(true)

            }
        }
    }
    class func moveToResetPasswordViewControllerViewControllerIfNotExists(_ navigationController : UINavigationController , userID: String) {
        let viewControllers = navigationController.viewControllers
        if viewControllers.count > 0 {
            if !(viewControllers.last is ResetPasswordViewController) {
                
                let restPasswordVC = UIStoryboard.instantiateResetPasswordViewController()
                restPasswordVC.userID = userID
                navigationController.pushViewController(restPasswordVC, animated: false)
            }else {
                
            }
        }
    }
    
    class func moveToCompanyListViewControllerIfNotExists(_ navigationController : UINavigationController) {
        let viewControllers = navigationController.viewControllers

        if viewControllers.count > 0 {

            if !(viewControllers.last is CompanyProfileListingViewController) {

                let shopVC = UIStoryboard.instantiateCompanyListViewController()
                navigationController.pushViewController(shopVC, animated: false)
            }
        }
    }
    class func moveToSavedSteamDetailsBaseViewControllerIfNotExists(_ navigationController : UINavigationController,  rootCategoryID: String? , otherIDsArray: [String]) {
        let viewControllers = navigationController.viewControllers
        
        if viewControllers.count > 0 {
            
            if !(viewControllers.last is SavedSteamDetailsBaseViewController) {
                let savedStreaDetail = UIStoryboard.instantiateSavedSteamDetailsBaseViewController()
                if let idTogetData = rootCategoryID{
                    savedStreaDetail.StreamID = idTogetData
                    savedStreaDetail.storedIds = otherIDsArray
                }
                navigationController.pushViewController(savedStreaDetail, animated: false)
            }
        }
    }

    class func moveToInterviewsViewControllerIfNotExists(_ navigationController : UINavigationController) {
        let viewControllers = navigationController.viewControllers
        
        if viewControllers.count > 0 {
            
            if !(viewControllers.last is TrendsViewController) {
                
                let interviewVC = UIStoryboard.instantiatInterviewsViewController()
                navigationController.pushViewController(interviewVC, animated: false)
            }
        }
    }
    class func moveToTermsViewControllerIfNotExists(_ navigationController : UINavigationController) {
        let viewControllers = navigationController.viewControllers
        
        if viewControllers.count > 0 {
            
            if !(viewControllers.last is AcceptTermsVC) {
                
                let interviewVC = UIStoryboard.instantiateAcceptTermsViewController()
                navigationController.pushViewController(interviewVC, animated: false)
            }
        }
    }
    class func moveToArticlesViewControllerIfNotExists(_ navigationController : UINavigationController) {
        let viewControllers = navigationController.viewControllers
        
        if viewControllers.count > 0 {
            
            if !(viewControllers.last is ArticlesViewController) {
                
                let articleVC = UIStoryboard.instantiatArticlesViewController()
                navigationController.pushViewController(articleVC, animated: false)
            }
        }
    }
    
    class func moveToInformativeViewControllerIfNotExists(_ navigationController : UINavigationController) {
        let viewControllers = navigationController.viewControllers
        
        if viewControllers.count > 0 {
            
            if !(viewControllers.last is InformativeViewController) {
                
                let articleVC = UIStoryboard.instantiatInformativeViewController()
                navigationController.pushViewController(articleVC, animated: true)
            }
        }
    }
    
    class func moveToDetailedViewControllerIfNotExists(_ navigationController : UINavigationController ,detailsArticle: ArticlesDatum? , informationDict:NSDictionary? ) {
        let viewControllers = navigationController.viewControllers
        
        if viewControllers.count > 0 {
            
            if !(viewControllers.last is DetailedViewController) {
                
                let articleVC = UIStoryboard.instantiatDetailedViewController()
                articleVC.articleInformation = detailsArticle
                articleVC.detailedInformation = informationDict
                navigationController.pushViewController(articleVC, animated: false)
            }
        }
    }
    class func moveToWebinarDetailedViewControllerIfNotExists(_ navigationController : UINavigationController  , informationDict:NSDictionary? ) {
        let viewControllers = navigationController.viewControllers
        
        if viewControllers.count > 0 {
            
            if !(viewControllers.last is WebinarDetailViewController) {
                
                let articleVC = UIStoryboard.instantiatWebinarDetailViewController()
                articleVC.detailedInformation = informationDict
                navigationController.pushViewController(articleVC, animated: false)
            }
        }
    }
    class func moveToVideoListingViewControllerIfNotExists(_ navigationController : UINavigationController) {
        let viewControllers = navigationController.viewControllers
        
        if viewControllers.count > 0 {
            
            if !(viewControllers.last is VideoListingViewController) {
                
                let articleVC = UIStoryboard.instantiatVideoListingViewController()
                navigationController.pushViewController(articleVC, animated: false)
            }
        }
    }
    
    class func moveToVideoPlayerViewControllerIfNotExists(_ navigationController : UINavigationController ,  details: NSDictionary?) {
        let viewControllers = navigationController.viewControllers
        
        if viewControllers.count > 0 {
            
            if !(viewControllers.last is VideoPlayerViewController) {
                
                let vedioVC = UIStoryboard.instantiatVideoPlayerViewController()
                vedioVC.vedioDetails = details!
                navigationController.pushViewController(vedioVC, animated: false)
            }
        }
    }
    class func moveToEditProfileViewControllerIfNotExists(_ navigationController : UINavigationController, userDetail: NSDictionary? ) {
        let viewControllers = navigationController.viewControllers
        
        if viewControllers.count > 0 {
            
            if !(viewControllers.last is EditProfileViewController) {
                
                let editPVC = UIStoryboard.instantiateEditProfileViewController()
                editPVC.userDetailDictionary = userDetail
                navigationController.pushViewController(editPVC, animated: false)
            }
        }
    }
    class func moveToAllCoursesListingViewControllerIfNotExists(_ navigationController : UINavigationController, courseDictDetail: NSDictionary? ) {
        let viewControllers = navigationController.viewControllers
        
        if viewControllers.count > 0 {
            
            if !(viewControllers.last is AllCoursesListingViewController) {
                
                let courselistPVC = UIStoryboard.instantiateAllCoursesListingViewController()
                courselistPVC.courseDetailDictionary = courseDictDetail
                navigationController.pushViewController(courselistPVC, animated: false)
            }
        }
    }
    class func moveToCourseDetailViewControllerIfNotExists(_ navigationController : UINavigationController, courseId: String ) {
        let viewControllers = navigationController.viewControllers
        
        if viewControllers.count > 0 {
            
            if !(viewControllers.last is CourseDetailViewController) {
                
                let courselistPVC = UIStoryboard.instantiateCourseDetailViewController()
                courselistPVC.courseID = courseId
                navigationController.pushViewController(courselistPVC, animated: false)
            }
        }
    }
    class func moveToMyEnrollmentViewControllerIfNotExists(_ navigationController : UINavigationController, enrolmentID: String , payStatus: String ) {
        let viewControllers = navigationController.viewControllers
        
        if viewControllers.count > 0 {
            
            if !(viewControllers.last is MyEnrollmentViewController) {
                
                let courselistPVC = UIStoryboard.instantiateMyEnrollmentViewController()
                courselistPVC.enrollmentId = enrolmentID
                courselistPVC.paymentStatus = payStatus
                navigationController.pushViewController(courselistPVC, animated: false)
            }
        }
    }
    
    class func moveToMyEnrollmentListViewControllerIfNotExists(_ navigationController : UINavigationController ) {
        let viewControllers = navigationController.viewControllers
        
        if viewControllers.count > 0 {
            
            if !(viewControllers.last is MyEnrollmentListViewController) {
                
                let courselistPVC = UIStoryboard.instantiateMyEnrollmentListViewController()
                navigationController.pushViewController(courselistPVC, animated: false)
            }
        }
    }
    class func moveToWishListViewControllerIfNotExists(_ navigationController : UINavigationController) {
        let viewControllers = navigationController.viewControllers
        
        if viewControllers.count > 0 {
            
            if !(viewControllers.last is WishListViewController) {
                
                let courselistPVC = UIStoryboard.instantiateWishListViewController()
                navigationController.pushViewController(courselistPVC, animated: false)
            }
        }
    }
    
    class func moveToWishListDetailViewControllerIfNotExists(_ navigationController : UINavigationController, enrolmentID: String ) {
        let viewControllers = navigationController.viewControllers
        
        if viewControllers.count > 0 {
            
            if !(viewControllers.last is WishListDetailViewController) {
                
                let courselistPVC = UIStoryboard.instantiateWishListDetailViewController()
                courselistPVC.enrollmentId = enrolmentID
                navigationController.pushViewController(courselistPVC, animated: false)
            }
        }
    }
    class func moveToRatingViewControllerIfNotExists(_ navigationController : UINavigationController, orderID: String ) {
        let viewControllers = navigationController.viewControllers
        
        if viewControllers.count > 0 {
            
            if !(viewControllers.last is RatingViewController) {
                
                let courselistPVC =  UIStoryboard.instantiateRatingViewController()
                courselistPVC.courseID = orderID
                navigationController.pushViewController(courselistPVC, animated: false)
            }
        }
    }
    
    class func moveToContestQuestionsViewControllerIfNotExists(_ navigationController : UINavigationController, contestId: String , time: Int) {
        
        let viewControllers = navigationController.viewControllers
        
        if viewControllers.count > 0 {
            
            if !(viewControllers.last is ContestQuestionsViewController) {
                
                let courselistPVC =  UIStoryboard.instantiateContestQuestionsViewController()
                courselistPVC.contestID = contestId
                courselistPVC.contestTime = time
                navigationController.pushViewController(courselistPVC, animated: false)
            }
        }
    }
    
    class func moveToAboutUsViewControllerIfNotExists(_ navigationController : UINavigationController) {
        let viewControllers = navigationController.viewControllers
        
        if viewControllers.count > 0 {
            
            if !(viewControllers.last is AboutUsViewController) {
                
                let interviewVC = UIStoryboard.instantiateAboutUsViewController()
                navigationController.pushViewController(interviewVC, animated: false)
            }
        }
    }
    
    class func moveToContactUsViewControllerIfNotExists(_ navigationController : UINavigationController) {
        let viewControllers = navigationController.viewControllers
        
        if viewControllers.count > 0 {
            
            if !(viewControllers.last is ContactUsViewController) {
                
                let interviewVC = UIStoryboard.instantiateContactUsViewController()
                navigationController.pushViewController(interviewVC, animated: false)
            }
        }
    }
    
    class func moveToFilterCompanyProfileViewControllerIfNotExists(_ navigationController : UINavigationController , fileterDel : FilterParamDelegate) {
        let viewControllers = navigationController.viewControllers
        
        if viewControllers.count > 0 {
            
            //call willAppear for filter view
            for controller in navigationController.viewControllers as Array {
                if controller.isKind(of: FilterCompanyProfileViewController.self) {
                    (controller as! FilterCompanyProfileViewController).viewWillAppear(true)
                    
                    
                    // self.navigationController!.popToViewController(controller, animated: true)
                    break
                }
            }
            
        }
        let filterVC = UIStoryboard.instantiateFilterCompanyProfileViewController()
        filterVC.filterDelegate = fileterDel
        navigationController.pushViewController(filterVC, animated: false)
    }
    
    class func moveToContestResultViewControllerIfNotExists(_ navigationController : UINavigationController, resultDictDetail: NSDictionary? ) {
        let viewControllers = navigationController.viewControllers
        
        if viewControllers.count > 0 {
            
            if !(viewControllers.last is ContestResultViewController) {
                
                let courselistPVC = UIStoryboard.instantiateContestResultViewController()
                courselistPVC.resultDictionary = resultDictDetail
                navigationController.pushViewController(courselistPVC, animated: false)
            }
        }
    }
    
    class func moveToRedeemPointsViewControllerIfNotExists(_ navigationController : UINavigationController, resultDictDetail: NSDictionary? ) {
        let viewControllers = navigationController.viewControllers
        
        if viewControllers.count > 0 {
            
            if !(viewControllers.last is RedeemPointsViewController) {
                
                let redeemlistPVC = UIStoryboard.instantiateRedeemPointsViewController()
                redeemlistPVC.resultDictionary = resultDictDetail
                navigationController.pushViewController(redeemlistPVC, animated: false)
            }
        }
    }
    
    class func moveToGiftViewControllerIfNotExists(_ navigationController : UINavigationController, resultDictDetail: NSDictionary? ) {
        let viewControllers = navigationController.viewControllers
        
        if viewControllers.count > 0 {
            
            if !(viewControllers.last is GiftViewController) {
                
                let redeemlistPVC = UIStoryboard.instantiateGiftViewController()
                redeemlistPVC.resultDictionary = resultDictDetail
                navigationController.pushViewController(redeemlistPVC, animated: false)
            }
        }
    }
}
