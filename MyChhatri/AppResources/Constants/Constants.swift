//
//  Constants.swift
//  Template
//
//  Created by Rajeev on 23/01/17.
//  Copyright © 2017 Rajeev. All rights reserved.
//

import UIKit
import CoreLocation

struct URLS {
    

    static let BASE_URL =       kAppDelegate.kBOOL_LIVE ? "http://mychatri.com/testing/api/"  : "http://111.93.53.59/mychhatriweb/api/"
    
    static let IMAGE_BASE_URL =  kAppDelegate.kBOOL_LIVE ? "http://mychatri.com/testing/" : "http://111.93.53.59/mychhatriweb/"
    
    static let SPECIALIZATION = "collection/"
    static let SUBSPECIALIZATION = "collection/1/categories"
    static let LOGIN = "resource/login"
    static let REGISTER = "resource/register"
    static let STREAM = "collection/stream"
    static let HOMESTREAM = "collection/homestream"
    static let SAVEDSTREAM = "resource/stream/getusersavedtopic"
    static let REGISTER_SPECIALIZATION = "/categories"
    static let COUNTRY = "resourcecumcollection/country"
    static let CITY = "resourcecumcollection/city"
    static let STATE = "resourcecumcollection/state"
    static let GET_OTP = "resource/otp"
    static let VERIFY_OTP = "resource/verifyotp"
    static let RESET_PASSWORD =  "resource/reset-password"
    static let INSTITUTE_TYPE = "collection/institute-type"
    static let SAVE_TOPIC1 = "resource/stream/saveusertopic"
    static let SAVE_TOPIC = "resource/stream/saveusertopictemp"
    static let GET_ARTICLES = "collection/articles"
    static let GET_INTERVIEW_VIDEO = "collection/interviews"
    static let REMOVE_TOPIC = "resource/stream/removesavedusertopic"
    static let GET_USERDETAILS = "resource/user"
    static let GET_WEBINARS = "collection/webinars"
    static let GET_INNOVATION = "collection/innovations"
    static let GET_SEMINAR = "collection/seminars"
    static let GET_OPPORTUNITY = "collection/opportunities"
    static let GET_TRENDS = "collection/trends"
    static let GET_Company = "collection/vendors"
    static let IS_STREAMSAVED = "resource/stream/getusertopicstatus"
    static let COURSEDETAIL = "resource/coursedetails"
    static let ENROLL_BATCH = "resource/enrollbatch"
    static let ADD_BATCH_WISHLIST = "resource/addbatchinwishlist"
    static let USER_ENROLLED_BATCHES = "resource/userenrolledbatches"
    static let USER_ENROLLED_BATCHES_DETAILS = "resource/enrolledbatchedetails"
    static let USER_BATCHES_WISHLIST =   "resource/userwishlistbatches"
     static let REMOVE_FROM_WISHLIST = "resource/removewishlistbatch"
    static let USER_BATCHES_WISHLIST_DETAILS =  "resource/wishlistbatchedetails"
    static let CANCEL_ENROLLED_BATCH = "resource/cancelenrolledbatche"
    static let GET_NEWS = "collection/news"
    static let SEARCH = "collection/vendors"
    static let ABOUTUS = "collection/cmspage?slug=about-us"
    static let CONTACTUS = "collection/contactus"
    static let ADVERTISE = "collection/advertisement"
    static let TABCOUNT = "collection/topicdatacountbyid"
    static let SESSIONEXP = "collection/checkuserlogin"
    static let COMPANYCITY = "collection/getcoursecity"
    static let UPDATEUSER =  "resource/updateprofile"
    static let UPDATEEMAILORMOBILE = "collection/sendverificationemailormobile"
    static let VERIFYEMAILORMOBILE = "collection/verifyemailormobile"
    static let RATINGQUE = "collection/getratingquestions"
    static let SUBMITRATING = "collection/saveuserratings"
    static let CONTESTLIST = "online/contests/list"
    static let CONTESTQUESTIONLIST = "online/contest/questions/list"
    static let CONTESTQUESTIONSAVE = "online/contest/save"
    static let CONTESTGIFTLIST = "contest/gifts/list"
    static let CONTESTADDADDRESS = "user/address/add"
    static let CONTESTGIFTREQUEST = "user/gift/request"
    static let CONTESTPLAYEDLIST = "online/played/contests"

}

let APP_NAME = "Mychatri"
let kAppDelegate     =    UIApplication.shared.delegate as! AppDelegate
let screenWidth = UIScreen.main.bounds.size.width
let screenHeight = UIScreen.main.bounds.size.height
let appDelegate = UIApplication.shared.delegate as! AppDelegate

let CLIENTID =  kAppDelegate.kBOOL_LIVE ?"9":"16"
let CLIENTSECRET =  kAppDelegate.kBOOL_LIVE ? "BqpRsBdQhfSWNnK94NcCj3R5wBuoC0NNI6OB8zur" : "k87ciDnEFUr89TDL52YsjfRLu0vpgTCBZp3Qkj2Y"
var DEVICE_TOKEN = ""

let userDefaults : UserDefaults = UserDefaults.standard

let isIphone5 = UIScreen.main.bounds.size.width > 320 ? false : true

let ISIphoneX: Bool   =   (UIScreen.main.bounds.size.height == 812.0)

var sendOTPCount = 0

var popedControllerPageIndex = 0 //To maintain the screen. When user pops back after saving his stream, it should move to Saved Stream instead of carrer Option

struct ColorConstants {
    
    static func RGBCOLOR(_ red: Int, green: Int, blue: Int) -> UIColor {
        
        return UIColor(red: CGFloat(red) / 255.0, green: CGFloat(green) / 255.0, blue: CGFloat(blue) / 255.0, alpha: 1)
    }
    static func textSelectedCOLOR() -> UIColor {
        
        return UIColor(red:0.77, green:0.53, blue:0.85, alpha:1)
    }
    
    static func textDeSelectedCOLOR() -> UIColor {
        
        return UIColor.white
    }
    static func appBackGroundCOLOR() -> UIColor {
        
        return UIColor(red:0.99, green:0.61, blue:0.22, alpha:1)
        
    }
    static func cellSelectionCOLOR() -> UIColor {
        
        return UIColor(red:0.99, green:0.61, blue:0.22, alpha:1)
        
    }
    static func btnBlueCOLOR() -> UIColor {
        
        return UIColor(red:0.00, green:0.47, blue:0.70, alpha:1)
        
    }
    
    
}

enum Language:String {
    case english
    case hindi
}

func getLanguageKey (lang : Language) -> String {
    
    switch lang {
        
    case .english: return "en"
    case .hindi:    return "hi"
   // case .malayalam:  return "ml-IN"
        
    }
}

struct Local_DB_Key {
    static let language = "current_language"
    
}

struct AlertMessages {
    
  static let MODELERRORMESSAGE = "Response structure has been changed."
  static let NOTADICTIONARYMESSAGE = "Response is not a dictionary."
  static let UnidetifiedErrorMessage = "Something went wrong!!"
  static let NoSavedStream   = "You do not have any saved stream yet."
  static let NoDataKeyFoundInResponse   = "data key is not present in response"

    
}

//PRAGMA MARK:- Set 6 month from today's date, to show month calender
struct CalenderStruct {
    
    static let  DATE_TIME_ZONE_FORMAT_LOCAL   =  "yyyy-MM-dd HH:mm:ss"
    
   static func setUPCalenderMonth() -> Array<String> {
        
        let monthsToAdd = 6
        let daysToAdd = 0
        let yearsToAdd = 0
        let currentDate = getCurrentDate()
        
        var dateComponent = DateComponents()
        
        dateComponent.month = monthsToAdd
        dateComponent.day = daysToAdd
        dateComponent.year = yearsToAdd
        
        var newDateComponent = DateComponents()
        let futureDate = Calendar.current.date(byAdding: dateComponent, to: currentDate)
        
        print(currentDate)
        print(futureDate!)
        
        let flags = NSCalendar.Unit.month
        let calendar = NSCalendar.current as NSCalendar
        let components = calendar.components(flags, from: currentDate, to: futureDate!)
        let numberOfMonthBetweenFirstAndLastDate = components.month
        let fmt = DateFormatter()
        fmt.dateFormat = "MMMM"
        var monthArray = Array<String>()
        for i in 0..<numberOfMonthBetweenFirstAndLastDate! {
            newDateComponent.month = i
            let nextDate =  calendar.date(byAdding: newDateComponent, to: currentDate, options: NSCalendar.Options(rawValue: 0))
            monthArray.append(fmt.string(from: nextDate!))
        }
        return monthArray
    }
    
    static func setUPCalenderYear() -> Array<String> {
        
        let monthsToAdd = 0
        let daysToAdd = 0
        let yearsToAdd = 10
        
        var dateComponent = DateComponents()
        
        dateComponent.month = monthsToAdd
        dateComponent.day = daysToAdd
        dateComponent.year = yearsToAdd
        
        var newDateComponent = DateComponents()

        let backDate = Calendar.current.date(byAdding: .year, value: -5, to: Date())
        let laterDate = Calendar.current.date(byAdding: .year, value: 5, to: Date())

        
        let flags = NSCalendar.Unit.year
        let calendar = NSCalendar.current as NSCalendar
        let components = calendar.components(flags, from: backDate!, to: laterDate!)
        let numberOfMonthBetweenFirstAndLastDate = components.year
        let fmt = DateFormatter()
        fmt.dateFormat = "YYYY"
        var yearArray = Array<String>()
        for i in 0..<numberOfMonthBetweenFirstAndLastDate! {
            newDateComponent.year = i
            let nextDate =  calendar.date(byAdding: newDateComponent, to: backDate!, options: NSCalendar.Options(rawValue: 0))
            yearArray.append(fmt.string(from: nextDate!))
        }
        return yearArray
    }

   static func getCurrentDate()-> Date {
        var now = Date()
        var nowComponents = DateComponents()
        let calendar = Calendar.current
        nowComponents.year = Calendar.current.component(.year, from: now)
        nowComponents.month = Calendar.current.component(.month, from: now)
        nowComponents.day = Calendar.current.component(.day, from: now)
        now = calendar.date(from: nowComponents)!
        return now as Date
    }
    
    static func getFormatedDateString(from date: Date?, inFormat strFormat: String?) -> String? {
        let df = DateFormatter()
        df.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale
        df.dateFormat = strFormat ?? ""
        //[df setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
        if let aDate = date {
            return df.string(from: aDate)
        }
        return nil
    }
    
    static func getFormatedDateStringWithouOrdinal(fromDateUTC date: Date?, inFormat strFormat: String?) -> String? {
        
        let df = DateFormatter()
        df.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale
        //[df setLocale:[NSLocale currentLocale]];
        df.dateFormat = strFormat ?? ""
        
        
        let ordinalFormatter = NumberFormatter()
        ordinalFormatter.numberStyle = .ordinal
        let day = Calendar.current.component(.day, from: date!)
        let dayOrdinal = ordinalFormatter.string(from: NSNumber(value: day))!
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "'\(dayOrdinal)' MMM YYYY"
        
        //[df setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
        if let aDate = date {
            // return df.string(from: aDate)
            return dateFormatter.string(from: aDate)
        }
        return nil
    }
    
    static func getFormatedDateString(fromDateUTC date: Date?, inFormat strFormat: String?) -> String? {
        
        let df = DateFormatter()
        df.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale
        //[df setLocale:[NSLocale currentLocale]];
        df.dateFormat = strFormat ?? ""
        
        
        let ordinalFormatter = NumberFormatter()
        ordinalFormatter.numberStyle = .ordinal
        let day = Calendar.current.component(.day, from: date!)
        let dayOrdinal = ordinalFormatter.string(from: NSNumber(value: day))!
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "'\(dayOrdinal)' MMMM YYYY"
        
        //[df setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
        if let aDate = date {
           // return df.string(from: aDate)
            return dateFormatter.string(from: aDate)
        }
        return nil
    }
    
    static func getDateFromDateString(forUTC dateString: String?, byFormat strFormat: String?) -> Date? {
        let df = DateFormatter()
        //[df setLocale:[NSLocale currentLocale]];
        df.dateFormat = strFormat ?? ""
        df.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale
        if let anAbbreviation = NSTimeZone(abbreviation: "UTC") {
            df.timeZone = anAbbreviation as TimeZone
        }
        return df.date(from: dateString ?? "")
    }
}
/**
 Specifies one of the controller added on Home screen.
 */
public enum Controller : Int {
    
    case careerOption , MySavedStream
    
}
//PRAGMA MARK:- SETTING MENU ITEMS
public enum SettingMenu : Int {
    
    case Language , Courses , MyWishList , MySubscription ,MyRewards , AboutApp , ContactUs , ChangePassword
    
}
enum Days: String {
    
    case Sunday , Monday ,Tuesday , WednesDay , Thursday , Friday , Saturday

}

func loadDayName(forDay: Int) -> String{
    
    switch forDay {
    case 0:
        return "Sun"
    case 1:
        return "Mon"
    case 2:
        return "Tues"
    case 3:
        return "Wed"
    case 4:
        return "Thur"
    case 5:
        return "Fri"
    case 6:
        return "Sat"
    default:
        return ""
    }
}

