//
//  CountryView.swift
//  Bizzalley
//
//  Created by Anshul on 16/03/17.
//  Copyright © 2017 Anuj Jha. All rights reserved.
//

import UIKit
import SDWebImage

@objc  protocol CountryViewDelegate {
    
    @objc  func removeCountryView()
    @objc  func getCountryCode(data: NSDictionary , type:Int)
    @objc optional func otherEntry()
    
}

class CountryView: UIView {
    
    @IBOutlet weak var containerView:UIView!
    @IBOutlet weak var tableView:UITableView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var btnOther: UIButton!
    // Search setup...
    @IBOutlet weak var searchBar: UISearchBar!
    var searchActive : Bool = false
    var filtered: NSArray?
    
    var delegate:CountryViewDelegate?
    var countryViewArray:NSMutableArray?
    var selectedIndex:NSInteger?
    
    //MARK:- Intializer
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    override func draw(_ rect: CGRect) {
        super.draw(rect)
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func registerNibs(){
        
        //print("selectedIndex :\(selectedIndex)")
        self.tableView.register(UINib.init(nibName: "CategoryTableViewCell", bundle: nil), forCellReuseIdentifier: "CategoryTableViewCell")
        
        //UISearchBar left alignment setup...
        // 320:43  375:57  414:67
        let spaceCount = UIScreen.main.bounds.size.width == 320 ? 43 : UIScreen.main.bounds.size.width == 375 ? 57 : 67
        let newText = "Search \((Array(repeating: " ", count: Int(spaceCount)).joined(separator: "")))"
        searchBar.placeholder = newText
        
        tableView.estimatedRowHeight = 60
        tableView.rowHeight = UITableViewAutomaticDimension        //containerView.addGrediant()
      
    }
    
    func managerRadius(){
        containerView.layer.cornerRadius = 5
    }
    
    @IBAction func outSideTapped(sender:UIButton){
        if delegate != nil {
            delegate?.removeCountryView()
        }
    }
    
    //MARK:- GET COUNTRY FROM API
    
    func getAllCountries(){
        
        ProgressHUD.showProgressHUDWithLabel(label: "Loading...")
        
        APIManager.callGetWithoutAuthantication("\(URLS.BASE_URL)\(URLS.COUNTRY)", param: nil, header: ["":""]) { (isSuccess, result, error) in
            
            DispatchQueue.main.async {
                
                weak var weakSelf = self
                
                ProgressHUD.hideProgressHUD()
                
                if (error != nil) {
                    weakSelf?.showAlert(title: "Error", message: (error!.localizedDescription), withTag: 101, button: ["Ok"], isActionRequired: false)
                } else {
                    if isSuccess {
                        
                        do {
                            let jsonData = try JSONSerialization.jsonObject(with: result!, options: .mutableContainers)
                            
                            if let statusCode = (jsonData as! NSDictionary).object(forKey: "status"), (statusCode is Int) {
                                if (statusCode as! Int) == 200 {
                                    if let data = (jsonData as! NSDictionary).object(forKey: "data"), (data is NSArray) {
                                        if (data as! NSArray).count > 0 {
                                            weakSelf?.countryViewArray = (data as! NSArray).mutableCopy() as? NSMutableArray
                                            weakSelf?.tableView.reloadData()
                                        }
                                    }
                                } else {
                                    weakSelf?.showAlert(title: APP_NAME, message: "Something went wrong!!", withTag: 101, button: ["Ok"], isActionRequired: false)
                                }
                            }
                            
                            //print(jsonData as! NSDictionary)
                            
                        }catch{
                            
                            weakSelf?.showAlert(title: "sorry", message: AlertMessages.MODELERRORMESSAGE , withTag: 101, button:  ["Ok"], isActionRequired: false)
                            return
                        }
                        
                    } else{
                        weakSelf?.showAlert(title: APP_NAME, message: "Something went wrong!!", withTag: 101, button: ["Ok"], isActionRequired: false)
                    }
                }
            }
        }
        
    }
    
    func getAllCitiesHavingCompany(){
        
        ProgressHUD.showProgressHUDWithLabel(label: "Loading...")
        
        APIManager.callGet("\(URLS.BASE_URL)\(URLS.COMPANYCITY)", param: [:], header: ["":""]) { (isSuccess, result, error) in
            
            DispatchQueue.main.async {
                
                weak var weakSelf = self
                
                ProgressHUD.hideProgressHUD()
                
                if (error != nil) {
                    weakSelf?.showAlert(title: "Error", message: (error!.localizedDescription), withTag: 101, button: ["Ok"], isActionRequired: false)
                } else {
                    if isSuccess {
                        
                        do {
                            let jsonData = try JSONSerialization.jsonObject(with: result!, options: .mutableContainers)
                            
                            if let statusCode = (jsonData as! NSDictionary).object(forKey: "status"), (statusCode is Int) {
                                if (statusCode as! Int) == 200 {
                                    if let data = (jsonData as! NSDictionary).object(forKey: "data"), (data is NSArray) {
                                        if (data as! NSArray).count > 0 {
                                            weakSelf?.countryViewArray = (data as! NSArray).mutableCopy() as? NSMutableArray
                                            weakSelf?.tableView.reloadData()
                                        }
                                    }
                                } else {
                                    weakSelf?.showAlert(title: APP_NAME, message: "Something went wrong!!", withTag: 101, button: ["Ok"], isActionRequired: false)
                                }
                            }
                            
                            //print(jsonData as! NSDictionary)
                            
                        }catch{
                            
                            weakSelf?.showAlert(title: "sorry", message: AlertMessages.MODELERRORMESSAGE , withTag: 101, button:  ["Ok"], isActionRequired: false)
                            return
                        }
                        
                    } else{
                        weakSelf?.showAlert(title: APP_NAME, message: "Something went wrong!!", withTag: 101, button: ["Ok"], isActionRequired: false)
                    }
                }
            }
        }
        
    }
    //MARK:- GET STATE FROM API
    
    func getAllStates(countryId: String){
        
        ProgressHUD.showProgressHUDWithLabel(label: "Loading...")
        
        APIManager.callGetWithoutAuthantication("\(URLS.BASE_URL)\(URLS.STATE)/\(countryId)", param: nil, header: ["":""]) { (isSuccess, result, error) in
            
            DispatchQueue.main.async {
                
                weak var weakSelf = self
                
                ProgressHUD.hideProgressHUD()
                
                if (error != nil) {
                    weakSelf?.showAlert(title: "Error", message: (error!.localizedDescription), withTag: 101, button: ["Ok"], isActionRequired: false)
                } else {
                    if isSuccess {
                        
                        do {
                            let jsonData = try JSONSerialization.jsonObject(with: result!, options: .mutableContainers)
                            
                            if let statusCode = (jsonData as! NSDictionary).object(forKey: "status"), (statusCode is Int) {
                                if (statusCode as! Int) == 200 {
                                    if let data = (jsonData as! NSDictionary).object(forKey: "data"), (data is NSArray) {
                                        if (data as! NSArray).count > 0 {
                                            weakSelf?.countryViewArray = (data as! NSArray).mutableCopy() as? NSMutableArray
                                            weakSelf?.tableView.reloadData()
                                        }
                                    }
                                } else {
                                    weakSelf?.showAlert(title: APP_NAME, message: "Something went wrong!!", withTag: 101, button: ["Ok"], isActionRequired: false)
                                }
                            }
                            
                            //print(jsonData as! NSDictionary)
                            
                        }catch{
                            
                            weakSelf?.showAlert(title: "sorry", message: AlertMessages.MODELERRORMESSAGE , withTag: 101, button:  ["Ok"], isActionRequired: false)
                            return
                        }
                        
                    } else{
                        weakSelf?.showAlert(title: APP_NAME, message: "Something went wrong!!", withTag: 101, button: ["Ok"], isActionRequired: false)
                    }
                }
            }
        }
        
    }
    
    //MARK:- GET CITY FROM API
    
    func getAllCities(stateid: String){
        
        ProgressHUD.showProgressHUDWithLabel(label: "Loading...")
        
        APIManager.callGetWithoutAuthantication("\(URLS.BASE_URL)collection/\(stateid)/city", param: nil, header: ["":""]) { (isSuccess, result, error) in
            
            DispatchQueue.main.async {
                
                weak var weakSelf = self
                
                ProgressHUD.hideProgressHUD()
                
                if (error != nil) {
                    weakSelf?.showAlert(title: "Error", message: (error!.localizedDescription), withTag: 101, button: ["Ok"], isActionRequired: false)
                } else {
                    if isSuccess {
                        
                        do {
                            let jsonData = try JSONSerialization.jsonObject(with: result!, options: .mutableContainers)
                            
                            if let statusCode = (jsonData as! NSDictionary).object(forKey: "status"), (statusCode is Int) {
                                if (statusCode as! Int) == 200 {
                                    if let data = (jsonData as! NSDictionary).object(forKey: "data"), (data is NSArray) {
                                        if (data as! NSArray).count > 0 {
                                            weakSelf?.countryViewArray = (data as! NSArray).mutableCopy() as? NSMutableArray
                                            weakSelf?.tableView.reloadData()
                                        }
                                    }
                                } else {
                                    weakSelf?.showAlert(title: APP_NAME, message: "Something went wrong!!", withTag: 101, button: ["Ok"], isActionRequired: false)
                                }
                            }
                            
                            //print(jsonData as! NSDictionary)
                            
                        }catch{
                            
                            weakSelf?.showAlert(title: "sorry", message: AlertMessages.MODELERRORMESSAGE , withTag: 101, button:  ["Ok"], isActionRequired: false)
                            return
                        }
                        
                    } else{
                        weakSelf?.showAlert(title: APP_NAME, message: "Something went wrong!!", withTag: 101, button: ["Ok"], isActionRequired: false)
                    }
                }
            }
        }
        
    }
    
    
    func getAllColleges(cityid: String){
        
        ProgressHUD.showProgressHUDWithLabel(label: "Loading...")
        
        APIManager.callGetWithoutAuthantication("\(URLS.BASE_URL)collection/colleges?city_id=\(cityid)", param: nil, header: ["":""]) { (isSuccess, result, error) in
            
            DispatchQueue.main.async {
                
                weak var weakSelf = self
                
                ProgressHUD.hideProgressHUD()
                
                if (error != nil) {
                    weakSelf?.showAlert(title: "Error", message: (error!.localizedDescription), withTag: 101, button: ["Ok"], isActionRequired: false)
                } else {
                    if isSuccess {
                        
                        do {
                            let jsonData = try JSONSerialization.jsonObject(with: result!, options: .mutableContainers)
                            
                            if let statusCode = (jsonData as! NSDictionary).object(forKey: "status"), (statusCode is Int) {
                                if (statusCode as! Int) == 200 {
                                    if let data = (jsonData as! NSDictionary).object(forKey: "data"), (data is NSArray) {
                                        if (data as! NSArray).count > 0 {
                                            weakSelf?.countryViewArray = (data as! NSArray).mutableCopy() as? NSMutableArray
                                            weakSelf?.tableView.reloadData()
                                        }
                                    }
                                } else {
                                    weakSelf?.showAlert(title: APP_NAME, message: "Something went wrong!!", withTag: 101, button: ["Ok"], isActionRequired: false)
                                }
                            }
                            
                            //print(jsonData as! NSDictionary)
                            
                        }catch{
                            
                            weakSelf?.showAlert(title: "sorry", message: AlertMessages.MODELERRORMESSAGE , withTag: 101, button:  ["Ok"], isActionRequired: false)
                            return
                        }
                        
                    } else{
                        weakSelf?.showAlert(title: APP_NAME, message: "Something went wrong!!", withTag: 101, button: ["Ok"], isActionRequired: false)
                    }
                }
            }
        }
        
    }
    //MARK:- IBAction
    
    @IBAction func doneBtnTapped(sender:UIButton){
        if delegate != nil {
            delegate?.removeCountryView()
            if selectedIndex != nil {
                
                //let countryId = (countryViewArray?[selectedIndex!] as! NSDictionary).object(forKey: "CountryId") as! Int
                
                //delegate?.getCountryCode(countryID: String(describing: countryId))
            }
        }
    }
    
     @IBAction func otherBtnTapped(sender:UIButton){
        if delegate != nil {
            delegate?.removeCountryView()
            delegate?.otherEntry!()
        }
    }
    //MARK:- Custom Methods
    
    func showAlertMessgae(_ message: String, _ tag : Int) {
        
        let alertController = UIAlertController(title: APP_NAME, message: message, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "Ok", style: .default, handler: { action in
            
            if tag == 0 {
                
            }
            
        }))
        
        appDelegate.menuNavController?.present(alertController, animated: true, completion: nil)
    }
}

extension CountryView : UITableViewDelegate,UITableViewDataSource,UISearchBarDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var count = 0
        
        if(searchActive) {
            return filtered!.count
        }
        
        if let array = countryViewArray {
            count = array.count
        }
        return count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CategoryTableViewCell", for: indexPath) as? CategoryTableViewCell
        cell?.selectionStyle = .none
        
        if (searchActive) {
            let titleText = (filtered?[indexPath.row] as! NSDictionary).object(forKey: "name")
            if tag == 4 {
                //Show college name with city
                if let city = (countryViewArray?[indexPath.row] as! NSDictionary).object(forKey: "city") as? String{
                    cell?.titleLbl.text = "\(titleText!) - \(city)"
                }
            }else{
                cell?.titleLbl.text = titleText as! String?
            }
            
            
        } else {
            
            let titleText = (countryViewArray?[indexPath.row] as! NSDictionary).object(forKey: "name")
            if tag == 4 {
             //Show college name with city
                if let city = (countryViewArray?[indexPath.row] as! NSDictionary).object(forKey: "city") as? String{
                    cell?.titleLbl.text = "\(titleText!) - \(city)"
                }
            }else{
            cell?.titleLbl.text = titleText as! String?
            }
        }
        
        return cell!
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        tableView.separatorInset = UIEdgeInsets.zero
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedIndex = indexPath.row
        
        if(searchActive) {
            
            //let countryID = (filtered?[indexPath.row] as! NSDictionary).object(forKey: "CountryId")
            //let countryName = (filtered?[indexPath.row] as! NSDictionary).object(forKey: "CountryName")
            
            delegate?.getCountryCode(data: filtered?[indexPath.row] as! NSDictionary , type: self.tag)
            
            //delegate?.getCountryCode(countryID: "\(countryID!)" as NSString, countryName: "\(countryName!)" as NSString)
        } else {
            
//            let countryID = (countryViewArray?[indexPath.row] as! NSDictionary).object(forKey: "CountryId")
//            let countryName = (countryViewArray?[indexPath.row] as! NSDictionary).object(forKey: "CountryName")
//            delegate?.getCountryCode(countryID: "\(countryID!)" as NSString, countryName: "\(countryName!)" as NSString)
            
            delegate?.getCountryCode(data: countryViewArray?[indexPath.row] as! NSDictionary , type: self.tag)
        }
        
        if delegate != nil {
            delegate?.removeCountryView()
        }
    }
    
    //MARK:- UISearchBarDelegate
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        //searchActive = true;
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchActive = false;
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchActive = false;
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        self.endEditing(true)
        searchActive = false;
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        if countryViewArray != nil {
            
            let predicate = NSPredicate(format: "name contains[c] %@", searchText.trim())
            
            filtered = countryViewArray?.filtered(using: predicate) as NSArray?
            
            //print("----------------------- \(filtered?.count)")
            //print("----------------------- \(filtered!)")
            
            if(searchText.count == 0){
                searchActive = false;
            } else {
                searchActive = true;
            }
            self.tableView.reloadData()
        }
        
    }
}

