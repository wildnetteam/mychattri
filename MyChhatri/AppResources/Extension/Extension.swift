//
//  ViewController.swift
//  Template
//
//  Created by Rajeev on 23/01/17.
//  Copyright © 2017 Rajeev. All rights reserved.
//


import UIKit
import SYPhotoBrowser

extension UIViewController {
    
    // UIAlertController
    func showAlert(title:String, message:String, withTag:Int, button:Array<String>, isActionRequired:Bool) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        if isActionRequired {
            for i in 0..<button.count {
                if button[i] == "Ok" {
                    alertController.addAction(UIAlertAction(title: button[i], style: .default, handler: { (action) in
                        self.alertButtonAction(alertTag: withTag, btnTitle: button[i])
                    }))
                } else if button[i].contains("Delete") {
                    alertController.addAction(UIAlertAction(title: button[i], style: .destructive, handler: { (action) in
                        self.alertButtonAction(alertTag: withTag, btnTitle: button[i])
                    }))
                } else {
                    alertController.addAction(UIAlertAction(title: button[i], style: .cancel, handler: { (action) in
                        self.alertButtonAction(alertTag: withTag, btnTitle: button[i])
                    }))
                }
            }
        } else {
            alertController.addAction(UIAlertAction(title: button[0], style: .default, handler: nil))
        }
        if let  navControler = appDelegate.menuNavController {
            
            navControler.present(alertController, animated: true, completion: nil)
        }else{
            
            self.navigationController?.present(alertController, animated: true, completion: nil)
        }
    }
    
    @objc func alertButtonAction(alertTag:Int, btnTitle:String) {
        
    }
    
    // .............................full screen image setup............................//
    // display image in full view...
    func showImageInScroll(imageURIArray : NSArray, pageIndex: Int) {
        let photobrowser = SYPhotoBrowser(imageSourceArray: imageURIArray as! [Any], caption: "")
        photobrowser?.initialPageIndex = UInt(pageIndex)
        photobrowser?.pageControlStyle = SYPhotoBrowserPageControlStyle(rawValue: 1)!
        self.present(photobrowser!, animated: true, completion: nil)
    }
    // .............................full screen image setup............................//
    
}

extension UIView {
    
    // UIAlertController
    func showAlert(title:String, message:String, withTag:Int, button:Array<String>, isActionRequired:Bool) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        if isActionRequired {
            for i in 0..<button.count {
                if button[i] == "Ok" {
                    alertController.addAction(UIAlertAction(title: button[i], style: .default, handler: { (action) in
                        self.alertButtonAction(alertTag: withTag, btnTitle: button[i])
                    }))
                } else if button[i].contains("Delete") {
                    alertController.addAction(UIAlertAction(title: button[i], style: .destructive, handler: { (action) in
                        self.alertButtonAction(alertTag: withTag, btnTitle: button[i])
                    }))
                } else {
                    alertController.addAction(UIAlertAction(title: button[i], style: .cancel, handler: { (action) in
                        self.alertButtonAction(alertTag: withTag, btnTitle: button[i])
                    }))
                }
            }
        } else {
            alertController.addAction(UIAlertAction(title: button[0], style: .default, handler: nil))
        }
        appDelegate.menuNavController?.present(alertController, animated: true, completion: nil)
    }
    
    @objc func alertButtonAction(alertTag:Int, btnTitle:String) {
        
    }
    
    // .............................full screen image setup............................//
    // display image in full view...
    func showImageInScroll(imageURIArray : NSArray, pageIndex: Int) {
        let photobrowser = SYPhotoBrowser(imageSourceArray: imageURIArray as! [Any], caption: "")
        photobrowser?.initialPageIndex = UInt(pageIndex)
        photobrowser?.pageControlStyle = SYPhotoBrowserPageControlStyle(rawValue: 1)!
        appDelegate.menuNavController?.present(photobrowser!, animated: true, completion: nil)
    }
    // .............................full screen image setup............................//
    
    
    
}

extension UIViewController {
    
    func getDateFormate(str:String)->String{
        let index = str.index(str.startIndex, offsetBy: 10)
        let st1 = String(str[..<index])
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "YYYY-mm-dd"
        let date1 = dateFormatter.date(from: st1)
        
        let dateFormatter2 = DateFormatter()
        dateFormatter2.dateFormat = "mm/dd/yyyy"
        return dateFormatter2.string(from: date1!)

    }
}

extension UIStoryboard {
    
    class func mainStoryBoard() -> UIStoryboard{
        
        if let currentLang = LocalDB.shared.currentLanguage {
            
            if currentLang == Language.english {
                
                  return UIStoryboard(name: "Main", bundle: Bundle.main)
                
            }else  {
                
                  return UIStoryboard(name: "Hindi_Storyboard", bundle: Bundle.main)
            }
        }else{
            
            LocalDB.shared.currentLanguage = Language.english
            return UIStoryboard(name: "Main", bundle: Bundle.main)

        }
        
//        if language == "hi"{
//
//            //Its Hindi
//
//            return UIStoryboard(name: "Hindi_Storyboard", bundle: Bundle.main)
//        }
//        else{
//            return UIStoryboard(name: "Main", bundle: Bundle.main)
//
//        }
    }
    
    class func instantiateLoginViewController()->LoginVC {
        
        return mainStoryBoard().instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
    }
    
    class func instantiateForgotPasswordViewController()->ForgotPasswordVC {
        
        return mainStoryBoard().instantiateViewController(withIdentifier: "ForgotPasswordVC") as! ForgotPasswordVC
    }
    
    class func instantiateRegisterStepOneViewController()->RegisterStepOneVC {
        
        return mainStoryBoard().instantiateViewController(withIdentifier: "RegisterStepOneVC") as! RegisterStepOneVC
    }
    
    class func instantiateRegisterStepTwoViewController()->RegisterStepTwoVC {
        
        return mainStoryBoard().instantiateViewController(withIdentifier: "RegisterStepTwoVC") as! RegisterStepTwoVC
    }
    
    class func instantiateVerifyOTPViewController()->VerifyOTPVC {
        
        return mainStoryBoard().instantiateViewController(withIdentifier: "VerifyOTPVC") as! VerifyOTPVC
    }
    
    class func instantiateOTPForMailOrPhoneViewController()->OTPForMailOrPhoneViewController {
        
        return mainStoryBoard().instantiateViewController(withIdentifier: "OTPForMailOrPhoneViewControllerID") as! OTPForMailOrPhoneViewController
    }
    //All Streama
    class func instantiateMyPagesViewController()->MyPagesVC {
        
        return mainStoryBoard().instantiateViewController(withIdentifier: "MyPagesVC") as! MyPagesVC
    }
    
    //Base where page menu is added
    class func instantiateMyPagesBaseViewController()->MyPageBaseViewController {
        return mainStoryBoard().instantiateViewController(withIdentifier: "MyPageBaseViewControllerID") as! MyPageBaseViewController
    }
    //Saved Streams
    class func instantiateMySavedPagesViewController()->MySavedpageViewController {
        
        return mainStoryBoard().instantiateViewController(withIdentifier: "MySavedpageViewControllerID") as! MySavedpageViewController
    }
    class func instantiateBlogsViewController()->BlogsVC {
        
        return mainStoryBoard().instantiateViewController(withIdentifier: "BlogsVC") as! BlogsVC
    }
    
    class func instantiateContestViewController()-> ContestViewController {
        
        return mainStoryBoard().instantiateViewController(withIdentifier: "ContestViewControllerID") as! ContestViewController
    }
    
    class func instantiateSearchViewController()-> SearchVC {
        
        return mainStoryBoard().instantiateViewController(withIdentifier: "SearchVC") as! SearchVC
    }
    
    class func instantiateNewsViewController()->NewsVC {
        
        return mainStoryBoard().instantiateViewController(withIdentifier: "NewsVC") as! NewsVC
    }
    
    class func instantiateProfileViewController()->ProfileVC {
        return mainStoryBoard().instantiateViewController(withIdentifier: "ProfileVC") as! ProfileVC
    }
    
    class func instantiateAcceptTermsViewController()->AcceptTermsVC {
        
        return mainStoryBoard().instantiateViewController(withIdentifier: "AcceptTermsVC") as! AcceptTermsVC
    }
    class func instantiateStreamSelectionViewController()->StreamSelectionViewController {
        return mainStoryBoard().instantiateViewController(withIdentifier: "StreamSelectionViewControllerID") as! StreamSelectionViewController
    }
    
    class func instantiateSubjectSelectionViewController()->SubjectSelectionViewController {
        
        return mainStoryBoard().instantiateViewController(withIdentifier: "SubjectSelectionViewControllerID") as! SubjectSelectionViewController
    }
    
    class func instantiateResetPasswordViewController()->ResetPasswordViewController {
        
        return mainStoryBoard().instantiateViewController(withIdentifier: "ResetPasswordViewControllerID") as! ResetPasswordViewController
    }
    class func instantiateCompanyListViewController()->CompanyProfileListingViewController {
        
        return mainStoryBoard().instantiateViewController(withIdentifier: "MyPageViewRegisterControllerID") as! CompanyProfileListingViewController
    }
    
    
    class func instantiateSavedSteamDetailsBaseViewController()->SavedSteamDetailsBaseViewController {
        
        return mainStoryBoard().instantiateViewController(withIdentifier: "SavedSteamDetailsBaseViewControllerID") as! SavedSteamDetailsBaseViewController
    }
    
    class func instantiatInterviewsViewController()->TrendsViewController {
        
        return mainStoryBoard().instantiateViewController(withIdentifier: "InterviewsViewControllerID") as! TrendsViewController
    }
    
    class func instantiatArticlesViewController()->ArticlesViewController {
        
        return mainStoryBoard().instantiateViewController(withIdentifier: "ArticlesViewControllerID") as! ArticlesViewController
    }
    class func instantiatInformativeViewController()->InformativeViewController {
        
        return mainStoryBoard().instantiateViewController(withIdentifier: "InformativeViewControllerID") as! InformativeViewController
    }
    class func instantiatDetailedViewController()->DetailedViewController {
        
        return mainStoryBoard().instantiateViewController(withIdentifier: "DetailedViewControllerID") as! DetailedViewController
    }
    class func instantiatWebinarDetailViewController()->WebinarDetailViewController {
        
        return mainStoryBoard().instantiateViewController(withIdentifier: "WebinarDetailViewControllerID") as! WebinarDetailViewController
    }
    class func instantiatVideoListingViewController()->VideoListingViewController {
        
        return mainStoryBoard().instantiateViewController(withIdentifier: "VideoListingViewControllerID") as! VideoListingViewController
    }
    class func instantiatWebinarViewController()->WebinarViewController {
        
        return mainStoryBoard().instantiateViewController(withIdentifier: "WebinarViewControllerID") as! WebinarViewController
    }
    class func instantiatOpportunityViewController()->OpportunityViewController {
        
        return mainStoryBoard().instantiateViewController(withIdentifier: "OpportunityViewControllerID") as! OpportunityViewController
    }
    class func instantiatVideoPlayerViewController()->VideoPlayerViewController {
        
        return mainStoryBoard().instantiateViewController(withIdentifier: "VideoPlayerViewControllerID") as! VideoPlayerViewController
    }
    
    class func instantiateEditProfileViewController()->EditProfileViewController {
        
        return mainStoryBoard().instantiateViewController(withIdentifier: "EditProfileViewControllerID") as! EditProfileViewController
    }
    
    class func instantiateSettingViewController()->SettingViewController {
        
        return mainStoryBoard().instantiateViewController(withIdentifier: "SettingViewControllerID") as! SettingViewController
    }
    class func instantiateSeminarViewController()->SeminarViewController {
        
        return mainStoryBoard().instantiateViewController(withIdentifier: "SeminarViewControllerID") as! SeminarViewController
    }
    class func instantiateInnovationViewController()->InnovationViewController {
        
        return mainStoryBoard().instantiateViewController(withIdentifier: "InnovationViewControllerID") as! InnovationViewController
    }
    class func instantiateAllCoursesListingViewController()->AllCoursesListingViewController {
        
        return mainStoryBoard().instantiateViewController(withIdentifier: "AllCoursesListingViewControllerID") as! AllCoursesListingViewController
    }
    class func instantiateCourseDetailViewController()->CourseDetailViewController {
        
        return mainStoryBoard().instantiateViewController(withIdentifier: "CourseDetailViewControllerID") as! CourseDetailViewController
    }
    class func instantiateMyEnrollmentViewController()->MyEnrollmentViewController {
        
        return mainStoryBoard().instantiateViewController(withIdentifier: "MyEnrollmentViewControllerID") as! MyEnrollmentViewController
    }
    class func instantiateMyEnrollmentListViewController()->MyEnrollmentListViewController {
        
        return mainStoryBoard().instantiateViewController(withIdentifier: "MyEnrollmentListViewControllerID") as! MyEnrollmentListViewController
    }
    class func instantiateWishListViewController()->WishListViewController {
        
        return mainStoryBoard().instantiateViewController(withIdentifier: "WishListViewControllerID") as! WishListViewController
    }
    class func instantiateWishListDetailViewController()->WishListDetailViewController {
        
        return mainStoryBoard().instantiateViewController(withIdentifier: "WishListDetailViewControllerID") as! WishListDetailViewController
    }
    class func instantiateAboutUsViewController()->AboutUsViewController {
        
        return mainStoryBoard().instantiateViewController(withIdentifier: "AboutUsViewControllerID") as! AboutUsViewController
    }
    class func instantiateContactUsViewController()->ContactUsViewController {
        
        return mainStoryBoard().instantiateViewController(withIdentifier: "ContactUsViewControllerID") as! ContactUsViewController
    }
    class func instantiateInternshipListingViewController() -> InternshipListingViewController {
        
        return mainStoryBoard().instantiateViewController(withIdentifier: "InternshipListingViewControllerID") as! InternshipListingViewController
    }
    
    class func instantiateFilterCompanyProfileViewController() -> FilterCompanyProfileViewController {
        
        return mainStoryBoard().instantiateViewController(withIdentifier: "FilterCompanyProfileViewControllerID") as! FilterCompanyProfileViewController
    }
    class func instantiateRatingViewController() -> RatingViewController {
        
        return mainStoryBoard().instantiateViewController(withIdentifier: "RatingViewControllerID") as! RatingViewController
    }
    
    class func instantiateContestQuestionsViewController() -> ContestQuestionsViewController {
        
        return mainStoryBoard().instantiateViewController(withIdentifier: "ContestQuestionsViewControllerID") as! ContestQuestionsViewController
    }
    
    class func instantiateContestResultViewController() -> ContestResultViewController {
        
        return mainStoryBoard().instantiateViewController(withIdentifier: "ContestResultViewControllerID") as! ContestResultViewController
    }
    class func instantiateRedeemPointsViewController() -> RedeemPointsViewController {
        
        return mainStoryBoard().instantiateViewController(withIdentifier: "RedeemPointsViewControllerID") as! RedeemPointsViewController
    }
    
    class func instantiateGiftViewController() -> GiftViewController {
        
        return mainStoryBoard().instantiateViewController(withIdentifier: "GiftViewControllerID") as! GiftViewController
    }
    
    
}

extension UICollectionView {
    func indexPathForView(view: AnyObject) -> IndexPath? {
        let originInCollectioView = self.convert(CGPoint.zero, from: (view as! UIView))
        return self.indexPathForItem(at: originInCollectioView) as IndexPath?
    }
    
    var centerPoint : CGPoint {
        get {
            return CGPoint(x: self.center.x + self.contentOffset.x, y: self.center.y + self.contentOffset.y);
        }
    }
    
    var centerCellIndexPath: IndexPath? {
        if let centerIndexPath = self.indexPathForItem(at: self.centerPoint) {
            return centerIndexPath
        }
        return nil
    }
}

extension String {
    
    var htmlToAttributedString: NSAttributedString? {
        guard let data = data(using: .utf8) else { return NSAttributedString() }
        do {
            return try NSAttributedString(data: data, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding:String.Encoding.utf8.rawValue], documentAttributes: nil)
        } catch {
            return NSAttributedString()
        }
    }
    var htmlToString: String {
        return htmlToAttributedString?.string ?? ""
    }
    
    func localized(lang:String) ->String {
        
        let path = Bundle.main.path(forResource: lang, ofType: "lproj")
        let bundle = Bundle(path: path!)
        
        return NSLocalizedString(self, tableName: nil, bundle: bundle!, value: "", comment: "")
    }
    
    func trim() -> String{
        return self.trimmingCharacters(in: NSCharacterSet.whitespacesAndNewlines) 
    }
    
    func isValidEmail() -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: self)
    }
    
    func isValidAmount() -> Bool {
        let amtRegEx = "^[0-9]+$"
        let amount = NSPredicate(format:"SELF MATCHES %@", amtRegEx)
        return amount.evaluate(with: self)
    }
    
    func replace(string:String, replacement:String) -> String {
        return self.replacingOccurrences(of: string, with: replacement)
    }
    
    func removeWhitespace() -> String {
        return self.replace(string: " ", replacement: "%20")
    }
    
    public var withoutHtml: String {
        guard let data = self.data(using: .utf8) else {
            return self
        }
        
        let options: [NSAttributedString.DocumentReadingOptionKey: Any] = [
            .documentType: NSAttributedString.DocumentType.html,
            .characterEncoding: String.Encoding.utf8.rawValue
        ]
        
        guard let attributedString = try? NSAttributedString(data: data, options: options, documentAttributes: nil) else {
            return self
        }
        
        return attributedString.string
    }
}

extension UIImage {
    
    class func imageWithColor(color: UIColor, size: CGSize) -> UIImage {
        let rect: CGRect = CGRect(x:0, y:0,width: size.width, height:2)
        UIGraphicsBeginImageContextWithOptions(size, false, 0)
        color.setFill()
        UIRectFill(rect)
        let image: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return image
    }
    
}
extension UIFont {
    
    func sizeOfString (string: String, constrainedToWidth width: Double) -> CGSize {
        return NSString(string: string).boundingRect(with: CGSize(width: width, height: .greatestFiniteMagnitude),
                                                     options: NSStringDrawingOptions.usesLineFragmentOrigin,
                                                             attributes: [NSAttributedStringKey.font: self],
                                                             context: nil).size
    }
}

extension UINavigationController {
    open override var childViewControllerForStatusBarStyle: UIViewController? {
        return self.topViewController
    }
    /*
    
     open override var childViewControllerForStatusBarStyle: UIViewController? {
     return visibleViewController
     }
 */
    open override var childViewControllerForStatusBarHidden: UIViewController? {
        return self.topViewController
    }
}


extension UITabBarController {
    
    open override var childViewControllerForStatusBarStyle: UIViewController? {
        return selectedViewController
        ////        return self.childViewControllers.first
        //return nil

    }
    
    open override var childViewControllerForStatusBarHidden: UIViewController? {
        return self.childViewControllers.first
    }
}
extension UIColor{
    static var random: UIColor {
        return UIColor(hue: CGFloat(arc4random_uniform(.max))/CGFloat(UInt32.max), saturation: 1, brightness: 1, alpha: 1)
    }
}
