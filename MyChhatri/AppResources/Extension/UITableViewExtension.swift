//
//  UITableViewExtension.swift
//  SnatchApp
//
//  Created by Vishal Chikara on 06/05/16.
//  Copyright © 2016 Snatch. All rights reserved.
//

import Foundation
import UIKit

extension UITableView {
    func scrollToBottom(_tblView:UITableView!) {
        let sections = self.numberOfSections
        let rows = self.numberOfRows(inSection: sections - 1)
        if rows > 0 {
            self.scrollToRow(at: IndexPath(row: rows-1 , section: sections-1), at: UITableViewScrollPosition.bottom, animated: false)
        }
    }
    
    func reloadTableWithCallback(_ callback: @escaping (_ result: Bool)->()){
        UIView.animate(withDuration: 0.01, animations: {
            self.reloadData()
        }) { (finished) in
            callback(true)
        }
    }
    
    func indexPathForView(_ view: UIView) -> IndexPath? {
        let center = view.center
        let viewCenter = self.convert(center, from: view.superview)
        let indexPath = self.indexPathForRow(at: viewCenter)
        return indexPath
    }
    
}
