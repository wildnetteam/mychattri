//
//  EnrollmentListTableViewCell.swift
//  MyChhatri
//
//  Created by Arpana on 13/09/18.
//  Copyright © 2018 Arpana. All rights reserved.
//

import UIKit

class EnrollmentListTableViewCell: UITableViewCell {

    @IBOutlet weak var lblCompanyName: UILabel!
    @IBOutlet weak var lblRegNo: UILabel!
    @IBOutlet weak var lblStartDate: UILabel!
    @IBOutlet weak var lblEndDate: UILabel!
    @IBOutlet weak var lblStatus: UILabel!
    @IBOutlet weak var lblProgramName: UILabel!
    @IBOutlet weak var viewDetailButton: UIButton!
    @IBOutlet weak var DetailView: UIView!
    @IBOutlet weak var BtnRate: UIButton!
    var payStatus : String = ""
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func  setCellData(dict : NSDictionary)  {
        
        var courseIsRated: Int = 0
        if let isCourseRated = dict.value(forKey: "is_rated") as? String {
            courseIsRated = Int(isCourseRated)!
        } else if let isCourseRated = dict.value(forKey: "is_rated") as? Int{
              courseIsRated = isCourseRated
            }

        //If courseIsRated == 0 mean we need to give option to rate it
            
        if courseIsRated ==  1{
            BtnRate.setTitle("Rate Us", for: .normal)
            BtnRate.isEnabled = true
            
        }else
        {
            BtnRate.setTitle("Already rated", for: .normal)
            BtnRate.isEnabled = false
            
        }
        if let statusPaid = dict.value(forKey: "status") as? String {
            
            payStatus = statusPaid
        }
        
        if let endDate = dict.value(forKey: "end_date") as? String {
            
             lblEndDate.text = CalenderStruct.getFormatedDateStringWithouOrdinal(fromDateUTC:  CalenderStruct.getDateFromDateString(forUTC: endDate, byFormat: "yyyy-MM-dd"), inFormat: "dd MMM yyyy")
        }
        if let status = dict.value(forKey: "status_text") as? String {
            
            lblStatus.text = status
        }
        
        if  (lblStatus.text)?.caseInsensitiveCompare("completed") == .orderedSame {
            
            BtnRate.isHidden = false
        }
        if let idToViewDetail = dict.value(forKey: "id") as? String {
            
            if let idAsInt =  Int(idToViewDetail){
                viewDetailButton.tag = idAsInt
            }
        }else  if let idToViewDetailAsInt = dict.value(forKey: "id") as? Int {
            
                viewDetailButton.tag = idToViewDetailAsInt
            
        }
        
        if let startDate = dict.value(forKey: "start_date") as? String {
            
            lblStartDate.text = CalenderStruct.getFormatedDateStringWithouOrdinal(fromDateUTC:  CalenderStruct.getDateFromDateString(forUTC: startDate, byFormat: "yyyy-MM-dd"), inFormat: "dd MMM yyyy")
        }
        if let regNo = dict.value(forKey: "order_no") as? String {
            
           lblRegNo.text = regNo
        }
        if let courseDict = dict.value(forKey: "course") as? NSDictionary {
            if let title = courseDict.value(forKey: "title") as? String{
                lblProgramName.text = title
            }
        }
        if let vendorDict = dict.value(forKey: "vendor") as? NSDictionary {
            if let titlename = vendorDict.value(forKey: "name") as? String{
                lblCompanyName.text = titlename
            }
        }
    }
    @IBAction func viewDetails(_ sender: Any){
        
        if let Id = String(viewDetailButton.tag) as? String {
            NavigationManager.moveToMyEnrollmentViewControllerIfNotExists(kAppDelegate.menuNavController!, enrolmentID: Id , payStatus: payStatus)
        }
    }
      @IBAction func rateUs(_ sender: Any){
        
        if let Id = lblRegNo.text  {
            NavigationManager.moveToRatingViewControllerIfNotExists(kAppDelegate.menuNavController!, orderID: Id )
        }
        
    }
}
